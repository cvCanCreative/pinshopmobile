package ai.agusibrhim.design.topedc.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RekeningModel implements Parcelable {
    @SerializedName("ID_REKENING")
    @Expose
    private String iDREKENING;
    @SerializedName("ID_USER")
    @Expose
    private String iDUSER;
    @SerializedName("RK_IMAGE")
    @Expose
    private String rKIMAGE;

    @SerializedName("RK_BANK")
    @Expose
    private String rKBANK;
    @SerializedName("RK_NOMOR")
    @Expose
    private String rKNOMOR;
    @SerializedName("RK_NAME")
    @Expose
    private String rKNAME;
    @SerializedName("CREATED_AT")
    @Expose
    private String cREATEDAT;
    @SerializedName("UPDATE_AT")
    @Expose
    private String uPDATEAT;
    @SerializedName("US_USERNAME")
    @Expose
    private String uSUSERNAME;
    @SerializedName("US_TELP")
    @Expose
    private String uSTELP;
    @SerializedName("US_PASSWORD")
    @Expose
    private String uSPASSWORD;
    @SerializedName("US_EMAIL")
    @Expose
    private String uSEMAIL;
    @SerializedName("US_RULE")
    @Expose
    private String uSRULE;
    @SerializedName("US_FCM")
    @Expose
    private String uSFCM;
    @SerializedName("US_TOKEN")
    @Expose
    private String uSTOKEN;
    @SerializedName("US_FORGOT_PASS")
    @Expose
    private String uSFORGOTPASS;
    @SerializedName("US_REQUEST_FORGOT_PASS")
    @Expose
    private String uSREQUESTFORGOTPASS;
    @SerializedName("UPDATED_AT")
    @Expose
    private String uPDATEDAT;
    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("message")
    @Expose
    private String message;

    protected RekeningModel(Parcel in) {
        iDREKENING = in.readString();
        iDUSER = in.readString();
        rKIMAGE = in.readString();
        rKBANK = in.readString();
        rKNOMOR = in.readString();
        rKNAME = in.readString();
        cREATEDAT = in.readString();
        uPDATEAT = in.readString();
        uSUSERNAME = in.readString();
        uSTELP = in.readString();
        uSPASSWORD = in.readString();
        uSEMAIL = in.readString();
        uSRULE = in.readString();
        uSFCM = in.readString();
        uSTOKEN = in.readString();
        uSFORGOTPASS = in.readString();
        uSREQUESTFORGOTPASS = in.readString();
        uPDATEDAT = in.readString();
        if (in.readByte() == 0) {
            success = null;
        } else {
            success = in.readInt();
        }
        message = in.readString();
    }

    public static final Creator<RekeningModel> CREATOR = new Creator<RekeningModel>() {
        @Override
        public RekeningModel createFromParcel(Parcel in) {
            return new RekeningModel(in);
        }

        @Override
        public RekeningModel[] newArray(int size) {
            return new RekeningModel[size];
        }
    };

    public String getiDREKENING() {
        return iDREKENING;
    }

    public void setiDREKENING(String iDREKENING) {
        this.iDREKENING = iDREKENING;
    }

    public String getiDUSER() {
        return iDUSER;
    }

    public void setiDUSER(String iDUSER) {
        this.iDUSER = iDUSER;
    }

    public String getrKIMAGE() {
        return rKIMAGE;
    }

    public void setrKIMAGE(String rKIMAGE) {
        this.rKIMAGE = rKIMAGE;
    }

    public String getrKBANK() {
        return rKBANK;
    }

    public void setrKBANK(String rKBANK) {
        this.rKBANK = rKBANK;
    }

    public String getrKNOMOR() {
        return rKNOMOR;
    }

    public void setrKNOMOR(String rKNOMOR) {
        this.rKNOMOR = rKNOMOR;
    }

    public String getrKNAME() {
        return rKNAME;
    }

    public void setrKNAME(String rKNAME) {
        this.rKNAME = rKNAME;
    }

    public String getcREATEDAT() {
        return cREATEDAT;
    }

    public void setcREATEDAT(String cREATEDAT) {
        this.cREATEDAT = cREATEDAT;
    }

    public String getuPDATEAT() {
        return uPDATEAT;
    }

    public void setuPDATEAT(String uPDATEAT) {
        this.uPDATEAT = uPDATEAT;
    }

    public String getuSUSERNAME() {
        return uSUSERNAME;
    }

    public void setuSUSERNAME(String uSUSERNAME) {
        this.uSUSERNAME = uSUSERNAME;
    }

    public String getuSTELP() {
        return uSTELP;
    }

    public void setuSTELP(String uSTELP) {
        this.uSTELP = uSTELP;
    }

    public String getuSPASSWORD() {
        return uSPASSWORD;
    }

    public void setuSPASSWORD(String uSPASSWORD) {
        this.uSPASSWORD = uSPASSWORD;
    }

    public String getuSEMAIL() {
        return uSEMAIL;
    }

    public void setuSEMAIL(String uSEMAIL) {
        this.uSEMAIL = uSEMAIL;
    }

    public String getuSRULE() {
        return uSRULE;
    }

    public void setuSRULE(String uSRULE) {
        this.uSRULE = uSRULE;
    }

    public String getuSFCM() {
        return uSFCM;
    }

    public void setuSFCM(String uSFCM) {
        this.uSFCM = uSFCM;
    }

    public String getuSTOKEN() {
        return uSTOKEN;
    }

    public void setuSTOKEN(String uSTOKEN) {
        this.uSTOKEN = uSTOKEN;
    }

    public String getuSFORGOTPASS() {
        return uSFORGOTPASS;
    }

    public void setuSFORGOTPASS(String uSFORGOTPASS) {
        this.uSFORGOTPASS = uSFORGOTPASS;
    }

    public String getuSREQUESTFORGOTPASS() {
        return uSREQUESTFORGOTPASS;
    }

    public void setuSREQUESTFORGOTPASS(String uSREQUESTFORGOTPASS) {
        this.uSREQUESTFORGOTPASS = uSREQUESTFORGOTPASS;
    }

    public String getuPDATEDAT() {
        return uPDATEDAT;
    }

    public void setuPDATEDAT(String uPDATEDAT) {
        this.uPDATEDAT = uPDATEDAT;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(iDREKENING);
        parcel.writeString(iDUSER);
        parcel.writeString(rKIMAGE);
        parcel.writeString(rKBANK);
        parcel.writeString(rKNOMOR);
        parcel.writeString(rKNAME);
        parcel.writeString(cREATEDAT);
        parcel.writeString(uPDATEAT);
        parcel.writeString(uSUSERNAME);
        parcel.writeString(uSTELP);
        parcel.writeString(uSPASSWORD);
        parcel.writeString(uSEMAIL);
        parcel.writeString(uSRULE);
        parcel.writeString(uSFCM);
        parcel.writeString(uSTOKEN);
        parcel.writeString(uSFORGOTPASS);
        parcel.writeString(uSREQUESTFORGOTPASS);
        parcel.writeString(uPDATEDAT);
        if (success == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(success);
        }
        parcel.writeString(message);
    }
}
