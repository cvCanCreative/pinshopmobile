package ai.agusibrhim.design.topedc.Activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.fxn.pix.Pix;
import com.fxn.utility.PermUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.MainActivity;
import ai.agusibrhim.design.topedc.Model.RekeningModel;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import okhttp3.ResponseBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TambahSaldoActivity extends AppCompatActivity {

    private EditText edtNominal;
    private EditText edtBank;
    private Button btnTopup;
    private EditText edtNama;
    private EditText edtRekening;
    private ImageView imgBukti;
    private ImageView imgAmbilFoto;
    SharedPref sharedPref;
    File imageFile;
    private String imagePath, h;
    ArrayList<RekeningModel> list = new ArrayList<>();
    int posisi;
    private TextView tvBank;
    private ImageView imgClose;
    String pilih;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_saldo);
        initView();
        awal();

        btnTopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                new AlertDialog.Builder(TambahSaldoActivity.this)
//                        .setMessage("Fitur Belum tersedia.")
//                        .setCancelable(false)
//                        .setNegativeButton("Oke", null)
//                        .show();
                tambahSaldo();
            }
        });

        imgAmbilFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EasyImage.openChooserWithGallery(TambahSaldoActivity.this, "chose", 3);
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ListBankActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();
            }
        });

    }

    private void awal() {
        sharedPref = new SharedPref(this);
        getData();
    }

    private void getData() {
        String data = getIntent().getStringExtra("tipe");
        pilih = getIntent().getStringExtra("pilih");
        list = getIntent().getParcelableArrayListExtra("list");
        posisi = Integer.parseInt(getIntent().getStringExtra("posisi"));
        if (data.equals("bank_lain")) {
            edtBank.setText("" + list.get(0).getrKBANK());
            tvBank.setText("" + list.get(0).getrKBANK() + " " + list.get(0).getrKNOMOR());
        } else {
            edtBank.setFocusable(false);
            edtBank.setText("" + list.get(posisi).getrKBANK());
            tvBank.setText("" + list.get(posisi).getrKBANK() + " " + list.get(posisi).getrKNOMOR());
        }
    }

    private void tambahSaldo() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Loading ...");
        pd.setCancelable(true);
        pd.show();

//        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), imageFile);
//        MultipartBody.Part body = MultipartBody.Part.createFormData("image", imageFile.getName(), requestFile);
//
////        RequestBody id = RequestBody.create(MediaType.parse("text/plain"), sharedPref.getIdUser());
////        RequestBody session = RequestBody.create(MediaType.parse("text/plain"), sharedPref.getSESSION());
//        RequestBody rekening = RequestBody.create(MediaType.parse("text/plain"), edtRekening.getText().toString());
//        RequestBody nama = RequestBody.create(MediaType.parse("text/plain"), edtNama.getText().toString());
//        RequestBody nominal = RequestBody.create(MediaType.parse("text/plain"), edtNominal.getText().toString());
//        RequestBody bank = RequestBody.create(MediaType.parse("text/plain"), edtBank.getText().toString());
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.topupSaldo(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                edtRekening.getText().toString(),
                edtNama.getText().toString(),
                edtNominal.getText().toString(),
                edtBank.getText().toString()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String pesan = jsonObject.optString("message");
                        String kode = jsonObject.optString("success");
                        final String id_history = jsonObject.optString("id_history");
                        if (kode.equals("1")) {
                            new AlertDialog.Builder(TambahSaldoActivity.this)
                                    .setMessage("" + pesan)
                                    .setCancelable(false)
                                    .setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id3) {
                                            Intent intent = new Intent(getApplicationContext(), BuktiTfSaldoActivity.class);
                                            intent.putExtra("data", "saldo");
                                            intent.putExtra("jumlah",edtNominal.getText().toString());
                                            intent.putExtra("nama_bank",""+list.get(posisi).getrKBANK());
                                            intent.putExtra("img_bank",""+list.get(posisi).getrKIMAGE());
                                            intent.putExtra("rekening",""+list.get(posisi).getrKNOMOR());
                                            intent.putExtra("id", id_history);
                                            startActivity(intent);
                                        }
                                    })
                                    .show();
                        } else {
                            Toast.makeText(TambahSaldoActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(TambahSaldoActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    //gallery
    private void showFileChooserGalerry() {
        Intent qq = new Intent(Intent.ACTION_PICK);
        qq.setType("image/*");
        startActivityForResult(Intent.createChooser(qq, "Pilih Foto"), 100);
//        Intent intent = new Intent();
//        intent.setType("image/*");
//        intent.setAction(Intent.ACTION_GET_CONTENT);
//        startActivityForResult(Intent.createChooser(intent, "Pilih Foto"), Config.PICK_FILE_REQUEST);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                //Some error handling
                Toast.makeText(TambahSaldoActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onImagePicked(File image, EasyImage.ImageSource source, int type) {
                Glide.with(TambahSaldoActivity.this)
                        .load(new File(image.getPath()))
                        .into(imgBukti);
                imageFile = new File(image.getPath());
            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
                super.onCanceled(source, type);
            }
        });
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PermUtil.REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Pix.start(TambahSaldoActivity.this, 100);
                } else {
                    Toast.makeText(TambahSaldoActivity.this, "Approve permissions to open Pix ImagePicker", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

    private void initView() {
        edtNominal = (EditText) findViewById(R.id.edt_nominal);
        edtBank = (EditText) findViewById(R.id.edt_bank);
        btnTopup = (Button) findViewById(R.id.btn_topup);
        edtNama = (EditText) findViewById(R.id.edt_nama);
        edtRekening = (EditText) findViewById(R.id.edt_rekening);
        imgBukti = (ImageView) findViewById(R.id.img_bukti);
        imgAmbilFoto = (ImageView) findViewById(R.id.img_ambil_foto);
        tvBank = (TextView) findViewById(R.id.tv_bank);
        imgClose = (ImageView) findViewById(R.id.img_close);
    }
}
