package ai.agusibrhim.design.topedc.Adapter;

import android.app.AlertDialog;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import ai.agusibrhim.design.topedc.Activity.DetailActivity;
import ai.agusibrhim.design.topedc.Activity.KeranjangActivity;
import ai.agusibrhim.design.topedc.Activity.PembayaranActivity;
import ai.agusibrhim.design.topedc.Activity.PengaturanAkunActivity;
import ai.agusibrhim.design.topedc.Helper.Config;
import ai.agusibrhim.design.topedc.Helper.HelperClass;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.Helper.dataRoom.AppDatabase;
import ai.agusibrhim.design.topedc.Helper.dataRoom.Pesanan;
import ai.agusibrhim.design.topedc.MainActivity;
import ai.agusibrhim.design.topedc.Model.FavoritModel;
import ai.agusibrhim.design.topedc.Model.ListProduct;
import ai.agusibrhim.design.topedc.R;

public class PesananAdapter extends RecyclerView.Adapter<PesananAdapter.Holder>{

    ArrayList<Pesanan> list;
    KeranjangActivity context;
    Context context2;
    ArrayList<String> listJumlah = new ArrayList<>();
    String jumalahBarang;
    String tipe;
    int jml=0;
    ListProduct listProduct;
    FavoritModel favoritModel;
    SharedPref pref;

    private AppDatabase db;

    public PesananAdapter(ArrayList<Pesanan> list, KeranjangActivity context, String tipe,Context context2) {
        this.list = list;
        this.context = context;
        this.tipe = tipe;
        this.context2 = context2;


        db = Room.databaseBuilder(context2,
                AppDatabase.class, "pesanandb").allowMainThreadQueries().build();
    }

    public PesananAdapter(ArrayList<Pesanan> list, String tipe,Context context2) {
        this.list = list;
        this.tipe = tipe;
        this.context2 = context2;


        db = Room.databaseBuilder(context2,
                AppDatabase.class, "pesanandb").allowMainThreadQueries().build();
    }

    public PesananAdapter(ListProduct list, String tipe, Context context2) {
        this.listProduct = list;
        this.tipe = tipe;
        this.context2 = context2;

        pref = new SharedPref(context2);
    }

    public PesananAdapter(FavoritModel list, String tipe, Context context2) {
        this.favoritModel = list;
        this.tipe = tipe;
        this.context2 = context2;

        pref = new SharedPref(context2);
    }


    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_pesanan,parent,false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final Holder holder, final int position) {

        for (int i = 0; i <100 ; i++) {
            int nilai = i+1;
            listJumlah.add(""+nilai);
        }

        if (tipe.equals("langsung")){
            if (pref.getTIPEKLIK().equals("fav")){
                holder.btnTambah.setVisibility(View.GONE);
                holder.btnKurang.setVisibility(View.GONE);
                holder.hapus.setVisibility(View.GONE);
                holder.jumlahPesan.setVisibility(View.GONE);

                holder.namaToko.setText(favoritModel.getUSDTOKO());
                holder.namaBarang.setText(favoritModel.getBANAME());
                String jumlah = "1";

                holder.Jumlah.setText("Jumlah Pesanan " + jumlah);

                holder.Harga.setText(""+new HelperClass().convertRupiah(Integer.parseInt(favoritModel.getBAPRICE())));

                String gambar = favoritModel.getBAIMAGE();
                String image = new HelperClass().splitText(gambar);

                Picasso.with(context2)
                        .load(Config.IMAGE_BARANG + image)
                        .into(holder.imgBarang);


                holder.jumlahPesan.setText(jumlah);

            }else {
                holder.btnTambah.setVisibility(View.GONE);
                holder.btnKurang.setVisibility(View.GONE);
                holder.hapus.setVisibility(View.GONE);
                holder.jumlahPesan.setVisibility(View.GONE);

                holder.namaToko.setText(listProduct.getUSDTOKO());
                holder.namaBarang.setText(listProduct.getBANAME());
                String jumlah = "1";

                holder.Jumlah.setText("Jumlah Pesanan " + jumlah);

                holder.Harga.setText(""+new HelperClass().convertRupiah(Integer.parseInt(listProduct.getBAPRICE())));

                String gambar = listProduct.getBAIMAGE();
                String image = new HelperClass().splitText(gambar);

                Picasso.with(context2)
                        .load(Config.IMAGE_BARANG + image)
                        .into(holder.imgBarang);


                holder.jumlahPesan.setText(jumlah);
            }


        }else if (tipe.equals("bayar")){
            holder.btnTambah.setVisibility(View.GONE);
            holder.btnKurang.setVisibility(View.GONE);
            holder.hapus.setVisibility(View.GONE);
            holder.jumlahPesan.setVisibility(View.GONE);

            holder.namaToko.setText(list.get(position).getNamaToko());
            holder.namaBarang.setText(list.get(position).getNamaBarang());
            String jumlah = list.get(position).getJumlahBarang();

            holder.Jumlah.setText("Jumlah Pesanan " + jumlah);
            holder.Harga.setText(""+new HelperClass().convertRupiah(Integer.parseInt(list.get(position).getHargaBarang())));

            String gambar = list.get(position).getImgBarang();
            String image = new HelperClass().splitText(gambar);

            Picasso.with(context2)
                    .load(Config.IMAGE_BARANG + image)
                    .into(holder.imgBarang);


            holder.jumlahPesan.setText(jumlah);

        }else {
            holder.namaToko.setText(list.get(position).getNamaToko());
            holder.namaBarang.setText(list.get(position).getNamaBarang());
            String jumlah = list.get(position).getJumlahBarang();

            holder.Jumlah.setText("Jumlah Pesanan " + jumlah);
            holder.Harga.setText(""+new HelperClass().convertRupiah(Integer.parseInt(list.get(position).getHargaBarang())));

            String gambar = list.get(position).getImgBarang();
            String image = new HelperClass().splitText(gambar);

            Picasso.with(context2)
                    .load(Config.IMAGE_BARANG + image)
                    .into(holder.imgBarang);


            holder.jumlahPesan.setText(jumlah);

            holder. btnKurang.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String jumlah = list.get(position).getJumlahBarang();
                    jml = Integer.parseInt(jumlah);
                    if (jml == 1) {
                        Toast.makeText(context, "mentok", Toast.LENGTH_SHORT).show();
                    } else {
                        jml = jml -1;
                        holder.jumlahPesan.setText(String.valueOf(jml));

                        int harga = Integer.parseInt(list.get(position).getHargaBarang());
                        int sub = harga * jml;
                        db.pesananDAO().updatePesanan(""+jml,
                                ""+sub,
                                ""+list.get(position).getIdBarang());
                        ((KeranjangActivity)context).setHargaLagi();
//                    context.startActivity(new Intent(context, KeranjangActivity.class)
//                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                    }
                }
            });

            holder.btnTambah.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String jumlah = list.get(position).getJumlahBarang();
                    jml = Integer.parseInt(jumlah);
                    if (jml == Integer.parseInt(list.get(position).getStok())){

                        Toast.makeText(context, "Stok tidak cukup", Toast.LENGTH_SHORT).show();
                    }else {
                        jml = jml + 1;
                        holder.jumlahPesan.setText(String.valueOf(jml));
                        int harga = Integer.parseInt(list.get(position).getHargaBarang());
                        int sub = harga * jml;
                        db.pesananDAO().updatePesanan(""+jml,
                                ""+sub,
                                ""+list.get(position).getIdBarang());

                        ((KeranjangActivity)context).setHargaLagi();
                    }

//                context.startActivity(new Intent(context, KeranjangActivity.class)
//                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                }
            });

            holder.hapus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    new AlertDialog.Builder(view.getContext())
                            .setMessage("Apakah Anda ingin Menghapus item ini ?")
                            .setCancelable(false)
                            .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    db.pesananDAO().deletePesanan(list.get(position));

                                    ((KeranjangActivity)context).setHargaLagi();
//                                context.startActivity(new Intent(context, KeranjangActivity.class)
//                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                }
                            })
                            .setNegativeButton("Tidak", null)
                            .show();
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if (tipe.equals("langsung")){
            return 1;
        }else {
            return list.size();
        }

    }

    public class Holder extends RecyclerView.ViewHolder{

        TextView namaToko,namaBarang,Harga,Jumlah,hapus,jumlahPesan;
        ImageView imgBarang;
        Button btnKurang,btnTambah;
        public Holder(View itemView) {
            super(itemView);

            jumlahPesan = itemView.findViewById(R.id.txt_jumlah_pesan);
            btnKurang = itemView.findViewById(R.id.btn_kurang);
            btnTambah = itemView.findViewById(R.id.btn_tambah);
            hapus = itemView.findViewById(R.id.tv_hapus);
            namaToko = itemView.findViewById(R.id.tv_nama_toko);
            namaBarang = itemView.findViewById(R.id.tv_nama_barang);
            Harga = itemView.findViewById(R.id.tv_harga);
            Jumlah = itemView.findViewById(R.id.tv_jumlah);
            imgBarang = itemView.findViewById(R.id.img_barang);
        }
    }
}
