package ai.agusibrhim.design.topedc.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.loopj.android.image.SmartImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import ai.agusibrhim.design.topedc.DetailScrollingActivity;
import ai.agusibrhim.design.topedc.Helper.Config;
import ai.agusibrhim.design.topedc.Helper.HelperClass;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.Model.ListProduct;
import ai.agusibrhim.design.topedc.R;

public class AdapterProdukTerbaru extends RecyclerView.Adapter<AdapterProdukTerbaru.ViewHolder> {

    private Context context;
    private ArrayList<ListProduct> list;
    SharedPref pref;

    int size;

    public AdapterProdukTerbaru(Context context, ArrayList<ListProduct> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_new_product,
                parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        pref = new SharedPref(context);

        ListProduct cat = list.get(position);
        holder.itemproductTextViewName.setText(cat.getBANAME());

        String gambar = cat.getBAIMAGE();
        String image = new HelperClass().splitText(gambar);

        Picasso.with(context)
                .load(Config.IMAGE_BARANG + image)
                .placeholder(R.drawable.image_loading)
                .error(R.drawable.no_image_found)
                .noFade()
                .into(holder.itemproductImageView1);

        holder.itemproductTextViewPrice.setText("" + new HelperClass().convertRupiah(Integer.valueOf(cat.getBAPRICE())));

    }

    @Override
    public int getItemCount() {
        if (list.size() > 7) {
            size = 7;
        } else {
            size = list.size();
        }
        return size;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private SmartImageView itemproductImageView1;
        private TextView itemproductTextViewDisc;
        private TextView itemproductTextViewName;
        private TextView itemproductTextViewPrice;
        private TextView itemproductTextViewPold;
        private RatingBar itemproductRatingBar1;
        private TextView itemproductTextViewStore;

        public ViewHolder(View itemView) {
            super(itemView);
            itemproductImageView1 = itemView.findViewById(R.id.itemproductImageView1);
            itemproductTextViewDisc = itemView.findViewById(R.id.itemproductTextViewDisc);
            itemproductTextViewName = itemView.findViewById(R.id.itemproductTextViewName);
            itemproductTextViewPrice = itemView.findViewById(R.id.itemproductTextViewPrice);
            itemproductTextViewPold = itemView.findViewById(R.id.itemproductTextViewPold);
            itemproductRatingBar1 = itemView.findViewById(R.id.itemproductRatingBar1);
            itemproductTextViewStore = itemView.findViewById(R.id.itemproductTextViewStore);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    pref.setProduk(list.get(getAdapterPosition()));
                    pref.savePrefString(SharedPref.TIPEKLIK, "produk");

                    Intent intent = new Intent(context, DetailScrollingActivity.class);
                    intent.putExtra("data", "lihat");
                    intent.putExtra("list", list.get(getAdapterPosition()));
                    intent.putExtra("posisi", "" + getAdapterPosition());
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
//					Toast.makeText(context, "bb"+getAdapterPosition(), Toast.LENGTH_SHORT).show();
                }
            });

        }
    }

}