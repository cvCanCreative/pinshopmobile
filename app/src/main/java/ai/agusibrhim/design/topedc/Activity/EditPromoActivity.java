package ai.agusibrhim.design.topedc.Activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.Model.PromoAktif;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditPromoActivity extends AppCompatActivity {

    private ImageView imgClose;
    private EditText edtNama;
    private EditText edtKode;
    private EditText edtPotongan;
    private EditText edtMin;
    private EditText edtKadaluarsa;
    private EditText edtPromoUntuk;
    private Button btnTambah;
    SharedPref pref;
    DatePickerDialog datePickerDialog;
    int mYear;
    int mMonth;
    int mDay;
    String status;
    PromoAktif promoAktif;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_promo);
        initView();

        awal();
    }

    private void awal() {
        pref = new SharedPref(this);

        setData();
        mainButton();
    }

    private void mainButton() {

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edit();
            }
        });

        edtKadaluarsa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                settingTanggal();
                datePickerDialog.show();
            }
        });

        edtPromoUntuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilihan();
            }
        });
    }

    private void settingTanggal() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        datePickerDialog = new
                DatePickerDialog(EditPromoActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                mYear = year;
                mMonth = month;
                mDay = dayOfMonth;

                GregorianCalendar c = new GregorianCalendar(mYear, mMonth, mDay);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                edtKadaluarsa.setText(sdf.format(c.getTime()));

                edtKadaluarsa.setFocusable(false);
                edtKadaluarsa.setCursorVisible(false);
                //edtTanggal.setText(tanggal);

            }
        }, year, month, day);
    }


    private void edit() {
        if (!validasi()) {
            return;
        }
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Sending data...");
        pd.setCancelable(false);
        pd.show();
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.editPromo(pref.getIdUser(),
                pref.getSESSION(),
                promoAktif.getIDPROMO(),
                edtNama.getText().toString(),
                edtKode.getText().toString(),
                edtPotongan.getText().toString(),
                edtKadaluarsa.getText().toString(),
                edtMin.getText().toString(),
                "" + status).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String pesan = jsonObject.optString("message");
                        if (pesan.equals("Promo berhasil diubah")) {
                            Toast.makeText(getApplicationContext(), "" + pesan, Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(EditPromoActivity.this, TokoKitaActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("data", "sendiri");
                            startActivity(intent);
                        } else {
                            Toast.makeText(getApplicationContext(), "" + pesan, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getApplicationContext(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean validasi() {
        if (edtNama.getText().toString().isEmpty()) {
            edtNama.setError("Harus diisi");
            edtNama.requestFocus();

            return false;
        }
        if (edtKode.getText().toString().isEmpty()) {
            edtKode.setError("Harus diisi");
            edtKode.requestFocus();

            return false;
        }
        if (edtPotongan.getText().toString().isEmpty()) {
            edtPotongan.setError("Harus diisi");
            edtPotongan.requestFocus();

            return false;
        }
        if (edtKadaluarsa.getText().toString().isEmpty()) {
            edtKadaluarsa.setError("Harus diisi");
            edtKadaluarsa.requestFocus();

            return false;
        }
        if (edtPromoUntuk.getText().toString().isEmpty()) {
            edtPromoUntuk.setError("Harus diisi");
            edtPromoUntuk.requestFocus();

            return false;
        }

        return true;
    }

    private void pilihan() {
        final CharSequence[] dialogItem = {"Member", "Non Member"};
        AlertDialog.Builder builder = new AlertDialog.Builder(EditPromoActivity.this);
        builder.setTitle("Tentukan Pilihan Anda");
        builder.setItems(dialogItem, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                switch (i) {
                    case 0:
                        status = "MEMBER";
                        edtPromoUntuk.setText("Member");
                        break;
                    case 1:
                        status = "NON_MEMBER";
                        edtPromoUntuk.setText("Non Member");
                        break;
                }
            }
        });
        builder.create().show();
    }

    private void setData() {
        promoAktif = getIntent().getParcelableExtra("list");
        edtNama.setText(promoAktif.getpRMEMBER());
        edtKadaluarsa.setText(promoAktif.getPREXPIRED());
        edtKode.setText(promoAktif.getPRCODE());
        edtMin.setText(promoAktif.getpRMIN());
        edtPotongan.setText(promoAktif.getPRPOTONGAN());
        status = promoAktif.getPRNAME();
        if (status.equals("MEMBER")) {
            edtPromoUntuk.setText("Member");
        } else {
            edtPromoUntuk.setText("Non Member");
        }

    }

    private void initView() {
        imgClose = (ImageView) findViewById(R.id.img_close);
        edtNama = (EditText) findViewById(R.id.edt_nama);
        edtKode = (EditText) findViewById(R.id.edt_kode);
        edtPotongan = (EditText) findViewById(R.id.edt_potongan);
        edtMin = (EditText) findViewById(R.id.edt_min);
        edtKadaluarsa = (EditText) findViewById(R.id.edt_kadaluarsa);
        edtPromoUntuk = (EditText) findViewById(R.id.edt_promo_untuk);
        btnTambah = (Button) findViewById(R.id.btn_tambah);
    }
}
