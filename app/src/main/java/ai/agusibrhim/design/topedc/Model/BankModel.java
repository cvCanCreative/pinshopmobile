package ai.agusibrhim.design.topedc.Model;

import java.io.Serializable;

public class BankModel implements Serializable {

    public String ID_BANK;
    public String BK_NAME;
    public String BK_IMAGE;
    public String CREATED_AT;
    public String UPDATED_AT;
}
