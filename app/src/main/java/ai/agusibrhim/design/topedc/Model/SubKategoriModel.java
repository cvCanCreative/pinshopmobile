package ai.agusibrhim.design.topedc.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubKategoriModel implements Parcelable {
    @SerializedName("ID_SUB_KATEGORI")
    @Expose
    private String iDSUBKATEGORI;
    @SerializedName("ID_KATEGORI_BARANG")
    @Expose
    private String iDKATEGORIBARANG;
    @SerializedName("SK_IMAGE")
    @Expose
    private String sKIMAGE;
    @SerializedName("SK_NAME")
    @Expose
    private String sKNAME;
    @SerializedName("CREATED_AT")
    @Expose
    private String cREATEDAT;
    @SerializedName("UPDATED_AT")
    @Expose
    private String uPDATEDAT;
    @SerializedName("success")
    @Expose
    private Object success;
    @SerializedName("message")
    @Expose
    private Object message;

    protected SubKategoriModel(Parcel in) {
        iDSUBKATEGORI = in.readString();
        iDKATEGORIBARANG = in.readString();
        sKIMAGE = in.readString();
        sKNAME = in.readString();
        cREATEDAT = in.readString();
        uPDATEDAT = in.readString();
    }

    public static final Creator<SubKategoriModel> CREATOR = new Creator<SubKategoriModel>() {
        @Override
        public SubKategoriModel createFromParcel(Parcel in) {
            return new SubKategoriModel(in);
        }

        @Override
        public SubKategoriModel[] newArray(int size) {
            return new SubKategoriModel[size];
        }
    };

    public String getIDSUBKATEGORI() {
        return iDSUBKATEGORI;
    }

    public void setIDSUBKATEGORI(String iDSUBKATEGORI) {
        this.iDSUBKATEGORI = iDSUBKATEGORI;
    }

    public String getIDKATEGORIBARANG() {
        return iDKATEGORIBARANG;
    }

    public void setIDKATEGORIBARANG(String iDKATEGORIBARANG) {
        this.iDKATEGORIBARANG = iDKATEGORIBARANG;
    }

    public String getSKIMAGE() {
        return sKIMAGE;
    }

    public void setSKIMAGE(String sKIMAGE) {
        this.sKIMAGE = sKIMAGE;
    }

    public String getSKNAME() {
        return sKNAME;
    }

    public void setSKNAME(String sKNAME) {
        this.sKNAME = sKNAME;
    }

    public String getCREATEDAT() {
        return cREATEDAT;
    }

    public void setCREATEDAT(String cREATEDAT) {
        this.cREATEDAT = cREATEDAT;
    }

    public String getUPDATEDAT() {
        return uPDATEDAT;
    }

    public void setUPDATEDAT(String uPDATEDAT) {
        this.uPDATEDAT = uPDATEDAT;
    }

    public Object getSuccess() {
        return success;
    }

    public void setSuccess(Object success) {
        this.success = success;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(iDSUBKATEGORI);
        parcel.writeString(iDKATEGORIBARANG);
        parcel.writeString(sKIMAGE);
        parcel.writeString(sKNAME);
        parcel.writeString(cREATEDAT);
        parcel.writeString(uPDATEDAT);
    }
}
