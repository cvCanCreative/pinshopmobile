package ai.agusibrhim.design.topedc.Helper.dataRoom;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {Pesanan.class}, version = 2)
public abstract class AppDatabase extends RoomDatabase {

    public abstract PesananDAO pesananDAO();
}
