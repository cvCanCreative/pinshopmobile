package ai.agusibrhim.design.topedc.Activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Adapter.AdapterMemberUser;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.Model.MemberTokoModel;
import ai.agusibrhim.design.topedc.Model.MemberUserModel;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CalonMemberActivity extends AppCompatActivity {

    private ImageView imgClose;
    private TextView chatTvTitle;
    private RecyclerView rv;
    private TextView tvKet;
    AdapterMemberUser adapterUser;
    ArrayList<MemberUserModel> listUser = new ArrayList<>();
    SharedPref sharedPref;
    RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calon_member);
        initView();

        sharedPref = new SharedPref(this);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rv.setLayoutManager(layoutManager);

        getUser();

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void getUser() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Loading ...");
        pd.setCancelable(false);
        pd.show();

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getCalonLangganan(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                sharedPref.getIdUser()).enqueue(new Callback<ArrayList<MemberUserModel>>() {
            @Override
            public void onResponse(Call<ArrayList<MemberUserModel>> call, Response<ArrayList<MemberUserModel>> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    listUser = response.body();
                    if (listUser.get(0).getMessage().equals("Berhasil Diload")) {
                        adapterUser = new AdapterMemberUser(listUser, CalonMemberActivity.this,"calon");
                        rv.setAdapter(adapterUser);
                        tvKet.setVisibility(View.GONE);
                    } else {
                        tvKet.setVisibility(View.VISIBLE);
                        rv.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<MemberUserModel>> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getApplicationContext(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initView() {
        imgClose = (ImageView) findViewById(R.id.img_close);
        chatTvTitle = (TextView) findViewById(R.id.chat_tv_title);
        rv = (RecyclerView) findViewById(R.id.rv);
        tvKet = (TextView) findViewById(R.id.tv_ket);
    }
}
