package ai.agusibrhim.design.topedc.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PromoTokoModel {
    @SerializedName("ID_PROMO")
    @Expose
    private String iDPROMO;
    @SerializedName("ID_USER")
    @Expose
    private String iDUSER;
    @SerializedName("ID_SUB_KATEGORI")
    @Expose
    private String iDSUBKATEGORI;
    @SerializedName("PR_JENIS")
    @Expose
    private String pRJENIS;
    @SerializedName("PR_MEMBER")
    @Expose
    private String pRMEMBER;
    @SerializedName("PR_NAME")
    @Expose
    private String pRNAME;
    @SerializedName("PR_CODE")
    @Expose
    private String pRCODE;
    @SerializedName("PR_POTONGAN")
    @Expose
    private String pRPOTONGAN;
    @SerializedName("PR_MIN")
    @Expose
    private String pRMIN;
    @SerializedName("PR_TYPE")
    @Expose
    private String pRTYPE;
    @SerializedName("PR_STATUS")
    @Expose
    private String pRSTATUS;
    @SerializedName("PR_MULAI")
    @Expose
    private String pRMULAI;
    @SerializedName("PR_EXPIRED")
    @Expose
    private String pREXPIRED;
    @SerializedName("CREATED_AT")
    @Expose
    private String cREATEDAT;
    @SerializedName("UPDATED_AT")
    @Expose
    private String uPDATEDAT;
    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("message")
    @Expose
    private String message;

    public String getIDPROMO() {
        return iDPROMO;
    }

    public void setIDPROMO(String iDPROMO) {
        this.iDPROMO = iDPROMO;
    }

    public String getIDUSER() {
        return iDUSER;
    }

    public void setIDUSER(String iDUSER) {
        this.iDUSER = iDUSER;
    }

    public String getIDSUBKATEGORI() {
        return iDSUBKATEGORI;
    }

    public void setIDSUBKATEGORI(String iDSUBKATEGORI) {
        this.iDSUBKATEGORI = iDSUBKATEGORI;
    }

    public String getPRJENIS() {
        return pRJENIS;
    }

    public void setPRJENIS(String pRJENIS) {
        this.pRJENIS = pRJENIS;
    }

    public String getPRMEMBER() {
        return pRMEMBER;
    }

    public void setPRMEMBER(String pRMEMBER) {
        this.pRMEMBER = pRMEMBER;
    }

    public String getPRNAME() {
        return pRNAME;
    }

    public void setPRNAME(String pRNAME) {
        this.pRNAME = pRNAME;
    }

    public String getPRCODE() {
        return pRCODE;
    }

    public void setPRCODE(String pRCODE) {
        this.pRCODE = pRCODE;
    }

    public String getPRPOTONGAN() {
        return pRPOTONGAN;
    }

    public void setPRPOTONGAN(String pRPOTONGAN) {
        this.pRPOTONGAN = pRPOTONGAN;
    }

    public String getPRMIN() {
        return pRMIN;
    }

    public void setPRMIN(String pRMIN) {
        this.pRMIN = pRMIN;
    }

    public String getPRTYPE() {
        return pRTYPE;
    }

    public void setPRTYPE(String pRTYPE) {
        this.pRTYPE = pRTYPE;
    }

    public String getPRSTATUS() {
        return pRSTATUS;
    }

    public void setPRSTATUS(String pRSTATUS) {
        this.pRSTATUS = pRSTATUS;
    }

    public String getPRMULAI() {
        return pRMULAI;
    }

    public void setPRMULAI(String pRMULAI) {
        this.pRMULAI = pRMULAI;
    }

    public String getPREXPIRED() {
        return pREXPIRED;
    }

    public void setPREXPIRED(String pREXPIRED) {
        this.pREXPIRED = pREXPIRED;
    }

    public String getCREATEDAT() {
        return cREATEDAT;
    }

    public void setCREATEDAT(String cREATEDAT) {
        this.cREATEDAT = cREATEDAT;
    }

    public String getUPDATEDAT() {
        return uPDATEDAT;
    }

    public void setUPDATEDAT(String uPDATEDAT) {
        this.uPDATEDAT = uPDATEDAT;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
