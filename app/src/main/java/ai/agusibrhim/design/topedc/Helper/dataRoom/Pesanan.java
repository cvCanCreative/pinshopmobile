package ai.agusibrhim.design.topedc.Helper.dataRoom;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "tpesanan")
public class Pesanan implements Serializable {
    @PrimaryKey(autoGenerate = true)
    public int idPesanan;


    @ColumnInfo(name = "id_barang")
    public String idBarang;

    @ColumnInfo(name = "id_user")
    public String idUser;

    @ColumnInfo(name = "id_toko")
    public String idToko;

    @ColumnInfo(name = "nama_toko")
    public String namaToko;

    @ColumnInfo(name = "nama_barang")
    public String namaBarang;

    @ColumnInfo(name = "jumlah_barang")
    public String jumlahBarang;

    @ColumnInfo(name = "tipe_barang")
    public String tipeBarang;

    @ColumnInfo(name = "harga_barang")
    public String hargaBarang;

    @ColumnInfo(name = "img_barang")
    public String imgBarang;

    @ColumnInfo(name = "sub_harga_barang")
    public String subHargaBarang;

    @ColumnInfo(name = "berat")
    public String berat;

    @ColumnInfo(name = "id_kota")
    public String idKota;

    @ColumnInfo(name = "id_kecamatan")
    public String idKecamatan;

    @ColumnInfo(name = "stok")
    public String stok;

    public String getBerat() {
        return berat;
    }

    public void setBerat(String berat) {
        this.berat = berat;
    }

    public String getIdKota() {
        return idKota;
    }

    public void setIdKota(String idKota) {
        this.idKota = idKota;
    }

    public String getSubHargaBarang() {
        return subHargaBarang;
    }



    public void setSubHargaBarang(String subHargaBarang) {
        this.subHargaBarang = subHargaBarang;
    }

    public String getNamaToko() {
        return namaToko;
    }

    public void setNamaToko(String namaToko) {
        this.namaToko = namaToko;
    }

    public String getImgBarang() {
        return imgBarang;
    }

    public void setImgBarang(String imgBarang) {
        this.imgBarang = imgBarang;
    }

    public int getIdPesanan() {
        return idPesanan;
    }

    public void setIdPesanan(int idPesanan) {
        this.idPesanan = idPesanan;
    }

    public String getIdBarang() {
        return idBarang;
    }

    public void setIdBarang(String idBarang) {
        this.idBarang = idBarang;
    }

    public String getIdToko() {
        return idToko;
    }

    public void setIdToko(String idToko) {
        this.idToko = idToko;
    }

    public String getNamaBarang() {
        return namaBarang;
    }

    public void setNamaBarang(String namaBarang) {
        this.namaBarang = namaBarang;
    }

    public String getJumlahBarang() {
        return jumlahBarang;
    }

    public void setJumlahBarang(String jumlahBarang) {
        this.jumlahBarang = jumlahBarang;
    }

    public String getTipeBarang() {
        return tipeBarang;
    }

    public void setTipeBarang(String tipeBarang) {
        this.tipeBarang = tipeBarang;
    }

    public String getHargaBarang() {
        return hargaBarang;
    }

    public void setHargaBarang(String hargaBarang) {
        this.hargaBarang = hargaBarang;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getIdKecamatan() {
        return idKecamatan;
    }

    public void setIdKecamatan(String idKecamatan) {
        this.idKecamatan = idKecamatan;
    }

    public String getStok() {
        return stok;
    }

    public void setStok(String stok) {
        this.stok = stok;
    }
}
