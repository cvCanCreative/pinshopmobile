package ai.agusibrhim.design.topedc.Activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import ai.agusibrhim.design.topedc.Fragment.BarangTokoFragment;
import ai.agusibrhim.design.topedc.Fragment.DataTokoFragment;
import ai.agusibrhim.design.topedc.Fragment.PromoTokoragment;
import ai.agusibrhim.design.topedc.Fragment.TambahBarangFragment;
import ai.agusibrhim.design.topedc.Fragment.TambahPromoFragment;
import ai.agusibrhim.design.topedc.R;

public class TambahActivity extends AppCompatActivity {

    private LinearLayout ly;
    private TabLayout viewpagertab2;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah);
        initView();

        setupViewPager(viewPager);
        viewpagertab2.setupWithViewPager(viewPager);
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new TambahBarangFragment(), "Tambah Barang");
        adapter.addFragment(new TambahPromoFragment(), "Tambah Promo");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
//            return null;
        }
    }

    private void initView() {
        ly = (LinearLayout) findViewById(R.id.ly);
        viewpagertab2 = (TabLayout) findViewById(R.id.viewpagertab2);
        viewPager = (ViewPager) findViewById(R.id.view_pager);
    }
}
