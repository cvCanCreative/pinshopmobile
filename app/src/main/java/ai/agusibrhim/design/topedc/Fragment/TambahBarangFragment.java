package ai.agusibrhim.design.topedc.Fragment;


import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.fxn.pix.Pix;
import com.fxn.utility.PermUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import ai.agusibrhim.design.topedc.Activity.AddKeluhanActivity;
import ai.agusibrhim.design.topedc.Activity.HomeLikeShopeeActivity;
import ai.agusibrhim.design.topedc.Activity.IntruksiActivity;
import ai.agusibrhim.design.topedc.Activity.TambahBarangActivity;
import ai.agusibrhim.design.topedc.Adapter.CatagoryAdapter;
import ai.agusibrhim.design.topedc.Adapter.PromoAdapter;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.MainActivity;
import ai.agusibrhim.design.topedc.Model.SubKategoriModel;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class TambahBarangFragment extends Fragment {


    private EditText edtNama;
    private Spinner spnKategori;
    private Spinner spnHargaPin;
    private ImageView imgProduk;
    private ImageView imgProduk2;
    private ImageView imgProduk3;
    private EditText edtHarga;
    private EditText edtDeskripsi;
    private EditText edtSku;
    private Spinner spnPromo;
    private EditText edtStok;
    private EditText edtBerat;
    private EditText edtKondisi;
    private Button btnTambah;
    int idKategori;
    SharedPref sharedPref;
    List<String> imagesEncodedList;
    ArrayList<String> listFoto = new ArrayList<>();
    private String h, imagePath;
    List<File> imageFile = new ArrayList<>();
    File fileImages;
    File imageFileSatu, imageFileDua, imageFileTiga;
    CatagoryAdapter catagoryAdapter;
    ArrayList<SubKategoriModel> list = new ArrayList<>();
    ArrayList<String> listKategori = new ArrayList<>();
    ArrayList<String> idListKategori = new ArrayList<>();

    String pilih;
    ArrayList<String> listHarga = new ArrayList<>();
    ArrayList<String> listKodePromo = new ArrayList<>();
    PromoAdapter mAdapter;
    private String harga = "";


    public TambahBarangFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tambah_barang, container, false);
        initView(view);

        awal();

        mainButton();

        editText();

        return view;
    }

    private void mainButton(){
        imgProduk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilih = "satu";
                EasyImage.openChooserWithGallery(TambahBarangFragment.this, "chose", 3);

            }
        });

        imgProduk2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilih = "dua";
                EasyImage.openChooserWithGallery(TambahBarangFragment.this, "chose", 3);
            }
        });

        imgProduk3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilih = "tiga";
                EasyImage.openChooserWithGallery(TambahBarangFragment.this, "chose", 3);
            }
        });

        btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tambahBarang();
            }
        });

        edtKondisi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                kondisi();
            }
        });
    }

    private void editText() {

        // Edit Harga
        edtHarga.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            private String current = "";

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!s.toString().equals(current)) {

                    edtHarga.removeTextChangedListener(this);

                    String cleanString = s.toString().replaceAll("[Rp,.]", "");
                    Locale localeID = new Locale("in", "ID");
//                    NumberFormat format = NumberFormat.getCurrencyInstance(localeID);
//                    String formatted = format.format((double) Integer.valueOf(cleanString));
                    if (cleanString.isEmpty()) {
                        cleanString = "0";
                    }
                    double parsed = Double.parseDouble(cleanString);
                    String formatted = NumberFormat.getCurrencyInstance(localeID).format(parsed);

                    if (!(formatted.length() >= 18)) {
                        current = formatted;
                        harga = cleanString;
                        edtHarga.setText(formatted);
                        edtHarga.setSelection(formatted.length());

                        edtHarga.addTextChangedListener(this);
                    } else {
                        edtHarga.setText(current);
                        edtHarga.setSelection(current.length());

                        edtHarga.addTextChangedListener(this);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }

        });

        // Edit Berat
        edtBerat.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            private String current = "";

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().equals(current)) {

                    if (s.toString().isEmpty()) {
//                        edtBerat.setVisibility(View.GONE);
                    }
                    current = s.toString();
                    edtHarga.removeTextChangedListener(this);
//                    tvSatuanBerat.setVisibility(View.VISIBLE);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void awal() {
        Permission();

        edtKondisi.setFocusable(false);
        sharedPref = new SharedPref(getActivity());
        spnHargaPin.setVisibility(View.GONE);
        getKategori();
        getHargaPin();
    }

    private void getHargaPin() {
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getHargaPin(sharedPref.getIdUser(),
                sharedPref.getSESSION()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        JSONArray jsonArray = new JSONArray(response.body().string());
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.optJSONObject(i);
                            String harga = jsonObject.optString("PP_PRICE");

                            listHarga.add(harga);
                            ArrayAdapter<String> adp2 = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, listHarga);
                            spnHargaPin.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
                            spnHargaPin.setAdapter(adp2);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getKategori() {
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getAllSubKat().enqueue(new Callback<ArrayList<SubKategoriModel>>() {
            @Override
            public void onResponse(Call<ArrayList<SubKategoriModel>> call, Response<ArrayList<SubKategoriModel>> response) {
                if (response.isSuccessful()) {
                    list = response.body();

                    for (int i = 0; i < list.size(); i++) {
                        listKategori.add(list.get(i).getSKNAME());
                        idListKategori.add(list.get(i).getIDSUBKATEGORI());
                    }

                    ArrayAdapter<String> adp2 = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, listKategori);
                    spnKategori.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
                    spnKategori.setAdapter(adp2);

                    spnKategori.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            int idKat = spnKategori.getSelectedItemPosition();
                            idKategori = Integer.parseInt(idListKategori.get(idKat));
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<ArrayList<SubKategoriModel>> call, Throwable t) {
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void kondisi() {
        final CharSequence[] dialogItem = {"NEW", "SECOND"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Tentukan Pilihan Anda");
        builder.setItems(dialogItem, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                switch (i) {
                    case 0:

                        edtKondisi.setText("NEW");
                        break;
                    case 1:

                        edtKondisi.setText("SECOND");
                        break;
                }
            }
        });
        builder.create().show();
    }

    private void tambahBarang() {
        if (imageFileSatu != null){
            imageFile.add(imageFileSatu);
        }
        if (imageFileDua != null){
            imageFile.add(imageFileDua);
        }
        if (imageFileTiga != null){
            imageFile.add(imageFileTiga);
        }

        final ProgressDialog pd = new ProgressDialog(getActivity());
        pd.setTitle("Proses ...");
        pd.setCancelable(false);
        pd.show();

        MultipartBody.Part[] surveyImagesParts = new MultipartBody.Part[imageFile.size()];
        for (int index = 0; index < imageFile.size(); index++) {

//            File file = new File(imageFile.get(index));
            RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), imageFile.get(index));
            surveyImagesParts[index] = MultipartBody.Part.createFormData("image[]", imageFile.get(index).getName(), surveyBody);
        }

        RequestBody id_kategori = RequestBody.create(MediaType.parse("text/plain"), "" + idKategori);
//        RequestBody idPromo = RequestBody.create(MediaType.parse("text/plain"), "" + spnPromo.getSelectedItem().toString());
//        RequestBody caption = RequestBody.create(MediaType.parse("text/plain"), listCaption);
        final RequestBody nama = RequestBody.create(MediaType.parse("text/plain"), edtNama.getText().toString());
        RequestBody hargaku = RequestBody.create(MediaType.parse("text/plain"), harga);
//        RequestBody harga_pin = RequestBody.create(MediaType.parse("text/plain"), spnHargaPin.getSelectedItem().toString());
        RequestBody sku = RequestBody.create(MediaType.parse("text/plain"), edtSku.getText().toString());
        RequestBody deskripsi = RequestBody.create(MediaType.parse("text/plain"), edtDeskripsi.getText().toString());
        RequestBody stok = RequestBody.create(MediaType.parse("text/plain"), edtStok.getText().toString());
        RequestBody berat = RequestBody.create(MediaType.parse("text/plain"), edtBerat.getText().toString());
        RequestBody kondisi = RequestBody.create(MediaType.parse("text/plain"), edtKondisi.getText().toString());

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.tambahBarang(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                id_kategori,
//                idPromo,
                nama,
                hargaku,
//                harga_pin,
                sku,
                deskripsi,
                stok,
                berat,
                kondisi,
                surveyImagesParts).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String pesan = jsonObject.optString("message");
                        if (pesan.equals("Berhasil ditambahkan")) {
                            new AlertDialog.Builder(getActivity())
                                    .setMessage("" + pesan)
                                    .setCancelable(false)
                                    .setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            startActivity(new Intent(getActivity(), HomeLikeShopeeActivity.class)
                                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));

                                        }
                                    })
                                    .show();
                        } else {
                            Toast.makeText(getActivity(), "" + pesan, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EasyImage.handleActivityResult(requestCode, resultCode, data, getActivity(), new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                //Some error handling
                Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }


            @Override
            public void onImagePicked(File image, EasyImage.ImageSource source, int type) {
                if (pilih.equals("satu")){
                    Glide.with(getActivity())
                            .load(new File(image.getPath()))
                            .into(imgProduk);
                    imageFileSatu = new File(image.getPath());

                    try {
                        imageFileSatu = new Compressor(getActivity()).compressToFile(imageFileSatu);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                    imageFile.add(imageFileSatu);
                }else if (pilih.equals("dua")){
                    Glide.with(getActivity())
                            .load(new File(image.getPath()))
                            .into(imgProduk2);
                    imageFileDua = new File(image.getPath());

                    try {
                        imageFileDua = new Compressor(getActivity()).compressToFile(imageFileDua);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                    imageFile.add(imageFileDua);
                }else if (pilih.equals("tiga")){
                    Glide.with(getActivity())
                            .load(new File(image.getPath()))
                            .into(imgProduk3);
                    imageFileTiga = new File(image.getPath());

                    try {
                        imageFileTiga = new Compressor(getActivity()).compressToFile(imageFileTiga);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                    imageFile.add(imageFileTiga);
                }
            }



            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
                // Cancel handling, you might wanna remove taken photo if it was canceled
                Toast.makeText(getActivity(), "cancel", Toast.LENGTH_SHORT).show();
                if (source == EasyImage.ImageSource.CAMERA) {
                    File photoFile = EasyImage.lastlyTakenButCanceledPhoto(getActivity());
                    if (photoFile != null) photoFile.delete();
                }
            }

        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PermUtil.REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Pix.start(getActivity(), 100);
                } else {
                    Toast.makeText(getActivity(), "Approve permissions to open Pix ImagePicker", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

    private void Permission() {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.CAMERA)) {

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.CAMERA},
                        1);
            }
        }
    }
    private void initView(View view) {
        edtNama = (EditText) view.findViewById(R.id.edt_nama);
        spnKategori = (Spinner) view.findViewById(R.id.spn_kategori);
        spnHargaPin = (Spinner) view.findViewById(R.id.spn_harga_pin);
        imgProduk = (ImageView) view.findViewById(R.id.img_produk);
        imgProduk2 = (ImageView) view.findViewById(R.id.img_produk2);
        imgProduk3 = (ImageView) view.findViewById(R.id.img_produk3);
        edtHarga = (EditText) view.findViewById(R.id.edt_harga);
        edtDeskripsi = (EditText) view.findViewById(R.id.edt_deskripsi);
        edtSku = (EditText) view.findViewById(R.id.edt_sku);
        spnPromo = (Spinner) view.findViewById(R.id.spn_promo);
        edtStok = (EditText) view.findViewById(R.id.edt_stok);
        edtBerat = (EditText) view.findViewById(R.id.edt_berat);
        edtKondisi = (EditText) view.findViewById(R.id.edt_kondisi);
        btnTambah = (Button) view.findViewById(R.id.btn_tambah);
    }
}
