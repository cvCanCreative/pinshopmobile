package ai.agusibrhim.design.topedc.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import ai.agusibrhim.design.topedc.Helper.Config;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.MainActivity;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AccountActivity extends AppCompatActivity {

    SharedPref sharedPref;
    private SwipeRefreshLayout swipe;
    private TextView tvNama;
    private LinearLayout lyAturAkun;
    private TextView tvNoTlpProfil;
    private TextView tvEmailProfil;
    private TextView tvJenisKelaminProfil;
    private TextView tvTglLahirProfil;
    private CircleImageView imgProfil;
    private TextView tvBelumLogin;
    private LinearLayout mainLinearLayout1;
    private ImageView imgClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        initView();

        sharedPref = new SharedPref(this);
        awal();

        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (sharedPref.getSudahLogin()) {
                    mainLinearLayout1.setVisibility(View.VISIBLE);
                    profil();
                } else {
                    swipe.setRefreshing(false);
                    mainLinearLayout1.setVisibility(View.GONE);
                    tvBelumLogin.setVisibility(View.VISIBLE);

                    Toast.makeText(AccountActivity.this, "Anda belum Login", Toast.LENGTH_SHORT).show();
                }
            }
        });
        lyAturAkun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), PengaturanAkunActivity.class);
                startActivity(intent);
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), MainActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();
            }
        });


    }

    private void awal() {
        if (sharedPref.getSudahLogin()) {
            profil();
        } else {
            mainLinearLayout1.setVisibility(View.GONE);
            tvBelumLogin.setVisibility(View.VISIBLE);
            Toast.makeText(this, "Anda belum Login", Toast.LENGTH_SHORT).show();
        }
    }

    private void profil() {
        swipe.setRefreshing(true);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getProfil(sharedPref.getIdUser(), sharedPref.getSESSION()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    swipe.setRefreshing(false);
                    try {
                        JSONArray jsonArray = new JSONArray(response.body().string());
                        JSONObject jsonObject = jsonArray.optJSONObject(0);
                        String foto = jsonObject.optString("USD_FOTO");
                        String nama = jsonObject.optString("USD_FULLNAME");
                        String noHp = jsonObject.optString("US_TELP");
                        String email = jsonObject.optString("US_EMAIL");
                        String kelamin = jsonObject.optString("USD_GENDER");
                        String ttl = jsonObject.optString("USD_BIRTH");
                        String namaToko = jsonObject.optString("USD_TOKO");

//                        sharedPref.savePrefString(SharedPref.TOKO,namaToko);

                        tvNama.setText(nama);
                        tvEmailProfil.setText(email);
                        tvTglLahirProfil.setText(ttl);

                        if (kelamin.equals("LK")) {
                            tvJenisKelaminProfil.setText("Laki-laki");
                        } else {
                            tvJenisKelaminProfil.setText("Perempuan");
                        }

                        tvNoTlpProfil.setText(noHp);

                        Picasso.with(getApplicationContext())
                                .load(Config.IMAGE_PROFIL + foto)
                                .into(imgProfil);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(AccountActivity.this, "gagal", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                swipe.setRefreshing(false);
                Toast.makeText(AccountActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initView() {
        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe);

        tvNama = (TextView) findViewById(R.id.tv_nama);
        lyAturAkun = (LinearLayout) findViewById(R.id.ly_atur_akun);
        tvNoTlpProfil = (TextView) findViewById(R.id.tv_noTlp_profil);
        tvEmailProfil = (TextView) findViewById(R.id.tv_email_profil);
        tvJenisKelaminProfil = (TextView) findViewById(R.id.tv_jenis_kelamin_profil);
        tvTglLahirProfil = (TextView) findViewById(R.id.tv_tgl_lahir_profil);
        imgProfil = (CircleImageView) findViewById(R.id.img_profil);
        tvBelumLogin = (TextView) findViewById(R.id.tv_belum_login);
        mainLinearLayout1 = (LinearLayout) findViewById(R.id.mainLinearLayout1);
        imgClose = (ImageView) findViewById(R.id.img_close);
    }
}
