package ai.agusibrhim.design.topedc.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Activity.SubKategoriActivity;
import ai.agusibrhim.design.topedc.Model.ListCategory;
import ai.agusibrhim.design.topedc.R;

public class AdapterKategori extends RecyclerView.Adapter<AdapterKategori.ViewHolder> {

    private Context context;
    private ArrayList<ListCategory> list;

    int size;

    public AdapterKategori(Context context, ArrayList<ListCategory> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_kategori,
                parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final ListCategory cat =list.get(position);
        holder.kategoriTvTitle.setText(cat.getKBA_NAME());

        if(cat.getUrl()!=null && !cat.getUrl().isEmpty()){
            Picasso.with(context)
                    .load( cat.getUrl() + cat.getKBA_IMAGE())
                    .placeholder(R.drawable.ic_cat1)
                    .error(R.drawable.ic_cat2)
                    .fit()
                    .noFade()
                    .into(holder.kategoriIvThumbnail);
        }else{
            holder.kategoriIvThumbnail.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_cat8));
        }

        holder.kategoriDivMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, SubKategoriActivity.class);
                intent.putExtra("id",cat.getID_KATEGORI_BARANG());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        if (list.size() > 9) {
            size = 9;
        } else {
            size = list.size();
        }
        return size;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout kategoriDivMain;
        private ImageView kategoriIvThumbnail;
        private TextView kategoriTvTitle;
        public ViewHolder(View itemView) {
            super(itemView);
            kategoriDivMain = itemView.findViewById(R.id.kategori_div_main);
            kategoriIvThumbnail = itemView.findViewById(R.id.kategori_iv_thumbnail);
            kategoriTvTitle = itemView.findViewById(R.id.kategori_tv_title);
        }
    }

}