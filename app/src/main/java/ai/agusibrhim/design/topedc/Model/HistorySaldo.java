package ai.agusibrhim.design.topedc.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HistorySaldo implements Parcelable {
    @SerializedName("ID_HISTORY_SALDO")
    @Expose
    private String iDHISTORYSALDO;
    @SerializedName("ID_USER")
    @Expose
    private String iDUSER;
    @SerializedName("HSA_TRX_SALDO")
    @Expose
    private String hSATRXSALDO;
    @SerializedName("HSA_TYPE")
    @Expose
    private String hSATYPE;
    @SerializedName("HSA_FROM")
    @Expose
    private String hSAFROM;
    @SerializedName("HSA_STATUS")
    @Expose
    private String hSASTATUS;
    @SerializedName("HSA_SALDO")
    @Expose
    private String hSASALDO;
    @SerializedName("CREATED_AT")
    @Expose
    private String cREATEDAT;
    @SerializedName("UPDATED_AT")
    @Expose
    private String uPDATEDAT;
    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("message")
    @Expose
    private String message;

    protected HistorySaldo(Parcel in) {
        iDHISTORYSALDO = in.readString();
        iDUSER = in.readString();
        hSATRXSALDO = in.readString();
        hSATYPE = in.readString();
        hSAFROM = in.readString();
        hSASTATUS = in.readString();
        hSASALDO = in.readString();
        cREATEDAT = in.readString();
        uPDATEDAT = in.readString();
        if (in.readByte() == 0) {
            success = null;
        } else {
            success = in.readInt();
        }
        message = in.readString();
    }

    public static final Creator<HistorySaldo> CREATOR = new Creator<HistorySaldo>() {
        @Override
        public HistorySaldo createFromParcel(Parcel in) {
            return new HistorySaldo(in);
        }

        @Override
        public HistorySaldo[] newArray(int size) {
            return new HistorySaldo[size];
        }
    };

    public String getIDHISTORYSALDO() {
        return iDHISTORYSALDO;
    }

    public void setIDHISTORYSALDO(String iDHISTORYSALDO) {
        this.iDHISTORYSALDO = iDHISTORYSALDO;
    }

    public String getIDUSER() {
        return iDUSER;
    }

    public void setIDUSER(String iDUSER) {
        this.iDUSER = iDUSER;
    }

    public String getHSATRXSALDO() {
        return hSATRXSALDO;
    }

    public void setHSATRXSALDO(String hSATRXSALDO) {
        this.hSATRXSALDO = hSATRXSALDO;
    }

    public String getHSATYPE() {
        return hSATYPE;
    }

    public void setHSATYPE(String hSATYPE) {
        this.hSATYPE = hSATYPE;
    }

    public String getHSAFROM() {
        return hSAFROM;
    }

    public void setHSAFROM(String hSAFROM) {
        this.hSAFROM = hSAFROM;
    }

    public String getHSASTATUS() {
        return hSASTATUS;
    }

    public void setHSASTATUS(String hSASTATUS) {
        this.hSASTATUS = hSASTATUS;
    }

    public String getHSASALDO() {
        return hSASALDO;
    }

    public void setHSASALDO(String hSASALDO) {
        this.hSASALDO = hSASALDO;
    }

    public String getCREATEDAT() {
        return cREATEDAT;
    }

    public void setCREATEDAT(String cREATEDAT) {
        this.cREATEDAT = cREATEDAT;
    }

    public String getUPDATEDAT() {
        return uPDATEDAT;
    }

    public void setUPDATEDAT(String uPDATEDAT) {
        this.uPDATEDAT = uPDATEDAT;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(iDHISTORYSALDO);
        parcel.writeString(iDUSER);
        parcel.writeString(hSATRXSALDO);
        parcel.writeString(hSATYPE);
        parcel.writeString(hSAFROM);
        parcel.writeString(hSASTATUS);
        parcel.writeString(hSASALDO);
        parcel.writeString(cREATEDAT);
        parcel.writeString(uPDATEDAT);
        if (success == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(success);
        }
        parcel.writeString(message);
    }
}
