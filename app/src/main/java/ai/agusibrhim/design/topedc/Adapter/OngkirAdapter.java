package ai.agusibrhim.design.topedc.Adapter;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ai.agusibrhim.design.topedc.Activity.PembayaranActivity;
import ai.agusibrhim.design.topedc.Helper.HelperClass;
import ai.agusibrhim.design.topedc.Helper.dataRoom.AppDatabase;
import ai.agusibrhim.design.topedc.Helper.dataRoom.Pesanan;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OngkirAdapter extends RecyclerView.Adapter<OngkirAdapter.Holder> {

    List<String> list;
    Context context;
    ArrayList<String> jasaPengiriman =new ArrayList<>();
    ArrayList<String> listTipe =new ArrayList<>();
    ArrayList<Pesanan> listPesananByIdToko = new ArrayList<>();

    private AppDatabase db;

    public OngkirAdapter(List<String> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_pembayaran,parent,false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final Holder holder, int position) {
        db = Room.databaseBuilder(context,
                AppDatabase.class, "pesanandb").allowMainThreadQueries().build();

        final List<String> listKurir = new ArrayList<String>();
        listKurir.add("jne");
        listKurir.add("tiki");
        listKurir.add("pos");
        listKurir.add("pcp");
        listKurir.add("esl");
        listKurir.add("rpx");
        listKurir.add("pandu");
        listKurir.add("wahan");
        listKurir.add("jnt");
        listKurir.add("pahala");
        listKurir.add("cahaya");
        listKurir.add("sap");
        listKurir.add("jet");
        listKurir.add("indah");
        listKurir.add("dse");
        listKurir.add("slis");
        listKurir.add("first");
        listKurir.add("ncs");
        listKurir.add("sta");

        ArrayAdapter<String> adp2 = new ArrayAdapter<String>(context, R.layout.support_simple_spinner_dropdown_item, listKurir);
        holder.spn_kurir.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
        holder.spn_kurir.setAdapter(adp2);
        holder.spn_kurir.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Holder extends RecyclerView.ViewHolder{

        Spinner spn_kurir,spn_Harga;
        EditText edtCatatan;
        LinearLayout div_pem;
        public Holder(View itemView) {
            super(itemView);

            div_pem = itemView.findViewById(R.id.div_pembayaran);
            spn_kurir = itemView.findViewById(R.id.spn_kurir2);
            spn_Harga = itemView.findViewById(R.id.spn_harga2);
            edtCatatan = itemView.findViewById(R.id.edt_catatan);
        }
    }
}
