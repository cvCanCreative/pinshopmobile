package ai.agusibrhim.design.topedc.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.MainActivity;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddKomplainActivity extends AppCompatActivity {

    private LinearLayout ly;
    private EditText edtJudul;
    private ImageView imgHasil;
    private Button btnTambah;
    SharedPref sharedPref;
    String id;
    File fileImages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_komplain);
        initView();

        awal();

        imgHasil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EasyImage.openChooserWithGallery(AddKomplainActivity.this, "chose", 3);
            }
        });

        btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                komplain();
            }
        });

    }

    private void awal(){
        sharedPref = new SharedPref(this);
        id = getIntent().getStringExtra("id");
    }
    private void komplain(){
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Proses ...");
        pd.setCancelable(false);
        pd.show();

        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), fileImages);
        MultipartBody.Part body = MultipartBody.Part.createFormData("picture", fileImages.getName(), requestFile);

        RequestBody idku = RequestBody.create(MediaType.parse("text/plain"), id);
        RequestBody judul = RequestBody.create(MediaType.parse("text/plain"), edtJudul.getText().toString());
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.tambahKomplain(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                idku,
                judul,
                body).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String kode = jsonObject.optString("success");
                        String pesan = jsonObject.optString("message");
                        if (kode.equals("1")){
//                            startActivity(new Intent(getApplicationContext(), HomeLikeShopeeActivity.class)
//                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                            Toast.makeText(AddKomplainActivity.this, ""+pesan, Toast.LENGTH_SHORT).show();
                            edtJudul.setText("");
                            edtJudul.requestFocus();
                            imgHasil.setImageResource(R.drawable.no_image_found);
                        }else {
                            Toast.makeText(AddKomplainActivity.this, ""+pesan, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(AddKomplainActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                //Some error handling
                Toast.makeText(AddKomplainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onImagePicked(File image, EasyImage.ImageSource source, int type) {
                Glide.with(AddKomplainActivity.this)
                        .load(new File(image.getPath()))
                        .into(imgHasil);
                fileImages = new File(image.getPath());

                try {
                    fileImages = new Compressor(AddKomplainActivity.this).compressToFile(fileImages);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
                super.onCanceled(source, type);
            }
        });
    }

    private void initView() {
        ly = (LinearLayout) findViewById(R.id.ly);
        edtJudul = (EditText) findViewById(R.id.edt_judul);
        imgHasil = (ImageView) findViewById(R.id.img_hasil);
        btnTambah = (Button) findViewById(R.id.btn_tambah);
    }
}
