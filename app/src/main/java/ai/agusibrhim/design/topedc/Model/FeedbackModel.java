package ai.agusibrhim.design.topedc.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FeedbackModel implements Parcelable{
    @SerializedName("ID_FEEDBACK")
    @Expose
    private String iDFEEDBACK;
    @SerializedName("ID_TRANSAKSI_DETAIL")
    @Expose
    private String iDTRANSAKSIDETAIL;
    @SerializedName("FE_RATING")
    @Expose
    private String fERATING;
    @SerializedName("FE_COMMENT")
    @Expose
    private String fECOMMENT;
    @SerializedName("CREATED_AT")
    @Expose
    private String cREATEDAT;
    @SerializedName("UPDATED_AT")
    @Expose
    private String uPDATEDAT;
    @SerializedName("ID_USER_DETAIL")
    @Expose
    private String iDUSERDETAIL;
    @SerializedName("ID_USER")
    @Expose
    private String iDUSER;
    @SerializedName("USD_FOTO")
    @Expose
    private String uSDFOTO;
    @SerializedName("USD_FULLNAME")
    @Expose
    private String uSDFULLNAME;
    @SerializedName("USD_BIRTH")
    @Expose
    private String uSDBIRTH;
    @SerializedName("USD_ADDRESS")
    @Expose
    private String uSDADDRESS;
    @SerializedName("USD_GENDER")
    @Expose
    private String uSDGENDER;
    @SerializedName("USD_TOKO")
    @Expose
    private String uSDTOKO;
    @SerializedName("USD_TOKO_FOTO")
    @Expose
    private String uSDTOKOFOTO;
    @SerializedName("USD_TOKO_DETAIL")
    @Expose
    private String uSDTOKODETAIL;
    @SerializedName("ID_TOKO_SKAT")
    @Expose
    private String iDTOKOSKAT;
    @SerializedName("USD_ID_PROV")
    @Expose
    private String uSDIDPROV;
    @SerializedName("USD_ID_KOTA")
    @Expose
    private String uSDIDKOTA;
    @SerializedName("USD_KOTA")
    @Expose
    private String uSDKOTA;
    @SerializedName("USD_ID_KECAMATAN")
    @Expose
    private String uSDIDKECAMATAN;
    @SerializedName("USD_KECAMATAN")
    @Expose
    private String uSDKECAMATAN;
    @SerializedName("USD_STATUS")
    @Expose
    private String uSDSTATUS;
    @SerializedName("USD_REFERRAL")
    @Expose
    private String uSDREFERRAL;
    @SerializedName("USD_REFERRAL_FROM")
    @Expose
    private String uSDREFERRALFROM;
    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("message")
    @Expose
    private String message;

    protected FeedbackModel(Parcel in) {
        iDFEEDBACK = in.readString();
        iDTRANSAKSIDETAIL = in.readString();
        fERATING = in.readString();
        fECOMMENT = in.readString();
        cREATEDAT = in.readString();
        uPDATEDAT = in.readString();
        iDUSERDETAIL = in.readString();
        iDUSER = in.readString();
        uSDFOTO = in.readString();
        uSDFULLNAME = in.readString();
        uSDBIRTH = in.readString();
        uSDADDRESS = in.readString();
        uSDGENDER = in.readString();
        uSDTOKO = in.readString();
        uSDTOKOFOTO = in.readString();
        uSDTOKODETAIL = in.readString();
        iDTOKOSKAT = in.readString();
        uSDIDPROV = in.readString();
        uSDIDKOTA = in.readString();
        uSDKOTA = in.readString();
        uSDIDKECAMATAN = in.readString();
        uSDKECAMATAN = in.readString();
        uSDSTATUS = in.readString();
        uSDREFERRAL = in.readString();
        uSDREFERRALFROM = in.readString();
        if (in.readByte() == 0) {
            success = null;
        } else {
            success = in.readInt();
        }
        message = in.readString();
    }

    public static final Creator<FeedbackModel> CREATOR = new Creator<FeedbackModel>() {
        @Override
        public FeedbackModel createFromParcel(Parcel in) {
            return new FeedbackModel(in);
        }

        @Override
        public FeedbackModel[] newArray(int size) {
            return new FeedbackModel[size];
        }
    };

    public String getIDFEEDBACK() {
        return iDFEEDBACK;
    }

    public void setIDFEEDBACK(String iDFEEDBACK) {
        this.iDFEEDBACK = iDFEEDBACK;
    }

    public String getIDTRANSAKSIDETAIL() {
        return iDTRANSAKSIDETAIL;
    }

    public void setIDTRANSAKSIDETAIL(String iDTRANSAKSIDETAIL) {
        this.iDTRANSAKSIDETAIL = iDTRANSAKSIDETAIL;
    }

    public String getFERATING() {
        return fERATING;
    }

    public void setFERATING(String fERATING) {
        this.fERATING = fERATING;
    }

    public String getFECOMMENT() {
        return fECOMMENT;
    }

    public void setFECOMMENT(String fECOMMENT) {
        this.fECOMMENT = fECOMMENT;
    }

    public String getCREATEDAT() {
        return cREATEDAT;
    }

    public void setCREATEDAT(String cREATEDAT) {
        this.cREATEDAT = cREATEDAT;
    }

    public String getUPDATEDAT() {
        return uPDATEDAT;
    }

    public void setUPDATEDAT(String uPDATEDAT) {
        this.uPDATEDAT = uPDATEDAT;
    }

    public String getIDUSERDETAIL() {
        return iDUSERDETAIL;
    }

    public void setIDUSERDETAIL(String iDUSERDETAIL) {
        this.iDUSERDETAIL = iDUSERDETAIL;
    }

    public String getIDUSER() {
        return iDUSER;
    }

    public void setIDUSER(String iDUSER) {
        this.iDUSER = iDUSER;
    }

    public String getUSDFOTO() {
        return uSDFOTO;
    }

    public void setUSDFOTO(String uSDFOTO) {
        this.uSDFOTO = uSDFOTO;
    }

    public String getUSDFULLNAME() {
        return uSDFULLNAME;
    }

    public void setUSDFULLNAME(String uSDFULLNAME) {
        this.uSDFULLNAME = uSDFULLNAME;
    }

    public String getUSDBIRTH() {
        return uSDBIRTH;
    }

    public void setUSDBIRTH(String uSDBIRTH) {
        this.uSDBIRTH = uSDBIRTH;
    }

    public String getUSDADDRESS() {
        return uSDADDRESS;
    }

    public void setUSDADDRESS(String uSDADDRESS) {
        this.uSDADDRESS = uSDADDRESS;
    }

    public String getUSDGENDER() {
        return uSDGENDER;
    }

    public void setUSDGENDER(String uSDGENDER) {
        this.uSDGENDER = uSDGENDER;
    }

    public String getUSDTOKO() {
        return uSDTOKO;
    }

    public void setUSDTOKO(String uSDTOKO) {
        this.uSDTOKO = uSDTOKO;
    }

    public String getUSDTOKOFOTO() {
        return uSDTOKOFOTO;
    }

    public void setUSDTOKOFOTO(String uSDTOKOFOTO) {
        this.uSDTOKOFOTO = uSDTOKOFOTO;
    }

    public String getUSDTOKODETAIL() {
        return uSDTOKODETAIL;
    }

    public void setUSDTOKODETAIL(String uSDTOKODETAIL) {
        this.uSDTOKODETAIL = uSDTOKODETAIL;
    }

    public String getIDTOKOSKAT() {
        return iDTOKOSKAT;
    }

    public void setIDTOKOSKAT(String iDTOKOSKAT) {
        this.iDTOKOSKAT = iDTOKOSKAT;
    }

    public String getUSDIDPROV() {
        return uSDIDPROV;
    }

    public void setUSDIDPROV(String uSDIDPROV) {
        this.uSDIDPROV = uSDIDPROV;
    }

    public String getUSDIDKOTA() {
        return uSDIDKOTA;
    }

    public void setUSDIDKOTA(String uSDIDKOTA) {
        this.uSDIDKOTA = uSDIDKOTA;
    }

    public String getUSDKOTA() {
        return uSDKOTA;
    }

    public void setUSDKOTA(String uSDKOTA) {
        this.uSDKOTA = uSDKOTA;
    }

    public String getUSDIDKECAMATAN() {
        return uSDIDKECAMATAN;
    }

    public void setUSDIDKECAMATAN(String uSDIDKECAMATAN) {
        this.uSDIDKECAMATAN = uSDIDKECAMATAN;
    }

    public String getUSDKECAMATAN() {
        return uSDKECAMATAN;
    }

    public void setUSDKECAMATAN(String uSDKECAMATAN) {
        this.uSDKECAMATAN = uSDKECAMATAN;
    }

    public String getUSDSTATUS() {
        return uSDSTATUS;
    }

    public void setUSDSTATUS(String uSDSTATUS) {
        this.uSDSTATUS = uSDSTATUS;
    }

    public String getUSDREFERRAL() {
        return uSDREFERRAL;
    }

    public void setUSDREFERRAL(String uSDREFERRAL) {
        this.uSDREFERRAL = uSDREFERRAL;
    }

    public String getUSDREFERRALFROM() {
        return uSDREFERRALFROM;
    }

    public void setUSDREFERRALFROM(String uSDREFERRALFROM) {
        this.uSDREFERRALFROM = uSDREFERRALFROM;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(iDFEEDBACK);
        parcel.writeString(iDTRANSAKSIDETAIL);
        parcel.writeString(fERATING);
        parcel.writeString(fECOMMENT);
        parcel.writeString(cREATEDAT);
        parcel.writeString(uPDATEDAT);
        parcel.writeString(iDUSERDETAIL);
        parcel.writeString(iDUSER);
        parcel.writeString(uSDFOTO);
        parcel.writeString(uSDFULLNAME);
        parcel.writeString(uSDBIRTH);
        parcel.writeString(uSDADDRESS);
        parcel.writeString(uSDGENDER);
        parcel.writeString(uSDTOKO);
        parcel.writeString(uSDTOKOFOTO);
        parcel.writeString(uSDTOKODETAIL);
        parcel.writeString(iDTOKOSKAT);
        parcel.writeString(uSDIDPROV);
        parcel.writeString(uSDIDKOTA);
        parcel.writeString(uSDKOTA);
        parcel.writeString(uSDIDKECAMATAN);
        parcel.writeString(uSDKECAMATAN);
        parcel.writeString(uSDSTATUS);
        parcel.writeString(uSDREFERRAL);
        parcel.writeString(uSDREFERRALFROM);
        if (success == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(success);
        }
        parcel.writeString(message);
    }
}
