package ai.agusibrhim.design.topedc.Activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.fxn.pix.Pix;
import com.fxn.utility.PermUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Helper.Config;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.Model.BarangTokoModel;
import ai.agusibrhim.design.topedc.Model.SubKategoriModel;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditBarangActivity extends AppCompatActivity {

    private EditText edtNama;
    private Spinner spnKategori;
    private Spinner spnHargaPin;
    private ImageView imgProduk;
    private ImageView imgProduk2;
    private ImageView imgProduk3;
    private EditText edtHarga;
    private EditText edtDeskripsi;
    private EditText edtSku;
    private Spinner spnPromo;
    private EditText edtStok;
    private EditText edtBerat;
    private EditText edtKondisi;
    private Button btnTambah;
    int idKategori;
    File imageFileSatu, imageFileDua, imageFileTiga;
    SharedPref sharedPref;
    String pilih, nama1, nama2, nama3;
    ArrayList<String> listHarga = new ArrayList<>();
    ArrayList<String> listKodePromo = new ArrayList<>();
    ArrayList<SubKategoriModel> list = new ArrayList<>();
    ArrayList<String> listKategori = new ArrayList<>();
    ArrayList<String> idListKategori = new ArrayList<>();
//    ArrayList<BarangTokoModel> listBarang = new ArrayList<>();
    BarangTokoModel listBarang;
    ArrayList<String> listNamaGambar = new ArrayList<>();
    int posisi;
    private TextView btnHapus1;
    private TextView btnHapus2;
    private TextView btnHapus3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_barang);
        initView();
        awal();
        imgProduk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilih = "satu";
                EasyImage.openChooserWithGallery(EditBarangActivity.this, "chose", 3);

            }
        });

        imgProduk2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilih = "dua";
                EasyImage.openChooserWithGallery(EditBarangActivity.this, "chose", 3);
            }
        });

        imgProduk3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilih = "tiga";
                EasyImage.openChooserWithGallery(EditBarangActivity.this, "chose", 3);
            }
        });

        btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edit();
            }
        });

        edtKondisi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                kondisi();
            }
        });

        btnHapus1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hapus(nama1,"pertama");
            }
        });

        btnHapus2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hapus(nama2,"kedua");
            }
        });

        btnHapus3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hapus(nama3,"ketiga");
            }
        });

        spnKategori.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int idKat = spnKategori.getSelectedItemPosition();
                idKategori = Integer.parseInt(idListKategori.get(idKat));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void hapus(String nama, final String posisi){
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Proses ...");
        pd.setCancelable(false);
        pd.show();
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.deleteGambar(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                nama).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String kode = jsonObject.optString("success");
                        String pesan = jsonObject.optString("message");
                        if (kode.equals("1")){
                            if (posisi.equals("pertama")){
                                Toast.makeText(EditBarangActivity.this, ""+pesan, Toast.LENGTH_SHORT).show();
                                imgProduk.setImageResource(R.drawable.no_image_found);
                                nama1 = null;
                            }else if (posisi.equals("kedua")){
                                Toast.makeText(EditBarangActivity.this, ""+pesan, Toast.LENGTH_SHORT).show();
                                imgProduk2.setImageResource(R.drawable.no_image_found);
                                nama2 = null;
                            }else if (posisi.equals("ketiga")){
                                Toast.makeText(EditBarangActivity.this, ""+pesan, Toast.LENGTH_SHORT).show();
                                imgProduk3.setImageResource(R.drawable.no_image_found);
                                nama3 = null;
                            }
                        }else {
                            Toast.makeText(EditBarangActivity.this, ""+pesan, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(EditBarangActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void awal() {
        sharedPref = new SharedPref(this);
        btnTambah.setText("EDIT");
        listBarang = getIntent().getParcelableExtra("list");
        posisi = Integer.parseInt(getIntent().getStringExtra("posisi"));

        setImage();
        edtKondisi.setFocusable(false);

        edtNama.setText(listBarang.getBANAME());
        edtBerat.setText(listBarang.getBAWEIGHT());
        edtDeskripsi.setText(listBarang.getBADESCRIPTION());
        edtHarga.setText(listBarang.getBAPRICE());
        edtKondisi.setText(listBarang.getBACONDITION());
        edtSku.setText(listBarang.getBASKU());
        edtStok.setText(listBarang.getBASTOCK());
        listBarang.getIDKATEGORIBARANG();
        idKategori = Integer.parseInt(listBarang.getIDKATEGORIBARANG());

        getKategori();
        getHargaPin();
    }

    private void setImage() {
        String gambar = listBarang.getBAIMAGE();
        String gb = gambar.replace("|", " ");
        String strArray[] = gb.split(" ");
        if (strArray.length == 1) {
            nama1 = strArray[0];
            Glide.with(EditBarangActivity.this)
                    .load(Config.IMAGE_BARANG + strArray[0])
                    .into(imgProduk);
        } else if (strArray.length == 2) {
            nama1 = strArray[0];
            Glide.with(EditBarangActivity.this)
                    .load(Config.IMAGE_BARANG + strArray[0])
                    .into(imgProduk);
            nama2 = strArray[1];
            Glide.with(EditBarangActivity.this)
                    .load(Config.IMAGE_BARANG + strArray[1])
                    .into(imgProduk2);

        } else if (strArray.length == 3) {
            nama1 = strArray[0];
            Glide.with(EditBarangActivity.this)
                    .load(Config.IMAGE_BARANG + strArray[0])
                    .into(imgProduk);
            nama2 = strArray[1];
            Glide.with(EditBarangActivity.this)
                    .load(Config.IMAGE_BARANG + strArray[1])
                    .into(imgProduk2);
            nama3 = strArray[2];
            Glide.with(EditBarangActivity.this)
                    .load(Config.IMAGE_BARANG + strArray[2])
                    .into(imgProduk3);
        }
    }

    private void kondisi() {
        final CharSequence[] dialogItem = {"NEW", "SECOND"};
        AlertDialog.Builder builder = new AlertDialog.Builder(EditBarangActivity.this);
        builder.setTitle("Tentukan Pilihan Anda");
        builder.setItems(dialogItem, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                switch (i) {
                    case 0:
                        edtKondisi.setText("NEW");
                        break;
                    case 1:
                        edtKondisi.setText("SECOND");
                        break;
                }
            }
        });
        builder.create().show();
    }

    private void edit() {
        if (nama1 != null) {
            listNamaGambar.add(nama1);
        }
        if (nama2 != null) {
            listNamaGambar.add(nama2);
        }
        if (nama3 != null) {
            listNamaGambar.add(nama3);
        }

        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Proses ...");
        pd.setCancelable(false);
        pd.show();

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.editBarang(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                listBarang.getIDBARANG(),
                "" + idKategori,
                "",
                edtNama.getText().toString(),
                edtDeskripsi.getText().toString(),
                edtStok.getText().toString(),
                edtBerat.getText().toString(),
                edtKondisi.getText().toString(),
                listNamaGambar,
                edtHarga.getText().toString(),
                edtSku.getText().toString()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String kode = jsonObject.optString("success");
                        String pesan = jsonObject.optString("message");
                        if (kode.equals("1")) {
                            Toast.makeText(EditBarangActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(EditBarangActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(EditBarangActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getHargaPin() {
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getHargaPin(sharedPref.getIdUser(),
                sharedPref.getSESSION()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        JSONArray jsonArray = new JSONArray(response.body().string());
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.optJSONObject(i);
                            String harga = jsonObject.optString("PP_PRICE");

                            listHarga.add(harga);
                            ArrayAdapter<String> adp2 = new ArrayAdapter<String>(EditBarangActivity.this, R.layout.support_simple_spinner_dropdown_item, listHarga);
                            spnHargaPin.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
                            spnHargaPin.setAdapter(adp2);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(EditBarangActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getKategori() {
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getAllSubKat().enqueue(new Callback<ArrayList<SubKategoriModel>>() {
            @Override
            public void onResponse(Call<ArrayList<SubKategoriModel>> call, Response<ArrayList<SubKategoriModel>> response) {
                if (response.isSuccessful()) {
                    list = response.body();

                    for (int i = 0; i < list.size(); i++) {
                        listKategori.add(list.get(i).getSKNAME());
                        idListKategori.add(list.get(i).getIDSUBKATEGORI());
                    }

                    ArrayAdapter<String> adp2 = new ArrayAdapter<String>(EditBarangActivity.this, R.layout.support_simple_spinner_dropdown_item, listKategori);
                    spnKategori.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
                    spnKategori.setAdapter(adp2);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<SubKategoriModel>> call, Throwable t) {
                Toast.makeText(EditBarangActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void uploudImage(final String data, final File file) {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Mohon Tunggu ....");
        pd.setCancelable(false);
        pd.show();

        File f = new File(imageFileSatu.getPath());

        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), f);

        final MultipartBody.Part part = MultipartBody.Part.createFormData("image", f.getName(), requestFile);

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.editImageBarang(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                part).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                pd.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String kode = jsonObject.optString("success");
                    String pesan = jsonObject.optString("message");
                    String nama = jsonObject.optString("nama_file");
                    if (kode.equals("1")) {
                        Toast.makeText(EditBarangActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                        if (data.equals("satu")) {
                            nama1 = nama;
                            Glide.with(EditBarangActivity.this)
                                    .load(file.getPath())
                                    .into(imgProduk);
                        } else if (data.equals("dua")) {
                            nama2 = nama;
                            Glide.with(EditBarangActivity.this)
                                    .load(file.getPath())
                                    .into(imgProduk2);
                        } else {
                            nama3 = nama;
                            Glide.with(EditBarangActivity.this)
                                    .load(file.getPath())
                                    .into(imgProduk3);
                        }
                    } else {
                        Toast.makeText(EditBarangActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(EditBarangActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                //Some error handling
                Toast.makeText(EditBarangActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onImagePicked(File image, EasyImage.ImageSource source, int type) {
                if (pilih.equals("satu")) {

                    imageFileSatu = new File(image.getPath());
                    uploudImage(pilih, imageFileSatu);


                } else if (pilih.equals("dua")) {

                    imageFileSatu = new File(image.getPath());
                    uploudImage(pilih, imageFileSatu);


                } else if (pilih.equals("tiga")) {

                    imageFileSatu = new File(image.getPath());
                    uploudImage(pilih, imageFileSatu);


                }
            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
                super.onCanceled(source, type);
            }
        });
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PermUtil.REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Pix.start(EditBarangActivity.this, 100);
                } else {
                    Toast.makeText(EditBarangActivity.this, "Approve permissions to open Pix ImagePicker", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

    private void initView() {
        edtNama = (EditText) findViewById(R.id.edt_nama);
        spnKategori = (Spinner) findViewById(R.id.spn_kategori);
        spnHargaPin = (Spinner) findViewById(R.id.spn_harga_pin);
        imgProduk = (ImageView) findViewById(R.id.img_produk);
        imgProduk2 = (ImageView) findViewById(R.id.img_produk2);
        imgProduk3 = (ImageView) findViewById(R.id.img_produk3);
        edtHarga = (EditText) findViewById(R.id.edt_harga);
        edtDeskripsi = (EditText) findViewById(R.id.edt_deskripsi);
        edtSku = (EditText) findViewById(R.id.edt_sku);
        spnPromo = (Spinner) findViewById(R.id.spn_promo);
        edtStok = (EditText) findViewById(R.id.edt_stok);
        edtBerat = (EditText) findViewById(R.id.edt_berat);
        edtKondisi = (EditText) findViewById(R.id.edt_kondisi);
        btnTambah = (Button) findViewById(R.id.btn_tambah);
        btnHapus1 = (TextView) findViewById(R.id.btn_hapus1);
        btnHapus2 = (TextView) findViewById(R.id.btn_hapus2);
        btnHapus3 = (TextView) findViewById(R.id.btn_hapus3);
    }
}
