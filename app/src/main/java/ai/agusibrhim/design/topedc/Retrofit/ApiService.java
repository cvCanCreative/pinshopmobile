package ai.agusibrhim.design.topedc.Retrofit;

import java.util.ArrayList;
import java.util.List;

import ai.agusibrhim.design.topedc.Model.AlamatModel;
import ai.agusibrhim.design.topedc.Model.BarangTokoModel;
import ai.agusibrhim.design.topedc.Model.DetailTransaksiModel;
import ai.agusibrhim.design.topedc.Model.FavoritModel;
import ai.agusibrhim.design.topedc.Model.FeedbackModel;
import ai.agusibrhim.design.topedc.Model.HelpModel;
import ai.agusibrhim.design.topedc.Model.HistoryPembayaran;
import ai.agusibrhim.design.topedc.Model.HistorySaldo;
import ai.agusibrhim.design.topedc.Model.HistoryTransaksi;
import ai.agusibrhim.design.topedc.Model.IsiChatModel;
import ai.agusibrhim.design.topedc.Model.ListCategory;
import ai.agusibrhim.design.topedc.Model.ListChatModel;
import ai.agusibrhim.design.topedc.Model.ListProduct;
import ai.agusibrhim.design.topedc.Model.MemberTokoModel;
import ai.agusibrhim.design.topedc.Model.MemberUserModel;
import ai.agusibrhim.design.topedc.Model.NotifModel;
import ai.agusibrhim.design.topedc.Model.PromoAktif;
import ai.agusibrhim.design.topedc.Model.PromoModel;
import ai.agusibrhim.design.topedc.Model.PromoTokoModel;
import ai.agusibrhim.design.topedc.Model.RekeningByIdModel;
import ai.agusibrhim.design.topedc.Model.RekeningModel;
import ai.agusibrhim.design.topedc.Model.RequestTokoDetailModel;
import ai.agusibrhim.design.topedc.Model.RequestTokoModel;
import ai.agusibrhim.design.topedc.Model.ResponModel;
import ai.agusibrhim.design.topedc.Model.ResponseModel;
import ai.agusibrhim.design.topedc.Model.SubKategoriModel;
import ai.agusibrhim.design.topedc.Model.VoucherModel;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ApiService {
    @FormUrlEncoded
//    @POST("register_user.php")
    @POST("register_user.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> register(
//            @Query("api") String api,
            @Field("username") String username,
            @Field("password") String password,
            @Field("email") String email,
            @Field("telp") String tlp,
            @Field("birth") String ultah,
            @Field("referral") String referal);

    @FormUrlEncoded
    @POST("login_user.php")
    Call<ResponseBody> login(
            @Query("api") String api,
            @Field("username") String username,
            @Field("password") String password);

    @FormUrlEncoded
    @POST("update_profile.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> editProfil(
            @Query("id_user") String id,
            @Query("session") String session,
            @Field("nama") String nama,
            @Field("gender") String gender,
            @Field("alamat") String alamat,
            @Field("birth") String ttl,
            @Field("telp") String tlp);

    @FormUrlEncoded
    @POST("update_euser.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> updateGmail(
            @Query("id_user") String id,
            @Query("session") String session,
            @Field("username") String username,
            @Field("password") String password,
            @Field("referral") String referral,
            @Field("telp") String tlp);

    @FormUrlEncoded
    @POST("add_toko.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> tambahToko(@Query("id_user") String id,
                                  @Query("session") String session,
                                  @Field("address") String alamat,
                                  @Field("deskripsi") String deskripsi,
                                  @Field("toko") String nama_toko,
                                  @Field("id_prov") String id_prov,
                                  @Field("id_kota") String id_kota,
                                  @Field("nama_kota") String nama_kota,
                                  @Field("nama_kecamatan") String nama_kec,
                                  @Field("id_kecamatan") String id_kec);

    @Multipart
    @POST("add_barang.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> tambahBarang(@Query("id_user") String id,
                                    @Query("session") String session,
                                    @Part("id_sub_kategori") RequestBody id_kategori,
//                                    @Part("id_promo") RequestBody id_promo,
                                    @Part("names") RequestBody nama,
                                    @Part("myprice") RequestBody harga,
//                                    @Part("pin_price") RequestBody pin_harga,
                                    @Part("sku") RequestBody sku,
                                    @Part("description") RequestBody deskripsi,
                                    @Part("stock") RequestBody stok,
                                    @Part("weight") RequestBody berat,
                                    @Part("condition") RequestBody kondisi,
                                    @Part MultipartBody.Part[] file);

    @FormUrlEncoded
    @POST("update_password.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> gantiPassword(@Query("id_user") String id,
                                     @Query("session") String session,
                                     @Field("password_old") String lama,
                                     @Field("password_new") String baru);

    @FormUrlEncoded
    @POST("edit_barang.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> editBarang(@Query("id_user") String id,
                                  @Query("session") String session,
                                  @Field("id_barang") String idbarang,
                                  @Field("id_sub_kategori") String id_kategori,
                                  @Field("id_promo") String id_promo,
                                  @Field("names") String nama,
                                  @Field("description") String desskripsi,
                                  @Field("stock") String stok,
                                  @Field("weight") String weight,
                                  @Field("condition") String condition,
                                  @Field("image_name[]") List<String> image,
                                  @Field("price") String price,
                                  @Field("sku") String sku);

    @Multipart
    @POST("edit_barang_uploadfoto.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> editImageBarang(
            @Query("id_user") String id,
            @Query("session") String session,
            @Part MultipartBody.Part file);

    @FormUrlEncoded
    @POST("delete_promo.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> deletePromo(@Query("id_user") String id,
                                   @Query("session") String session,
                                   @Field("id_promo") String id_promo);


    @Multipart
    @POST("update_pfoto.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> gantiFotoProfil(@Query("id_user") String id,
                                       @Query("session") String session,
                                       @Part MultipartBody.Part file);

    @Multipart
    @POST("update_tfoto.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> gantiFotoToko(@Query("id_user") String id,
                                     @Query("session") String session,
                                     @Part MultipartBody.Part file);

    @FormUrlEncoded
    @POST("topup_saldo.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> topupSaldo(
            @Query("id_user") String id,
            @Query("session") String session,
            @Field("rekening") String rekening,
            @Field("atas_nama") String nama,
            @Field("nominal") String nominal,
            @Field("bank") String bank);

    @FormUrlEncoded
    @POST("add_promo.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> addPromo(
            @Query("id_user") String id,
            @Query("session") String session,
            @Field("name") String name,
            @Field("code") String code,
            @Field("potongan") String potongan,
            @Field("expired") String expired,
            @Field("min_pot") String min,
            @Field("member") String member);

    @FormUrlEncoded
    @POST("edit_promo.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> editPromo(
            @Query("id_user") String id,
            @Query("session") String session,
            @Query("id_promo") String idPromo,
            @Field("name") String name,
            @Field("code") String code,
            @Field("potongan") String potongan,
            @Field("expired") String expired,
            @Field("min_pot") String min,
            @Field("member") String member);

    @Multipart
    @POST("topup_saldo_bukti.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> topupSaldoBukti(
            @Query("id_user") String id,
            @Query("session") String session,
            @Part("id_history") RequestBody bank,
            @Part MultipartBody.Part file);

    @Multipart
    @POST("add_bukti_transfer.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> buktiTransfer(
            @Query("id_user") String id,
            @Query("session") String session,
            @Part("rekening") RequestBody rekening,
            @Part("nama") RequestBody nama,
            @Part("bank") RequestBody bank,
            @Part("jumlah_tf") RequestBody tf,
            @Part("kode_payment") RequestBody kode,
            @Part MultipartBody.Part file);

    @Multipart
    @POST("add_keluhan.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> addKeluhan(
            @Query("id_user") String id,
            @Query("session") String session,
            @Part("judul") RequestBody judul,
            @Part("detail") RequestBody detail,
            @Part MultipartBody.Part[] file);

    @Multipart
    @POST("withdraw_saldo.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> tarikSaldo(
            @Query("id_user") String id,
            @Query("session") String session,
            @Part("id_rekening") RequestBody id_rekening,
            @Part("nominal") RequestBody nominal,
            @Part MultipartBody.Part file,
            @Part MultipartBody.Part file2);

    @Multipart
    @POST("add_chat.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> kirimPesan(
            @Query("id_user") String id,
            @Query("session") String session,
            @Part("user_to") RequestBody user_to,
            @Part("lastview_usr_from") RequestBody last_view,
//            @Part MultipartBody.Part file,
//            @Part MultipartBody.Part file2,
            @Part("message") RequestBody pesan);

    @Multipart
    @POST("add_chat.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> kirimPesanAdaImage(
            @Query("id_user") String id,
            @Query("session") String session,
            @Part("user_to") RequestBody user_to,
            @Part("lastview_usr_from") RequestBody last_view,
            @Part MultipartBody.Part file,
//            @Part MultipartBody.Part file2,
            @Part("message") RequestBody pesan);

    @FormUrlEncoded
    @POST("get_chat.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<IsiChatModel>> isiChat(@Query("id_user") String id,
                                          @Query("session") String sessionr,
                                          @Field("user_to") String user_to);

    @FormUrlEncoded
    @POST("get_feedback_byBarang.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<FeedbackModel>> getFeedback(@Query("id_user") String id,
                                               @Query("session") String sessionr,
                                               @Field("id_barang") String id_barang);

    @FormUrlEncoded
    @POST("transaksi_selesai.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> terimaPesanan(@Query("id_user") String id,
                                     @Query("session") String sessionr,
                                     @Field("id_trx") String id_trans,
                                     @Field("status") String status);

    @Multipart
    @POST("add_komplain.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> tambahKomplain(@Query("id_user") String id,
                                      @Query("session") String sessionr,
                                      @Part("id_trx") RequestBody id_trans,
                                      @Part("detail") RequestBody status,
                                      @Part MultipartBody.Part file);


    @FormUrlEncoded
    @POST("get_request_toko_detail.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<RequestTokoDetailModel>> getRequestTokoDetail(@Query("id_user") String id,
                                                                 @Query("session") String sessionr,
                                                                 @Field("id_trx") String id_trans);

    @FormUrlEncoded
    @POST("forgot_pass.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> lupa_password(@Field("user_email") String email);

    @GET("get_kategori_barang.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<ListCategory>> getDataCatagory();

    @GET("get_profile_byid.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> getProfilById(@Query("id_toko") String id_toko);

    @GET("get_stat_member.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> getStatusMember(@Query("id_user") String id,
                                       @Query("session") String session,
                                       @Query("id_toko") String id_toko);

    @GET("get_request_toko.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<RequestTokoModel>> getRequestToko(@Query("id_user") String id,
                                                     @Query("session") String session);

    @GET("get_promo_byid.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<PromoAktif>> getPromoToko(@Query("id_user") String id,
                                             @Query("session") String session);

    @GET("get_skat_byid.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<SubKategoriModel>> getSubKategori(@Query("id_kategori") String id_kategori);

    @GET("get_pin_price.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> getHargaPin(@Query("id_user") String id,
                                   @Query("session") String session);

    @GET("get_list_subkategori.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<SubKategoriModel>> getAllSubKat();


    @FormUrlEncoded
    @POST("get_barang_bysearch.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<ListProduct>> searchAll(@Query("id_user") String id,
                                           @Query("session") String sessionr,
                                           @Field("search") String search,
                                           @Query("page") int page);

    @GET("get_baranghrg_byKategori_asc.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<ListProduct>> filterBarangKat(@Query("id_sub_kategori") String id,
                                                 @Query("min") String min,
                                                 @Query("max") String max,
                                                 @Query("page") int page);

    @GET("get_barangkat_bykota.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<ListProduct>> filterKotaBarangKat(@Query("id_sub_kategori") String id,
                                                     @Query("id_kota") String id_kota,
                                                     @Query("page") int page);

    @FormUrlEncoded
    @POST("get_barangkatkot_bysearch.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<ListProduct>> filterKotaBarangKatSearch(@Query("id_sub_kategori") String id,
                                                           @Query("id_kota") String id_kota,
                                                           @Field("search") String search,
                                                           @Query("page") int page);

    @FormUrlEncoded
    @POST("get_barangkathrg_bysearch_asc.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<ListProduct>> filterMinMaxSearchKat(@Query("id_sub_kategori") String id,
                                                       @Query("min") String min,
                                                       @Query("max") String id_max,
                                                       @Field("search") String search,
                                                       @Query("page") int page);

    @FormUrlEncoded
    @POST("get_baranghrg_bysearch_asc.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<ListProduct>> filterMinMaxSearchAll(@Query("min") String min,
                                                       @Query("max") String id_max,
                                                       @Field("search") String search,
                                                       @Query("page") int page);

    @FormUrlEncoded
    @POST("get_tbarangharg_bysearch_asc.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<BarangTokoModel>> filterMinMaxSearchToko(@Query("id_toko") String id,
                                                            @Query("min") String min,
                                                            @Query("max") String id_max,
                                                            @Field("search") String search);

    @GET("get_tbarang_byharga_asc.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<BarangTokoModel>> filterMinMaxToko(@Query("id_toko") String id,
                                                      @Query("min") String min,
                                                      @Query("max") String id_max);

    @FormUrlEncoded
    @POST("delete_chat.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> deleteChat(@Query("id_user") String id,
                                  @Query("session") String session,
                                  @Field("id_chat") String id_chat);

    @FormUrlEncoded
    @POST("delete_gambar.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> deleteGambar(@Query("id_user") String id,
                                    @Query("session") String session,
                                    @Field("nama_foto") String id_chat);


    @FormUrlEncoded
    @POST("get_barangkon_bysearch.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<ListProduct>> searchAllFilter(@Query("id_user") String id,
                                                 @Query("session") String sessionr,
                                                 @Query("kondisi") String kondisi,
                                                 @Field("search") String search,
                                                 @Query("page") int page);

    @FormUrlEncoded
    @POST("get_tbarangkon_bysearch.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<BarangTokoModel>> searchBarangTokoFilter(@Query("id_user") String id,
                                                            @Query("session") String sessionr,
                                                            @Query("kondisi") String kondisi,
                                                            @Query("id_toko") String id_toko,
                                                            @Field("search") String search,
                                                            @Query("page") int page);

    @FormUrlEncoded
    @POST("get_barangkatkon_bysearch.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<ListProduct>> searchKategoriFilter(@Query("id_user") String id,
                                                      @Query("session") String session,
                                                      @Query("id_sub_kategori") String id_sub_kategori,
                                                      @Query("kondisi") String kondisi,
                                                      @Field("search") String search,
                                                      @Query("page") int page);

    @FormUrlEncoded
    @POST("delete_barang.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> hapusBarang(@Query("id_user") String id,
                                   @Query("session") String sessionr,
                                   @Field("id_barang") String id_barang);

    @FormUrlEncoded
    @POST("delete_rekening.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseModel> hapusRekening(@Query("id_user") String id,
                                      @Query("session") String sessionr,
                                      @Field("id_rekening") String id_rekening);

    @FormUrlEncoded
    @POST("edit_rekening.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseModel> editRekening(@Query("id_user") String id,
                                     @Query("session") String sessionr,
                                     @Query("id_rekening") String id_rekening,
                                     @Field("id_bank") String bank,
                                     @Field("nomor") String nomor,
                                     @Field("name") String name);

    @FormUrlEncoded
    @POST("cek_email.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> cekEmail(@Field("email") String email,
                                @Field("telp") String tlp);

    @FormUrlEncoded
    @POST("acc_member.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> accMember(@Query("id_user") String id,
                                 @Query("session") String sessionr,
                                 @Field("id_member") String idMember);

    @FormUrlEncoded
    @POST("tolak_member.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> tolakMember(@Query("id_user") String id,
                                   @Query("session") String sessionr,
                                   @Query("jenis") String jenis,
                                   @Field("id_member") String idMember);

    @FormUrlEncoded
    @POST("tolak_member.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> hapusMember(@Query("id_user") String id,
                                   @Query("session") String sessionr,
                                   @Field("id_member") String idMember);

    @FormUrlEncoded
    @POST("get_barangkat_bysearch.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<ListProduct>> searchPerKategori(@Query("id_user") String id,
                                                   @Query("session") String sessionr,
                                                   @Query("id_sub_kategori") String id_kat,
                                                   @Query("page") int page,
                                                   @Field("search") String search);

    @FormUrlEncoded
    @POST("get_tbarang_bysearch.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<BarangTokoModel>> searchBarangAllToko(@Query("id_user") String id,
                                                         @Query("session") String sessionr,
                                                         @Query("id_toko") String id_toko,
                                                         @Field("search") String search);


    @GET("get_tbarang_byid.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<BarangTokoModel>> getBarangByIdToko(@Query("id_toko") String id_toko);


    @GET("get_about.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> getAbout();

    @GET("get_help.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<HelpModel>> gethelp();

    @GET("cek_token.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> getToken(@Query("id_user") String id,
                                @Query("session") String session);

    @GET("get_stat_barang.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> getStatusFavorit(@Query("id_user") String id,
                                        @Query("session") String session,
                                        @Query("id_barang") String id_barang);


    @GET("get_toko_byid.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<MemberTokoModel>> tokoYangDiikuti(@Query("id_user") String id,
                                                     @Query("session") String session,
                                                     @Query("id_toko") String id_toko);

    @GET("get_member_byid.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<MemberUserModel>> orangYangMengikutiKita(@Query("id_user") String id,
                                                            @Query("session") String session,
                                                            @Query("id_toko") String id_toko);

    @GET("get_member_waitingacc.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<MemberUserModel>> getCalonLangganan(@Query("id_user") String id,
                                                       @Query("session") String session,
                                                       @Query("id_toko") String id_toko);


    @GET("get_barang_byKategori.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<List<ListProduct>> getProduct(@Query("id_kategori") String id);

    @GET("get_barang_byKategori.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<ListProduct>> getProductByCatagori(@Query("id_sub_kategori") String id,
                                                      @Query("page") int page);

    @GET("get_barang_byid.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<BarangTokoModel>> getBarangToko(@Query("id_user") String id,
                                                   @Query("session") String session);


    //            (folder foto slider : ../images/img_slider/)
    @GET("get_slider.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<PromoModel>> getPromo();

    @GET("get_saldo.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> getSlado(@Query("id_user") String id,
                                @Query("session") String session);

    @GET("get_inbox_byid.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<NotifModel>> getNotif(@Query("id_user") String id,
                                         @Query("session") String session);

    @GET("get_promo_bytoko.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<PromoAktif>> getPromoAktif(@Query("id_toko") String id_toko);


    @GET("get_profile.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> getProfil(@Query("id_user") String id,
                                 @Query("session") String session);

    @GET("get_history_saldo.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<HistorySaldo>> getHistorySaldo(@Query("id_user") String id,
                                                  @Query("session") String session);

    @GET("get_history_saldo_wd.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<HistorySaldo>> getHistoryWd(@Query("id_user") String id,
                                               @Query("session") String session);

    @GET("get_history_saldo_topup.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<HistorySaldo>> getHistoryTopup(@Query("id_user") String id,
                                                  @Query("session") String session);

    @GET("get_inbox_byid.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<HistoryPembayaran>> getHistoryPembayaran(@Query("id_user") String id,
                                                            @Query("session") String session);

    @GET("get_wishlist_barangbyid.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<FavoritModel>> getFavorit(@Query("id_user") String id,
                                             @Query("session") String session);

    @FormUrlEncoded
    @POST("get_tstat_byid.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseModel> HistoryTransByKategori(@Query("id_user") String id,
                                               @Query("session") String session,
                                               @Field("status") String status);

    @FormUrlEncoded
    @POST("get_tstat_byid.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> HistoryTransByKategori2(@Query("id_user") String id,
                                               @Query("session") String session,
                                               @Field("status") String status);

    @FormUrlEncoded
    @POST("get_tstat_shop_byid.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> HistoryTransToko(@Query("id_user") String id,
                                        @Query("session") String session,
                                        @Field("status") String status);

    @GET("get_chat_list.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<ListChatModel>> getListChat(@Query("id_user") String id,
                                               @Query("session") String session);


    @FormUrlEncoded
    @POST("add_wishlist.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> addWishList(@Query("id_user") String id,
                                   @Query("session") String session,
                                   @Field("id_barang") String id_barang);

    @FormUrlEncoded
    @POST("check_promo_shop.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<VoucherModel>> cekVoucher(@Query("id_user") String id,
                                             @Query("session") String session,
                                             @Field("kode_promo") String kode_promo);

    @FormUrlEncoded
    @POST("check_promo_shop.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> cekVoucher2(@Query("id_user") String id,
                                   @Query("session") String session,
                                   @Field("id_barang[]") List<String> id_barang,
                                   @Field("kode_promo") String kode_promo,
                                   @Field("total_harga") String total);

    @FormUrlEncoded
    @POST("get_promo_bybarang.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<PromoTokoModel>> daftarPromo(@Query("id_user") String id,
                                                @Query("session") String session,
                                                @Field("id_barang[]") List<String> kode_promo);


    @FormUrlEncoded
    @POST("RajaOngkir/get_tracking.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> getPengiriman(@Query("id_user") String id,
                                     @Query("session") String session,
                                     @Field("waybill") String waybill,
                                     @Field("courier") String courier);

    @FormUrlEncoded
    @POST("add_feedback.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> addRating(@Query("id_user") String id,
                                 @Query("session") String session,
                                 @Field("id_trx_detail") String id_trans,
                                 @Field("rating") String rating,
                                 @Field("comment") String komen);


    @FormUrlEncoded
    @POST("delete_wishlist.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> hapusWishList(@Query("id_user") String id,
                                     @Query("session") String session,
                                     @Field("id_barang") String id_wish);

    @FormUrlEncoded
    @POST("get_transaksi_detail_byid.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<DetailTransaksiModel>> getDetailTransaksi(@Query("id_user") String id,
                                                             @Query("session") String session,
                                                             @Field("id_trx") String id_trx);

    @FormUrlEncoded
    @POST("get_transaksi_toko_detail_byid.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<DetailTransaksiModel>> getDetailTransaksiToko(@Query("id_user") String id,
                                                                 @Query("session") String session,
                                                                 @Field("id_trx") String id_trx);

    @GET("RajaOngkir/get_provinsi.php")
    Call<ResponseBody> getProvinsi();

    @FormUrlEncoded
    @POST("RajaOngkir/get_kota.php")
    Call<ResponseBody> getKota(@Field("prov") String prov);

    @FormUrlEncoded
    @POST("RajaOngkir/get_kecamatan.php")
    Call<ResponseBody> getKecamatan(@Query("id_user") String id,
                                    @Query("session") String session,
                                    @Field("kota") String prov);

    @FormUrlEncoded
    @POST("RajaOngkir/get_ongkir.php")
    Call<ResponseBody> getOngkir(@Field("originType") String origin_type,
                                 @Field("origin") String origin,
                                 @Field("destinationType") String destination_type,
                                 @Field("destination") String tujuan,
                                 @Field("weight") String berat,
                                 @Field("courier") String courier);

    @FormUrlEncoded
    @POST("transaksi_shop.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> bayarSatu(@Query("id_user") String id,
                                 @Query("session") String session,
                                 @Field("id_user_toko") String idToko,
                                 @Field("no_rek") String no_rek,
                                 @Field("nama_rek") String nama,
                                 @Field("bank") String bank,
                                 @Field("bank_pinshop") String bankP,
                                 @Field("rek_pinshop") String rekP,
                                 @Field("tf_pinshop") String tf,
                                 @Field("total_barang") String total_barang,
                                 @Field("metode_bayar") String metode,
                                 @Field("saldo_point") String saldo,
                                 @Field("total_harga") String totalHarga,
                                 @Field("kode_unik") String kode,
                                 @Field("total_ongkir") String ongkir,
                                 @Field("total_bayar") String totalBayar);


    @FormUrlEncoded
    @POST("transaksi_shop_detail.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> bayarDua(@Query("id_user") String id,
                                @Query("session") String session,
                                @Field("id_transaksi") String idTrans,
                                @Field("id_pengiriman") String id_peng,
                                @Field("id_barang") String id_barang,
                                @Field("qty") String qty,
                                @Field("catatan") String catatan,
                                @Field("kode_promo") String kode_promo,
                                @Field("kurir") String kurir,
                                @Field("jns_kurir") String jns_kurir,
                                @Field("ongkir") String ongkir,
                                @Field("harga_asli") String harga_asli,
                                @Field("harga_bayar") String harga_bayar);


    @GET("get_pengiriman_byid.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<AlamatModel>> getAlamatPengiriman(@Query("id_user") String id,
                                                     @Query("session") String session);

    @GET("get_rekening_admin.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseModel> getRekeningAdmin(@Query("id_user") String id,
                                         @Query("session") String session);

    @GET("get_rekening_byid.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseModel> getRekeningById(@Query("id_user") String id,
                                        @Query("session") String session);

    @GET("get_barangkat_bykondisi.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<ListProduct>> getBarangByKondisi(@Query("id_sub_kategori") String sub,
                                                    @Query("kondisi") String kondisi,
                                                    @Query("page") int page);

    @GET("get_tbarang_bykondisi.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ArrayList<BarangTokoModel>> getBarangTokoByKondisi(@Query("id_toko") String sub,
                                                            @Query("kondisi") String kondisi,
                                                            @Query("page") int page);

    @FormUrlEncoded
    @POST("delete_pengiriman.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> hapusAlamat(@Query("id_user") String id,
                                   @Query("session") String session,
                                   @Field("id_pengiriman") String idPengiriman);

    @FormUrlEncoded
    @POST("batal_trx.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseModel> batalTrans(@Query("id_user") String id,
                                   @Query("session") String session,
                                   @Field("kode_payment") String kode_payment);


    @FormUrlEncoded
    @POST("add_member.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> addMember(@Query("id_user") String id,
                                 @Query("session") String session,
                                 @Field("id_toko") String idToko);

    @FormUrlEncoded
    @POST("delete_member_byid.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> deleteMember(@Query("id_user") String id,
                                    @Query("session") String session,
                                    @Field("id_toko") String idToko);

    @FormUrlEncoded
    @POST("add_rekening.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseModel> addRekening(@Query("id_user") String id,
                                    @Query("session") String session,
                                    @Field("id_bank") String bank,
                                    @Field("nomor") String nomor,
                                    @Field("name") String name);


    @GET("get_bank.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseModel> getBank(@Query("id_user") String id,
                                @Query("session") String session);

    @FormUrlEncoded
    @POST("acc_shop.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> masukkanResi(@Query("id_user") String id,
                                    @Query("session") String session,
                                    @Field("id_trx") String id_trx,
                                    @Field("status") String status,
                                    @Field("resi") String resi);

    @FormUrlEncoded
    @POST("add_pengiriman.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> addAlamatPengiriman(@Query("id_user") String id,
                                           @Query("session") String session,
                                           @Field("judul") String judul,
                                           @Field("nama") String nama,
                                           @Field("alamat") String alamat,
                                           @Field("provinsi") String prov,
                                           @Field("kota") String kota,
                                           @Field("id_kota") String id_kota,
                                           @Field("kecamatan") String kecamatan,
                                           @Field("id_kecamatan") String id_kecamatan,
                                           @Field("kode_pos") String kode_pos,
                                           @Field("telp") String tlp);

    @FormUrlEncoded
    @POST("update_pengiriman.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> updateAlamatPengiriman(@Query("id_user") String id,
                                              @Query("session") String session,
                                              @Field("id_pengiriman") String idPeng,
                                              @Field("judul") String judul,
                                              @Field("nama") String nama,
                                              @Field("alamat") String alamat,
                                              @Field("provinsi") String prov,
                                              @Field("kota") String kota,
                                              @Field("id_kota") String id_kota,
                                              @Field("kecamatan") String kecamatan,
                                              @Field("id_kecamatan") String id_kecamatan,
                                              @Field("kode_pos") String kode_pos,
                                              @Field("telp") String tlp);


    @FormUrlEncoded
    @POST("transaksi_shop_newer.php?api=vxnoew9cs98dasydhcnoql0cancoid")
    Call<ResponseBody> pembayaranBarang(@Query("id_user") String id,
                                        @Query("session") String session,
                                        @Field("no_rek") String no_rek,
                                        @Field("nama_rek") String nama_rek,
                                        @Field("bank") String bank,
                                        @Field("bank_pinshop") String bank_pinshop,
                                        @Field("rek_pinshop") String rek_pinshop,
//                                        @Field("tf_pinshop") String tf_pinshop,
                                        @Field("metode_bayar") String metode_bayar,
                                        @Field("kode_promo") String kode_promo,
                                        @Field("saldo_point") String saldo_point,
                                        @Field("kode_unik") String kode_unik,
                                        @Field("kurir[]") List<String> kurir,
                                        @Field("jns_kurir[]") List<String> jns_kurir,
                                        @Field("ongkir[]") List<String> ongkir,
                                        @Field("id_user_toko[]") List<String> id_user_toko,
                                        @Field("id_penerima[]") List<String> id_penerima,
                                        @Field("id_barang[]") List<String> id_barang,
                                        @Field("qty[]") List<String> qty,
                                        @Field("catatan[]") List<String> catatan);
//                                        @Field("harga_barang[]") List<String> harga_barang);

}
