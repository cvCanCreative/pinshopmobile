package ai.agusibrhim.design.topedc.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CekPengirimanActivity extends AppCompatActivity {

    private TextView tvKurir;
    private TextView tvResi;
    private LinearLayout div;
    SharedPref sharedPref;
    private TextView tvKet;
    private ImageView imgClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cek_pengiriman);
        initView();

        awal();

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void awal() {
        sharedPref = new SharedPref(this);
        tvKurir.setText(getIntent().getStringExtra("kurir"));
        tvResi.setText(getIntent().getStringExtra("resi"));
        getData();

    }

    private void getData() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Loading ...");
        pd.setCancelable(false);
        pd.show();
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getPengiriman(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                "" + getIntent().getStringExtra("resi"),
                getIntent().getStringExtra("kurir")).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String jumlah = jsonObject.optString("tracked_count");
                        String kode = jsonObject.optString("success");

                        if (kode.equals("1")) {
                            if (jumlah.equals("0")) {
                                tvKet.setVisibility(View.VISIBLE);
                                div.setVisibility(View.GONE);
                            } else {
                                JSONArray jsonArray = jsonObject.optJSONArray("manifest");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject1 = jsonArray.optJSONObject(i);
                                    String alamat = jsonObject1.optString("manifest_description");
                                    String tgl = jsonObject1.optString("manifest_date");
                                    String waktu = jsonObject1.optString("manifest_time");
                                    LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                    View view = layoutInflater.inflate(R.layout.row_cek_pengiriman, null);

                                    TextView tvAlamat = view.findViewById(R.id.tv_alamat);
                                    TextView tvTanggal = view.findViewById(R.id.tv_tanggal);

                                    tvAlamat.setText(alamat);
                                    tvTanggal.setText(tgl + "\n" + waktu);

                                    div.addView(view);
                                }
                            }
                        } else {
                            tvKet.setVisibility(View.VISIBLE);
                            div.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(CekPengirimanActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initView() {
        tvKurir = (TextView) findViewById(R.id.tv_kurir);
        tvResi = (TextView) findViewById(R.id.tv_resi);
        div = (LinearLayout) findViewById(R.id.div);
        tvKet = (TextView) findViewById(R.id.tv_ket);
        imgClose = (ImageView) findViewById(R.id.img_close);
    }
}
