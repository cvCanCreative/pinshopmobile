package ai.agusibrhim.design.topedc.Adapter;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.solver.widgets.Helper;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import ai.agusibrhim.design.topedc.Helper.HelperClass;
import ai.agusibrhim.design.topedc.Model.PromoTokoModel;
import ai.agusibrhim.design.topedc.R;

import static android.content.Context.CLIPBOARD_SERVICE;

public class PromoTokoAdapter extends RecyclerView.Adapter<PromoTokoAdapter.Holder> {

    Context context;
    List<PromoTokoModel> list;


    public PromoTokoAdapter(Context context, List<PromoTokoModel> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_promo_toko, parent, false);

        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final Holder holder, int position) {
        holder.tvNama.setText(""+list.get(position).getPRNAME());
        holder.tvJenis.setText("Jenis Promo "+list.get(position).getPRJENIS().toLowerCase());
        holder.tvPotongan.setText(""+new HelperClass().convertRupiah(Integer.parseInt(list.get(position).getPRPOTONGAN())));
        if (list.get(position).getPRMIN().equals("0")){
            holder.tvKetentuan.setText("Tidak ada minimum transaksi");
        }else {
            holder.tvKetentuan.setText("Minimal transaksi "+new HelperClass().convertRupiah(Integer.parseInt(list.get(position).getPRMIN())));
        }

        holder.tvSalin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClipboardManager clipboard = (ClipboardManager) context.getSystemService(CLIPBOARD_SERVICE);
                String getstring = holder.tvKode.getText().toString();
                Toast.makeText(context, "Kode disalin", Toast.LENGTH_SHORT).show();
                ClipData clip = ClipData.newPlainText("", getstring);
                clipboard.setPrimaryClip(clip);
            }
        });
        holder.tvKode.setText(""+list.get(position).getPRCODE());
        holder.tvExpired.setText("Berlaku Sampai : " + new HelperClass().convertDateFormat(list.get(position).getPREXPIRED()));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        private TextView tvNama;
        private TextView tvJenis;
        private TextView tvPotongan;
        private TextView tvKetentuan;
        private TextView tvKode;
        private TextView tvSalin;
        private TextView tvExpired;
        public Holder(View itemView) {
            super(itemView);

            tvNama = (TextView) itemView.findViewById(R.id.tv_nama);
            tvJenis = (TextView) itemView.findViewById(R.id.tv_jenis);
            tvPotongan = (TextView) itemView.findViewById(R.id.tv_potongan);
            tvKetentuan = (TextView) itemView.findViewById(R.id.tv_ketentuan);
            tvKode = (TextView) itemView.findViewById(R.id.tv_kode);
            tvSalin = (TextView) itemView.findViewById(R.id.tv_salin);
            tvExpired = (TextView) itemView.findViewById(R.id.tv_expired);
        }
    }
}
