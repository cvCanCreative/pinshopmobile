package ai.agusibrhim.design.topedc.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Helper.Config;
import ai.agusibrhim.design.topedc.Helper.HelperClass;
import ai.agusibrhim.design.topedc.Model.RequestTokoDetailModel;
import ai.agusibrhim.design.topedc.R;

public class PesananTokoDetailAdapter extends RecyclerView.Adapter<PesananTokoDetailAdapter.Holder> {

    ArrayList<RequestTokoDetailModel> list;
    Context context;
    int totalHarga;



    public PesananTokoDetailAdapter(ArrayList<RequestTokoDetailModel> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_request_toko_detail, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        holder.tvNama.setText(list.get(position).getBANAME());
        holder.tvKodeTrans.setText(list.get(position).getTSKODETRX());
        holder.tvCatatan.setText(list.get(position).getTSDCATATAN());

        holder.tvAlamat.setText(list.get(position).getPEALAMAT() + "\n" + "Kode Pos: " + list.get(position).getPEKODEPOS() + "\n" +
                list.get(position).getPEKECAMATAN() + " " + list.get(position).getPEKOTA() + " " + list.get(position).getPEPROVINSI());
        holder.tvTlp.setText(list.get(position).getPETELP());

        holder.tvJmlBeli.setText("x " + list.get(position).getTSDQTY());

        holder.tvKurir.setText("" + list.get(position).getTSDKURIR().toUpperCase() + " " + list.get(position).getTSDJENISKURIR());
        String gambar = list.get(position).getBAIMAGE();
        String gb = new HelperClass().splitText(gambar);
        Glide.with(context)
                .load(Config.IMAGE_BARANG + gb)
                .into(holder.imgBarang);

        int qt = Integer.parseInt(list.get(position).getTSDQTY());
        int hargaBar = Integer.parseInt(list.get(position).getBAPRICE()) * qt;
        int kodUnik = Integer.parseInt(list.get(position).getTSKODEUNIK());

        int ongkirAsli = Integer.parseInt(list.get(position).getTSDONGKIR());

        totalHarga =  ongkirAsli + hargaBar + kodUnik;

        holder.tvKodeUnik.setText(new HelperClass().convertRupiah(kodUnik));

        holder.tvOngkir.setText(new HelperClass().convertRupiah(ongkirAsli));

        holder.tvHargaProduk.setText(new HelperClass().convertRupiah(hargaBar));
        holder.tvHargaBarang.setText(new HelperClass().convertRupiah(Integer.parseInt(list.get(position).getBAPRICE())));

        holder.tvTotalSemua.setText(new HelperClass().convertRupiah(totalHarga));

        holder.tvKodePromo.setText(list.get(position).getTSDKODEPROMO());
        holder.tvJenisPromo.setText(list.get(position).getTSJNSPROMO());
        holder.tvNilaiPromo.setText(new HelperClass().convertRupiah(Integer.parseInt(list.get(position).getTSPOTPROMO())));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }



    public class Holder extends RecyclerView.ViewHolder {

        TextView tvKodeTrans, tvCatatan, tvAlamat, tvTlp, tvNama;
        ImageView imgBarang;
        private TextView tvJmlBeli;
        private TextView tvHargaProduk;
        private TextView tvOngkir;
        private TextView tvTotalSemua;
        private TextView tvKurir;
        private TextView tvHargaBarang;
        private TextView tvJenisPromo;
        private TextView tvKodePromo;
        private TextView tvNilaiPromo;
        private TextView tvKodeUnik;

        public Holder(View itemView) {
            super(itemView);

            tvNama = itemView.findViewById(R.id.tv_nama_barang);
            imgBarang = itemView.findViewById(R.id.img_barang);
            tvKodeTrans = itemView.findViewById(R.id.tv_kode_trans);
            tvCatatan = itemView.findViewById(R.id.tv_catatan);

            tvAlamat = itemView.findViewById(R.id.tv_alamat);
            tvTlp = itemView.findViewById(R.id.tv_tlp);
            tvJmlBeli = (TextView) itemView.findViewById(R.id.tv_jml_beli);
            tvHargaProduk = (TextView) itemView.findViewById(R.id.tv_harga_produk);
            tvOngkir = (TextView) itemView.findViewById(R.id.tv_ongkir);
            tvTotalSemua = (TextView) itemView.findViewById(R.id.tv_total_semua);
            tvKurir = (TextView) itemView.findViewById(R.id.tv_kurir);

            tvHargaBarang = (TextView) itemView.findViewById(R.id.tv_harga_barang);
            tvJenisPromo = (TextView) itemView.findViewById(R.id.tv_jenis_promo);
            tvKodePromo = (TextView) itemView.findViewById(R.id.tv_kode_promo);
            tvNilaiPromo = (TextView) itemView.findViewById(R.id.tv_nilai_promo);
            tvKodeUnik = (TextView) itemView.findViewById(R.id.tv_kode_unik);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
        }
    }
}
