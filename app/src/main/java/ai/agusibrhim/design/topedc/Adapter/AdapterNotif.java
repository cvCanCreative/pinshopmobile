package ai.agusibrhim.design.topedc.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Model.NotifModel;
import ai.agusibrhim.design.topedc.R;

public class AdapterNotif extends RecyclerView.Adapter<AdapterNotif.Holder> {

    ArrayList<NotifModel> list;
    Context context;

    public AdapterNotif(ArrayList<NotifModel> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_notif,parent,false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        holder.tvTanggal.setText(""+list.get(position).getCREATEDAT());
        holder.tvTipe.setText(""+list.get(position).getTYPE());
        holder.tvJudul.setText(""+list.get(position).getTITLE());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Holder extends RecyclerView.ViewHolder{

        TextView tvJudul,tvTipe,tvTanggal;
        public Holder(View itemView) {
            super(itemView);

            tvJudul = itemView.findViewById(R.id.tv_judul);
            tvTipe = itemView.findViewById(R.id.tv_tipe);
            tvTanggal = itemView.findViewById(R.id.tv_tanggal);
        }
    }
}
