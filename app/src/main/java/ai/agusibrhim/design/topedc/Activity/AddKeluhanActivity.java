package ai.agusibrhim.design.topedc.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.fxn.pix.Pix;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.MainActivity;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddKeluhanActivity extends AppCompatActivity {

    private EditText edtJudul;
    private EditText edtDetail;

    private ImageView imgHasil;
    SharedPref sharedPref;
    List<String> imagesEncodedList;
    ArrayList<String> listFoto = new ArrayList<>();
    private String h, imagePath;
    List<File> imageFile = new ArrayList<>();
    File fileImages;
    File imageFileSatu, imageFileDua, imageFileTiga;
    private Button btnTambah;
    private ImageView imgHasil2;
    private ImageView imgHasil3;
    String pilih;

    private ImageView imgClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_keluhan);
        initView();

        sharedPref = new SharedPref(this);


        imgHasil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilih = "satu";
                EasyImage.openChooserWithGallery(AddKeluhanActivity.this, "chose", 3);
            }
        });

        imgHasil2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilih = "dua";
                EasyImage.openChooserWithGallery(AddKeluhanActivity.this, "chose", 3);
            }
        });

        imgHasil3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilih = "tiga";
                EasyImage.openChooserWithGallery(AddKeluhanActivity.this, "chose", 3);
            }
        });
        btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tambah();
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),HomeLikeShopeeActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("data","profil");
                finish();
            }
        });
    }

    private void tambah() {
        if (imageFileSatu != null) {
            imageFile.add(imageFileSatu);
        }
        if (imageFileDua != null) {
            imageFile.add(imageFileDua);
        }
        if (imageFileTiga != null) {
            imageFile.add(imageFileTiga);
        }

        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Proses ...");
        pd.setCancelable(false);
        pd.show();

        MultipartBody.Part[] surveyImagesParts = new MultipartBody.Part[imageFile.size()];
        for (int index = 0; index < imageFile.size(); index++) {

//            File file = new File(imageFile.get(index));
            RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), imageFile.get(index));
            surveyImagesParts[index] = MultipartBody.Part.createFormData("image[]", imageFile.get(index).getName(), surveyBody);
        }

        RequestBody judul = RequestBody.create(MediaType.parse("text/plain"), edtJudul.getText().toString());
        RequestBody detail = RequestBody.create(MediaType.parse("text/plain"), edtDetail.getText().toString());

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.addKeluhan(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                judul,
                detail,
                surveyImagesParts).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String pesan = jsonObject.optString("message");
                        if (pesan.equals("Berhasil menambahkan keluhan")) {
                            Toast.makeText(AddKeluhanActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                            edtDetail.setText("");
                            edtJudul.setText("");
                            edtJudul.requestFocus();
                            imgHasil.setImageResource(R.drawable.no_image_found);
                            imgHasil2.setImageResource(R.drawable.no_image_found);
                            imgHasil3.setImageResource(R.drawable.no_image_found);
                        } else {
                            Toast.makeText(AddKeluhanActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(AddKeluhanActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    //gallery
    private void showFileChooserGalerry() {
        Intent qq = new Intent(Intent.ACTION_PICK);
        qq.setType("image/*");
        startActivityForResult(Intent.createChooser(qq, "Pilih Foto"), 100);
//        Intent intent = new Intent();
//        intent.setType("image/*");
//        intent.setAction(Intent.ACTION_GET_CONTENT);
//        startActivityForResult(Intent.createChooser(intent, "Pilih Foto"), Config.PICK_FILE_REQUEST);
    }

    //TODO like WA
    /*
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == 100) {
            ArrayList<String> returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);
//            Toast.makeText(this, ""+returnValue.size(), Toast.LENGTH_SHORT).show();
            ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
            imagesEncodedList = new ArrayList<>();

            for (int i = 0; i < returnValue.size(); i++) {


//                imagePath = returnValue.get(i);
                imagesEncodedList.add(returnValue.get(i));
                listFoto.add(returnValue.get(i));
                Glide.with(this).load(new File(imagesEncodedList.get(0))).into(imgHasil);

                h = new File(imagesEncodedList.get(0)).getName();

                Toast.makeText(this, "" + imagesEncodedList.size(), Toast.LENGTH_SHORT).show();

                imageFile = imagesEncodedList;


            }
        } else {
            Toast.makeText(this, "Gambar Tidak Ada", Toast.LENGTH_SHORT).show();
        }
    }
    */

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                //Some error handling
                Toast.makeText(AddKeluhanActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onImagePicked(File image, EasyImage.ImageSource source, int type) {
                if (pilih.equals("satu")) {

                    Glide.with(AddKeluhanActivity.this)
                            .load(new File(image.getPath()))
                            .into(imgHasil);
                    imageFileSatu = new File(image.getPath());

                    try {
                        imageFileSatu = new Compressor(AddKeluhanActivity.this).compressToFile(imageFileSatu);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                } else if (pilih.equals("dua")) {

                    Glide.with(AddKeluhanActivity.this)
                            .load(new File(image.getPath()))
                            .into(imgHasil2);
                    imageFileDua = new File(image.getPath());

                    try {
                        imageFileDua = new Compressor(AddKeluhanActivity.this).compressToFile(imageFileDua);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                } else if (pilih.equals("tiga")) {

                    Glide.with(AddKeluhanActivity.this)
                            .load(new File(image.getPath()))
                            .into(imgHasil3);
                    imageFileTiga = new File(image.getPath());

                    try {
                        imageFileTiga = new Compressor(AddKeluhanActivity.this).compressToFile(imageFileTiga);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
                super.onCanceled(source, type);
            }
        });
    }

    private void initView() {
        edtJudul = (EditText) findViewById(R.id.edt_judul);
        edtDetail = (EditText) findViewById(R.id.edt_detail);

        imgHasil = (ImageView) findViewById(R.id.img_hasil);
        btnTambah = (Button) findViewById(R.id.btn_tambah);
        imgHasil2 = (ImageView) findViewById(R.id.img_hasil2);
        imgHasil3 = (ImageView) findViewById(R.id.img_hasil3);

        imgClose = (ImageView) findViewById(R.id.img_close);
    }
}
