package ai.agusibrhim.design.topedc.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.image.SmartImageView;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Activity.DetailActivity;
import ai.agusibrhim.design.topedc.Activity.DetailBarangTokoActivity;
import ai.agusibrhim.design.topedc.Activity.EditAlamatActivity;
import ai.agusibrhim.design.topedc.Activity.EditBarangActivity;
import ai.agusibrhim.design.topedc.Activity.HomeLikeShopeeActivity;
import ai.agusibrhim.design.topedc.Activity.TambahAlamatActivity;
import ai.agusibrhim.design.topedc.Helper.Config;
import ai.agusibrhim.design.topedc.Helper.HelperClass;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.MainActivity;
import ai.agusibrhim.design.topedc.Model.BarangTokoModel;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BarangTokoAdapter extends RecyclerView.Adapter<BarangTokoAdapter.Holder> {

    ArrayList<BarangTokoModel> list;
    Context context;
    SharedPref sharedPref;
    String data;

    public BarangTokoAdapter(ArrayList<BarangTokoModel> list, Context context, String data) {
        this.list = list;
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product,null,false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        holder.name.setText(list.get(position).getBANAME());

        String gambar = list.get(position).getBAIMAGE();

        String image = new HelperClass().splitText(gambar);

        Picasso.with(context)
                .load(Config.IMAGE_BARANG + image)
                .placeholder(R.drawable.image_loading)
                .error(R.drawable.no_image_found)
                .noFade()
                .into(holder.img);

        holder.store.setText(list.get(position).getUSDTOKO());
        holder.price.setText(""+new HelperClass().convertRupiah(Integer.valueOf(list.get(position).getBAPRICE())));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Holder extends RecyclerView.ViewHolder{
        TextView name, price, priceold, discount, store;
        SmartImageView img;

        public Holder(View itemView) {
            super(itemView);

            name=(TextView) itemView.findViewById(R.id.itemproductTextViewName);
            price=(TextView) itemView.findViewById(R.id.itemproductTextViewPrice);
            priceold=(TextView) itemView.findViewById(R.id.itemproductTextViewPold);
            discount=(TextView) itemView.findViewById(R.id.itemproductTextViewDisc);
            store=(TextView) itemView.findViewById(R.id.itemproductTextViewStore);
            img=(SmartImageView) itemView.findViewById(R.id.itemproductImageView1);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    if (data.equals("tamu")){

                    }else {
                        final CharSequence[] dialogItem = {"Hapus", "Edit","Lihat"};
                        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                        builder.setTitle("Tentukan Pilihan Anda");
                        builder.setItems(dialogItem, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                switch (i) {
                                    case 0:
                                        new AlertDialog.Builder(view.getContext())
                                                .setMessage("Apakah anda akan menghapus barang ini ?")
                                                .setCancelable(false)
                                                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        sharedPref = new SharedPref(view.getContext());
                                                        final ProgressDialog pd = new ProgressDialog(view.getContext());
                                                        pd.setCancelable(false);
                                                        pd.setTitle("Proses ...");
                                                        pd.show();
                                                        ApiService apiService = ApiConfig.getInstanceRetrofit();
                                                        apiService.hapusBarang(sharedPref.getIdUser(),
                                                                sharedPref.getSESSION(),
                                                                list.get(getAdapterPosition()).getIDBARANG()).enqueue(new Callback<ResponseBody>() {
                                                            @Override
                                                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                                                if (response.isSuccessful()){
                                                                    pd.dismiss();
                                                                    try {
                                                                        JSONObject jsonObject = new JSONObject(response.body().string());
                                                                        String pesan = jsonObject.optString("message");
                                                                        String sukses = jsonObject.optString("success");
                                                                        if (pesan.equals("Berhasil delete")){
                                                                            Toast.makeText(context, ""+pesan, Toast.LENGTH_SHORT).show();
                                                                            context.startActivity(new Intent(view.getContext(), HomeLikeShopeeActivity.class)
                                                                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                                                        }else {
                                                                            Toast.makeText(context, ""+pesan, Toast.LENGTH_SHORT).show();
                                                                        }

                                                                    } catch (JSONException e) {
                                                                        e.printStackTrace();
                                                                    } catch (IOException e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                            }

                                                            @Override
                                                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                                                pd.dismiss();
                                                                Toast.makeText(context, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                                                            }
                                                        });
                                                    }
                                                })
                                                .setNegativeButton("Tidak", null)
                                                .show();
                                        break;
                                    case 1:
                                        Intent intent = new Intent(context,EditBarangActivity.class);
                                        intent.putExtra("list",list.get(getAdapterPosition()));
                                        intent.putExtra("posisi",""+getAdapterPosition());
                                        context.startActivity(intent);
                                        break;
                                    case 2:
                                        Intent intent2 = new Intent(context, DetailBarangTokoActivity.class);
                                        intent2.putExtra("data", "lihat");
                                        intent2.putExtra("list", list.get(getAdapterPosition()));
                                        intent2.putExtra("posisi",""+getAdapterPosition());
                                        intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        context.startActivity(intent2);
                                        break;
                                }
                            }
                        });
                        builder.create().show();
                    }
                }
            });
        }
    }
}
