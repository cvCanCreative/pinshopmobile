package ai.agusibrhim.design.topedc.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Helper.Config;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.MainActivity;
import ai.agusibrhim.design.topedc.Model.RekeningByIdModel;
import ai.agusibrhim.design.topedc.Model.RekeningModel;
import ai.agusibrhim.design.topedc.Model.ResponseModel;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ListBankActivity extends AppCompatActivity {

    private LinearLayout div;
    private RelativeLayout lyBankLain;
    private ImageView image;
    private TextView tvLainnya;
    SharedPref sharedPref;
    ArrayList<RekeningByIdModel> list = new ArrayList<>();
    private SwipeRefreshLayout swipe;
    private ImageView imgClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_bank);
        initView();

        awal();

        lyBankLain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), TambahSaldoActivity.class);
                intent.putExtra("tipe", "bank_lain");
                intent.putParcelableArrayListExtra("list", list);
                intent.putExtra("posisi", "0");
                intent.putExtra("pilih","lain");
                startActivity(intent);
            }
        });

        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData(true);
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), HomeLikeShopeeActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();
            }
        });

    }

    private void awal() {
        sharedPref = new SharedPref(this);
        getData(true);
    }

    private void getData(boolean rm) {
        if (rm) {
            if (div.getChildCount() > 0) div.removeAllViews();
        }
        swipe.setRefreshing(true);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getRekeningAdmin(sharedPref.getIdUser(),
                sharedPref.getSESSION()).enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                if (response.isSuccessful()) {
                    swipe.setRefreshing(false);
                    list = response.body().rekening;

                    for (int i = 0; i < list.size(); i++) {
                        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View view = layoutInflater.inflate(R.layout.row_bank, null);

                        TextView nama_bank = view.findViewById(R.id.tv_nama_bank);
                        ImageView imageView = view.findViewById(R.id.image);
                        RelativeLayout ly = view.findViewById(R.id.ly_bank);

                        nama_bank.setText("" + list.get(i).RK_BANK);
                        Picasso.with(getApplicationContext())
                                .load(Config.IMAGE_REKENING + list.get(i).BK_IMAGE)
                                .into(imageView);

                        final String posisi = "" + i;
                        ly.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(getApplicationContext(), TambahSaldoActivity.class);
                                intent.putParcelableArrayListExtra("list", list);
                                intent.putExtra("posisi", posisi);
                                intent.putExtra("tipe", "data_list");
                                intent.putExtra("pilih","ada");
                                startActivity(intent);
                            }
                        });

                        div.addView(view);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                swipe.setRefreshing(false);
                Toast.makeText(ListBankActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initView() {
        div = (LinearLayout) findViewById(R.id.div);
        lyBankLain = (RelativeLayout) findViewById(R.id.ly_bank_lain);
        image = (ImageView) findViewById(R.id.image);
        tvLainnya = (TextView) findViewById(R.id.tv_lainnya);
        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe);
        imgClose = (ImageView) findViewById(R.id.img_close);
    }
}
