package ai.agusibrhim.design.topedc.Fragment;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Adapter.AdapterMemberToko;
import ai.agusibrhim.design.topedc.Adapter.AdapterMemberUser;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.Model.MemberTokoModel;
import ai.agusibrhim.design.topedc.Model.MemberUserModel;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class LanggananFragment extends Fragment {

    String data;
    SharedPref sharedPref;
    private SwipeRefreshLayout swipe;
    private RecyclerView rv;
    private TextView tvToko;
    private TextView tvUser;
    RecyclerView.LayoutManager layoutManager;
    AdapterMemberToko adapterToko;
    AdapterMemberUser adapterUser;
    ArrayList<MemberTokoModel> listToko = new ArrayList<>();
    ArrayList<MemberUserModel> listUser = new ArrayList<>();

    @SuppressLint("ValidFragment")
    public LanggananFragment(String data) {
        // Required empty public constructor
        this.data = data;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_langganan, container, false);
        initView(view);

        awal();
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (data.equals("toko")) {
                    getToko();
                } else {
                    getUser();
                }
            }
        });
        return view;
    }

    private void awal() {
        sharedPref = new SharedPref(getActivity());
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rv.setLayoutManager(layoutManager);

        if (data.equals("toko")) {
            getToko();
        } else {
            getUser();
        }
    }

    private void getToko() {
        swipe.setRefreshing(true);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.tokoYangDiikuti(sharedPref.getIdUser(),
                sharedPref.getSESSION(),""
                ).enqueue(new Callback<ArrayList<MemberTokoModel>>() {
            @Override
            public void onResponse(Call<ArrayList<MemberTokoModel>> call, Response<ArrayList<MemberTokoModel>> response) {
                if (response.isSuccessful()) {
                    swipe.setRefreshing(false);
                    listToko = response.body();
                    if (listToko.get(0).getMessage().equals("Berhasil Diload")) {
                        adapterToko = new AdapterMemberToko(listToko, getActivity());
                        rv.setAdapter(adapterToko);
                    } else {
                        tvToko.setVisibility(View.VISIBLE);
                        rv.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<MemberTokoModel>> call, Throwable t) {
                swipe.setRefreshing(false);
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getUser() {
        swipe.setRefreshing(true);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.orangYangMengikutiKita(sharedPref.getIdUser(),
                sharedPref.getSESSION(),"").enqueue(new Callback<ArrayList<MemberUserModel>>() {
            @Override
            public void onResponse(Call<ArrayList<MemberUserModel>> call, Response<ArrayList<MemberUserModel>> response) {
                if (response.isSuccessful()) {
                    swipe.setRefreshing(false);
                    listUser = response.body();
                    if (listUser.get(0).getMessage().equals("Berhasil Diload")) {
                        adapterUser = new AdapterMemberUser(listUser, getActivity(),"member");
                        rv.setAdapter(adapterUser);
                    } else {
                        tvUser.setVisibility(View.VISIBLE);
                        rv.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<MemberUserModel>> call, Throwable t) {
                swipe.setRefreshing(false);
                Toast.makeText(getActivity(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initView(View view) {
        swipe = (SwipeRefreshLayout) view.findViewById(R.id.swipe);
        rv = (RecyclerView) view.findViewById(R.id.rv);
        tvToko = (TextView) view.findViewById(R.id.tv_toko);
        tvUser = (TextView) view.findViewById(R.id.tv_user);
    }
}
