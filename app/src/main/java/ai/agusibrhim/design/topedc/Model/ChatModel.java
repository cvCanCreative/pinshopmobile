package ai.agusibrhim.design.topedc.Model;

public class ChatModel {
    private String id,text,nama,pengirim,penerima;
    private Long time;

    public ChatModel(String text, String nama, String pengirim, String penerima, Long time) {
        this.text = text;
        this.nama = nama;
        this.pengirim = pengirim;
        this.penerima = penerima;
        this.time = time;
    }

    public ChatModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getPengirim() {
        return pengirim;
    }

    public void setPengirim(String pengirim) {
        this.pengirim = pengirim;
    }

    public String getPenerima() {
        return penerima;
    }

    public void setPenerima(String penerima) {
        this.penerima = penerima;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }
}
