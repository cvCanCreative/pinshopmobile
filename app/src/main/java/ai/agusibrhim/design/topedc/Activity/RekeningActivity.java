package ai.agusibrhim.design.topedc.Activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Helper.HelperClass;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.Model.BankModel;
import ai.agusibrhim.design.topedc.Model.RekeningByIdModel;
import ai.agusibrhim.design.topedc.Model.ResponseModel;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RekeningActivity extends AppCompatActivity {


    SharedPref sharedPref;
    AlertDialog.Builder dialog;
    String nama, data;
    int posisi;
    RekeningByIdModel list;
    ArrayList<String> listNamaBank = new ArrayList<>();
    ArrayList<String> listIdBank = new ArrayList<>();
    ArrayList<BankModel> bankModels = new ArrayList<>();
    private ImageView imgClose;
    private Spinner spnBank;
    private EditText edtNoRek;
    private EditText edtNama;
    private Button btnTambah;
    String idBank;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rekening);
        initView();

        awal();

    }

    private void awal() {
        sharedPref = new SharedPref(this);

        data = getIntent().getStringExtra("data");
        if (data.equals("tambah")) {

            getBank();

        } else {

            list = getIntent().getParcelableExtra("list");

            btnTambah.setText("Edit");

            edtNoRek.setText(list.RK_NOMOR);
            edtNama.setText(list.RK_NAME);
            idBank = list.ID_BANK;

            getBank();
        }

        mainButton();
    }

    private void edit() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Proses ...");
        pd.setCancelable(false);
        pd.show();
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.editRekening(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                list.ID_REKENING,
                ""+idBank,
                edtNoRek.getText().toString(),
                edtNama.getText().toString()).enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                if (response.isSuccessful()){
                    pd.dismiss();
                    String pesan = response.body().getMessage();

                    if (response.body().getSuccess() == 1) {

                        startActivity(new Intent(getApplicationContext(), HomeLikeShopeeActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                        finish();
                        Toast.makeText(RekeningActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                    } else {

                        Toast.makeText(RekeningActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                    }

                }else {

                    pd.dismiss();
                    Toast.makeText(RekeningActivity.this, ""+response.message(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                pd.dismiss();
                new android.app.AlertDialog.Builder(RekeningActivity.this)
                        .setMessage("" + t.getMessage())
                        .setCancelable(false)
                        .setPositiveButton("Ulangi", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                edit();
                            }
                        })
                        .show();
            }
        });
    }

    private void tambah() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Proses ...");
        pd.setCancelable(false);
        pd.show();
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.addRekening(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                "" + idBank,
                edtNoRek.getText().toString(),
                edtNama.getText().toString()).enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();

                    if (response.body().getSuccess() == 1){

                        Toast.makeText(RekeningActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        startActivity(new Intent(getApplicationContext(), HomeLikeShopeeActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                        finish();
                    }
                }else {
                    pd.dismiss();
                    Toast.makeText(RekeningActivity.this, ""+response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                pd.dismiss();
                new android.app.AlertDialog.Builder(RekeningActivity.this)
                        .setMessage("" + t.getMessage())
                        .setCancelable(false)
                        .setPositiveButton("Ulangi", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                               tambah();
                            }
                        })
                        .show();

            }
        });
    }

    private void getBank() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Tunggu ....");
        pd.setCancelable(false);
        pd.show();

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getBank(sharedPref.getIdUser(),
                sharedPref.getSESSION()).enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    if (response.body().getSuccess() == 1) {
                        bankModels = response.body().bank;

                        for (int i = 0; i < bankModels.size(); i++) {

                            listNamaBank.add(bankModels.get(i).BK_NAME);
                            listIdBank.add(bankModels.get(i).ID_BANK);

                        }

                        setSpinner(listNamaBank);
                    } else {
                        Toast.makeText(RekeningActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                pd.dismiss();
                new android.app.AlertDialog.Builder(RekeningActivity.this)
                        .setMessage("" + t.getMessage())
                        .setCancelable(false)
                        .setPositiveButton("Ulangi", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                getBank();
                            }
                        })
                        .show();
            }
        });
    }

    private void mainButton() {
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(getApplicationContext(), LihatRekeningActivity.class)
//                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                finish();

                onBackPressed();
            }
        });
        btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (data.equals("tambah")) {
                    tambah();
                } else {
                    edit();
                }
            }
        });
    }

    private void setSpinner(final ArrayList<String> listNamaBank) {
        ArrayAdapter<String> adp2 = new ArrayAdapter<String>(RekeningActivity.this, R.layout.support_simple_spinner_dropdown_item, listNamaBank);
        spnBank.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
        spnBank.setAdapter(adp2);

        spnBank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int a = spnBank.getSelectedItemPosition();

                idBank = listIdBank.get(a);
//                Toast.makeText(RekeningActivity.this, ""+idBank, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


    private void initView() {

        imgClose = (ImageView) findViewById(R.id.img_close);
        spnBank = (Spinner) findViewById(R.id.spn_bank);
        edtNoRek = (EditText) findViewById(R.id.edt_no_rek);
        edtNama = (EditText) findViewById(R.id.edt_nama);
        btnTambah = (Button) findViewById(R.id.btn_tambah);
    }
}
