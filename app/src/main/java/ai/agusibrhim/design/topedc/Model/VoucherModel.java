package ai.agusibrhim.design.topedc.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VoucherModel implements Parcelable {
    @SerializedName("ID_BARANG")
    @Expose
    private String iDBARANG;
    @SerializedName("PR_JENIS")
    @Expose
    private String pRJENIS;
    @SerializedName("PR_POTONGAN")
    @Expose
    private String pRPOTONGAN;
    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("message")
    @Expose
    private String message;

    protected VoucherModel(Parcel in) {
        iDBARANG = in.readString();
        pRJENIS = in.readString();
        pRPOTONGAN = in.readString();
        success = in.readString();
        message = in.readString();
    }

    public static final Creator<VoucherModel> CREATOR = new Creator<VoucherModel>() {
        @Override
        public VoucherModel createFromParcel(Parcel in) {
            return new VoucherModel(in);
        }

        @Override
        public VoucherModel[] newArray(int size) {
            return new VoucherModel[size];
        }
    };

    public String getIDBARANG() {
        return iDBARANG;
    }

    public void setIDBARANG(String iDBARANG) {
        this.iDBARANG = iDBARANG;
    }

    public String getPRJENIS() {
        return pRJENIS;
    }

    public void setPRJENIS(String pRJENIS) {
        this.pRJENIS = pRJENIS;
    }

    public String getPRPOTONGAN() {
        return pRPOTONGAN;
    }

    public void setPRPOTONGAN(String pRPOTONGAN) {
        this.pRPOTONGAN = pRPOTONGAN;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(iDBARANG);
        parcel.writeString(pRJENIS);
        parcel.writeString(pRPOTONGAN);
        parcel.writeString(success);
        parcel.writeString(message);
    }
}
