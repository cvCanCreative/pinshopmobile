package ai.agusibrhim.design.topedc.Model;

public class ListCategory {

    String ID_KATEGORI_BARANG, KBA_IMAGE, CREATED_AT, UPDATED_AT, success, message;

    String KBA_NAME = "" + message;

    String url = "http://pinshop.can.co.id/images/img_kategori/";

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getID_KATEGORI_BARANG() {
        return ID_KATEGORI_BARANG;
    }

    public void setID_KATEGORI_BARANG(String ID_KATEGORI_BARANG) {
        this.ID_KATEGORI_BARANG = ID_KATEGORI_BARANG;
    }

    public String getKBA_IMAGE() {
        return KBA_IMAGE;
    }

    public void setKBA_IMAGE(String KBA_IMAGE) {
        this.KBA_IMAGE = KBA_IMAGE;
    }

    public String getKBA_NAME() {
        return KBA_NAME;
    }

    public void setKBA_NAME(String KBA_NAME) {
        this.KBA_NAME = KBA_NAME;
    }

    public String getCREATED_AT() {
        return CREATED_AT;
    }

    public void setCREATED_AT(String CREATED_AT) {
        this.CREATED_AT = CREATED_AT;
    }

    public String getUPDATED_AT() {
        return UPDATED_AT;
    }

    public void setUPDATED_AT(String UPDATED_AT) {
        this.UPDATED_AT = UPDATED_AT;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
