package ai.agusibrhim.design.topedc.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MemberTokoModel {

    @SerializedName("ID_MEMBER_TOKO")
    @Expose
    private String iDMEMBERTOKO;
    @SerializedName("ID_USER")
    @Expose
    private String iDUSER;
    @SerializedName("ID_TOKO")
    @Expose
    private String iDTOKO;
    @SerializedName("MT_STATUS")
    @Expose
    private String mTSTATUS;
    @SerializedName("CREATED_AT")
    @Expose
    private String cREATEDAT;
    @SerializedName("UPDATE_AT")
    @Expose
    private String uPDATEAT;
    @SerializedName("ID_USER_DETAIL")
    @Expose
    private String iDUSERDETAIL;
    @SerializedName("USD_FOTO")
    @Expose
    private String uSDFOTO;
    @SerializedName("USD_FULLNAME")
    @Expose
    private String uSDFULLNAME;
    @SerializedName("USD_BIRTH")
    @Expose
    private String uSDBIRTH;
    @SerializedName("USD_ADDRESS")
    @Expose
    private String uSDADDRESS;
    @SerializedName("USD_GENDER")
    @Expose
    private String uSDGENDER;
    @SerializedName("USD_TOKO")
    @Expose
    private String uSDTOKO;
    @SerializedName("USD_TOKO_FOTO")
    @Expose
    private String uSDTOKOFOTO;
    @SerializedName("USD_TOKO_DETAIL")
    @Expose
    private String uSDTOKODETAIL;
    @SerializedName("ID_TOKO_SKAT")
    @Expose
    private String iDTOKOSKAT;
    @SerializedName("USD_ID_PROV")
    @Expose
    private String uSDIDPROV;
    @SerializedName("USD_ID_KOTA")
    @Expose
    private String uSDIDKOTA;
    @SerializedName("USD_KOTA")
    @Expose
    private String uSDKOTA;
    @SerializedName("USD_ID_KECAMATAN")
    @Expose
    private String uSDIDKECAMATAN;
    @SerializedName("USD_KECAMATAN")
    @Expose
    private String uSDKECAMATAN;
    @SerializedName("USD_STATUS")
    @Expose
    private String uSDSTATUS;
    @SerializedName("USD_REFERRAL")
    @Expose
    private String uSDREFERRAL;
    @SerializedName("USD_REFERRAL_FROM")
    @Expose
    private String uSDREFERRALFROM;
    @SerializedName("UPDATED_AT")
    @Expose
    private String uPDATEDAT;
    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("message")
    @Expose
    private String message;

    public String getIDMEMBERTOKO() {
        return iDMEMBERTOKO;
    }

    public void setIDMEMBERTOKO(String iDMEMBERTOKO) {
        this.iDMEMBERTOKO = iDMEMBERTOKO;
    }

    public String getIDUSER() {
        return iDUSER;
    }

    public void setIDUSER(String iDUSER) {
        this.iDUSER = iDUSER;
    }

    public String getIDTOKO() {
        return iDTOKO;
    }

    public void setIDTOKO(String iDTOKO) {
        this.iDTOKO = iDTOKO;
    }

    public String getMTSTATUS() {
        return mTSTATUS;
    }

    public void setMTSTATUS(String mTSTATUS) {
        this.mTSTATUS = mTSTATUS;
    }

    public String getCREATEDAT() {
        return cREATEDAT;
    }

    public void setCREATEDAT(String cREATEDAT) {
        this.cREATEDAT = cREATEDAT;
    }

    public String getUPDATEAT() {
        return uPDATEAT;
    }

    public void setUPDATEAT(String uPDATEAT) {
        this.uPDATEAT = uPDATEAT;
    }

    public String getIDUSERDETAIL() {
        return iDUSERDETAIL;
    }

    public void setIDUSERDETAIL(String iDUSERDETAIL) {
        this.iDUSERDETAIL = iDUSERDETAIL;
    }

    public String getUSDFOTO() {
        return uSDFOTO;
    }

    public void setUSDFOTO(String uSDFOTO) {
        this.uSDFOTO = uSDFOTO;
    }

    public String getUSDFULLNAME() {
        return uSDFULLNAME;
    }

    public void setUSDFULLNAME(String uSDFULLNAME) {
        this.uSDFULLNAME = uSDFULLNAME;
    }

    public String getUSDBIRTH() {
        return uSDBIRTH;
    }

    public void setUSDBIRTH(String uSDBIRTH) {
        this.uSDBIRTH = uSDBIRTH;
    }

    public String getUSDADDRESS() {
        return uSDADDRESS;
    }

    public void setUSDADDRESS(String uSDADDRESS) {
        this.uSDADDRESS = uSDADDRESS;
    }

    public String getUSDGENDER() {
        return uSDGENDER;
    }

    public void setUSDGENDER(String uSDGENDER) {
        this.uSDGENDER = uSDGENDER;
    }

    public String getUSDTOKO() {
        return uSDTOKO;
    }

    public void setUSDTOKO(String uSDTOKO) {
        this.uSDTOKO = uSDTOKO;
    }

    public String getUSDTOKOFOTO() {
        return uSDTOKOFOTO;
    }

    public void setUSDTOKOFOTO(String uSDTOKOFOTO) {
        this.uSDTOKOFOTO = uSDTOKOFOTO;
    }

    public String getUSDTOKODETAIL() {
        return uSDTOKODETAIL;
    }

    public void setUSDTOKODETAIL(String uSDTOKODETAIL) {
        this.uSDTOKODETAIL = uSDTOKODETAIL;
    }

    public String getIDTOKOSKAT() {
        return iDTOKOSKAT;
    }

    public void setIDTOKOSKAT(String iDTOKOSKAT) {
        this.iDTOKOSKAT = iDTOKOSKAT;
    }

    public String getUSDIDPROV() {
        return uSDIDPROV;
    }

    public void setUSDIDPROV(String uSDIDPROV) {
        this.uSDIDPROV = uSDIDPROV;
    }

    public String getUSDIDKOTA() {
        return uSDIDKOTA;
    }

    public void setUSDIDKOTA(String uSDIDKOTA) {
        this.uSDIDKOTA = uSDIDKOTA;
    }

    public String getUSDKOTA() {
        return uSDKOTA;
    }

    public void setUSDKOTA(String uSDKOTA) {
        this.uSDKOTA = uSDKOTA;
    }

    public String getUSDIDKECAMATAN() {
        return uSDIDKECAMATAN;
    }

    public void setUSDIDKECAMATAN(String uSDIDKECAMATAN) {
        this.uSDIDKECAMATAN = uSDIDKECAMATAN;
    }

    public String getUSDKECAMATAN() {
        return uSDKECAMATAN;
    }

    public void setUSDKECAMATAN(String uSDKECAMATAN) {
        this.uSDKECAMATAN = uSDKECAMATAN;
    }

    public String getUSDSTATUS() {
        return uSDSTATUS;
    }

    public void setUSDSTATUS(String uSDSTATUS) {
        this.uSDSTATUS = uSDSTATUS;
    }

    public String getUSDREFERRAL() {
        return uSDREFERRAL;
    }

    public void setUSDREFERRAL(String uSDREFERRAL) {
        this.uSDREFERRAL = uSDREFERRAL;
    }

    public String getUSDREFERRALFROM() {
        return uSDREFERRALFROM;
    }

    public void setUSDREFERRALFROM(String uSDREFERRALFROM) {
        this.uSDREFERRALFROM = uSDREFERRALFROM;
    }

    public String getUPDATEDAT() {
        return uPDATEDAT;
    }

    public void setUPDATEDAT(String uPDATEDAT) {
        this.uPDATEDAT = uPDATEDAT;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
