package ai.agusibrhim.design.topedc.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.image.SmartImageView;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Activity.DetailBarangTokoActivity;
import ai.agusibrhim.design.topedc.Activity.EditBarangActivity;
import ai.agusibrhim.design.topedc.Helper.Config;
import ai.agusibrhim.design.topedc.Helper.HelperClass;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.MainActivity;
import ai.agusibrhim.design.topedc.Model.BarangTokoModel;
import ai.agusibrhim.design.topedc.Model.ListProduct;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BarangTokoAdapterIndex extends RecyclerView.Adapter<BarangTokoAdapterIndex.Holder> implements Filterable {

    ArrayList<BarangTokoModel> list;
    ArrayList<BarangTokoModel>filteredList;
    Context context;
    SharedPref sharedPref;
    String data;
    public final int TYPE_AMBIL = 0;
    public final int TYPE_LOAD = 1;
    OnLoadMoreListener loadMoreListener;
    boolean isLoading = false, isMoreDataAvailable = true;

    public BarangTokoAdapterIndex(ArrayList<BarangTokoModel> list, Context context, String data) {
        this.list = list;
        this.filteredList = filteredList;
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        if(viewType==TYPE_AMBIL){
            return new Holder(inflater.inflate(R.layout.item_product,parent,false));
        }else{
            return new Holder(inflater.inflate(R.layout.row_load,parent,false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        if(position>=getItemCount()-1 && isMoreDataAvailable && !isLoading && loadMoreListener!=null){
            isLoading = true;
            loadMoreListener.onLoadMore();
        }

        if(getItemViewType(position)==TYPE_AMBIL){
            ((Holder)holder).bindData(filteredList.get(position));
//            final BeritaModel bm = beritaModels.get(position);
//            holder.bm = bm;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if(filteredList.get(position).getType().equals("barang")){
            return TYPE_AMBIL;
        }else{
            return TYPE_LOAD;
        }
    }

    @Override
    public int getItemCount() {
        return filteredList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString().toLowerCase();

                if (charString.isEmpty()) {
                    filteredList = list;
                } else {
                    ArrayList<BarangTokoModel> list2 = new ArrayList<>();

                    for (BarangTokoModel model : list) {
                        if (model.getBANAME().toLowerCase().contains(charSequence) ||
                                model.getIDBARANG().toLowerCase().contains(charSequence)) {
                            Log.d("ID", "performFiltering: "+model.getIDBARANG().toLowerCase());
                            list2.add(model);
                        }
                    }
                    filteredList = list2;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredList = (ArrayList<BarangTokoModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class Holder extends RecyclerView.ViewHolder{

        TextView name, price, priceold, discount, store;
        SmartImageView img;
        public Holder(View itemView) {
            super(itemView);

            name=(TextView) itemView.findViewById(R.id.itemproductTextViewName);
            price=(TextView) itemView.findViewById(R.id.itemproductTextViewPrice);
            priceold=(TextView) itemView.findViewById(R.id.itemproductTextViewPold);
            discount=(TextView) itemView.findViewById(R.id.itemproductTextViewDisc);
            store=(TextView) itemView.findViewById(R.id.itemproductTextViewStore);
            img=(SmartImageView) itemView.findViewById(R.id.itemproductImageView1);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    if (data.equals("tamu")){

                    }else {
                        final CharSequence[] dialogItem = {"Hapus", "Edit","Lihat"};
                        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                        builder.setTitle("Tentukan Pilihan Anda");
                        builder.setItems(dialogItem, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                switch (i) {
                                    case 0:
                                        new AlertDialog.Builder(view.getContext())
                                                .setMessage("Apakah anda akan menghapus barang ini ?")
                                                .setCancelable(false)
                                                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        sharedPref = new SharedPref(view.getContext());
                                                        final ProgressDialog pd = new ProgressDialog(view.getContext());
                                                        pd.setCancelable(false);
                                                        pd.setTitle("Proses ...");
                                                        pd.show();
                                                        ApiService apiService = ApiConfig.getInstanceRetrofit();
                                                        apiService.hapusBarang(sharedPref.getIdUser(),
                                                                sharedPref.getSESSION(),
                                                                list.get(getAdapterPosition()).getIDBARANG()).enqueue(new Callback<ResponseBody>() {
                                                            @Override
                                                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                                                if (response.isSuccessful()){
                                                                    pd.dismiss();
                                                                    try {
                                                                        JSONObject jsonObject = new JSONObject(response.body().string());
                                                                        String pesan = jsonObject.optString("message");
                                                                        String sukses = jsonObject.optString("success");
                                                                        if (pesan.equals("Berhasil delete")){
                                                                            Toast.makeText(context, ""+pesan, Toast.LENGTH_SHORT).show();
                                                                            context.startActivity(new Intent(view.getContext(), MainActivity.class)
                                                                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                                                        }else {
                                                                            Toast.makeText(context, ""+pesan, Toast.LENGTH_SHORT).show();
                                                                        }

                                                                    } catch (JSONException e) {
                                                                        e.printStackTrace();
                                                                    } catch (IOException e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                            }

                                                            @Override
                                                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                                                pd.dismiss();
                                                                Toast.makeText(context, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                                                            }
                                                        });
                                                    }
                                                })
                                                .setNegativeButton("Tidak", null)
                                                .show();
                                        break;
                                    case 1:
                                        Intent intent = new Intent(context,EditBarangActivity.class);
                                        intent.putExtra("list",list.get(getAdapterPosition()));
                                        intent.putExtra("posisi",""+getAdapterPosition());
                                        context.startActivity(intent);
                                        break;
                                    case 2:
                                        Intent intent2 = new Intent(context, DetailBarangTokoActivity.class);
                                        intent2.putExtra("data", "lihat");
                                        intent2.putExtra("list", list.get(getAdapterPosition()));
                                        intent2.putExtra("posisi",""+getAdapterPosition());
                                        intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        context.startActivity(intent2);
                                        break;
                                }
                            }
                        });
                        builder.create().show();
                    }
                }
            });
        }

        void bindData(BarangTokoModel data){
            BarangTokoModel cat=data;
            name.setText(cat.getBANAME());

            String gambar = cat.getBAIMAGE();

            String image = new HelperClass().splitText(gambar);

            Picasso.with(context)
                    .load(Config.IMAGE_BARANG + image)
                    .placeholder(R.drawable.image_loading)
                    .error(R.drawable.no_image_found)
                    .noFade()
                    .into(img);

            store.setText(cat.getUSDTOKO());

            price.setText(""+new HelperClass().convertRupiah(Integer.valueOf(cat.getBAPRICE())));
        }
    }

    static class LoadHolder extends RecyclerView.ViewHolder{
        public LoadHolder(View itemView) {
            super(itemView);
        }
    }

    public void setMoreDataAvailable(boolean moreDataAvailable) {
        isMoreDataAvailable = moreDataAvailable;
    }

    /* notifyDataSetChanged is final method so we can't override it
         call adapter.notifyDataChanged(); after update the list
         */
    public void notifyDataChanged(){
        notifyDataSetChanged();
        isLoading = false;
    }


    public interface OnLoadMoreListener{
        void onLoadMore();
    }

    public void setLoadMoreListener(OnLoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }
}
