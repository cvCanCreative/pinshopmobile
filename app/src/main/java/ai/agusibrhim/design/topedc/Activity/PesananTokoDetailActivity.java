package ai.agusibrhim.design.topedc.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Adapter.PesananTokoDetailAdapter;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.MainActivity;
import ai.agusibrhim.design.topedc.Model.RequestTokoDetailModel;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PesananTokoDetailActivity extends AppCompatActivity {

    private TextView tvKet;
    private RecyclerView rv;
    SharedPref sharedPref;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<RequestTokoDetailModel> list = new ArrayList<>();
    PesananTokoDetailAdapter mAdapter;
    private LinearLayout ly;
    private EditText edtResi;
    private Button btnInput;
    String totalHarga;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pesanan_toko_detail);
        initView();

        awal();


        btnInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inputResi();
            }
        });
    }

    private void awal() {
        sharedPref = new SharedPref(this);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rv.setLayoutManager(layoutManager);


        getData();
    }

    private void inputResi(){
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Proses ...");
        pd.setCancelable(false);
        pd.show();
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.masukkanResi(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                getIntent().getStringExtra("id"),
                "PROSES_KIRIM",
                ""+edtResi.getText().toString()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String kode = jsonObject.optString("success");
                        String pesan = jsonObject.optString("message");
                        if (kode.equals("1")){
                            Toast.makeText(PesananTokoDetailActivity.this, ""+pesan, Toast.LENGTH_SHORT).show();
                            edtResi.setText("");
                            Intent intent = new Intent(getApplicationContext(),TokoKitaActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("data","resi");
                            startActivity(intent);
                            finish();
                        }else {
                            Toast.makeText(PesananTokoDetailActivity.this, ""+pesan, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(PesananTokoDetailActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getData() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Loading ...");
        pd.setCancelable(false);
        pd.show();

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getRequestTokoDetail(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                getIntent().getStringExtra("id")).enqueue(new Callback<ArrayList<RequestTokoDetailModel>>() {
            @Override
            public void onResponse(Call<ArrayList<RequestTokoDetailModel>> call, Response<ArrayList<RequestTokoDetailModel>> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    list = response.body();
                    if (list.get(0).getSuccess() == 1) {
                        ly.setVisibility(View.VISIBLE);
                        rv.setVisibility(View.VISIBLE);
                        tvKet.setVisibility(View.GONE);
                        mAdapter = new PesananTokoDetailAdapter(list, getApplicationContext());
                        rv.setAdapter(mAdapter);
                    } else {
                        ly.setVisibility(View.GONE);
                        rv.setVisibility(View.GONE);
                        tvKet.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<RequestTokoDetailModel>> call, Throwable t) {
               pd.dismiss();
                Toast.makeText(PesananTokoDetailActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initView() {
        tvKet = (TextView) findViewById(R.id.tv_ket);
        rv = (RecyclerView) findViewById(R.id.rv);
        ly = (LinearLayout) findViewById(R.id.ly);
        edtResi = (EditText) findViewById(R.id.edt_resi);
        btnInput = (Button) findViewById(R.id.btn_input);
    }
}
