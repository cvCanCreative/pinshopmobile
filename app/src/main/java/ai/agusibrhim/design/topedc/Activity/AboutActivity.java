package ai.agusibrhim.design.topedc.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import ai.agusibrhim.design.topedc.MainActivity;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AboutActivity extends AppCompatActivity {

    private TextView tvAbout;
    private ProgressBar pd;
    private ImageView imgClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        initView();

        getAbout();
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),HomeLikeShopeeActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("data","profil");
                finish();
            }
        });
    }

    private void getAbout() {
        pd.setVisibility(View.VISIBLE);
        tvAbout.setVisibility(View.GONE);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getAbout().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.setVisibility(View.GONE);
                    tvAbout.setVisibility(View.VISIBLE);
                    try {
                        JSONArray jsonArray = new JSONArray(response.body().string());
                        JSONObject jsonObject = jsonArray.optJSONObject(0);
                        String about = jsonObject.optString("AB_DESCRIPTION");

                        tvAbout.setText("" + Html.fromHtml(about));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.setVisibility(View.GONE);
                tvAbout.setVisibility(View.VISIBLE);
                Toast.makeText(AboutActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initView() {
        tvAbout = (TextView) findViewById(R.id.tv_about);
        pd = (ProgressBar) findViewById(R.id.pd);
        imgClose = (ImageView) findViewById(R.id.img_close);
    }
}
