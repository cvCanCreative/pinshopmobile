package ai.agusibrhim.design.topedc.Fragment;


import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.SliderLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Activity.AllKategoriActivity;
import ai.agusibrhim.design.topedc.Activity.HomeLikeShopeeActivity;
import ai.agusibrhim.design.topedc.Activity.IntruksiActivity;
import ai.agusibrhim.design.topedc.Activity.KeranjangActivity;
import ai.agusibrhim.design.topedc.Activity.ListBankActivity;
import ai.agusibrhim.design.topedc.Activity.ListChatActivity;
import ai.agusibrhim.design.topedc.Activity.SaldoActivity;
import ai.agusibrhim.design.topedc.Activity.SearchActivity;
import ai.agusibrhim.design.topedc.Adapter.AdapterKategori;
import ai.agusibrhim.design.topedc.Adapter.AdapterProdukTerbaru;
import ai.agusibrhim.design.topedc.Adapter.AdapterSlider;
import ai.agusibrhim.design.topedc.Adapter.CatagoryAdapter;
import ai.agusibrhim.design.topedc.Adapter.ListProductAdapter;
import ai.agusibrhim.design.topedc.Adapter.PromoHariIniAdapter;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.Model.ListCategory;
import ai.agusibrhim.design.topedc.Model.ListProduct;
import ai.agusibrhim.design.topedc.Model.PromoModel;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    private SliderLayout sliderSlider;
    private TextView tvLihatPromo;
    private LinearLayout lySaldo;
    private TextView tvSaldo;
    private ProgressBar pdSaldo;
    private TextView tvTambahSaldo;
    private TextView cardTextView1;
    private TextView tvAllKategori;
    private ProgressBar pdKategori;
    private RecyclerView rv;
    private TextView tvAllPromo;
    private ProgressBar pdPromo;
    private RecyclerView rvPromo;
    private ProgressBar pdProduk;
    private RecyclerView rvTerbaru;
    private ImageView actionSearch;
    private ImageView imgChat;
    private ImageView imgKeranjang;
    private ImageView imgPromo;
    private ImageView imgPromo2;
    private ProgressBar pdProdukTerlaris;
    private RecyclerView rvTerlaris;

    RecyclerView.LayoutManager linearLayoutManager;
    ArrayList<ListCategory> list = new ArrayList<>();
    CatagoryAdapter catagoryAdapter;
    ArrayList<PromoModel> listPromo = new ArrayList<>();
    ArrayList<ListProduct> listProduk = new ArrayList<>();
    ListProductAdapter produkAdapter;
    PromoHariIniAdapter promoHariIniAdapter;

    SharedPref sharedPref;

    private RelativeLayout homeDivToolbar;
    private ImageView homeIvChat;
    private ImageView homeIvSaldo;
    private ViewPager homeViewPager;
    private LinearLayout homeCvSearch;
    private TextView homeTvSemuaterbaru;
    private RecyclerView homeRvProdukterbaru;
    private TextView homeTvSemuakategori;
    private RecyclerView homeRvKategori;
    private RecyclerView homeRvRekomendasi;
    private TextView homeTvSemuaterlaris;
    private RecyclerView homeRvTerlaris;
    private FloatingActionButton homeFabCart;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        initView(view);

        awal();
        mainButton();

        return view;
    }

    private void awal() {
        sharedPref = new SharedPref(getActivity());

        if (sharedPref.getPertamaMasuk()) {
            startActivity(new Intent(getActivity(), IntruksiActivity.class));
            sharedPref.savePrefBoolean(SharedPref.PERTAMA_MASUK, false);
        } else {
            Permission3();

            getSlider();
            getProdukTerbaru();

            getKategori();

            if (sharedPref.getSudahLogin()) {
                homeIvSaldo.setVisibility(View.VISIBLE);
                saldo();
                getToken();
            } else {
                homeIvSaldo.setVisibility(View.GONE);
            }
        }


    }

    private void getProdukTerbaru() {

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getProductByCatagori("2", 1).enqueue(new Callback<ArrayList<ListProduct>>() {
            @Override
            public void onResponse(Call<ArrayList<ListProduct>> call, Response<ArrayList<ListProduct>> response) {
                if (response.isSuccessful()){
                    listProduk = response.body();
                    homeRvProdukterbaru.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
                    AdapterProdukTerbaru adapterTrendProdukShop = new AdapterProdukTerbaru(getActivity(), listProduk);
                    homeRvProdukterbaru.setAdapter(adapterTrendProdukShop);

                    homeRvTerlaris.setLayoutManager(new GridLayoutManager(getActivity(), 2));
                    produkAdapter = new ListProductAdapter(listProduk, getActivity());
                    homeRvTerlaris.setAdapter(produkAdapter);

                }
            }

            @Override
            public void onFailure(Call<ArrayList<ListProduct>> call, Throwable t) {

            }
        });

    }

    private void getSlider() {

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getPromo().enqueue(new Callback<ArrayList<PromoModel>>() {
            @Override
            public void onResponse(Call<ArrayList<PromoModel>> call, Response<ArrayList<PromoModel>> response) {
                if (response.isSuccessful()) {
                    listPromo = response.body();

                    if (listPromo.size() == 0) {

                    } else {
                        if (getActivity() != null){

                            AdapterSlider adapter = new AdapterSlider(listPromo, getActivity());
                            homeViewPager.setAdapter(adapter);
                            homeViewPager.setPadding(30, 0, 30, 0);
                        }

                    }

                }
            }

            @Override
            public void onFailure(Call<ArrayList<PromoModel>> call, Throwable t) {

            }
        });

    }

    private void getToken() {
            ApiService apiService = ApiConfig.getInstanceRetrofit();
            apiService.getToken(sharedPref.getIdUser(),
                    sharedPref.getSESSION()).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        try {
                            JSONObject jsonObject = new JSONObject(response.body().string());
                            String pesan = jsonObject.optString("message");
                            if (pesan.equals("Berhasil")) {

                            } else {
                                sharedPref.savePrefString(SharedPref.ID_USER, "");
                                sharedPref.savePrefString(SharedPref.SESSION, "");
                                sharedPref.savePrefBoolean(SharedPref.SUDAH_LOGIN, false);
                                if (getActivity() != null) {
                                    new AlertDialog.Builder(getActivity())
                                            .setMessage("" + pesan)
                                            .setCancelable(false)
                                            .setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    sharedPref.savePrefString(SharedPref.ID_USER, "");
                                                    sharedPref.savePrefString(SharedPref.SESSION, "");
                                                    sharedPref.savePrefBoolean(SharedPref.SUDAH_LOGIN, false);
                                                    startActivity(new Intent(getActivity(), HomeLikeShopeeActivity.class)
                                                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                                }
                                            })
                                            .show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(getActivity(), "error cek token", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    if (getActivity() != null){
                        Toast.makeText(getActivity(), "error cek token " + t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });

    }

    private void saldo() {
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getSlado(sharedPref.getIdUser(), sharedPref.getSESSION()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String pesan = jsonObject.optString("message");
                        String saldo = jsonObject.optString("SA_SALDO");
//                        Toast.makeText(MainActivity.this, ""+pesan, Toast.LENGTH_SHORT).show();
                        if (pesan.equals("Berhasil")) {
                            if (saldo.equals("")) {
//                                tvSaldo.setText("" + new HelperClass().convertRupiah(Integer.parseInt("0")));
                                sharedPref.savePrefString(SharedPref.SALDO, saldo);
                            } else {
//                                tvSaldo.setText("" + new HelperClass().convertRupiah(Integer.parseInt(saldo)));
                                sharedPref.savePrefString(SharedPref.SALDO, saldo);
                            }
                        } else {
//                            tvSaldo.setText("" + new HelperClass().convertRupiah(Integer.parseInt("0")));
                            sharedPref.savePrefString(SharedPref.SALDO, saldo);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pdSaldo.setVisibility(View.GONE);
//                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getKategori() {

        linearLayoutManager = new GridLayoutManager(getActivity(), 3);
        homeRvKategori.setLayoutManager(linearLayoutManager);

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getDataCatagory().enqueue(new Callback<ArrayList<ListCategory>>() {
            @Override
            public void onResponse(Call<ArrayList<ListCategory>> call, Response<ArrayList<ListCategory>> response) {
                if (response.isSuccessful()) {
                    list = response.body();
                    AdapterKategori adapterKategori = new AdapterKategori(getActivity(), list);
                    homeRvKategori.setAdapter(adapterKategori);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ListCategory>> call, Throwable t) {

            }
        });
    }

    private void initView(View view) {
        sliderSlider = (SliderLayout) view.findViewById(R.id.sliderSlider);
        tvLihatPromo = (TextView) view.findViewById(R.id.tv_lihat_promo);
        lySaldo = (LinearLayout) view.findViewById(R.id.ly_saldo);
        tvSaldo = (TextView) view.findViewById(R.id.tv_saldo);
        pdSaldo = (ProgressBar) view.findViewById(R.id.pd_saldo);
        tvTambahSaldo = (TextView) view.findViewById(R.id.tv_tambah_saldo);
        cardTextView1 = (TextView) view.findViewById(R.id.cardTextView1);
        tvAllKategori = (TextView) view.findViewById(R.id.tv_all_kategori);
        pdKategori = (ProgressBar) view.findViewById(R.id.pd_kategori);
        rv = (RecyclerView) view.findViewById(R.id.rv);
        tvAllPromo = (TextView) view.findViewById(R.id.tv_all_promo);
        pdPromo = (ProgressBar) view.findViewById(R.id.pd_promo);
        rvPromo = (RecyclerView) view.findViewById(R.id.rv_promo);
        pdProduk = (ProgressBar) view.findViewById(R.id.pd_produk);
        rvTerbaru = (RecyclerView) view.findViewById(R.id.rv_terbaru);
        actionSearch = (ImageView) view.findViewById(R.id.action_search);
        imgChat = (ImageView) view.findViewById(R.id.img_chat);
        imgKeranjang = (ImageView) view.findViewById(R.id.img_keranjang);
        imgPromo = (ImageView) view.findViewById(R.id.img_promo);
        imgPromo2 = (ImageView) view.findViewById(R.id.img_promo2);
        pdProdukTerlaris = (ProgressBar) view.findViewById(R.id.pd_produk_terlaris);
        rvTerlaris = (RecyclerView) view.findViewById(R.id.rv_terlaris);

        homeDivToolbar = view.findViewById(R.id.home_div_toolbar);
        homeIvChat = view.findViewById(R.id.home_iv_chat);
        homeIvSaldo = view.findViewById(R.id.home_iv_saldo);
        homeViewPager = view.findViewById(R.id.home_viewPager);
        homeCvSearch = view.findViewById(R.id.home_cv_search);
        homeTvSemuaterbaru = view.findViewById(R.id.home_tv_semuaterbaru);
        homeRvProdukterbaru = view.findViewById(R.id.home_rv_produkterbaru);
        homeTvSemuakategori = view.findViewById(R.id.home_tv_semuakategori);
        homeRvKategori = view.findViewById(R.id.home_rv_kategori);
        homeRvRekomendasi = view.findViewById(R.id.home_rv_rekomendasi);
        homeTvSemuaterlaris = view.findViewById(R.id.home_tv_semuaterlaris);
        homeRvTerlaris = view.findViewById(R.id.home_rv_terlaris);
        homeFabCart = view.findViewById(R.id.home_fab_cart);
    }

    private void mainButton() {
        homeTvSemuakategori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), AllKategoriActivity.class));
            }
        });

        homeIvChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), ListChatActivity.class));
            }
        });

        homeFabCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), KeranjangActivity.class));
            }
        });

        homeCvSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), SearchActivity.class);
                intent.putExtra("data", "semua");
                startActivity(intent);
            }
        });

        tvTambahSaldo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), ListBankActivity.class));
            }
        });

        homeIvSaldo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), SaldoActivity.class);
                intent.putExtra("data", "tarik");
                startActivity(intent);
            }
        });
    }

    private void Permission3() {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.CAMERA)) {
                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.CAMERA},
                        1);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }
}
