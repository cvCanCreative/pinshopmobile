package ai.agusibrhim.design.topedc.Activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.fxn.pix.Pix;
import com.fxn.utility.PermUtil;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Helper.Config;
import ai.agusibrhim.design.topedc.Helper.HelperClass;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.MainActivity;
import ai.agusibrhim.design.topedc.Model.RekeningByIdModel;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BuktiTfSaldoActivity extends AppCompatActivity {


    SharedPref sharedPref;
    File imageFile;
    private String imagePath;
    String h ="";
    String id, data;
    String rekening, bank, jumlah, nama;

    ArrayList<RekeningByIdModel> list = new ArrayList<>();
    private LinearLayout ly;
    private ImageView imgClose;
    private TextView tvJumlahTagihan;
    private TextView tvSalinTagihan;
    private ImageView imgBank;
    private TextView tvNamaRek;
    private TextView tvRek;
    private TextView tvSalinRek;
    private ImageView imgDown;
    private ImageView imgUp;
    private LinearLayout lyKetentuan;
    private ImageView imgTf;
    private TextView tvAmbilGambar;
    private Button btnKirim;
    String kodePayment;
    String bankPin,rekPin,imgPin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bukti_tf_saldo);
        initView();
        awal();

        tvAmbilGambar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EasyImage.openChooserWithGallery(BuktiTfSaldoActivity.this, "chose", 3);
            }
        });

        btnKirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (data.equals("saldo")) {
                    if (h.equals("")){
                        Toast.makeText(BuktiTfSaldoActivity.this, "Silahkan Ambil Gambar", Toast.LENGTH_SHORT).show();
                    }else {
                        postData();
                    }

                } else {
                    if (h.equals("")){
                        Toast.makeText(BuktiTfSaldoActivity.this, "Silahkan Ambil Gambar", Toast.LENGTH_SHORT).show();
                    }else {
                        postBayar();
                    }
                }
            }
        });

        imgDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgDown.setVisibility(View.GONE);
                imgUp.setVisibility(View.VISIBLE);
                lyKetentuan.setVisibility(View.VISIBLE);
            }
        });

        imgUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgDown.setVisibility(View.VISIBLE);
                imgUp.setVisibility(View.GONE);
                lyKetentuan.setVisibility(View.GONE);
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        tvSalinTagihan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClipboardManager clipboard = (ClipboardManager)getSystemService(CLIPBOARD_SERVICE);
                String getstring = jumlah;
                Toast.makeText(getApplicationContext(), "Berhasil disalin", Toast.LENGTH_SHORT).show();
                ClipData clip = ClipData.newPlainText("", getstring);
                clipboard.setPrimaryClip(clip);
            }
        });

        tvSalinRek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClipboardManager clipboard = (ClipboardManager)getSystemService(CLIPBOARD_SERVICE);
                String getstring = tvRek.getText().toString();
                Toast.makeText(getApplicationContext(), "Berhasil disalin", Toast.LENGTH_SHORT).show();
                ClipData clip = ClipData.newPlainText("", getstring);
                clipboard.setPrimaryClip(clip);
            }
        });
    }

    private void awal() {
        sharedPref = new SharedPref(this);
        data = getIntent().getStringExtra("data");
        if (data.equals("saldo")) {
            id = getIntent().getStringExtra("id");

            Picasso.with(getApplicationContext())
                    .load(Config.IMAGE_REKENING + getIntent().getStringExtra("img_bank"))
                    .into(imgBank);
            jumlah = getIntent().getStringExtra("jumlah");

            tvJumlahTagihan.setText("" + new HelperClass().convertRupiah(Integer.parseInt(jumlah)));
            tvNamaRek.setText("Nomor Rekening " + getIntent().getStringExtra("nama_bank"));
            tvRek.setText("" + getIntent().getStringExtra("rekening"));
//            ly.setVisibility(View.GONE);
        } else if (data.equals("bayar")) {
//            ly.setVisibility(View.VISIBLE);
//            lyRekening.setVisibility(View.VISIBLE);
            String jml = getIntent().getStringExtra("jumlah");

            String jml2 = jml.replace("Rp", "");
            jumlah = jml2.replace(".", "");

            rekening = getIntent().getStringExtra("rekening");
            bank = getIntent().getStringExtra("bank");
            nama = getIntent().getStringExtra("nama");

            Picasso.with(getApplicationContext())
                    .load(Config.IMAGE_REKENING + getIntent().getStringExtra("img"))
                    .into(imgBank);

            kodePayment = getIntent().getStringExtra("kode");
            tvJumlahTagihan.setText("" + new HelperClass().convertRupiah(Integer.parseInt(jumlah)));
            tvNamaRek.setText("Nomor Rekening " + getIntent().getStringExtra("bank"));
            tvRek.setText("" + getIntent().getStringExtra("rekening"));
        } else {
//            ly.setVisibility(View.GONE);
            bankPin = getIntent().getStringExtra("bankpin");
            rekPin = getIntent().getStringExtra("rekpin");
            imgPin = getIntent().getStringExtra("imgpin");
            Picasso.with(getApplicationContext())
                    .load(Config.IMAGE_REKENING + imgPin)
                    .error(R.drawable.no_image_found)
                    .into(imgBank);

            rekening = getIntent().getStringExtra("rekening");
            bank = getIntent().getStringExtra("bank");
            jumlah = getIntent().getStringExtra("jumlah");
            nama = getIntent().getStringExtra("nama");
//            Toast.makeText(this, "req "+rekening, Toast.LENGTH_SHORT).show();
            kodePayment = getIntent().getStringExtra("kode");
//            Toast.makeText(this, "kode "+kodePayment, Toast.LENGTH_SHORT).show();
            tvJumlahTagihan.setText("" + new HelperClass().convertRupiah(Integer.parseInt(jumlah)));
            tvNamaRek.setText("Nomor Rekening " + bankPin);
            tvRek.setText("" + rekPin);
        }
    }

    private void postBayar() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Loading ...");
        pd.setCancelable(true);
        pd.show();

        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), imageFile);
        MultipartBody.Part body = MultipartBody.Part.createFormData("image", imageFile.getName(), requestFile);

        RequestBody mRekeing = RequestBody.create(MediaType.parse("text/plain"), rekening);
        RequestBody mBank = RequestBody.create(MediaType.parse("text/plain"), bank);
        RequestBody mJumlah = RequestBody.create(MediaType.parse("text/plain"), jumlah);
        RequestBody mNama = RequestBody.create(MediaType.parse("text/plain"), nama);
        RequestBody mKode = RequestBody.create(MediaType.parse("text/plain"), ""+kodePayment);

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.buktiTransfer(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                mRekeing,
                mNama,
                mBank,
                mJumlah,
                mKode,
                body).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String pesan = jsonObject.optString("message");
                        String sukses = jsonObject.optString("success");
                        if (sukses.equals("1")) {
//                            Toast.makeText(BuktiTfSaldoActivity.this, ""+mRekeing, Toast.LENGTH_SHORT).show();
                            new AlertDialog.Builder(BuktiTfSaldoActivity.this)
                                    .setMessage("" + pesan)
                                    .setCancelable(false)
                                    .setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id3) {
                                            startActivity(new Intent(getApplicationContext(), HomeLikeShopeeActivity.class)
                                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                            finish();
                                        }
                                    })
                                    .show();
                        } else {
                            Toast.makeText(BuktiTfSaldoActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(BuktiTfSaldoActivity.this, "Tidak Berhasil", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(BuktiTfSaldoActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void postData() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Loading ...");
        pd.setCancelable(true);
        pd.show();

        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), imageFile);
        MultipartBody.Part body = MultipartBody.Part.createFormData("image", imageFile.getName(), requestFile);

        RequestBody idHis = RequestBody.create(MediaType.parse("text/plain"), id);

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.topupSaldoBukti(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                idHis,
                body).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String pesan = jsonObject.optString("message");
                        if (pesan.equals("Success")) {
                            new AlertDialog.Builder(BuktiTfSaldoActivity.this)
                                    .setMessage("" + pesan)
                                    .setCancelable(false)
                                    .setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id3) {
                                            startActivity(new Intent(getApplicationContext(), HomeLikeShopeeActivity.class)
                                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                            finish();
                                        }
                                    })
                                    .show();
                        } else {
                            Toast.makeText(BuktiTfSaldoActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(BuktiTfSaldoActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                //Some error handling
                Toast.makeText(BuktiTfSaldoActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onImagePicked(File image, EasyImage.ImageSource source, int type) {
                Glide.with(BuktiTfSaldoActivity.this)
                        .load(new File(image.getPath()))
                        .into(imgTf);
                imageFile = new File(image.getPath());

                h= imageFile.getName();


                try {
                    imageFile = new Compressor(BuktiTfSaldoActivity.this).compressToFile(imageFile);
                } catch (IOException e) {
                    e.printStackTrace();
                }

//                Toast.makeText(BuktiTfSaldoActivity.this, "Nama " + imageFile.getName(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
                super.onCanceled(source, type);
            }
        });
    }

    private Bitmap setReducedImageSize() {
        int targetImageViewWidth = imgTf.getWidth();
        int targetImageViewHeight = imgTf.getHeight();

        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imagePath, bmOptions);
        int cameraImageWidth = bmOptions.outWidth;
        int cameraImageHeight = bmOptions.outHeight;

        int scaleFactor = Math.min(cameraImageWidth / targetImageViewWidth, cameraImageHeight / targetImageViewHeight);
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inJustDecodeBounds = false;

        // Bitmap photoReducedSizeBitmp = BitmapFactory.decodeFile(mImageFileLocation, bmOptions);
        // mPhotoCapturedImageView.setImageBitmap(photoReducedSizeBitmp);
        return BitmapFactory.decodeFile(imagePath, bmOptions);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PermUtil.REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Pix.start(BuktiTfSaldoActivity.this, 100);
                } else {
                    Toast.makeText(BuktiTfSaldoActivity.this, "Approve permissions to open Pix ImagePicker", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

    private void initView() {

        ly = (LinearLayout) findViewById(R.id.ly);
        imgClose = (ImageView) findViewById(R.id.img_close);
        tvJumlahTagihan = (TextView) findViewById(R.id.tv_jumlah_tagihan);
        tvSalinTagihan = (TextView) findViewById(R.id.tv_salin_tagihan);
        imgBank = (ImageView) findViewById(R.id.img_bank);
        tvNamaRek = (TextView) findViewById(R.id.tv_nama_rek);
        tvRek = (TextView) findViewById(R.id.tv_rek);
        tvSalinRek = (TextView) findViewById(R.id.tv_salin_rek);
        imgDown = (ImageView) findViewById(R.id.img_down);
        imgUp = (ImageView) findViewById(R.id.img_up);
        lyKetentuan = (LinearLayout) findViewById(R.id.ly_ketentuan);
        imgTf = (ImageView) findViewById(R.id.img_tf);
        tvAmbilGambar = (TextView) findViewById(R.id.tv_ambil_gambar);
        btnKirim = (Button) findViewById(R.id.btn_kirim);
    }

    @Override
    public void onBackPressed() {

        if (data.equals("saldo")) {
            new AlertDialog.Builder(BuktiTfSaldoActivity.this)
                    .setMessage("Anda Harus Kirim bukti, jika tidak transaksi akan hilang")
                    .setCancelable(false)
                    .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            startActivity(new Intent(getApplicationContext(), HomeLikeShopeeActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                            finish();
                        }
                    })
                    .setNegativeButton("Tidak", null)
                    .show();
        } else if (data.equals("bayar")) {
            new AlertDialog.Builder(BuktiTfSaldoActivity.this)
                    .setMessage("Anda Harus Kirim bukti, jika tidak transaksi akan hilang")
                    .setCancelable(false)
                    .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            startActivity(new Intent(getApplicationContext(), HomeLikeShopeeActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                            finish();
                        }
                    })
                    .setNegativeButton("Tidak", null)
                    .show();
        } else {
            super.onBackPressed();
        }

    }
}
