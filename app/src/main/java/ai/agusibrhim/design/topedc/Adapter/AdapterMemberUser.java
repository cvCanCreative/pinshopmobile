package ai.agusibrhim.design.topedc.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Activity.CalonMemberActivity;
import ai.agusibrhim.design.topedc.Activity.PromoActivity;
import ai.agusibrhim.design.topedc.Activity.TokoKitaActivity;
import ai.agusibrhim.design.topedc.Helper.Config;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.Model.MemberUserModel;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdapterMemberUser extends RecyclerView.Adapter<AdapterMemberUser.Holder> {

    ArrayList<MemberUserModel> list;
    Context context;
    String status;
    SharedPref sharedPref;

    public AdapterMemberUser(ArrayList<MemberUserModel> list, Context context, String status) {
        this.list = list;
        this.context = context;
        this.status = status;

        sharedPref = new SharedPref(context);
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_member,parent,false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        holder.username.setVisibility(View.VISIBLE);
        holder.img.setVisibility(View.VISIBLE);

        holder.nama.setText(""+list.get(position).getUSDFULLNAME());
        holder.username.setText(""+list.get(position).getUSUSERNAME());
        Glide.with(context)
                .load(Config.IMAGE_PROFIL + list.get(position).getUSDFOTO())
                .into(holder.img);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private void acc(String id){
        final ProgressDialog pd = new ProgressDialog(context);
        pd.setTitle("Proses ...");
        pd.setCancelable(false);
        pd.show();
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.accMember(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                ""+id).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    pd.dismiss();
                    try {
                        JSONObject jsonObject =new JSONObject(response.body().string());
                        String pesan = jsonObject.optString("message");
                        if (pesan.equals("Member berhasil di tambahkan")){
                            Toast.makeText(context, ""+pesan, Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(context, TokoKitaActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("data", "sendiri");
                            context.startActivity(intent);
                        }else {
                            Toast.makeText(context, ""+pesan, Toast.LENGTH_SHORT).show();
                        }
                        Toast.makeText(context, ""+pesan, Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(context, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void tolak(String id){
        final ProgressDialog pd = new ProgressDialog(context);
        pd.setTitle("Proses ...");
        pd.setCancelable(false);
        pd.show();
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.tolakMember(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                "tolak",
                ""+id).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    pd.dismiss();
                    try {
                        JSONObject jsonObject =new JSONObject(response.body().string());
                        String pesan = jsonObject.optString("message");
                        String sukses = jsonObject.optString("success");
                        if (sukses.equals("1")){
                            Toast.makeText(context, ""+pesan, Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(context, TokoKitaActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("data", "sendiri");
                            context.startActivity(intent);
                        }else {
                            Toast.makeText(context, ""+pesan, Toast.LENGTH_SHORT).show();
                        }
                        Toast.makeText(context, ""+pesan, Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(context, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void hapus(String id){
        final ProgressDialog pd = new ProgressDialog(context);
        pd.setTitle("Proses ...");
        pd.setCancelable(false);
        pd.show();
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.hapusMember(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                ""+id).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    pd.dismiss();
                    try {
                        JSONObject jsonObject =new JSONObject(response.body().string());
                        String pesan = jsonObject.optString("message");
                        String suksek = jsonObject.optString("success");
                        if (suksek.equals("1")){
                            Toast.makeText(context, ""+pesan, Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(context, TokoKitaActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("data", "sendiri");
                            context.startActivity(intent);
                        }else {
                            Toast.makeText(context, ""+pesan, Toast.LENGTH_SHORT).show();
                        }
                        Toast.makeText(context, ""+pesan, Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(context, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    public class Holder extends RecyclerView.ViewHolder{
        TextView nama,username;
        ImageView img;
        public Holder(View itemView) {
            super(itemView);

            nama = itemView.findViewById(R.id.tv_nama);
            username = itemView.findViewById(R.id.tv_username);
            img = itemView.findViewById(R.id.img_avatar);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    if (status.equals("member")){
                        final CharSequence[] dialogItem = {"Hapus"};
                        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                        builder.setTitle("Tentukan Pilihan Anda");
                        builder.setItems(dialogItem, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                switch (i) {
                                    case 0:
                                       hapus(""+list.get(getAdapterPosition()).getIDMEMBERTOKO());
                                        break;
                                }
                            }
                        });
                        builder.create().show();
                    }else {

                        final CharSequence[] dialogItem = {"Acc","Tolak"};
                        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                        builder.setTitle("Tentukan Pilihan Anda");
                        builder.setItems(dialogItem, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                switch (i) {
                                    case 0:
                                       acc(""+list.get(getAdapterPosition()).getIDMEMBERTOKO());
                                        break;
                                    case 1:
                                        tolak(""+list.get(getAdapterPosition()).getIDMEMBERTOKO());
                                        break;
                                }
                            }
                        });
                        builder.create().show();
                    }
                }
            });
        }
    }
}
