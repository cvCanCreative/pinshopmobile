package ai.agusibrhim.design.topedc.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Activity.PesananTokoDetailActivity;
import ai.agusibrhim.design.topedc.Helper.Config;
import ai.agusibrhim.design.topedc.Helper.HelperClass;
import ai.agusibrhim.design.topedc.Model.RequestTokoModel;
import ai.agusibrhim.design.topedc.Model.TransaksiDetail;
import ai.agusibrhim.design.topedc.R;


public class PesananTokoAdater extends RecyclerView.Adapter<PesananTokoAdater.Holder> {

    ArrayList<RequestTokoModel> list;
    Context context;
    String totalHarga;
    ArrayList<TransaksiDetail> transaksiDetail = new ArrayList<>();


    public PesananTokoAdater(ArrayList<RequestTokoModel> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_request_toko, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        transaksiDetail = list.get(position).transaksiDetail;
        for (int i = 0; i < transaksiDetail.size(); i++) {

            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(R.layout.row_isi_toko, null);

            ImageView imgBarang = (ImageView) view.findViewById(R.id.img_barang);
            TextView tvNamaBarang = (TextView) view.findViewById(R.id.tv_nama_barang);
            TextView tvJumlahBeli = (TextView) view.findViewById(R.id.tv_jumlah_beli);
            TextView tvHargaBarang = (TextView) view.findViewById(R.id.tv_harga_barang);

            LinearLayout card = view.findViewById(R.id.card);

            Picasso.with(context)
                    .load(Config.IMAGE_BARANG + transaksiDetail.get(i).BA_IMAGE)
                    .into(imgBarang);

            tvNamaBarang.setText(""+transaksiDetail.get(i).BA_NAME);
            tvJumlahBeli.setText("x "+transaksiDetail.get(i).TSD_QTY);
            tvHargaBarang.setText(new HelperClass().convertRupiah(Integer.parseInt(transaksiDetail.get(i).TSD_HARGA_ASLI)));

            final String idTrans = transaksiDetail.get(i).ID_TRANSAKSI;
            card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    Toast.makeText(context, ""+idTrans, Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(context, PesananTokoDetailActivity.class);
                    intent.putExtra("id", ""+idTrans);
                    context.startActivity(intent);
                }
            });
            holder.div.addView(view);
        }
//        totalHarga = "" + total;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class Holder extends RecyclerView.ViewHolder {

        private LinearLayout div;

        public Holder(View itemView) {
            super(itemView);

            div = (LinearLayout) itemView.findViewById(R.id.div);

        }
    }
}
