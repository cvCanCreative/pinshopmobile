package ai.agusibrhim.design.topedc.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.nex3z.notificationbadge.NotificationBadge;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ai.agusibrhim.design.topedc.Adapter.AdapterMemberUser;
import ai.agusibrhim.design.topedc.Fragment.BarangTokoFragment;
import ai.agusibrhim.design.topedc.Fragment.DataTokoFragment;
import ai.agusibrhim.design.topedc.Fragment.PesananTokoFragment;
import ai.agusibrhim.design.topedc.Fragment.PromoTokoragment;
import ai.agusibrhim.design.topedc.Helper.Config;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.Model.MemberTokoModel;
import ai.agusibrhim.design.topedc.Model.MemberUserModel;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TokoKitaActivity extends AppCompatActivity {

    private TextView tvNamaToko;
    private TextView tvAlamatToko;
    private Button btnLihatBarang;
    private Button btnTambahBarang;
    SharedPref sharedPref;
    private Button btnAddMember;
    private Button btnBatalMember;
    String data;
    String id_toko;
    private CircleImageView imgToko;
    File imageFile;
    private String imagePath, h;
    private Button btnLihatPromo;
    private TextView tvNoToko;
    private TextView tvEmailToko;
    private Button btnLihatPesanan;
    private CardView cardPromo;
    private LinearLayout cardIkuti;
    private CardView cardBarang;
    private LinearLayout cardTambah;
    private LinearLayout cardPesanan;
    private LinearLayout cardMengikuti;
    private TabLayout viewpagertab2;
    private ViewPager viewPager;
    private TextView tvPengikut;
    private TextView tvMengikuti;
    ArrayList<MemberTokoModel> listToko = new ArrayList<>();
    ArrayList<MemberUserModel> listUser = new ArrayList<>();
    private ImageView imgCalonMember;
    private LinearLayout cardMenunggu;
    private NotificationBadge badge;
    ArrayList<MemberUserModel> listUser2 = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toko_kita);
        initView();

        awal();


    }

    //gallery
    private void showFileChooserGalerry() {
        Intent qq = new Intent(Intent.ACTION_PICK);
        qq.setType("image/*");
        startActivityForResult(Intent.createChooser(qq, "Pilih Foto"), 100);
//        Intent intent = new Intent();
//        intent.setType("image/*");
//        intent.setAction(Intent.ACTION_GET_CONTENT);
//        startActivityForResult(Intent.createChooser(intent, "Pilih Foto"), Config.PICK_FILE_REQUEST);
    }

    private void batalBerlangganan() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Mohon Tunggu ...");
        pd.setCancelable(false);
        pd.show();
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.deleteMember(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                id_toko).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String pesan = jsonObject.optString("message");
                        if (pesan.equals("Berhasil delete")) {
                            Toast.makeText(TokoKitaActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                            cardMengikuti.setVisibility(View.GONE);
                            cardIkuti.setVisibility(View.VISIBLE);
                            cardMenunggu.setVisibility(View.GONE);
                        }
                        Toast.makeText(TokoKitaActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(TokoKitaActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void berlangganan() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Mohon Tunggu ...");
        pd.setCancelable(false);
        pd.show();
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.addMember(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                id_toko).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String pesan = jsonObject.optString("message");
                        if (pesan.equals("Silahkan menunggu acc toko untuk menjadi member")) {
                            cardIkuti.setVisibility(View.GONE);
                            cardMenunggu.setVisibility(View.VISIBLE);
                            cardMengikuti.setVisibility(View.GONE);
                            Toast.makeText(TokoKitaActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(TokoKitaActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(TokoKitaActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void awal() {
        sharedPref = new SharedPref(this);

        data = getIntent().getStringExtra("data");
        if (data.equals("tamu")) {
            id_toko = getIntent().getStringExtra("id");
            if (id_toko.equals(sharedPref.getIdUser())) {
                cardIkuti.setVisibility(View.GONE);
                cardMengikuti.setVisibility(View.GONE);
                imgCalonMember.setVisibility(View.VISIBLE);
                getUser();
                getData();
                setupViewPager(viewPager);
                viewpagertab2.setupWithViewPager(viewPager);
            } else {
                cardPesanan.setVisibility(View.GONE);
                cardTambah.setVisibility(View.GONE);
                imgCalonMember.setVisibility(View.GONE);
                getStatus();
                getDataTamu();
                setupViewPager2(viewPager);
                viewpagertab2.setupWithViewPager(viewPager);
            }
        } else if (data.equals("sendiri")) {
            id_toko = sharedPref.getIdUser();
            cardIkuti.setVisibility(View.GONE);
            cardMengikuti.setVisibility(View.GONE);
            imgCalonMember.setVisibility(View.VISIBLE);
            badge.setVisibility(View.VISIBLE);
            getUser();
            getData();
            setupViewPager(viewPager);
            viewpagertab2.setupWithViewPager(viewPager);
        } else {
            id_toko = sharedPref.getIdUser();
            cardIkuti.setVisibility(View.GONE);
            cardMengikuti.setVisibility(View.GONE);
            imgCalonMember.setVisibility(View.VISIBLE);
            badge.setVisibility(View.VISIBLE);
            getUser();
            getData();
            setupViewPager(viewPager);
            viewpagertab2.setupWithViewPager(viewPager);

            TabLayout.Tab tab = viewpagertab2.getTabAt(2);
            tab.select();
        }
        getMengikuti();
        getPengikut();

        mainButton();
    }

    private void getUser() {
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getCalonLangganan(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                sharedPref.getIdUser()).enqueue(new Callback<ArrayList<MemberUserModel>>() {
            @Override
            public void onResponse(Call<ArrayList<MemberUserModel>> call, Response<ArrayList<MemberUserModel>> response) {
                if (response.isSuccessful()) {
                    listUser2 = response.body();
                    if (listUser2.get(0).getMessage().equals("Berhasil Diload")) {
                        badge.setVisibility(View.VISIBLE);
                        badge.setNumber(listUser2.size());
                    } else {
                        badge.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<MemberUserModel>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getMengikuti() {
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.tokoYangDiikuti(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                id_toko).enqueue(new Callback<ArrayList<MemberTokoModel>>() {
            @Override
            public void onResponse(Call<ArrayList<MemberTokoModel>> call, Response<ArrayList<MemberTokoModel>> response) {
                if (response.isSuccessful()) {

                    listToko = response.body();
                    if (listToko.get(0).getSuccess() == 1) {
                        tvMengikuti.setText("Mengikuti " + listToko.size());
                    } else {
                        tvMengikuti.setText("Mengikuti " + "0");
                    }

                }
            }

            @Override
            public void onFailure(Call<ArrayList<MemberTokoModel>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getPengikut() {
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.orangYangMengikutiKita(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                id_toko).enqueue(new Callback<ArrayList<MemberUserModel>>() {
            @Override
            public void onResponse(Call<ArrayList<MemberUserModel>> call, Response<ArrayList<MemberUserModel>> response) {
                if (response.isSuccessful()) {
                    listUser = response.body();
                    if (listUser.get(0).getSuccess() == 1) {
                        tvPengikut.setText("Pengikut " + listUser.size());
                    } else {
                        tvPengikut.setText("Pengikut " + "0");
                    }

                }
            }

            @Override
            public void onFailure(Call<ArrayList<MemberUserModel>> call, Throwable t) {

                Toast.makeText(getApplicationContext(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getDataTamu() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Mohon Tunggu ...");
        pd.setCancelable(false);
        pd.show();
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getProfilById(id_toko).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONArray jsonArray = new JSONArray(response.body().string());
                        JSONObject jsonObject = jsonArray.optJSONObject(0);
                        String alamat = jsonObject.optString("USD_ADDRESS");
                        String kota = jsonObject.optString("USD_KOTA");
                        String namaToko = jsonObject.optString("USD_TOKO");
                        String foto = jsonObject.optString("USD_TOKO_FOTO");
                        String email = jsonObject.optString("US_EMAIL");
                        String no = jsonObject.optString("US_TELP");
                        if (foto.equals("")) {
                            imgToko.setImageResource(R.drawable.avatar);
                        } else {
                            Picasso.with(getApplicationContext())
                                    .load(Config.IMAGE_TOKO + foto)
                                    .into(imgToko);
                        }
//                        tvAlamatToko.setText(alamat + " " + kota);
                        tvNamaToko.setText("" + namaToko);
//                        tvNoToko.setText("" + no);
//                        tvEmailToko.setText("" + email);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(TokoKitaActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getData() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Mohon Tunggu ...");
        pd.setCancelable(false);
        pd.show();
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getProfil(sharedPref.getIdUser(), sharedPref.getSESSION()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONArray jsonArray = new JSONArray(response.body().string());
                        JSONObject jsonObject = jsonArray.optJSONObject(0);
                        String alamat = jsonObject.optString("USD_ADDRESS");
                        String kota = jsonObject.optString("USD_KOTA");
                        String namaToko = jsonObject.optString("USD_TOKO");
                        String foto = jsonObject.optString("USD_TOKO_FOTO");
                        String email = jsonObject.optString("US_EMAIL");
                        String no = jsonObject.optString("US_TELP");
                        if (foto.equals("")) {
                            imgToko.setImageResource(R.drawable.avatar);
                        } else {
                            Picasso.with(getApplicationContext())
                                    .load(Config.IMAGE_TOKO + foto)
                                    .into(imgToko);
                        }
//                        tvAlamatToko.setText(alamat + " " + kota);
                        tvNamaToko.setText("" + namaToko);
//                        tvNoToko.setText("" + no);
//                        tvEmailToko.setText("" + email);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(TokoKitaActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getStatus() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Mohon Tunggu ...");
        pd.setCancelable(false);
        pd.show();
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getStatusMember(sharedPref.getIdUser(),
                sharedPref.getSESSION(), id_toko).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONArray jsonArray = new JSONArray(response.body().string());
                        JSONObject jsonObject = jsonArray.optJSONObject(0);
                        String status = jsonObject.optString("MT_STATUS");
                        if (status.equals("Berlangganan")) {
                            cardIkuti.setVisibility(View.GONE);
                            cardMenunggu.setVisibility(View.GONE);
                            cardMengikuti.setVisibility(View.VISIBLE);
                        } else if (status.equals("Menunggu Acc")) {
                            cardIkuti.setVisibility(View.GONE);
                            cardMengikuti.setVisibility(View.GONE);
                            cardMenunggu.setVisibility(View.VISIBLE);
                            cardMenunggu.setClickable(false);
                            cardMenunggu.setEnabled(false);
                        } else {
                            cardIkuti.setVisibility(View.VISIBLE);
                            cardMengikuti.setVisibility(View.GONE);
                            cardMenunggu.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(TokoKitaActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void mainButton(){
        cardBarang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (data.equals("tamu")) {
                    id_toko = getIntent().getStringExtra("id");
                    if (id_toko.equals(sharedPref.getIdUser())) {
                        Intent intent = new Intent(getApplicationContext(), BarangTokoActivity.class);
                        intent.putExtra("data", "sendiri");
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(getApplicationContext(), BarangTokoActivity.class);
                        intent.putExtra("id", id_toko);
                        intent.putExtra("data", "tamu");
                        startActivity(intent);
                    }
                } else {
                    Intent intent = new Intent(getApplicationContext(), BarangTokoActivity.class);
                    intent.putExtra("data", "sendiri");
                    startActivity(intent);
                }
            }
        });

        cardTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), TambahActivity.class));
            }
        });

        cardPromo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (data.equals("tamu")) {
                    id_toko = getIntent().getStringExtra("id");
                    if (id_toko.equals(sharedPref.getIdUser())) {
                        Intent intent = new Intent(getApplicationContext(), PromoActivity.class);
                        intent.putExtra("data", "sendiri");
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(getApplicationContext(), PromoActivity.class);
                        intent.putExtra("id", id_toko);
                        intent.putExtra("data", "tamu");
                        startActivity(intent);
                    }
                } else {
                    Intent intent = new Intent(getApplicationContext(), PromoActivity.class);
                    intent.putExtra("data", "sendiri");
                    intent.putExtra("id", sharedPref.getIdUser());
                    startActivity(intent);
                }
            }
        });

        cardIkuti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                berlangganan();
            }
        });

        cardMengikuti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                batalBerlangganan();
            }
        });


        cardPesanan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                startActivity(new Intent(getApplicationContext(), PesananTokoActivity.class));
                startActivity(new Intent(getApplicationContext(), HistoryTransaksiTokoActivity.class));
            }
        });

        imgToko.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (data.equals("tamu")) {
                    id_toko = getIntent().getStringExtra("id");
                    if (id_toko.equals(sharedPref.getIdUser())) {
                        showFileChooserGalerry();
                    } else {

                    }
                } else {
                    showFileChooserGalerry();
                }
            }
        });

        tvMengikuti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), LanggananActivity.class);
                intent.putExtra("data", "toko");
                intent.putExtra("id", id_toko);
                startActivity(intent);
            }
        });

        tvPengikut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), LanggananActivity.class);
                intent.putExtra("data", "user");
                intent.putExtra("id", id_toko);
                startActivity(intent);
            }
        });

        imgCalonMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),CalonMemberActivity.class));
            }
        });
    }

    private void editFoto(File imageFile) {
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), imageFile);
        MultipartBody.Part body = MultipartBody.Part.createFormData("picture", imageFile.getName(), requestFile);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.gantiFotoToko(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                body).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String pesan = jsonObject.optString("message");
                        Toast.makeText(TokoKitaActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(TokoKitaActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(DataTokoFragment.newInstance("" + data, id_toko), "Data Toko");
        adapter.addFragment(new BarangTokoFragment("" + data, id_toko), "Barang");
        adapter.addFragment(new PesananTokoFragment(), "Pesanan");
        adapter.addFragment(new PromoTokoragment("" + data, id_toko), "Promo");
        viewPager.setAdapter(adapter);
    }

    private void setupViewPager2(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(DataTokoFragment.newInstance("" + data, id_toko), "Data Toko");
        adapter.addFragment(new BarangTokoFragment("" + data, id_toko), "Barang");
        adapter.addFragment(new PromoTokoragment("" + data, id_toko), "Promo");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
//            return null;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                Toast.makeText(this, "Gambar Tidak Ada", Toast.LENGTH_SHORT).show();
                return;

            }
//            else {
//                Toast.makeText(this, "Gambar Ada", Toast.LENGTH_SHORT).show();
//            }
            Uri selectImageUri = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor c = getContentResolver().query(selectImageUri, filePathColumn, null, null, null);
            if (c != null) {
                c.moveToFirst();

                int columnIndex = c.getColumnIndex(filePathColumn[0]);
                imagePath = c.getString(columnIndex);

//                Glide.with(this).load(new File(imagePath)).into(cvEditProfil);
                Glide.with(this).load(new File(imagePath)).into(imgToko);
                h = new File(imagePath).getName();
//                uploadImage();i
//                Toast.makeText(this, "Mbuh", Toast.LENGTH_SHORT).show();
                c.close();
                imageFile = new File(imagePath);

                editFoto(imageFile);

//                te.setVisibility(View.GONE);
//                imageVi.setVisibility(View.VISIBLE);
            } else {
//                te.setVisibility(View.VISIBLE);
//                imageVi.setVisibility(View.GONE);
                Toast.makeText(this, "Gambar Tidak Ada", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void initView() {
        tvNamaToko = (TextView) findViewById(R.id.tv_nama_toko);
        tvAlamatToko = (TextView) findViewById(R.id.tv_alamat_toko);
        btnLihatBarang = (Button) findViewById(R.id.btn_lihat_barang);
        btnTambahBarang = (Button) findViewById(R.id.btn_tambah_barang);
        btnAddMember = (Button) findViewById(R.id.btn_add_member);
        btnBatalMember = (Button) findViewById(R.id.btn_batal_member);
        imgToko = (CircleImageView) findViewById(R.id.img_toko);
        btnLihatPromo = (Button) findViewById(R.id.btn_lihat_promo);
        tvNoToko = (TextView) findViewById(R.id.tv_no_toko);
        tvEmailToko = (TextView) findViewById(R.id.tv_email_toko);
        btnLihatPesanan = (Button) findViewById(R.id.btn_lihat_pesanan);
        cardPromo = (CardView) findViewById(R.id.card_promo);
        cardIkuti = (LinearLayout) findViewById(R.id.card_ikuti);
        cardBarang = (CardView) findViewById(R.id.card_barang);
        cardTambah = (LinearLayout) findViewById(R.id.card_tambah);
        cardPesanan = (LinearLayout) findViewById(R.id.card_pesanan);
        cardMengikuti = (LinearLayout) findViewById(R.id.card_mengikuti);
        viewpagertab2 = (TabLayout) findViewById(R.id.viewpagertab2);
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        tvPengikut = (TextView) findViewById(R.id.tv_pengikut);
        tvMengikuti = (TextView) findViewById(R.id.tv_mengikuti);
        imgCalonMember = (ImageView) findViewById(R.id.img_calon_member);
        cardMenunggu = (LinearLayout) findViewById(R.id.card_menunggu);
        badge = (NotificationBadge) findViewById(R.id.badge);
    }
}
