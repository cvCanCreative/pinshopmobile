package ai.agusibrhim.design.topedc.Activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import ai.agusibrhim.design.topedc.Fragment.TransaksiFragment;
import ai.agusibrhim.design.topedc.Fragment.TransaksiTokoFragment;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.R;

public class HistoryTransaksiTokoActivity extends AppCompatActivity {

    private TabLayout viewpagertab2;
    private ViewPager viewPager;
    SharedPref sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_transaksi_toko);
        initView();

        awal();
    }

    private void awal(){
        sharedPref = new SharedPref(this);
        setupViewPager(viewPager);
        viewpagertab2.setupWithViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new TransaksiTokoFragment("PROSES_KIRIM"), "dikirim");
        adapter.addFragment(new TransaksiTokoFragment("TERIMA"), "selesai");
        adapter.addFragment(new TransaksiTokoFragment("BATAL"), "batal");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
//            return null;
        }
    }

    private void initView() {
        viewpagertab2 = (TabLayout) findViewById(R.id.viewpagertab2);
        viewPager = (ViewPager) findViewById(R.id.view_pager);
    }
}
