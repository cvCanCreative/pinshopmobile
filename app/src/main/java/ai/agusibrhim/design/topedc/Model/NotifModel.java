package ai.agusibrhim.design.topedc.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NotifModel implements Parcelable {

    @SerializedName("ID")
    @Expose
    private String iD;
    @SerializedName("TYPE")
    @Expose
    private String tYPE;
    @SerializedName("TITLE")
    @Expose
    private String tITLE;
    @SerializedName("CREATED_AT")
    @Expose
    private String cREATEDAT;
    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("message")
    @Expose
    private String message;

    protected NotifModel(Parcel in) {
        iD = in.readString();
        tYPE = in.readString();
        tITLE = in.readString();
        cREATEDAT = in.readString();
        success = in.readString();
        message = in.readString();
    }

    public static final Creator<NotifModel> CREATOR = new Creator<NotifModel>() {
        @Override
        public NotifModel createFromParcel(Parcel in) {
            return new NotifModel(in);
        }

        @Override
        public NotifModel[] newArray(int size) {
            return new NotifModel[size];
        }
    };

    public String getID() {
        return iD;
    }

    public void setID(String iD) {
        this.iD = iD;
    }

    public String getTYPE() {
        return tYPE;
    }

    public void setTYPE(String tYPE) {
        this.tYPE = tYPE;
    }

    public String getTITLE() {
        return tITLE;
    }

    public void setTITLE(String tITLE) {
        this.tITLE = tITLE;
    }

    public String getCREATEDAT() {
        return cREATEDAT;
    }

    public void setCREATEDAT(String cREATEDAT) {
        this.cREATEDAT = cREATEDAT;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(iD);
        parcel.writeString(tYPE);
        parcel.writeString(tITLE);
        parcel.writeString(cREATEDAT);
        parcel.writeString(success);
        parcel.writeString(message);
    }
}
