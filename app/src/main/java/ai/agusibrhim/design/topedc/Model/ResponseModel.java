package ai.agusibrhim.design.topedc.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ResponseModel {
    @SerializedName("data")
    @Expose
    private ArrayList<HistoryTransaksiBaru> data = null;
    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("message")
    @Expose
    private String message;

    public ArrayList<BankModel> bank = new ArrayList<>();

    public ArrayList<RekeningByIdModel> rekening = new ArrayList<>();

    public ArrayList<HistoryTransaksiBaru> getData() {
        return data;
    }

    public void setData(ArrayList<HistoryTransaksiBaru> data) {
        this.data = data;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
