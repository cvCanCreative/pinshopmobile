package ai.agusibrhim.design.topedc.Activity;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

import ai.agusibrhim.design.topedc.Adapter.PesananAdapter;
import ai.agusibrhim.design.topedc.Helper.HelperClass;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.Helper.dataRoom.AppDatabase;
import ai.agusibrhim.design.topedc.Helper.dataRoom.Pesanan;
import ai.agusibrhim.design.topedc.MainActivity;
import ai.agusibrhim.design.topedc.R;

public class KeranjangActivity extends AppCompatActivity {

    private SwipeRefreshLayout swipe;
    private RecyclerView rv;
    private AppDatabase db;
    PesananAdapter mAdapter;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<Pesanan> list = new ArrayList<>();
    private TextView tvBelumLogin;
    private LinearLayout lyKosong;
    SharedPref sharedPref;
    private TextView tvSubTotal;
    private Button btnBayar;
    int a = 0;
    private LinearLayout lyAda;
    String tipe = "keranjang";
    private ImageView imgClose;
    private LinearLayout divTotalbayar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_keranjang);
        initView();

        awal();
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (sharedPref.getSudahLogin()) {
                    swipe.setRefreshing(false);
                    getPesanan();
                } else {
                    tvBelumLogin.setVisibility(View.VISIBLE);
                    lyAda.setVisibility(View.GONE);
                }
            }
        });

        btnBayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), EditAlamatActivity.class);
                intent.putExtra("data", "bayar");
                sharedPref.savePrefString(SharedPref.TIPEBELI,"keranjang");
                startActivity(intent);
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), HomeLikeShopeeActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();
            }
        });
    }

    private void awal() {
        sharedPref = new SharedPref(this);
        db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "pesanandb").allowMainThreadQueries().build();

        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rv.setLayoutManager(layoutManager);

        if (sharedPref.getSudahLogin()) {
            getPesanan();
        } else {
            tvBelumLogin.setVisibility(View.VISIBLE);
            lyAda.setVisibility(View.GONE);
        }

    }

    private void getPesanan() {
        list.removeAll(list);
        a = 0;
        list.addAll(Arrays.asList(db.pesananDAO().lihatPesananById("" + sharedPref.getIdUser())));
//        Toast.makeText(this, ""+list.size(), Toast.LENGTH_SHORT).show();
        if (list.size() == 0) {
            lyKosong.setVisibility(View.VISIBLE);
            lyAda.setVisibility(View.GONE);
            divTotalbayar.setVisibility(View.GONE);

        } else {
            mAdapter = new PesananAdapter(list, KeranjangActivity.this, tipe,getApplicationContext());
            rv.setAdapter(mAdapter);

            for (int i = 0; i < list.size(); i++) {
                int nilai = Integer.parseInt(list.get(i).getSubHargaBarang());
                a = a + nilai;
            }

            tvSubTotal.setText("" + new HelperClass().convertRupiah(a));
        }
    }

    public void setHargaLagi(){
        getPesanan();
    }

    public static Intent getActIntent(Activity activity) {
        // kode untuk pengambilan Intent
        return new Intent(activity, KeranjangActivity.class);
    }

    private void initView() {
        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe);
        rv = (RecyclerView) findViewById(R.id.rv);
        tvBelumLogin = (TextView) findViewById(R.id.tv_belum_login);
        lyKosong = (LinearLayout) findViewById(R.id.ly_kosong);
        tvSubTotal = (TextView) findViewById(R.id.tv_sub_total);
        btnBayar = (Button) findViewById(R.id.btn_bayar);
        lyAda = (LinearLayout) findViewById(R.id.ly_ada);
        imgClose = (ImageView) findViewById(R.id.img_close);
        divTotalbayar = (LinearLayout) findViewById(R.id.div_totalbayar);
    }
}
