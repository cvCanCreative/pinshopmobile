package ai.agusibrhim.design.topedc.Helper.dataRoom;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

@Dao
public interface PesananDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertPesanan(Pesanan pesanan);

    @Query("SELECT * FROM tpesanan")
    Pesanan[] selectAllPesanan();

//        @Query("DELETE FROM tpesanan WHERE id_barang = :idPesanan")
//    void deletePesanan(String idPesanan);
    @Delete
    int deletePesanan(Pesanan pesanan);

    @Query("UPDATE tpesanan SET jumlah_barang =:jumlah, sub_harga_barang=:sub WHERE id_barang=:id")
    void updatePesanan(String jumlah, String sub, String id);

    @Query("SELECT * FROM tpesanan WHERE id_toko =:idToko")
    Pesanan[] lihatPesanan(String idToko);

    @Query("SELECT * FROM tpesanan WHERE id_user =:idUser")
    Pesanan[] lihatPesananById(String idUser);
}
