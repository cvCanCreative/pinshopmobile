package ai.agusibrhim.design.topedc;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ai.agusibrhim.design.topedc.Activity.AboutActivity;
import ai.agusibrhim.design.topedc.Activity.AccountActivity;
import ai.agusibrhim.design.topedc.Activity.AddKeluhanActivity;
import ai.agusibrhim.design.topedc.Activity.AllKategoriActivity;
import ai.agusibrhim.design.topedc.Activity.BuatTokoActivity;
import ai.agusibrhim.design.topedc.Activity.FavoritActivity;
import ai.agusibrhim.design.topedc.Activity.HelpActivity;
import ai.agusibrhim.design.topedc.Activity.HistoryActivity;
import ai.agusibrhim.design.topedc.Activity.HistoryTransaksiActivity;
import ai.agusibrhim.design.topedc.Activity.IntruksiActivity;
import ai.agusibrhim.design.topedc.Activity.KeranjangActivity;
import ai.agusibrhim.design.topedc.Activity.LihatRekeningActivity;
import ai.agusibrhim.design.topedc.Activity.ListBankActivity;
import ai.agusibrhim.design.topedc.Activity.ListChatActivity;
import ai.agusibrhim.design.topedc.Activity.LoginActivity;
import ai.agusibrhim.design.topedc.Activity.NotifActivity;
import ai.agusibrhim.design.topedc.Activity.PromoActivity;
import ai.agusibrhim.design.topedc.Activity.SearchActivity;
import ai.agusibrhim.design.topedc.Activity.TokoKitaActivity;
import ai.agusibrhim.design.topedc.Adapter.CatagoryAdapter;
import ai.agusibrhim.design.topedc.Adapter.ListProductAdapter;
import ai.agusibrhim.design.topedc.Adapter.PromoHariIniAdapter;
import ai.agusibrhim.design.topedc.Helper.Config;
import ai.agusibrhim.design.topedc.Helper.HelperClass;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.Model.ListCategory;
import ai.agusibrhim.design.topedc.Model.ListProduct;
import ai.agusibrhim.design.topedc.Model.PromoModel;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, ViewPagerEx.OnPageChangeListener, BaseSliderView.OnSliderClickListener, GoogleApiClient.OnConnectionFailedListener {
    Toolbar toolbar;
    private LinearLayout llroot;
    private EditText searchbox;
    private NavigationView nav_view;
    private CardView search_bar;
    private SwipeRefreshLayout swipe_refresh;
    private View parent_view;
    private FloatingActionButton fab;
    HashMap<String, String> url_maps;
    HashMap<String, Integer> file_maps;
    SharedPref sharedPref;

    View card1;
    RecyclerView rv1;
    List<ListCategory> mItemCategory = new ArrayList<>();
    List<ListProduct> mItemProduct = new ArrayList<>();

    ArrayList<ListCategory> list = new ArrayList<>();
    CatagoryAdapter catagoryAdapter;
    RecyclerView.LayoutManager linearLayoutManager;

    ArrayList<PromoModel> listPromo = new ArrayList<>();
    ArrayList<ListProduct> listProduk = new ArrayList<>();
    ListProductAdapter produkAdapter;

    String s;
    private SliderLayout sliderSlider;
    private RecyclerView rv;
    private RecyclerView rvTerbaru;
    private LinearLayout lySaldo;
    private TextView tvSaldo;
    private ProgressBar pdSaldo;
    private TextView tvTambahSaldo;
    private ProgressBar pdKategori;
    private ProgressBar pdProduk;
    NavigationView navigationView;
    private TextView tvLihatPromo;
    GoogleApiClient googleApiClient;
    private ImageView imgChat;
    private TextView tvAllKategori;
    private RecyclerView rvPromo;
    PromoHariIniAdapter promoHariIniAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        initView();
        setSupportActionBar(toolbar);

//		setupSearchBox(searchbox);

        Permission3();
        sharedPref = new SharedPref(this);
//        Toast.makeText(this, "ini "+sharedPref.getTOKO(), Toast.LENGTH_SHORT).show();
//        Toast.makeText(this, ""+sharedPref.getSESSION(), Toast.LENGTH_SHORT).show();
        // launch instruction when first launch
        if (sharedPref.getPertamaMasuk()) {
            startActivity(new Intent(this, IntruksiActivity.class));
            sharedPref.savePrefBoolean(SharedPref.PERTAMA_MASUK, false);
        }

        initComponent();
        awal();
        swipe_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe_refresh.setRefreshing(false);
                getKategori();
                getDataAwal();
                getPromo();

                if (sharedPref.getSudahLogin()) {
                    lySaldo.setVisibility(View.VISIBLE);
                    saldo();
                    getToken();
                    profil();
                } else {
                    lySaldo.setVisibility(View.GONE);
                }
            }
        });

        tvTambahSaldo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ListBankActivity.class));
            }
        });

        tvLihatPromo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), PromoActivity.class));
            }
        });

        tvSaldo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), LihatRekeningActivity.class);
                intent.putExtra("data", "tarik");
                startActivity(intent);
            }
        });

        imgChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ListChatActivity.class));
            }
        });

        tvAllKategori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), AllKategoriActivity.class));
            }
        });
    }

    void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        searchbox = (EditText) findViewById(R.id.search_box);
        llroot = (LinearLayout) findViewById(R.id.mainLinearLayout1);

        card1 = getLayoutInflater().inflate(R.layout.card, null);
        rv1 = (RecyclerView) card1.findViewById(R.id.cardListView1);
        sliderSlider = (SliderLayout) findViewById(R.id.sliderSlider);
        rv = (RecyclerView) findViewById(R.id.rv);
        rvTerbaru = (RecyclerView) findViewById(R.id.rv_terbaru);
        lySaldo = (LinearLayout) findViewById(R.id.ly_saldo);
        tvSaldo = (TextView) findViewById(R.id.tv_saldo);
        pdSaldo = (ProgressBar) findViewById(R.id.pd_saldo);
        tvTambahSaldo = (TextView) findViewById(R.id.tv_tambah_saldo);
        pdKategori = (ProgressBar) findViewById(R.id.pd_kategori);
        pdProduk = (ProgressBar) findViewById(R.id.pd_produk);
        tvLihatPromo = (TextView) findViewById(R.id.tv_lihat_promo);
        imgChat = (ImageView) findViewById(R.id.img_chat);
        tvAllKategori = (TextView) findViewById(R.id.tv_all_kategori);
        rvPromo = (RecyclerView) findViewById(R.id.rv_promo);
    }

    private void profil() {

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getProfil(sharedPref.getIdUser(), sharedPref.getSESSION()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {

                    try {
                        JSONArray jsonArray = new JSONArray(response.body().string());
                        JSONObject jsonObject = jsonArray.optJSONObject(0);

                        String namaToko = jsonObject.optString("USD_TOKO");

                        sharedPref.savePrefString(SharedPref.TOKO, namaToko);
//                        Toast.makeText(getApplicationContext(), "ini "+sharedPref.getTOKO(), Toast.LENGTH_SHORT).show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(MainActivity.this, "gagal", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Toast.makeText(MainActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void awal() {
        linearLayoutManager = new GridLayoutManager(this, 5);
//        linearLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        rv.setLayoutManager(linearLayoutManager);

        rvTerbaru.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        rvPromo.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));

        //        profil();
        getKategori();
        getDataAwal();
        getPromo();


        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        googleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this, this).addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions).build();

        if (sharedPref.getSudahLogin()) {
            lySaldo.setVisibility(View.VISIBLE);
            saldo();
            getToken();
        } else {
            lySaldo.setVisibility(View.GONE);
        }
        navigationMenu();
    }

    private void getToken() {
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getToken(sharedPref.getIdUser(),
                sharedPref.getSESSION()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String pesan = jsonObject.optString("message");
                        if (pesan.equals("Berhasil")) {

                        } else {
                            sharedPref.savePrefString(SharedPref.ID_USER, "");
                            sharedPref.savePrefString(SharedPref.SESSION, "");
                            sharedPref.savePrefBoolean(SharedPref.SUDAH_LOGIN, false);
                            new AlertDialog.Builder(MainActivity.this)
                                    .setMessage("" + pesan)
                                    .setCancelable(false)
                                    .setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            sharedPref.savePrefString(SharedPref.ID_USER, "");
                                            sharedPref.savePrefString(SharedPref.SESSION, "");
                                            sharedPref.savePrefBoolean(SharedPref.SUDAH_LOGIN, false);
                                            startActivity(new Intent(getApplicationContext(), MainActivity.class)
                                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                            finish();
                                        }
                                    })
                                    .show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(MainActivity.this, "error cek token", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(MainActivity.this, "error cek token " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getPromo() {
        listPromo.removeAll(listPromo);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getPromo().enqueue(new Callback<ArrayList<PromoModel>>() {
            @Override
            public void onResponse(Call<ArrayList<PromoModel>> call, Response<ArrayList<PromoModel>> response) {
                if (response.isSuccessful()) {
                    swipe_refresh.setRefreshing(false);
                    listPromo = response.body();
//                    Toast.makeText(MainActivity.this, ""+listPromo.get(0).getSLTITLE(), Toast.LENGTH_SHORT).show();
                    Slider(listPromo);
                    promoHariIniAdapter = new PromoHariIniAdapter(listPromo,MainActivity.this);
                    rvPromo.setAdapter(promoHariIniAdapter);

                }
            }

            @Override
            public void onFailure(Call<ArrayList<PromoModel>> call, Throwable t) {
                swipe_refresh.setRefreshing(false);
                Toast.makeText(getApplicationContext(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void Slider(ArrayList<PromoModel> listPromo) {
        sliderSlider.removeAllSliders();
        for (PromoModel img : listPromo) {
            if (!img.getSLTITLE().isEmpty()) {
                TextSliderView textSliderView = new TextSliderView(getApplicationContext());
                textSliderView.image(Config.IMAGE_Promo + img.getSLFILE())
                        .setScaleType(BaseSliderView.ScaleType.Fit).setOnSliderClickListener(MainActivity.this);
                ;
                textSliderView.bundle(new Bundle());
                sliderSlider.addSlider(textSliderView);
            }
        }
    }

    private void getKategori() {
        pdKategori.setVisibility(View.VISIBLE);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getDataCatagory().enqueue(new Callback<ArrayList<ListCategory>>() {
            @Override
            public void onResponse(Call<ArrayList<ListCategory>> call, Response<ArrayList<ListCategory>> response) {
                if (response.isSuccessful()) {
                    pdKategori.setVisibility(View.GONE);
                    list = response.body();
                    catagoryAdapter = new CatagoryAdapter(list, getApplicationContext(), "home");
                    rv.setAdapter(catagoryAdapter);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ListCategory>> call, Throwable t) {
                pdKategori.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getDataAwal() {
        pdProduk.setVisibility(View.VISIBLE);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getProductByCatagori("2", 1).enqueue(new Callback<ArrayList<ListProduct>>() {
            @Override
            public void onResponse(Call<ArrayList<ListProduct>> call, Response<ArrayList<ListProduct>> response) {
                if (response.isSuccessful()) {
                    pdProduk.setVisibility(View.GONE);
                    listProduk = response.body();
                    produkAdapter = new ListProductAdapter(listProduk, getApplicationContext());
                    rvTerbaru.setAdapter(produkAdapter);
                    produkAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ListProduct>> call, Throwable t) {
                pdProduk.setVisibility(View.GONE);
                Toast.makeText(MainActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void saldo() {
        pdSaldo.setVisibility(View.VISIBLE);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getSlado(sharedPref.getIdUser(), sharedPref.getSESSION()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pdSaldo.setVisibility(View.GONE);
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String pesan = jsonObject.optString("message");
                        String saldo = jsonObject.optString("SA_SALDO");
//                        Toast.makeText(MainActivity.this, ""+pesan, Toast.LENGTH_SHORT).show();
                        if (pesan.equals("Berhasil")) {
                            if (saldo.equals("")) {
                                tvSaldo.setText("" + new HelperClass().convertRupiah(Integer.parseInt("0")));
                                sharedPref.savePrefString(SharedPref.SALDO, saldo);
                            } else {
                                tvSaldo.setText("" + new HelperClass().convertRupiah(Integer.parseInt(saldo)));
                                sharedPref.savePrefString(SharedPref.SALDO, saldo);
                            }
                        } else {
                            tvSaldo.setText("" + new HelperClass().convertRupiah(Integer.parseInt("0")));
                            sharedPref.savePrefString(SharedPref.SALDO, saldo);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pdSaldo.setVisibility(View.GONE);
                Toast.makeText(MainActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    void navigationMenu() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);

        Menu menu = navigationView.getMenu();
        MenuItem navLogin = menu.findItem(R.id.nav_login);
        MenuItem navToko = menu.findItem(R.id.nav_toko);

        if (sharedPref.getSudahLogin()) {
            navLogin.setTitle("Logout");
            navLogin.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    if (sharedPref.getTipeLogin().equals("manual")) {
                        new AlertDialog.Builder(MainActivity.this)
                                .setMessage("Apakah Anda ingin Keluar?")
                                .setCancelable(false)
                                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        sharedPref.savePrefString(SharedPref.ID_USER, "");
                                        sharedPref.savePrefString(SharedPref.SESSION, "");
                                        sharedPref.savePrefString(SharedPref.SALDO, "");
                                        sharedPref.savePrefString(SharedPref.USERNAME, "");
                                        sharedPref.savePrefString(SharedPref.TOKO, "");
                                        sharedPref.savePrefBoolean(SharedPref.SUDAH_LOGIN, false);
                                        startActivity(new Intent(getApplicationContext(), MainActivity.class)
                                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                        finish();
                                    }
                                })
                                .setNegativeButton("Tidak", null)
                                .show();
                    } else if (sharedPref.getTipeLogin().equals("gmail")) {
                        new AlertDialog.Builder(MainActivity.this)
                                .setMessage("Apakah Anda ingin Keluar?")
                                .setCancelable(false)
                                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
                                            @Override
                                            public void onResult(@NonNull Status status) {
//                                                Toast.makeText(MainActivity.this, ""+status, Toast.LENGTH_SHORT).show();
                                                sharedPref.savePrefString(SharedPref.ID_USER, "");
                                                sharedPref.savePrefString(SharedPref.SESSION, "");
                                                sharedPref.savePrefString(SharedPref.SALDO, "");
                                                sharedPref.savePrefString(SharedPref.USERNAME, "");
                                                sharedPref.savePrefString(SharedPref.TOKO, "");
                                                sharedPref.savePrefBoolean(SharedPref.SUDAH_LOGIN, false);
                                                startActivity(new Intent(getApplicationContext(), MainActivity.class)
                                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                                finish();
                                            }
                                        });
                                    }
                                })
                                .setNegativeButton("Tidak", null)
                                .show();
                    } else if (sharedPref.getTipeLogin().equals("facebook")) {
                        new AlertDialog.Builder(MainActivity.this)
                                .setMessage("Apakah Anda ingin Keluar?")
                                .setCancelable(false)
                                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        LoginManager.getInstance().logOut();
                                        sharedPref.savePrefString(SharedPref.ID_USER, "");
                                        sharedPref.savePrefString(SharedPref.SESSION, "");
                                        sharedPref.savePrefString(SharedPref.SALDO, "");
                                        sharedPref.savePrefString(SharedPref.USERNAME, "");
                                        sharedPref.savePrefString(SharedPref.TOKO, "");
                                        sharedPref.savePrefBoolean(SharedPref.SUDAH_LOGIN, false);
                                        startActivity(new Intent(getApplicationContext(), MainActivity.class)
                                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                        finish();
                                    }
                                })
                                .setNegativeButton("Tidak", null)
                                .show();
                    }
                    return false;
                }
            });

            if (sharedPref.getTOKO().equals("")) {
                navToko.setTitle("Buat toko");
                navToko.setIcon(R.drawable.buattoko_ic_pinshop);

                navToko.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        new AlertDialog.Builder(MainActivity.this)
                                .setMessage("Anda Belum Membuat Toko, Apakah Anda ingin membuatnya ?")
                                .setCancelable(false)
                                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Intent intent = new Intent(getApplicationContext(), BuatTokoActivity.class);
                                        startActivity(intent);
                                    }
                                })
                                .setNegativeButton("Tidak", null)
                                .show();
                        return false;
                    }
                });
            } else {
                navToko.setTitle("Toko Saya");
                navToko.setIcon(R.drawable.tokosaya_ic_pinshop);
                navToko.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        Intent intent = new Intent(getApplicationContext(), TokoKitaActivity.class);
                        intent.putExtra("data", "sendiri");
                        startActivity(intent);
                        return false;
                    }
                });
            }
        } else {
            navLogin.setTitle("Login");
            navLogin.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                    return false;
                }
            });

            if (sharedPref.getTOKO().equals("")) {
                navToko.setTitle("Buat toko");
                navToko.setIcon(R.drawable.buattoko_ic_pinshop);

                navToko.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                        return false;
                    }
                });
            } else {
                navToko.setTitle("Toko Saya");
                navToko.setIcon(R.drawable.tokosaya_ic_pinshop);
                navToko.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                        return false;
                    }
                });
            }
        }
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.nav_account) {
            if (sharedPref.getSudahLogin()) {
                Intent intent = new Intent(MainActivity.this, AccountActivity.class);
                startActivity(intent);
            } else {
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            }
        } else if (id == R.id.nav_instruction) {
            startActivity(new Intent(getApplicationContext(), HelpActivity.class));
        } else if (id == R.id.nav_about) {
            startActivity(new Intent(getApplicationContext(), AboutActivity.class));
        } else if (id == R.id.nav_cart) {
            if (sharedPref.getSudahLogin()) {
                startActivity(new Intent(getApplicationContext(), KeranjangActivity.class));
            } else {
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            }
        } else if (id == R.id.nav_notif) {
            if (sharedPref.getSudahLogin()) {
                startActivity(new Intent(getApplicationContext(), NotifActivity.class));
            } else {
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            }
        } else if (id == R.id.nav_wish) {
            if (sharedPref.getSudahLogin()) {
                startActivity(new Intent(getApplicationContext(), FavoritActivity.class));
            } else {
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            }
        } else if (id == R.id.nav_history_saldo) {
            if (sharedPref.getSudahLogin()) {
                startActivity(new Intent(getApplicationContext(), HistoryActivity.class));
            } else {
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            }
        } else if (id == R.id.nav_history) {
            if (sharedPref.getSudahLogin()) {
                Intent intent = new Intent(getApplicationContext(), HistoryTransaksiActivity.class);
                intent.putExtra("data", "drawer");
                startActivity(intent);
            } else {
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            }
        } else if (id == R.id.nav_keluhan) {
            if (sharedPref.getSudahLogin()) {
                startActivity(new Intent(getApplicationContext(), AddKeluhanActivity.class));
            } else {
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            }
        }
        return true;
    }


    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    public class SimpleDividerItemDecoration extends RecyclerView.ItemDecoration {
        private Drawable mDivider;

        public SimpleDividerItemDecoration(Context context) {
            mDivider = context.getResources().getDrawable(R.drawable.line_divider);
        }

        @Override
        public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
            int left = parent.getPaddingLeft();
            int right = parent.getWidth() - parent.getPaddingRight();
            int childCount = parent.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View child = parent.getChildAt(i);
                RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();
                int top = child.getBottom() + params.bottomMargin;
                int bottom = top + mDivider.getIntrinsicHeight();
                mDivider.setBounds(left, top, right, bottom);
                mDivider.draw(c);
            }
        }
    }

    public static Drawable setTint(Drawable d, int color) {
        Drawable wrappedDrawable = DrawableCompat.wrap(d);
        DrawableCompat.setTint(wrappedDrawable, color);
        return wrappedDrawable;
    }

    private void initComponent() {
        parent_view = findViewById(R.id.parent_view);
        search_bar = (CardView) findViewById(R.id.search_bar);
        swipe_refresh = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        NestedScrollView nested_content = (NestedScrollView) findViewById(R.id.nested_content);
        nested_content.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY < oldScrollY) { // up
                    animateFab(false);
                    animateSearchBar(false);
                }
                if (scrollY > oldScrollY) { // down
                    animateFab(true);
                    animateSearchBar(true);
                }
            }
        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sharedPref.getSudahLogin()) {
                    Intent i = new Intent(getApplicationContext(), KeranjangActivity.class);
                    startActivity(i);
                } else {
//                    new AlertDialog.Builder(MainActivity.this)
//                            .setMessage("Anda Harus Login Terlebih Dahulu")
//                            .setCancelable(false)
//                            .setNegativeButton("Oke", null)
//                            .show();
                    Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(i);
                }
//                Toast.makeText(MainActivity.this, "belum", Toast.LENGTH_SHORT).show();
            }
        });

        // on swipe list
        swipe_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe_refresh.setRefreshing(false);
            }
        });

        ((ImageButton) findViewById(R.id.action_search)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//				ActivitySearch.navigate(ActivityMain.this);
//                Toast.makeText(MainActivity.this, "Commong Soon", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getApplicationContext(), SearchActivity.class);
                intent.putExtra("data", "semua");
                startActivity(intent);
            }
        });
    }

    boolean isFabHide = false;

    private void animateFab(final boolean hide) {
        if (isFabHide && hide || !isFabHide && !hide) return;
        isFabHide = hide;
        int moveY = hide ? (2 * fab.getHeight()) : 0;
        fab.animate().translationY(moveY).setStartDelay(100).setDuration(300).start();
    }

    boolean isSearchBarHide = false;

    private void animateSearchBar(final boolean hide) {
        if (isSearchBarHide && hide || !isSearchBarHide && !hide) return;
        isSearchBarHide = hide;
        int moveY = hide ? -(2 * search_bar.getHeight()) : 0;
        search_bar.animate().translationY(moveY).setStartDelay(100).setDuration(300).start();
    }

    private void Permission3() {
        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    android.Manifest.permission.CAMERA)) {
                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.CAMERA},
                        1);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }

}
