package ai.agusibrhim.design.topedc.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RekeningByIdModel implements Parcelable {

    public String ID_REKENING;
    public String ID_BANK;
    public String ID_USER;
    public String RK_IMAGE;
    public String RK_BANK;
    public String RK_NOMOR;
    public String RK_NAME;
    public String CREATED_AT;
    public String UPDATE_AT;
    public String BK_NAME;
    public String BK_IMAGE;
    public String UPDATED_AT;

    protected RekeningByIdModel(Parcel in) {
        ID_REKENING = in.readString();
        ID_BANK = in.readString();
        ID_USER = in.readString();
        RK_IMAGE = in.readString();
        RK_BANK = in.readString();
        RK_NOMOR = in.readString();
        RK_NAME = in.readString();
        CREATED_AT = in.readString();
        UPDATE_AT = in.readString();
        BK_NAME = in.readString();
        BK_IMAGE = in.readString();
        UPDATED_AT = in.readString();
    }

    public static final Creator<RekeningByIdModel> CREATOR = new Creator<RekeningByIdModel>() {
        @Override
        public RekeningByIdModel createFromParcel(Parcel in) {
            return new RekeningByIdModel(in);
        }

        @Override
        public RekeningByIdModel[] newArray(int size) {
            return new RekeningByIdModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(ID_REKENING);
        parcel.writeString(ID_BANK);
        parcel.writeString(ID_USER);
        parcel.writeString(RK_IMAGE);
        parcel.writeString(RK_BANK);
        parcel.writeString(RK_NOMOR);
        parcel.writeString(RK_NAME);
        parcel.writeString(CREATED_AT);
        parcel.writeString(UPDATE_AT);
        parcel.writeString(BK_NAME);
        parcel.writeString(BK_IMAGE);
        parcel.writeString(UPDATED_AT);
    }
}
