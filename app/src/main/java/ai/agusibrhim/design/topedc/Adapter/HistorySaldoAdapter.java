package ai.agusibrhim.design.topedc.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Helper.HelperClass;
import ai.agusibrhim.design.topedc.Model.HistorySaldo;
import ai.agusibrhim.design.topedc.R;

public class HistorySaldoAdapter extends RecyclerView.Adapter<HistorySaldoAdapter.Holder>{

    ArrayList<HistorySaldo> list;
    Context context;

    public HistorySaldoAdapter(ArrayList<HistorySaldo> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_history_saldo,parent,false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {

        String WAITING = "Menunggu Bukti Transfer";
        String PROSES = "Sedang di Verifikasi";
        String ACC = "Transaksi Berhasil";
        String INVALID = "Tansaksi Gagal";

        holder.tvKode.setText(list.get(position).getHSATRXSALDO());
        holder.tvTipe.setText(list.get(position).getHSATYPE());
        holder.tvJumlah.setText(""+ new HelperClass().convertRupiah(Integer.valueOf(list.get(position).getHSASALDO())));

        if (list.get(position).getHSASTATUS().equals("WAITING_IMG")){
            holder.tvStatus.setText(WAITING);
        } else if (list.get(position).getHSASTATUS().equals("PROSES")) {
            holder.tvStatus.setText(PROSES);
        } else if (list.get(position).getHSASTATUS().equals("ACC")) {
            holder.tvStatus.setText(ACC);
        } else {
            holder.tvStatus.setText(INVALID);
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Holder extends RecyclerView.ViewHolder{

        TextView tvKode,tvTipe,tvStatus,tvJumlah;
        public Holder(View itemView) {
            super(itemView);

            tvKode = itemView.findViewById(R.id.tv_kode);
            tvTipe = itemView.findViewById(R.id.tv_tipe);
            tvStatus = itemView.findViewById(R.id.tv_status);
            tvJumlah = itemView.findViewById(R.id.tv_jumlah);
        }
    }
}
