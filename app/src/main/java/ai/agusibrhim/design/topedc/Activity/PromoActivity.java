package ai.agusibrhim.design.topedc.Activity;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Adapter.PromoAdapter;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.Model.PromoAktif;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PromoActivity extends AppCompatActivity {

    private RecyclerView rv;
    private ImageView imgNo;
    private LinearLayout lyKosong;
    SharedPref sharedPref;
    RecyclerView.LayoutManager layoutManager;
    PromoAdapter mAdapter;
    private SwipeRefreshLayout swipe;
    ArrayList<PromoAktif> list = new ArrayList<>();
    String data,id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promo);
        initView();

        awal();
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (data.equals("tamu")){
                    getPromo(id);
                }else {
                    getPromoToko();
                }
            }
        });

    }

    private void awal() {
        sharedPref = new SharedPref(this);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rv.setLayoutManager(layoutManager);
        data = getIntent().getStringExtra("data");
        id = getIntent().getStringExtra("id");
        if (data.equals("tamu")){
            getPromo(id);
        }else {
            getPromoToko();
        }
    }

    private void getPromo(String id){
        swipe.setRefreshing(true);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getPromoAktif(id).enqueue(new Callback<ArrayList<PromoAktif>>() {
            @Override
            public void onResponse(Call<ArrayList<PromoAktif>> call, Response<ArrayList<PromoAktif>> response) {
                if (response.isSuccessful()){
                    swipe.setRefreshing(false);
                    list = response.body();
                    if (list.get(0).getMessage().equals("Berhasil Diload")){
                        mAdapter = new PromoAdapter(list,getApplicationContext(),data);
                        rv.setAdapter(mAdapter);
                    }else {
                        lyKosong.setVisibility(View.VISIBLE);
                        rv.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<PromoAktif>> call, Throwable t) {
                swipe.setRefreshing(false);
                Toast.makeText(PromoActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getPromoToko(){
        swipe.setRefreshing(true);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getPromoToko(sharedPref.getIdUser(),
                sharedPref.getSESSION()).enqueue(new Callback<ArrayList<PromoAktif>>() {
            @Override
            public void onResponse(Call<ArrayList<PromoAktif>> call, Response<ArrayList<PromoAktif>> response) {
                if (response.isSuccessful()){
                    swipe.setRefreshing(false);
                    list = response.body();
                    if (list.get(0).getMessage().equals("Berhasil Diload")){
                        mAdapter = new PromoAdapter(list,getApplicationContext(),data);
                        rv.setAdapter(mAdapter);
                    }else {
                        lyKosong.setVisibility(View.VISIBLE);
                        rv.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<PromoAktif>> call, Throwable t) {
                swipe.setRefreshing(false);
                Toast.makeText(PromoActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initView() {
        rv = (RecyclerView) findViewById(R.id.rv);
        imgNo = (ImageView) findViewById(R.id.img_no);
        lyKosong = (LinearLayout) findViewById(R.id.ly_kosong);
        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe);
    }
}
