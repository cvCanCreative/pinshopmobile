package ai.agusibrhim.design.topedc.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Model.HistoryPembayaran;
import ai.agusibrhim.design.topedc.R;

public class HistoryPembayaranAdapter extends RecyclerView.Adapter<HistoryPembayaranAdapter.Helper> {

    ArrayList<HistoryPembayaran> list;
    Context context;

    public HistoryPembayaranAdapter(ArrayList<HistoryPembayaran> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public Helper onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_history_saldo,parent,false);
        return new Helper(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Helper holder, int position) {
        holder.tvKode.setText(list.get(position).getTITLE());
        holder.tvTipe.setText(list.get(position).getTYPE());
        holder.tvStatus.setText(list.get(position).getCREATEDAT());
        holder.tvJumlah.setText("");
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Helper extends RecyclerView.ViewHolder{

        TextView tvKode,tvTipe,tvStatus,tvJumlah;
        public Helper(View itemView) {
            super(itemView);

            tvKode = itemView.findViewById(R.id.tv_kode);
            tvTipe = itemView.findViewById(R.id.tv_tipe);
            tvStatus = itemView.findViewById(R.id.tv_status);
            tvJumlah = itemView.findViewById(R.id.tv_jumlah);
        }
    }
}
