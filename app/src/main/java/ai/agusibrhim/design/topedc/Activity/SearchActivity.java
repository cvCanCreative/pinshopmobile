package ai.agusibrhim.design.topedc.Activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ai.agusibrhim.design.topedc.Adapter.BarangTokoAdapter;
import ai.agusibrhim.design.topedc.Adapter.ListProductAdapter;
import ai.agusibrhim.design.topedc.Adapter.ListProdukAdapterIndex;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.Model.BarangTokoModel;
import ai.agusibrhim.design.topedc.Model.ListProduct;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class SearchActivity extends AppCompatActivity {

    private EditText edtSearch;
    private Button btnSearch;
    private RecyclerView rv;
    private LinearLayout lyKosong;
    private SwipeRefreshLayout swipe;
    SharedPref sharedPref;
    String data, idToko, IdKategori;
    RecyclerView.LayoutManager layoutManager;
    ListProdukAdapterIndex semuaAdapter;
    ListProductAdapter adapter;
    BarangTokoAdapter tokoAdapter;
    ArrayList<ListProduct> listSemua = new ArrayList<>();
    ArrayList<BarangTokoModel> listToko = new ArrayList<>();
    private LinearLayout lySearch;
    private TextView tvNo;
    private Spinner spnFilter;
    private ImageView imgSearch;
    int indexSearch = 1;
    int indexSearchPerKat = 1;
    int indexSearchKatFilter = 1;
    int indexSearchAllFilter = 1;
    int indexFilterToko = 1;
    int indexKotaAll = 1;
    int indexKotaKat = 1;
    int indexMinMaxKat = 1;
    int indexMinMaxToko = 1;
    int indexMinMaxTamu = 1;
    int indexMinMaxAll = 1;
    String select ="biasa";
    private ImageView imgClose;
    BottomSheetBehavior behavior;
    private BottomSheetDialog mBottomSheetDialog;
    private LinearLayout lyDikirim;
    private LinearLayout lyFilter;
    private LinearLayout lyUrutkan;
    String pilihan="ALL";
    List<String> listProvinsi = new ArrayList<>();
    List<String> listIdProvinsi = new ArrayList<>();
    List<String> setKota = new ArrayList<>();
    List<String> listIdKota = new ArrayList<>();
    String idKota;
    String pilih = "1000";
    String min,max;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        initView();
        awal();


        imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cariData();
            }
        });

        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                cariData();
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        lyUrutkan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showBottomSheetDialog();
            }
        });

        lyDikirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sheetDikirim();
            }
        });

        lyFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sheetFilter();
            }
        });

        View bottomSheet = findViewById(R.id.bottom_sheet);
        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                // React to state change
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });
    }

    private void cariData() {
        if (data.equals("semua")) {
            if (!validasi()) {
                return;
            }
            if (select.equals("biasa")){
                indexSearch = 1;
                getSemua(indexSearch);
            }else if (select.equals("urut")){
                if (pilihan.equals("ALL")) {
                    indexSearch = 1;
                    getSemua(indexSearch);
                } else if (pilihan.equals("NEW")) {
                    indexSearchAllFilter = 1;
                    getFilterAll(indexSearchAllFilter);
                } else {
                    indexSearchAllFilter = 1;
                    getFilterAll(indexSearchAllFilter);
                }
            }else if (select.equals("minmax")){
                indexMinMaxAll = 1;
                filterMinMaxAll(indexMinMaxAll);
            }else {

            }

        } else if (data.equals("kategori")) {
            if (!validasi()) {
                return;
            }
            if (select.equals("biasa")){
                indexSearchPerKat = 1;
                getKategori(indexSearchPerKat);
            }else if (select.equals("urut")){
                if (pilihan.equals("ALL")) {
                    indexSearchPerKat = 1;
                    getKategori(indexSearchPerKat);
                } else {
                    indexSearchKatFilter = 1;
                    getFilterKategori(indexSearchKatFilter);
                }
            }else if (select.equals("minmax")){
                indexMinMaxKat = 1;
                filterMinMaxKat(indexMinMaxKat);
            }else {
                indexKotaKat = 1;
                filterKota(idKota,indexKotaKat);
            }


        } else if (data.equals("toko")) {
            if (!validasi()) {
                return;
            }
            if (select.equals("biasa")){
                getToko();
            }else if (select.equals("urut")){
                if (pilihan.toString().equals("ALL")) {
                    getToko();
                } else {
                    indexFilterToko = 1;
                    getFilterToko(indexFilterToko);
                }
            }else if (select.equals("minmax")){
                filterMinMaxToko();
            }

        } else if (data.equals("tamu")) {
            if (!validasi()) {
                return;
            }

            if (select.equals("biasa")){
                getToko2();
            }else if (select.equals("urut")){
                if (pilihan.equals("ALL")) {
                    getToko2();
                } else {
                    indexFilterToko = 1;
                    getFilterToko(indexFilterToko);
                }
            }else if (select.equals("minmax")){
                filterMinMaxToko();
            }

        }
    }

    private void setSpnProvinsi() {
        listProvinsi.removeAll(listProvinsi);
        listIdProvinsi.removeAll(listIdProvinsi);

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getProvinsi().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        JSONObject jsonObject1 = jsonObject.optJSONObject("rajaongkir");
                        JSONArray jsonArray = jsonObject1.optJSONArray("results");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject2 = jsonArray.optJSONObject(i);
                            String provinsi = jsonObject2.optString("province");
                            String id = jsonObject2.optString("province_id");

                            listProvinsi.add(provinsi);
                            listIdProvinsi.add(id);

                            getKota("2");
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(SearchActivity.this, "gagal ambil provinsi", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void getKota(String id) {
        setKota.removeAll(setKota);
        listIdKota.removeAll(listIdKota);

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getKota(id).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        JSONObject jsonObject1 = jsonObject.optJSONObject("rajaongkir");
                        JSONArray jsonArray = jsonObject1.optJSONArray("results");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject2 = jsonArray.optJSONObject(i);
                            String kota = jsonObject2.optString("city_name");
                            String kode = jsonObject2.optString("postal_code");
                            String tipe = jsonObject2.optString("type");
                            String idKota = jsonObject2.optString("city_id");
                            String idProvinsi = jsonObject2.optString("province_id");
                            String provinsi = jsonObject2.optString("province");

                            setKota.add(tipe + " " + kota);
                            listIdKota.add(idKota);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void awal() {
        sharedPref = new SharedPref(this);
        getSpinner();
        data = getIntent().getStringExtra("data");
        if (data.equals("semua")) {

        } else if (data.equals("kategori")) {

            IdKategori = getIntent().getStringExtra("id_kat");
        } else if (data.equals("toko")) {
            lyDikirim.setVisibility(View.GONE);
            idToko = getIntent().getStringExtra("id_toko");
        } else if (data.equals("tamu")) {
            lyDikirim.setVisibility(View.GONE);
            idToko = getIntent().getStringExtra("id_toko");
        }

        setSpnProvinsi();

        semuaAdapter = new ListProdukAdapterIndex(listSemua, this);

        layoutManager = new GridLayoutManager(this, 2);
        rv.setLayoutManager(layoutManager);
    }

    private void showBottomSheetDialog() {
        //untuk hide
//        behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        mBottomSheetDialog = new BottomSheetDialog(this);
        View view = getLayoutInflater().inflate(R.layout.sheet_urutkan, null);
        final LinearLayout lyBaru = view.findViewById(R.id.ly_baru);
        final LinearLayout lyLama = view.findViewById(R.id.ly_bekas);
        final LinearLayout lyLaris = view.findViewById(R.id.ly_laris);
        final LinearLayout lyNormal = view.findViewById(R.id.ly_normal);
        final ImageView imgClose = view.findViewById(R.id.img_close);

        TextView tvNoBaru = view.findViewById(R.id.tv_no_baru);
        TextView tvNoBekas = view.findViewById(R.id.tv_no_bekas);
        TextView tvNoAll = view.findViewById(R.id.tv_no_semua);

        ImageView imgBaru = view.findViewById(R.id.img_baru);
        ImageView imgBekas = view.findViewById(R.id.img_bekas);
        ImageView imgAll = view.findViewById(R.id.img_semua);

        tvNoBaru.setText("1");
        tvNoBekas.setText("2");
        tvNoAll.setText("3");

        if (pilih.equals(tvNoBaru.getText().toString())){
            imgBaru.setVisibility(View.VISIBLE);
        }else if (pilih.equals(tvNoBekas.getText().toString())){
            imgBekas.setVisibility(View.VISIBLE);
        }else if (pilih.equals(tvNoAll.getText().toString())){
            imgAll.setVisibility(View.VISIBLE);
        }else {
            imgAll.setVisibility(View.GONE);
            imgBaru.setVisibility(View.GONE);
            imgBekas.setVisibility(View.GONE);
        }

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
            }
        });

        lyBaru.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilihan = "NEW";
                pilih = "1";
                select = "urut";
//                indexFilter = 1;
//                getFilter(indexFilter);
                mBottomSheetDialog.dismiss();
            }
        });

        lyLama.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilihan = "SECOND";
                pilih = "2";
                select = "urut";
//                indexFilter = 1;
//                getFilter(indexFilter);
                mBottomSheetDialog.dismiss();
            }
        });

        lyNormal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilihan = "ALL";
                pilih ="3";
                select = "urut";
//                indexLoad = 1;
//                getData(indexLoad);
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });
    }

    private void sheetDikirim() {
        if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        mBottomSheetDialog = new BottomSheetDialog(this);
        View view = getLayoutInflater().inflate(R.layout.sheet_dikirim, null);
        final ImageView imgClose = view.findViewById(R.id.img_close);
        final Spinner spnProvinsi = view.findViewById(R.id.spn_provinsi);
        final Spinner spnKota = view.findViewById(R.id.spn_kota);
        final Button btnSimpan = view.findViewById(R.id.btn_simpan);

//        setSpnProvinsi();

        ArrayAdapter<String> adp2 = new ArrayAdapter<String>(SearchActivity.this, R.layout.support_simple_spinner_dropdown_item, listProvinsi);
        spnProvinsi.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
        spnProvinsi.setAdapter(adp2);

        spnProvinsi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int a = spnProvinsi.getSelectedItemPosition();
                int idProv = a + 1;
//                                Toast.makeText(TambahAlamatActivity.this, "id prov "+idProv, Toast.LENGTH_SHORT).show();
                setKota.removeAll(setKota);
                listIdKota.removeAll(listIdKota);

                ApiService apiService = ApiConfig.getInstanceRetrofit();
                apiService.getKota(""+idProv).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()) {
                            try {
                                JSONObject jsonObject = new JSONObject(response.body().string());
                                JSONObject jsonObject1 = jsonObject.optJSONObject("rajaongkir");
                                JSONArray jsonArray = jsonObject1.optJSONArray("results");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject2 = jsonArray.optJSONObject(i);
                                    String kota = jsonObject2.optString("city_name");
                                    String kode = jsonObject2.optString("postal_code");
                                    String tipe = jsonObject2.optString("type");
                                    String idKota = jsonObject2.optString("city_id");
                                    String idProvinsi = jsonObject2.optString("province_id");
                                    String provinsi = jsonObject2.optString("province");

                                    setKota.add(tipe + " " + kota);
                                    listIdKota.add(idKota);

                                    ArrayAdapter<String> adp = new ArrayAdapter<String>(SearchActivity.this, R.layout.support_simple_spinner_dropdown_item, setKota);
                                    spnKota.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
                                    spnKota.setAdapter(adp);

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int a = spnKota.getSelectedItemPosition();
                idKota = listIdKota.get(a);
//                Toast.makeText(LihatProductActivity.this, "id kota "+idKota , Toast.LENGTH_SHORT).show();
                select = "dikirim";
                pilih = "1000";
                mBottomSheetDialog.dismiss();
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });
    }

    private void sheetFilter() {
        if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        mBottomSheetDialog = new BottomSheetDialog(this);
        View view = getLayoutInflater().inflate(R.layout.sheet_filter, null);
        final ImageView imgClose = view.findViewById(R.id.img_close);
        final EditText edtMin = view.findViewById(R.id.edt_min);
        final EditText edtMax = view.findViewById(R.id.edt_max);
        final Button btnAktifkan = view.findViewById(R.id.btn_aktifkan);

        btnAktifkan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilih = "1000";
                min = edtMin.getText().toString();
                max = edtMax.getText().toString();
                select = "minmax";

                mBottomSheetDialog.dismiss();
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });
    }

    private boolean validasi() {
        if (edtSearch.getText().toString().isEmpty()) {
            edtSearch.setError("harus diisi");
            edtSearch.requestFocus();
            return false;
        }
        return true;
    }

    private void getSpinner() {
        final List<String> list = new ArrayList<String>();
        list.add("ALL");
        list.add("NEW");
        list.add("SECOND");

        ArrayAdapter<String> adp2 = new ArrayAdapter<String>(SearchActivity.this, R.layout.support_simple_spinner_dropdown_item, list);
        spnFilter.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
        spnFilter.setAdapter(adp2);
    }

    private void getFilterAll(int index) {
        swipe.setRefreshing(true);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.searchAllFilter(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                pilihan,
                edtSearch.getText().toString(),
                index).enqueue(new Callback<ArrayList<ListProduct>>() {
            @Override
            public void onResponse(Call<ArrayList<ListProduct>> call, Response<ArrayList<ListProduct>> response) {
                if (response.isSuccessful()) {
                    swipe.setRefreshing(false);
                    listSemua = response.body();
                    if (listSemua.size() > 0) {
                        lyKosong.setVisibility(View.GONE);
                        rv.setVisibility(View.VISIBLE);
                        semuaAdapter = new ListProdukAdapterIndex(listSemua, getApplicationContext());
                        rv.setAdapter(semuaAdapter);
                        semuaAdapter.notifyDataSetChanged();

                        semuaAdapter.setLoadMoreListener(new ListProdukAdapterIndex.OnLoadMoreListener() {
                            @Override
                            public void onLoadMore() {
                                rv.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        indexSearchAllFilter = indexSearchAllFilter + 1;
                                        loadMoreFilterAll(indexSearchAllFilter);
                                    }
                                });
                            }
                        });
                    } else {
                        lyKosong.setVisibility(View.VISIBLE);
                        rv.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ListProduct>> call, Throwable t) {
                swipe.setRefreshing(false);
                Toast.makeText(SearchActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void getFilterKategori(int index) {
        swipe.setRefreshing(true);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.searchKategoriFilter(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                IdKategori,
                pilihan,
                edtSearch.getText().toString(),
                index).enqueue(new Callback<ArrayList<ListProduct>>() {
            @Override
            public void onResponse(Call<ArrayList<ListProduct>> call, Response<ArrayList<ListProduct>> response) {
                if (response.isSuccessful()) {
                    swipe.setRefreshing(false);
                    listSemua = response.body();
//                    Toast.makeText(SearchActivity.this, ""+listSemua.size(), Toast.LENGTH_SHORT).show();
                    if (listSemua.size() > 0) {
                        lyKosong.setVisibility(View.GONE);
                        rv.setVisibility(View.VISIBLE);
                        semuaAdapter = new ListProdukAdapterIndex(listSemua, getApplicationContext());
                        rv.setAdapter(semuaAdapter);
                        semuaAdapter.notifyDataSetChanged();

                        semuaAdapter.setLoadMoreListener(new ListProdukAdapterIndex.OnLoadMoreListener() {
                            @Override
                            public void onLoadMore() {
                                rv.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        indexSearchKatFilter = indexSearchKatFilter + 1;
                                        loadMoreSerchKategoriFilter(indexSearchKatFilter);
                                    }
                                });
                            }
                        });
                    } else {
                        lyKosong.setVisibility(View.VISIBLE);
                        rv.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ListProduct>> call, Throwable t) {
                swipe.setRefreshing(false);
                Toast.makeText(SearchActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getFilterToko(int index) {
        swipe.setRefreshing(true);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.searchBarangTokoFilter(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                pilihan,
                idToko,
                edtSearch.getText().toString(),
                index).enqueue(new Callback<ArrayList<BarangTokoModel>>() {
            @Override
            public void onResponse(Call<ArrayList<BarangTokoModel>> call, Response<ArrayList<BarangTokoModel>> response) {
                if (response.isSuccessful()) {
                    swipe.setRefreshing(false);
                    listToko = response.body();
                    if (listToko.size() > 0) {
                        lyKosong.setVisibility(View.GONE);
                        rv.setVisibility(View.VISIBLE);
                        tokoAdapter = new BarangTokoAdapter(listToko, getApplicationContext(), data);
                        rv.setAdapter(tokoAdapter);
                    } else {
                        lyKosong.setVisibility(View.VISIBLE);
                        rv.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<BarangTokoModel>> call, Throwable t) {
                swipe.setRefreshing(false);
                Toast.makeText(SearchActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getSemua(int index) {
        swipe.setRefreshing(true);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.searchAll(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                edtSearch.getText().toString(), index).enqueue(new Callback<ArrayList<ListProduct>>() {
            @Override
            public void onResponse(Call<ArrayList<ListProduct>> call, Response<ArrayList<ListProduct>> response) {
                if (response.isSuccessful()) {
                    swipe.setRefreshing(false);
                    listSemua = response.body();

//                    Toast.makeText(SearchActivity.this, ""+listSemua.size(), Toast.LENGTH_SHORT).show();

                    if (listSemua.size() > 0) {

                        lyKosong.setVisibility(View.GONE);
                        rv.setVisibility(View.VISIBLE);

                        semuaAdapter = new ListProdukAdapterIndex(listSemua, getApplicationContext());
                        rv.setAdapter(semuaAdapter);

                        semuaAdapter.notifyDataSetChanged();

                        semuaAdapter.setLoadMoreListener(new ListProdukAdapterIndex.OnLoadMoreListener() {
                            @Override
                            public void onLoadMore() {
                                rv.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        indexSearch = indexSearch + 1;
                                        loadMoreSemua(indexSearch);
                                    }
                                });
                            }
                        });
                    } else {
                        lyKosong.setVisibility(View.VISIBLE);
                        rv.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ListProduct>> call, Throwable t) {
                swipe.setRefreshing(false);
                Toast.makeText(SearchActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getKategori(int index) {
        swipe.setRefreshing(true);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.searchPerKategori(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                IdKategori,
                index,
                edtSearch.getText().toString()).enqueue(new Callback<ArrayList<ListProduct>>() {
            @Override
            public void onResponse(Call<ArrayList<ListProduct>> call, Response<ArrayList<ListProduct>> response) {
                if (response.isSuccessful()) {
                    swipe.setRefreshing(false);

                    listSemua = response.body();

//                    Toast.makeText(SearchActivity.this, ""+listSemua.size(), Toast.LENGTH_SHORT).show();

                    if (listSemua.size() > 0) {
                        lyKosong.setVisibility(View.GONE);
                        rv.setVisibility(View.VISIBLE);
//                        Toast.makeText(SearchActivity.this, ""+listSemua.get(0).getMessage(), Toast.LENGTH_SHORT).show();
                        semuaAdapter = new ListProdukAdapterIndex(listSemua, getApplicationContext());

                        rv.setAdapter(semuaAdapter);

                        semuaAdapter.notifyDataSetChanged();

                        semuaAdapter.setLoadMoreListener(new ListProdukAdapterIndex.OnLoadMoreListener() {
                            @Override
                            public void onLoadMore() {
                                rv.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        indexSearchPerKat = indexSearchPerKat + 1;
                                        loadMorePerKategori(indexSearchPerKat);
                                    }
                                });
                            }
                        });
                    } else {
//                        Toast.makeText(SearchActivity.this, ""+listSemua.get(0).getMessage(), Toast.LENGTH_SHORT).show();
                        lyKosong.setVisibility(View.VISIBLE);
                        rv.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ListProduct>> call, Throwable t) {
                swipe.setRefreshing(false);
                Toast.makeText(SearchActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void getToko() {
        swipe.setRefreshing(true);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.searchBarangAllToko(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                idToko,
                edtSearch.getText().toString()).enqueue(new Callback<ArrayList<BarangTokoModel>>() {
            @Override
            public void onResponse(Call<ArrayList<BarangTokoModel>> call, Response<ArrayList<BarangTokoModel>> response) {
                if (response.isSuccessful()) {
                    swipe.setRefreshing(false);
                    listToko = response.body();
                    if (listToko.get(0).getMessage().equals("Berhasil")) {
                        lyKosong.setVisibility(View.GONE);
                        rv.setVisibility(View.VISIBLE);
                        tokoAdapter = new BarangTokoAdapter(listToko, getApplicationContext(), data);
                        rv.setAdapter(tokoAdapter);
                    } else {
                        lyKosong.setVisibility(View.VISIBLE);
                        rv.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<BarangTokoModel>> call, Throwable t) {
                swipe.setRefreshing(false);
                Toast.makeText(SearchActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getToko2() {
        swipe.setRefreshing(true);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.searchBarangAllToko(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                idToko,
                edtSearch.getText().toString()).enqueue(new Callback<ArrayList<BarangTokoModel>>() {
            @Override
            public void onResponse(Call<ArrayList<BarangTokoModel>> call, Response<ArrayList<BarangTokoModel>> response) {
                if (response.isSuccessful()) {
                    swipe.setRefreshing(false);
                    listToko = response.body();
                    if (listToko.get(0).getMessage().equals("Berhasil")) {
                        lyKosong.setVisibility(View.GONE);
                        rv.setVisibility(View.VISIBLE);
                        tokoAdapter = new BarangTokoAdapter(listToko, getApplicationContext(), data);
                        rv.setAdapter(tokoAdapter);
                    } else {
                        lyKosong.setVisibility(View.VISIBLE);
                        rv.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<BarangTokoModel>> call, Throwable t) {
                swipe.setRefreshing(false);
                Toast.makeText(SearchActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void filterMinMaxKat(int index){
        swipe.setRefreshing(true);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.filterMinMaxSearchKat(IdKategori,
                min,
                max,
                edtSearch.getText().toString(),
                index).enqueue(new Callback<ArrayList<ListProduct>>() {
            @Override
            public void onResponse(Call<ArrayList<ListProduct>> call, Response<ArrayList<ListProduct>> response) {
                if (response.isSuccessful()){
                    swipe.setRefreshing(false);
                    listSemua = response.body();
                    if (listSemua.size() > 0) {
                        lyKosong.setVisibility(View.GONE);
                        rv.setVisibility(View.VISIBLE);

                        semuaAdapter = new ListProdukAdapterIndex(listSemua, getApplicationContext());
                        rv.setAdapter(semuaAdapter);

                        semuaAdapter.notifyDataSetChanged();

                        semuaAdapter.setLoadMoreListener(new ListProdukAdapterIndex.OnLoadMoreListener() {
                            @Override
                            public void onLoadMore() {
                                rv.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        indexMinMaxKat = indexMinMaxKat + 1;
                                        loadMinMaxKat(indexMinMaxKat);
                                    }
                                });
                            }
                        });
                    } else {
                        lyKosong.setVisibility(View.VISIBLE);
                        rv.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ListProduct>> call, Throwable t) {
                swipe.setRefreshing(false);
                Toast.makeText(SearchActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void filterMinMaxAll(int index){
        swipe.setRefreshing(true);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.filterMinMaxSearchAll(min,
                max,
                edtSearch.getText().toString(),
                index).enqueue(new Callback<ArrayList<ListProduct>>() {
            @Override
            public void onResponse(Call<ArrayList<ListProduct>> call, Response<ArrayList<ListProduct>> response) {
                if (response.isSuccessful()){
                    swipe.setRefreshing(false);
                    listSemua = response.body();
                    if (listSemua.size() > 0) {
                        lyKosong.setVisibility(View.GONE);
                        rv.setVisibility(View.VISIBLE);

                        semuaAdapter = new ListProdukAdapterIndex(listSemua, getApplicationContext());
                        rv.setAdapter(semuaAdapter);

                        semuaAdapter.notifyDataSetChanged();

                        semuaAdapter.setLoadMoreListener(new ListProdukAdapterIndex.OnLoadMoreListener() {
                            @Override
                            public void onLoadMore() {
                                rv.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        indexMinMaxAll = indexMinMaxAll + 1;
                                        loadMinMaxAll(indexMinMaxAll);
                                    }
                                });
                            }
                        });
                    } else {
                        lyKosong.setVisibility(View.VISIBLE);
                        rv.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ListProduct>> call, Throwable t) {
                swipe.setRefreshing(false);
                Toast.makeText(SearchActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void filterMinMaxToko(){
        swipe.setRefreshing(true);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.filterMinMaxSearchToko(idToko,
                min,
                max,
                edtSearch.getText().toString()).enqueue(new Callback<ArrayList<BarangTokoModel>>() {
            @Override
            public void onResponse(Call<ArrayList<BarangTokoModel>> call, Response<ArrayList<BarangTokoModel>> response) {
                if (response.isSuccessful()) {
                    swipe.setRefreshing(false);
                    listToko = response.body();
                    if (listToko.get(0).getMessage().equals("Berhasil")) {
                        lyKosong.setVisibility(View.GONE);
                        rv.setVisibility(View.VISIBLE);
                        tokoAdapter = new BarangTokoAdapter(listToko, getApplicationContext(), data);
                        rv.setAdapter(tokoAdapter);
                    } else {
                        lyKosong.setVisibility(View.VISIBLE);
                        rv.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<BarangTokoModel>> call, Throwable t) {
                swipe.setRefreshing(false);
                Toast.makeText(SearchActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void filterKota(String id,int index){
        swipe.setRefreshing(true);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.filterKotaBarangKatSearch(IdKategori,
                id,
                edtSearch.getText().toString(),
                index).enqueue(new Callback<ArrayList<ListProduct>>() {
            @Override
            public void onResponse(Call<ArrayList<ListProduct>> call, Response<ArrayList<ListProduct>> response) {
                if (response.isSuccessful()){
                    swipe.setRefreshing(false);
                    listSemua = response.body();
                    if (listSemua.size() > 0) {
                        lyKosong.setVisibility(View.GONE);
                        rv.setVisibility(View.VISIBLE);

                        semuaAdapter = new ListProdukAdapterIndex(listSemua, getApplicationContext());
                        rv.setAdapter(semuaAdapter);

                        semuaAdapter.notifyDataSetChanged();

                        semuaAdapter.setLoadMoreListener(new ListProdukAdapterIndex.OnLoadMoreListener() {
                            @Override
                            public void onLoadMore() {
                                rv.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        indexKotaKat = indexKotaKat + 1;
                                        loadKotaKat(indexKotaKat);
                                    }
                                });
                            }
                        });
                    } else {
                        lyKosong.setVisibility(View.VISIBLE);
                        rv.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ListProduct>> call, Throwable t) {
                swipe.setRefreshing(false);
                Toast.makeText(SearchActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadMoreSemua(int index) {
        listSemua.add(new ListProduct("mencari"));
        semuaAdapter.notifyItemInserted(listSemua.size() - 1);

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.searchAll(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                edtSearch.getText().toString(), index).enqueue(new Callback<ArrayList<ListProduct>>() {
            @Override
            public void onResponse(Call<ArrayList<ListProduct>> call, Response<ArrayList<ListProduct>> response) {
                if (response.isSuccessful()) {
                    //remove loading view
                    listSemua.remove(listSemua.size() - 1);

                    ArrayList<ListProduct> result = response.body();
                    ArrayList<String> listNama = new ArrayList<>();
                    if (result.size() > 0) {
                        //add loaded data
                        listSemua.addAll(result);
                    } else {//result size 0 means there is no more data available at server
                        semuaAdapter.setMoreDataAvailable(false);
                        //telling adapter to stop calling load more as no more server data available
//                        Toast.makeText(context, "No More Data Available", Toast.LENGTH_SHORT).show();
                    }
                    semuaAdapter.notifyDataChanged();
                } else {
                    Log.e(TAG, " Load More Response Error " + String.valueOf(response.code()));
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ListProduct>> call, Throwable t) {
                Log.e(TAG, " Load More Response Error " + t.getMessage());
            }
        });
    }

    private void loadMoreFilterAll(int index) {
        listSemua.add(new ListProduct("mencari"));
        semuaAdapter.notifyItemInserted(listSemua.size() - 1);

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.searchAllFilter(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                pilihan,
                edtSearch.getText().toString(),
                index).enqueue(new Callback<ArrayList<ListProduct>>() {
            @Override
            public void onResponse(Call<ArrayList<ListProduct>> call, Response<ArrayList<ListProduct>> response) {
                if (response.isSuccessful()) {
                    //remove loading view
                    listSemua.remove(listSemua.size() - 1);

                    ArrayList<ListProduct> result = response.body();
                    ArrayList<String> listNama = new ArrayList<>();
                    if (result.size() > 0) {
                        //add loaded data
                        listSemua.addAll(result);
                    } else {//result size 0 means there is no more data available at server
                        semuaAdapter.setMoreDataAvailable(false);
                        //telling adapter to stop calling load more as no more server data available
//                        Toast.makeText(context, "No More Data Available", Toast.LENGTH_SHORT).show();
                    }
                    semuaAdapter.notifyDataChanged();
                } else {
                    Log.e(TAG, " Load More Response Error " + String.valueOf(response.code()));
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ListProduct>> call, Throwable t) {
                Log.e(TAG, " Load More Response Error " + t.getMessage());
            }
        });
    }

    private void loadMorePerKategori(int index) {
        listSemua.add(new ListProduct("mencari"));
        semuaAdapter.notifyItemInserted(listSemua.size() - 1);

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.searchPerKategori(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                IdKategori,
                index,
                edtSearch.getText().toString()).enqueue(new Callback<ArrayList<ListProduct>>() {
            @Override
            public void onResponse(Call<ArrayList<ListProduct>> call, Response<ArrayList<ListProduct>> response) {
                if (response.isSuccessful()) {
                    //remove loading view
                    listSemua.remove(listSemua.size() - 1);

                    ArrayList<ListProduct> result = response.body();
                    ArrayList<String> listNama = new ArrayList<>();
                    if (result.size() > 0) {
                        //add loaded data
                        listSemua.addAll(result);
                    } else {//result size 0 means there is no more data available at server
                        semuaAdapter.setMoreDataAvailable(false);
                        //telling adapter to stop calling load more as no more server data available
//                        Toast.makeText(context, "No More Data Available", Toast.LENGTH_SHORT).show();
                    }
                    semuaAdapter.notifyDataChanged();
                } else {
                    Log.e(TAG, " Load More Response Error " + String.valueOf(response.code()));
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ListProduct>> call, Throwable t) {
                Log.e(TAG, " Load More Response Error " + t.getMessage());
            }
        });
    }

    private void loadMoreSerchKategoriFilter(int index) {
        listSemua.add(new ListProduct("mencari"));
        semuaAdapter.notifyItemInserted(listSemua.size() - 1);

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.searchKategoriFilter(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                IdKategori,
                pilihan,
                edtSearch.getText().toString(),
                index).enqueue(new Callback<ArrayList<ListProduct>>() {
            @Override
            public void onResponse(Call<ArrayList<ListProduct>> call, Response<ArrayList<ListProduct>> response) {
                if (response.isSuccessful()) {
                    //remove loading view
                    listSemua.remove(listSemua.size() - 1);

                    ArrayList<ListProduct> result = response.body();
                    ArrayList<String> listNama = new ArrayList<>();
                    if (result.size() > 0) {
                        //add loaded data
                        listSemua.addAll(result);
                    } else {//result size 0 means there is no more data available at server
                        semuaAdapter.setMoreDataAvailable(false);
                        //telling adapter to stop calling load more as no more server data available
//                        Toast.makeText(context, "No More Data Available", Toast.LENGTH_SHORT).show();
                    }
                    semuaAdapter.notifyDataChanged();
                } else {
                    Log.e(TAG, " Load More Response Error " + String.valueOf(response.code()));
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ListProduct>> call, Throwable t) {
                Log.e(TAG, " Load More Response Error " + t.getMessage());
            }
        });
    }

    private void loadKotaKat(int index){
        listSemua.add(new ListProduct("mencari"));
        semuaAdapter.notifyItemInserted(listSemua.size() - 1);

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.filterKotaBarangKatSearch(IdKategori,
                idKota,
                edtSearch.getText().toString(),
                index).enqueue(new Callback<ArrayList<ListProduct>>() {
            @Override
            public void onResponse(Call<ArrayList<ListProduct>> call, Response<ArrayList<ListProduct>> response) {
                if (response.isSuccessful()) {
                    //remove loading view
                    listSemua.remove(listSemua.size() - 1);

                    ArrayList<ListProduct> result = response.body();
                    ArrayList<String> listNama = new ArrayList<>();
                    if (result.size() > 0) {
                        //add loaded data
                        listSemua.addAll(result);
                    } else {//result size 0 means there is no more data available at server
                        semuaAdapter.setMoreDataAvailable(false);
                        //telling adapter to stop calling load more as no more server data available
//                        Toast.makeText(context, "No More Data Available", Toast.LENGTH_SHORT).show();
                    }
                    semuaAdapter.notifyDataChanged();
                } else {
                    Log.e(TAG, " Load More Response Error " + String.valueOf(response.code()));
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ListProduct>> call, Throwable t) {
                Log.e(TAG, " Load More Response Error " + t.getMessage());
            }
        });
    }

    private void loadMinMaxKat(int index){
        listSemua.add(new ListProduct("mencari"));
        semuaAdapter.notifyItemInserted(listSemua.size() - 1);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.filterMinMaxSearchKat(IdKategori,
                min,
                max,
                edtSearch.getText().toString(),
                index).enqueue(new Callback<ArrayList<ListProduct>>() {
            @Override
            public void onResponse(Call<ArrayList<ListProduct>> call, Response<ArrayList<ListProduct>> response) {
                if (response.isSuccessful()) {
                    //remove loading view
                    listSemua.remove(listSemua.size() - 1);

                    ArrayList<ListProduct> result = response.body();
                    ArrayList<String> listNama = new ArrayList<>();
                    if (result.size() > 0) {
                        //add loaded data
                        listSemua.addAll(result);
                    } else {//result size 0 means there is no more data available at server
                        semuaAdapter.setMoreDataAvailable(false);
                        //telling adapter to stop calling load more as no more server data available
//                        Toast.makeText(context, "No More Data Available", Toast.LENGTH_SHORT).show();
                    }
                    semuaAdapter.notifyDataChanged();
                } else {
                    Log.e(TAG, " Load More Response Error " + String.valueOf(response.code()));
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ListProduct>> call, Throwable t) {
                Log.e(TAG, " Load More Response Error " + t.getMessage());
            }
        });
    }

    private void loadMinMaxAll(int index){
        listSemua.add(new ListProduct("mencari"));
        semuaAdapter.notifyItemInserted(listSemua.size() - 1);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.filterMinMaxSearchAll(min,
                max,
                edtSearch.getText().toString(),
                index).enqueue(new Callback<ArrayList<ListProduct>>() {
            @Override
            public void onResponse(Call<ArrayList<ListProduct>> call, Response<ArrayList<ListProduct>> response) {
                if (response.isSuccessful()) {
                    //remove loading view
                    listSemua.remove(listSemua.size() - 1);

                    ArrayList<ListProduct> result = response.body();
                    ArrayList<String> listNama = new ArrayList<>();
                    if (result.size() > 0) {
                        //add loaded data
                        listSemua.addAll(result);
                    } else {//result size 0 means there is no more data available at server
                        semuaAdapter.setMoreDataAvailable(false);
                        //telling adapter to stop calling load more as no more server data available
//                        Toast.makeText(context, "No More Data Available", Toast.LENGTH_SHORT).show();
                    }
                    semuaAdapter.notifyDataChanged();
                } else {
                    Log.e(TAG, " Load More Response Error " + String.valueOf(response.code()));
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ListProduct>> call, Throwable t) {
                Log.e(TAG, " Load More Response Error " + t.getMessage());
            }
        });
    }

    private void initView() {
        edtSearch = (EditText) findViewById(R.id.edt_search);
        btnSearch = (Button) findViewById(R.id.btn_search);
        rv = (RecyclerView) findViewById(R.id.rv);
        lyKosong = (LinearLayout) findViewById(R.id.ly_kosong);
        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe);
        lySearch = (LinearLayout) findViewById(R.id.ly_search);
        tvNo = (TextView) findViewById(R.id.tv_no);
        spnFilter = (Spinner) findViewById(R.id.spn_filter);
        imgSearch = (ImageView) findViewById(R.id.img_search);
        imgClose = (ImageView) findViewById(R.id.img_close);
        lyDikirim = (LinearLayout) findViewById(R.id.ly_dikirim);
        lyFilter = (LinearLayout) findViewById(R.id.ly_filter);
        lyUrutkan = (LinearLayout) findViewById(R.id.ly_urutkan);
    }
}
