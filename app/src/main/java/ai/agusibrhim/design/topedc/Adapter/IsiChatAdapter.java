package ai.agusibrhim.design.topedc.Adapter;

import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Activity.ChatActivity;
import ai.agusibrhim.design.topedc.Helper.Config;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.MainActivity;
import ai.agusibrhim.design.topedc.Model.IsiChatModel;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.CLIPBOARD_SERVICE;

public class IsiChatAdapter extends RecyclerView.Adapter<IsiChatAdapter.Holder> {

    ArrayList<IsiChatModel> list;
    Context context;
    SharedPref sharedPref;

    public IsiChatAdapter(ArrayList<IsiChatModel> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_chat,parent,false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        sharedPref = new SharedPref(context);

        if (list.get(position).getIDUSERFROM().equals(sharedPref.getIdUser())){
            holder.tanggal2.setText(list.get(position).getCREATEDAT());
            holder.chat2.setText(list.get(position).getCHMESSAGE());
            if (list.get(position).getCHIMAGE().equals("")){
                holder.imgChat2.setVisibility(View.GONE);
            }else {
                holder.imgChat2.setVisibility(View.VISIBLE);
                Picasso.with(context)
                        .load(Config.IMAGE_ISI_CHAT + list.get(position).getCHIMAGE())
                        .into(holder.imgChat2);
            }
            holder.ly1.setVisibility(View.GONE);
            holder.ly2.setVisibility(View.VISIBLE);
        }else {
            holder.ly1.setVisibility(View.VISIBLE);
            holder.ly2.setVisibility(View.GONE);
            if (list.get(position).getCHIMAGE().equals("")){
                holder.imgCaht1.setVisibility(View.GONE);
            }else {
                holder.imgCaht1.setVisibility(View.VISIBLE);
                Picasso.with(context)
                        .load(Config.IMAGE_ISI_CHAT + list.get(position).getCHIMAGE())
                        .into(holder.imgCaht1);
            }
            holder.tanggal1.setText(list.get(position).getCREATEDAT());
            holder.chat1.setText(list.get(position).getCHMESSAGE());
            if (list.get(position).getUSDTOKOFOTO().equals("")){
                holder.imgAvatar.setImageResource(R.drawable.avatar);
            }else {
                Picasso.with(context)
                        .load(Config.IMAGE_TOKO + list.get(position).getUSDTOKOFOTO())
                        .into(holder.imgAvatar);
            }

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Holder extends RecyclerView.ViewHolder{

        TextView tanggal1,tanggal2,chat1,chat2;
        ImageView imgAvatar,imgCaht1,imgChat2;
        LinearLayout ly1,ly2;

        public Holder(View itemView) {
            super(itemView);

            ly1= itemView.findViewById(R.id.ly_chat1);
            ly2= itemView.findViewById(R.id.ly_chat2);
            tanggal1 = itemView.findViewById(R.id.tv_chat_tanggal);
            tanggal2 = itemView.findViewById(R.id.tv_chat_tanggal2);
            chat1 = itemView.findViewById(R.id.tv_chat_message);
            chat2 = itemView.findViewById(R.id.tv_chat_message2);
            imgAvatar = itemView.findViewById(R.id.img_chat);
            imgCaht1 = itemView.findViewById(R.id.img_chat1);
            imgChat2 = itemView.findViewById(R.id.img_chat2);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    final CharSequence[] dialogItem = {"Delete", "Copy"};
                    AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                    builder.setTitle("Tentukan Pilihan Anda");
                    builder.setItems(dialogItem, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            switch (i) {
                                case 0:
                                    new AlertDialog.Builder(view.getContext())
                                            .setMessage("Apakah anda akan menghapus pesan ini ?")
                                            .setCancelable(false)
                                            .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    sharedPref = new SharedPref(context);

                                                    final ProgressDialog pd = new ProgressDialog(view.getContext());
                                                    pd.setTitle("Menghapus ....");
                                                    pd.setCancelable(false);
                                                    pd.show();
                                                    ApiService apiService = ApiConfig.getInstanceRetrofit();
                                                    apiService.deleteChat(sharedPref.getIdUser(),
                                                            sharedPref.getSESSION(),
                                                            list.get(getAdapterPosition()).getIDCHAT()).enqueue(new Callback<ResponseBody>() {
                                                        @Override
                                                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                                            if (response.isSuccessful()){
                                                                pd.dismiss();
                                                                try {
                                                                    JSONObject jsonObject = new JSONObject(response.body().string());
                                                                    String kode = jsonObject.optString("success");
                                                                    String pesan = jsonObject.optString("message");
                                                                    if (kode.equals("1")){
                                                                        Toast.makeText(view.getContext(), ""+pesan, Toast.LENGTH_SHORT).show();
                                                                        context.startActivity(new Intent(context,ChatActivity.class)
                                                                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                                                    }else {
                                                                        Toast.makeText(view.getContext(), ""+pesan, Toast.LENGTH_SHORT).show();
                                                                    }
                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                } catch (IOException e) {
                                                                    e.printStackTrace();
                                                                }
                                                            }
                                                        }

                                                        @Override
                                                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                                                            pd.dismiss();
                                                            Toast.makeText(view.getContext(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                                                        }
                                                    });
                                                }
                                            })
                                            .setNegativeButton("Tidak", null)
                                            .show();
                                    break;
                                case 1:
                                    ClipboardManager clipboard = (ClipboardManager) context.getSystemService(CLIPBOARD_SERVICE);
                                    String getstring = list.get(getAdapterPosition()).getCHMESSAGE();
                                    Toast.makeText(context, "disalin", Toast.LENGTH_SHORT).show();
                                    ClipData clip = ClipData.newPlainText("", getstring);
                                    clipboard.setPrimaryClip(clip);
                                    break;
                            }
                        }
                    });
                    builder.create().show();
                }
            });
        }
    }
}
