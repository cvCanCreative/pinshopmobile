package ai.agusibrhim.design.topedc.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Adapter.AdapterProductImage;
import ai.agusibrhim.design.topedc.Helper.HelperClass;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.Model.BarangTokoModel;
import ai.agusibrhim.design.topedc.Model.FavoritModel;
import ai.agusibrhim.design.topedc.Model.ListProduct;
import ai.agusibrhim.design.topedc.R;

public class DetailBarangTokoActivity extends AppCompatActivity {

    private ViewPager pager;
    private LinearLayout layoutDots;
    private TextView txtNamaBarang;
    private TextView txtHargaBarang;
    private TextView txtKondisi;
    private TextView txtStok;
    private TextView txtBerat;
    private TextView txtNamaToko;
    private TextView txtDeskripsiBarang;
    private TextView tvLihatFeedback;
    private TextView txtJumlahFeedback;
    private LinearLayout ly;
    private Button btnDelete;
    private LinearLayout ly2;
    private ImageView btnChat;
    private ImageView btnPesan;
    private Button btnFavorit;
    SharedPref sharedPref;
    ArrayList<String> listImage = new ArrayList<>();
    int stok;
    BarangTokoModel list;
    int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_barang_toko);
        initView();

        awal();
    }

    private void awal(){
        sharedPref = new SharedPref(this);
        list = getIntent().getParcelableExtra("list");
        position = Integer.parseInt(getIntent().getStringExtra("posisi"));
        ambilData(list,position);
        displayImageSlider(list, position);
    }

    private void ambilData(BarangTokoModel list, int posisi) {
        txtNamaBarang.setText("" + list.getBANAME());
        txtHargaBarang.setText("" + new HelperClass().convertRupiah(Integer.parseInt(list.getBAPRICE())));
        txtDeskripsiBarang.setText("" + Html.fromHtml(list.getBADESCRIPTION()));
        txtStok.setText(list.getBASTOCK());
        txtKondisi.setText(list.getBACONDITION());
        txtNamaToko.setText(list.getUSDTOKO());
        float berat = Float.parseFloat(list.getBAWEIGHT()) / 1000;
        txtBerat.setText(""+berat + " Kg");

        stok = Integer.parseInt(list.getBASTOCK());
    }

    private void displayImageSlider(BarangTokoModel list, int position) {
        final LinearLayout layout_dots = (LinearLayout) findViewById(R.id.layout_dots);
        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        final AdapterProductImage adapterSlider = new AdapterProductImage(this, new ArrayList<String>());

        String gambar = list.getBAIMAGE();
        String gb = gambar.replace("|", " ");
        String strArray[] = gb.split(" ");

        for (int i = 0; i < strArray.length; i++) {
            if (list != null) listImage.add(strArray[i]);
        }
        adapterSlider.setItems(listImage);
        viewPager.setAdapter(adapterSlider);

        // displaying selected image first
        viewPager.setCurrentItem(0);
        addBottomDots(layout_dots, adapterSlider.getCount(), 0);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int pos, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int pos) {
                addBottomDots(layout_dots, adapterSlider.getCount(), pos);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });


//        final ArrayList<String> images_list = new ArrayList<>();
//        for (int i = 0; i <listImage.size() ; i++) {
//            images_list.add(Config.IMAGE_BARANG+listImage.get(position));
//        }


        adapterSlider.setOnItemClickListener(new AdapterProductImage.OnItemClickListener() {
            @Override
            public void onItemClick(View view, String obj, int position) {
                Intent i = new Intent(DetailBarangTokoActivity.this, FullScreenActivity.class);
                i.putExtra("posisi", position);
                i.putStringArrayListExtra("list", listImage);
                startActivity(i);
            }
        });
    }


    private void addBottomDots(LinearLayout layout_dots, int size, int current) {
        ImageView[] dots = new ImageView[size];

        layout_dots.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new ImageView(this);
            int width_height = 15;
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(new ViewGroup.LayoutParams(width_height, width_height));
            params.setMargins(10, 10, 10, 10);
            dots[i].setLayoutParams(params);
            dots[i].setImageResource(R.drawable.shape_circle);
            dots[i].setColorFilter(ContextCompat.getColor(this, R.color.darkOverlaySoft));
            layout_dots.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[current].setColorFilter(ContextCompat.getColor(this, R.color.colorPrimaryLight));
    }

    private void initView() {
        pager = (ViewPager) findViewById(R.id.pager);
        layoutDots = (LinearLayout) findViewById(R.id.layout_dots);
        txtNamaBarang = (TextView) findViewById(R.id.txt_nama_barang);
        txtHargaBarang = (TextView) findViewById(R.id.txt_harga_barang);
        txtKondisi = (TextView) findViewById(R.id.txt_kondisi);
        txtStok = (TextView) findViewById(R.id.txt_stok);
        txtBerat = (TextView) findViewById(R.id.txt_berat);
        txtNamaToko = (TextView) findViewById(R.id.txt_nama_toko);
        txtDeskripsiBarang = (TextView) findViewById(R.id.txt_deskripsi_barang);
        tvLihatFeedback = (TextView) findViewById(R.id.tv_lihat_feedback);
        txtJumlahFeedback = (TextView) findViewById(R.id.txt_jumlah_feedback);
        ly = (LinearLayout) findViewById(R.id.ly);
        btnDelete = (Button) findViewById(R.id.btn_delete);
        ly2 = (LinearLayout) findViewById(R.id.ly2);
        btnChat = (ImageView) findViewById(R.id.btn_chat);
        btnPesan = (ImageView) findViewById(R.id.btn_pesan);
        btnFavorit = (Button) findViewById(R.id.btn_favorit);
    }
}
