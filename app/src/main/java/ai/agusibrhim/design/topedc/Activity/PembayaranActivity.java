package ai.agusibrhim.design.topedc.Activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ai.agusibrhim.design.topedc.Adapter.PesananAdapter;
import ai.agusibrhim.design.topedc.Adapter.PesananPemTokoAdapter;
import ai.agusibrhim.design.topedc.Helper.Config;
import ai.agusibrhim.design.topedc.Helper.HelperClass;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.Helper.dataRoom.AppDatabase;
import ai.agusibrhim.design.topedc.Helper.dataRoom.Pesanan;
import ai.agusibrhim.design.topedc.MainActivity;
import ai.agusibrhim.design.topedc.Model.AlamatModel;
import ai.agusibrhim.design.topedc.Model.FavoritModel;
import ai.agusibrhim.design.topedc.Model.ListProduct;
import ai.agusibrhim.design.topedc.Model.RekeningByIdModel;
import ai.agusibrhim.design.topedc.Model.RekeningModel;
import ai.agusibrhim.design.topedc.Model.ResponseModel;
import ai.agusibrhim.design.topedc.Model.VoucherModel;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PembayaranActivity extends AppCompatActivity {

    private RadioGroup rgMetode;
    private RadioButton rbTf;
    private LinearLayout lyTransfer;
    private RadioButton rbSaldo;
    private LinearLayout lySaldo;
    private EditText edtAlamat;
    private RecyclerView rv;
    private Spinner spnKurir;
    private CheckBox cbVoucher;
    private LinearLayout lyVoucher;
    private TextView totHargaBarang;
    private TextView biayaKirim;
    private TextView totalSeluruhnya;
    private Button btnBayar;
    SharedPref sharedPref;
    ArrayList<AlamatModel> listAlamat = new ArrayList<>();
    int posisi;
    String alamatKu, alamatToko, beratPesanan;
    private AppDatabase db;
    PesananAdapter mAdapter;
    RecyclerView.LayoutManager layoutManager, layoutManager2;
    ArrayList<Pesanan> listPesanan = new ArrayList<>();
    ArrayList<Pesanan> listPesananByIdToko = new ArrayList<>();
    List<String> jasaPengiriman = new ArrayList<>();
    List<String> listHarga = new ArrayList<>();
    List<String> listTipe = new ArrayList<>();
    String jenisKurir;

    List<String> listKurir = new ArrayList<>();
    List<String> listOngkir = new ArrayList<>();
    List<String> listJenisKurir = new ArrayList<>();
    List<String> listIdPengiriman = new ArrayList<>();
    ArrayList<String> listIdBarang = new ArrayList<>();
    List<String> listJumlahBarang = new ArrayList<>();
    List<String> listCatatan = new ArrayList<>();
    List<String> listKodePromo = new ArrayList<>();
    List<String> listHargaBarang = new ArrayList<>();
    List<String> listIdVoucher = new ArrayList<>();

    List<String> listCekVoucher = new ArrayList<>();
    ArrayList<VoucherModel> voucherModels = new ArrayList<>();

    List<String> jasaPengirimanTotal = new ArrayList<>();
    List<String> listHargaTotal = new ArrayList<>();
    List<String> listTipeTotal = new ArrayList<>();
    PesananPemTokoAdapter pemTokoAdapter;

    int posisiBank = 1000;
    String pemakaian;

    int a = 0;
    int b = 0;
    String metodeBayar = "";
    String ketSaldo;
    String kodeUnik = "kode";
    private Spinner spnHarga;
    String tipe = "bayar";
    private EditText edtNoRek;
    private EditText edtNama;
    private EditText edtBank;
    private EditText edtBankPin;
    private EditText edtRekPin;
    private EditText edtJmlTrans;
    int totHarga, ongkirku;
    String kodeKurir;
    String kurirku;
    int jumlahBarang;
    private TextView tvJumlahSaldo;
    private EditText edtPembayaranSaldo;
    List<String> listIdToko = new ArrayList<>();
    int semuaOngkir = 0;
    private LinearLayout lySatu;
    private EditText edtCatatan;
    private TextView tvCatatan;
    private RadioButton rbBankPin;
    private LinearLayout divBank;
    private RadioButton rbRekening;
    private LinearLayout divRekening;
    ArrayList<RekeningByIdModel> listBankPin = new ArrayList<>();
    ArrayList<RekeningByIdModel> listRek = new ArrayList<>();
    String rekPin, noRek, namaRek, namaBank;
    String bankPin = "";
    private LinearLayout lyBankPin;
    private LinearLayout lyRek;
    int nilai = 0;
    private LinearLayout lyPotongan;
    private TextView tvPotongan;
    private TextView tvNilaiPotongan;
    private Button btnGunakan;
    private EditText edtVoucher;
    String kodeVoucher;
    int potongan = 0;
    private LinearLayout lyPotonganSaldo;
    private TextView tvPotonganSaldo;
    private TextView tvNilaiPotonganSaldo;
    private TextView pilihBank;
    private ImageView image;
    private TextView tvNamaBank;
    private TextView pilihRek;
    private TextView tvNamaBank2;
    LayoutInflater inflater;
    View dialogView;
    android.support.v7.app.AlertDialog.Builder dialog;
    private ImageView imgClose;
    private LinearLayout lyRekening;
    private ImageView imgDown;
    private ImageView imgUp;
    private LinearLayout div2;
    String imgBank;
    private ImageView imgCkSaldo;
    private LinearLayout lyTf;
    private ImageView imgCkTf;
    String metode = "";
    BottomSheetBehavior behavior;
    private BottomSheetDialog mBottomSheetDialog;
    int saldoDigunakan = 0;
    int klikVoucher = 0;
    private LinearLayout lyKlikVoucher;
    private TextView tvKlikVoucher;
    private LinearLayout lyHasilVoucher;
    private TextView tvHasilVoucher;
    private TextView tvHapusVoucher;
    private RecyclerView rvPem;
    private LinearLayout div;
    private ListProduct product;
    private FavoritModel favoritModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pembayaran);
        initView();

        awal();


    }

    private void sheetSaldo() {
        if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        mBottomSheetDialog = new BottomSheetDialog(this);
        View view = getLayoutInflater().inflate(R.layout.sheet_saldo, null);
        final ImageView imgClose = view.findViewById(R.id.img_close);
        final EditText edtPemakaian = view.findViewById(R.id.edt_pemakaian);
        Button btnSimpan = view.findViewById(R.id.btn_simpan);
        TextView tvJumlahSaldo = view.findViewById(R.id.tv_jumlah_saldo);

        tvJumlahSaldo.setText("" + new HelperClass().convertRupiah(Integer.parseInt(sharedPref.getSALDO())));

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saldoDigunakan = Integer.parseInt(edtPemakaian.getText().toString());

                totHarga = totHarga - saldoDigunakan;

                totalSeluruhnya.setText("" + new HelperClass().convertRupiah(totHarga));

                tvPotonganSaldo.setText("Saldo yang digunakan");
                tvNilaiPotonganSaldo.setText(new HelperClass().convertRupiah(saldoDigunakan));

                mBottomSheetDialog.dismiss();
            }
        });

        edtPemakaian.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().isEmpty()) {
                } else {
                    int a = Integer.parseInt(edtPemakaian.getText().toString());
                    int jml = Integer.parseInt(sharedPref.getSALDO());

                    if (a > jml) {
                        edtPemakaian.setError("Melebihi Jumlah Saldo");
                        edtPemakaian.setText(sharedPref.getSALDO());
                    } else {

                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                pemakaian = edtPemakaian.getText().toString();
//                if (pemakaian.equals("0")){
//                    metode = "";
//                    imgCkSaldo.setVisibility(View.GONE);
//                }else {
//
//                }
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });
    }

    private void sheetBank() {
        getBank();

        if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        mBottomSheetDialog = new BottomSheetDialog(this);
        View view = getLayoutInflater().inflate(R.layout.sheet_bank, null);
        final ImageView imgClose = view.findViewById(R.id.img_close);
        LinearLayout divBank = view.findViewById(R.id.div_bank);

        for (int i = 0; i < listBankPin.size(); i++) {
            LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view2 = layoutInflater.inflate(R.layout.row_bank2, null);

            TextView nama_bank = view2.findViewById(R.id.tv_nama_bank);
            TextView tvPosisi = view2.findViewById(R.id.tv_posisi);
            final ImageView imagCk = view2.findViewById(R.id.img_ceklis);
            ImageView imageView = view2.findViewById(R.id.image);
            RelativeLayout ly = view2.findViewById(R.id.ly_bank);
            final RadioButton rb_bank = view2.findViewById(R.id.rb_bank);

            rb_bank.setEnabled(false);

            final int po = i;

            if (i == posisiBank) {
                imagCk.setVisibility(View.VISIBLE);
            } else {
                imagCk.setVisibility(View.GONE);
            }

            nama_bank.setText("" + listBankPin.get(i).RK_BANK);
            Picasso.with(getApplicationContext())
                    .load(Config.IMAGE_REKENING + listBankPin.get(i).RK_IMAGE)
                    .into(imageView);

            ly.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    posisiBank = po;
                    imagCk.setVisibility(View.VISIBLE);
                    bankPin = listBankPin.get(po).RK_BANK;
                    rekPin = listBankPin.get(po).RK_NOMOR;
                    mBottomSheetDialog.dismiss();
                }
            });

            divBank.addView(view2);
        }
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (posisiBank == 1000) {
                    metode = "";
                    imgCkTf.setVisibility(View.GONE);
                } else {
                    imgCkTf.setVisibility(View.VISIBLE);
                }
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });
    }

    private void awal() {
        sharedPref = new SharedPref(this);
        db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "pesanandb").allowMainThreadQueries().build();

        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rv.setLayoutManager(layoutManager);

        layoutManager2 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvPem.setLayoutManager(layoutManager2);

        getBank();
        getInten();

        if (sharedPref.getPotonganVoucher().equals("")) {
            potongan = 0;
            kodeVoucher = "";
            lyVoucher.setVisibility(View.GONE);
        } else {
            potongan = Integer.parseInt(sharedPref.getPotonganVoucher());

            lyHasilVoucher.setVisibility(View.VISIBLE);
            lyKlikVoucher.setVisibility(View.GONE);
            tvPotongan.setText("" + sharedPref.getTipeVoucher());
            kodeVoucher = ""+sharedPref.getKodeVoucher();

            tvNilaiPotongan.setText(new HelperClass().convertRupiah(potongan));
            tvHasilVoucher.setVisibility(View.VISIBLE);
            tvHasilVoucher.setText("" + sharedPref.getTipeVoucher().toLowerCase() + " " + tvNilaiPotongan.getText().toString());
        }

//        Toast.makeText(this, "pot " + potongan, Toast.LENGTH_SHORT).show();
//        getDataBank(true);
//        ambil(true);

        mainButton();

    }

    private void getBank() {
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getRekeningAdmin(sharedPref.getIdUser(),
                sharedPref.getSESSION()).enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                if (response.isSuccessful()) {
                    listBankPin = response.body().rekening;

                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {

            }
        });
    }

//    private void tampilBank() {
//        dialog = new android.support.v7.app.AlertDialog.Builder(PembayaranActivity.this);
//        inflater = getLayoutInflater();
//        dialogView = inflater.inflate(R.layout.row_dialog_bank, null);
//        dialog.setView(dialogView);
//        dialog.setIcon(R.drawable.ic_cat14);
//
//        final LinearLayout rvBank = (LinearLayout) dialogView.findViewById(R.id.div);
//
//        final android.support.v7.app.AlertDialog alertDialog = dialog.create();
//
//        ApiService apiService = ApiConfig.getInstanceRetrofit();
//        apiService.getRekeningAdmin(sharedPref.getIdUser(),
//                sharedPref.getSESSION()).enqueue(new Callback<ArrayList<RekeningModel>>() {
//            @Override
//            public void onResponse(Call<ArrayList<RekeningModel>> call, Response<ArrayList<RekeningModel>> response) {
//                if (response.isSuccessful()) {
//                    listBankPin = response.body();
//
//                    for (int i = 0; i < listBankPin.size(); i++) {
//                        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                        View view = layoutInflater.inflate(R.layout.row_bank2, null);
//
//                        TextView nama_bank = view.findViewById(R.id.tv_nama_bank);
//                        ImageView imageView = view.findViewById(R.id.image);
//                        RelativeLayout ly = view.findViewById(R.id.ly_bank);
//                        final RadioButton rb_bank = view.findViewById(R.id.rb_bank);
//
//                        rb_bank.setVisibility(View.GONE);
//
//                        rb_bank.setEnabled(false);
//
//                        nama_bank.setText("" + listBankPin.get(i).getrKBANK());
//                        Picasso.with(getApplicationContext())
//                                .load(Config.IMAGE_REKENING + listBankPin.get(i).getrKIMAGE())
//                                .into(imageView);
//
//                        final String posisi = "" + i;
//                        ly.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
////                                new AlertDialog.Builder(PembayaranActivity.this)
////                                        .setMessage("Transfer ke rekening ini ?")
////                                        .setCancelable(false)
////                                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
////                                            public void onClick(DialogInterface dialog, int id) {
////
////                                                bankPin = listBankPin.get(Integer.parseInt(posisi)).getrKBANK();
////                                                rekPin = listBankPin.get(Integer.parseInt(posisi)).getrKNOMOR();
////                                                String img = listBankPin.get(Integer.parseInt(posisi)).getrKIMAGE();
////
////                                                tvNamaBank.setText(""+bankPin);
////                                                Picasso.with(getApplicationContext())
////                                                        .load(Config.IMAGE_REKENING + img)
////                                                        .into(image);
////
////                                                alertDialog.dismiss();
////
////                                            }
////                                        })
////                                        .setNegativeButton("Tidak", null)
////                                        .show();
//                                bankPin = listBankPin.get(Integer.parseInt(posisi)).getrKBANK();
//                                rekPin = listBankPin.get(Integer.parseInt(posisi)).getrKNOMOR();
//                                String img = listBankPin.get(Integer.parseInt(posisi)).getrKIMAGE();
//
//                                tvNamaBank.setText("" + bankPin);
//                                Picasso.with(getApplicationContext())
//                                        .load(Config.IMAGE_REKENING + img)
//                                        .into(image);
//
//                                alertDialog.dismiss();
//                            }
//                        });
//
//                        rvBank.addView(view);
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ArrayList<RekeningModel>> call, Throwable t) {
//                Toast.makeText(PembayaranActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//        });
//
//        alertDialog.show();
//    }

    private void tampilRek() {
        dialog = new android.support.v7.app.AlertDialog.Builder(PembayaranActivity.this);
        inflater = getLayoutInflater();
        dialogView = inflater.inflate(R.layout.row_dialog_bank, null);
        dialog.setView(dialogView);
        dialog.setIcon(R.drawable.ic_cat14);

        final LinearLayout rvRek = (LinearLayout) dialogView.findViewById(R.id.div);

        final android.support.v7.app.AlertDialog alertDialog = dialog.create();

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getRekeningById(sharedPref.getIdUser(),
                sharedPref.getSESSION()).enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                if (response.isSuccessful()) {
                    listRek = response.body().rekening;
                    if (response.body().getSuccess() == 1) {
                        for (int i = 0; i < listRek.size(); i++) {
                            LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            View view = layoutInflater.inflate(R.layout.row_rek, null);
                            TextView tvNama = view.findViewById(R.id.tv_nama);
                            RadioButton rb_rek = view.findViewById(R.id.rb_rek);
                            RelativeLayout layout = view.findViewById(R.id.ly_bank);

                            rb_rek.setVisibility(View.GONE);

                            tvNama.setText("" + listRek.get(i).RK_BANK);
                            final String idRek = listRek.get(i).ID_REKENING;
                            final int posisi = i;
                            layout.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    namaRek = listRek.get(posisi).RK_NAME;
                                    noRek = listRek.get(posisi).RK_NOMOR;
                                    namaBank = listRek.get(posisi).RK_BANK;

                                    tvNamaBank2.setText("" + namaBank);

                                    alertDialog.dismiss();
                                }
                            });
                            rvRek.addView(view);
                        }
                    } else {

                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {

            }
        });
        alertDialog.show();
    }

    private void cekVoucher() {
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.cekVoucher(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                edtVoucher.getText().toString()).enqueue(new Callback<ArrayList<VoucherModel>>() {
            @Override
            public void onResponse(Call<ArrayList<VoucherModel>> call, Response<ArrayList<VoucherModel>> response) {
                if (response.isSuccessful()) {
                    lyPotongan.setVisibility(View.VISIBLE);
                    voucherModels = response.body();
                    if (voucherModels.get(0).getMessage().equals("Berhasil")) {
                        for (int i = 0; i < voucherModels.size(); i++) {
                            listIdVoucher.add(voucherModels.get(i).getIDBARANG());
                        }
                        for (int i = 0; i < listIdBarang.size(); i++) {
                            if (listIdVoucher.contains(listIdBarang.get(i))) {
                                if (voucherModels.get(0).getPRJENIS().equals("DISCOUNT")) {
                                    int tot2 = totHarga + potongan;
                                    totHarga = tot2;

                                    potongan = Integer.parseInt(voucherModels.get(0).getPRPOTONGAN());

                                    lyVoucher.setVisibility(View.VISIBLE);
                                    tvPotongan.setText("" + voucherModels.get(0).getPRJENIS());
                                    tvNilaiPotongan.setText(new HelperClass().convertRupiah(Integer.parseInt(voucherModels.get(0).getPRPOTONGAN())));

                                    kodeVoucher = edtVoucher.getText().toString();

                                    int tot = totHarga - potongan;
                                    totHarga = tot;
                                    klikVoucher = 1;
                                    totalSeluruhnya.setText("" + new HelperClass().convertRupiah(tot));
                                } else {
                                    lyVoucher.setVisibility(View.VISIBLE);
                                    tvPotongan.setText("" + voucherModels.get(0).getPRJENIS());
                                    tvNilaiPotongan.setText(new HelperClass().convertRupiah(Integer.parseInt(voucherModels.get(0).getPRPOTONGAN())));

                                    kodeVoucher = edtVoucher.getText().toString();

                                    int tot2 = totHarga + potongan;
                                    totHarga = tot2;
                                    totalSeluruhnya.setText("" + new HelperClass().convertRupiah(tot2));
                                    potongan = 0;
                                }

                            } else {

                            }
                        }

                    } else {
//                        potongan = 0;
                        edtVoucher.setText("");
                        lyPotongan.setVisibility(View.GONE);
//                        lyVoucher.setVisibility(View.GONE);
                        edtVoucher.setError("" + voucherModels.get(0).getMessage());
                        int tot = totHarga + potongan;

                        totHarga = tot;

                        totalSeluruhnya.setText("" + new HelperClass().convertRupiah(tot));
                        potongan = 0;
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<VoucherModel>> call, Throwable t) {
                Toast.makeText(PembayaranActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

//    private void getDataBank(boolean rm) {
//        if (rm) {
//            if (divBank.getChildCount() > 0) divBank.removeAllViews();
//        }
//        ApiService apiService = ApiConfig.getInstanceRetrofit();
//        apiService.getRekeningAdmin(sharedPref.getIdUser(),
//                sharedPref.getSESSION()).enqueue(new Callback<ArrayList<RekeningModel>>() {
//            @Override
//            public void onResponse(Call<ArrayList<RekeningModel>> call, Response<ArrayList<RekeningModel>> response) {
//                if (response.isSuccessful()) {
//                    listBankPin = response.body();
//
//                    for (int i = 0; i < listBankPin.size(); i++) {
//                        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                        View view = layoutInflater.inflate(R.layout.row_bank2, null);
//
//                        TextView nama_bank = view.findViewById(R.id.tv_nama_bank);
//                        ImageView imageView = view.findViewById(R.id.image);
//                        RelativeLayout ly = view.findViewById(R.id.ly_bank);
//                        final RadioButton rb_bank = view.findViewById(R.id.rb_bank);
//
//                        rb_bank.setEnabled(false);
//
//                        nama_bank.setText("" + listBankPin.get(i).getrKBANK());
//                        Picasso.with(getApplicationContext())
//                                .load(Config.IMAGE_REKENING + listBankPin.get(i).getrKIMAGE())
//                                .into(imageView);
//
//                        final String posisi = "" + i;
//                        ly.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                rb_bank.setChecked(false);
//
//                                new AlertDialog.Builder(PembayaranActivity.this)
//                                        .setMessage("Transfer ke rekening ini ?")
//                                        .setCancelable(false)
//                                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int id) {
//                                                rb_bank.setChecked(true);
//
//                                                bankPin = listBankPin.get(Integer.parseInt(posisi)).getrKBANK();
//                                                rekPin = listBankPin.get(Integer.parseInt(posisi)).getrKNOMOR();
//                                            }
//                                        })
//                                        .setNegativeButton("Tidak", null)
//                                        .show();
//                            }
//                        });
//
////                        rb_bank.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
////                            @Override
////                            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
////                                rb_bank.clearFocus();
////                                rb_bank.isChecked();
////
////                                bankPin = listBankPin.get(Integer.parseInt(posisi)).getrKBANK();
////                                rekPin = listBankPin.get(Integer.parseInt(posisi)).getrKNOMOR();
////                            }
////                        });
//
//                        divBank.addView(view);
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ArrayList<RekeningModel>> call, Throwable t) {
//                Toast.makeText(PembayaranActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//        });
//    }

    private void ambil(final boolean rm) {
        if (rm) {
            if (divRekening.getChildCount() > 0) divRekening.removeAllViews();
        }
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getRekeningById(sharedPref.getIdUser(),
                sharedPref.getSESSION()).enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                if (response.isSuccessful()) {
                    listRek = response.body().rekening;
                    if (response.body().getSuccess() == 1) {
                        for (int i = 0; i < listRek.size(); i++) {
                            LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            View view = layoutInflater.inflate(R.layout.row_rek, null);
                            TextView tvNama = view.findViewById(R.id.tv_nama);
                            RadioButton rb_rek = view.findViewById(R.id.rb_rek);
                            RelativeLayout layout = view.findViewById(R.id.ly_bank);

                            tvNama.setText("" + listRek.get(i).RK_BANK);
                            final String idRek = listRek.get(i).ID_REKENING;
                            final int posisi = i;
                            layout.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                }
                            });

                            rb_rek.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                    namaRek = listRek.get(posisi).RK_NAME;
                                    noRek = listRek.get(posisi).RK_NOMOR;
                                    namaBank = listRek.get(posisi).RK_BANK;
                                }
                            });
                            divRekening.addView(view);
                        }
                    } else {

                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Toast.makeText(PembayaranActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getInten() {
        listAlamat = getIntent().getParcelableArrayListExtra("list");
        posisi = Integer.parseInt(getIntent().getStringExtra("posisi"));

        alamatKu = listAlamat.get(posisi).getPEIDKECAMATAN();

        edtAlamat.setText(listAlamat.get(posisi).getPEPROVINSI() + "\n"
                + listAlamat.get(posisi).getPEKOTA() + "\n"
                + listAlamat.get(posisi).getPEKECAMATAN() + "\n"
                + listAlamat.get(posisi).getPEALAMAT() + "\n"
                + listAlamat.get(posisi).getPEKODEPOS());

//        tvJumlahSaldo.setText("" + new HelperClass().convertRupiah(Integer.parseInt(sharedPref.getSALDO())));

        getPesanan();
    }

    private void getPesanan() {
        if (sharedPref.getTIPEBELI().equals("langsung")){
            if (sharedPref.getTIPEKLIK().equals("fav")){

                favoritModel = sharedPref.getFav();
                listIdToko.add(favoritModel.getIDUSER());
                ambilDataFav(favoritModel);

            }else {
                product = sharedPref.getProduk();
                listIdToko.add(product.getIDUSER());
                ambilDataLangsung(product);
            }

            rv.setVisibility(View.VISIBLE);
            lySatu.setVisibility(View.VISIBLE);
            div.setVisibility(View.GONE);



        }else {
            listPesanan.removeAll(listPesanan);
            listPesanan.addAll(Arrays.asList(db.pesananDAO().lihatPesananById("" + sharedPref.getIdUser())));
//        Toast.makeText(this, ""+listPesanan.size(), Toast.LENGTH_SHORT).show();
            if (listPesanan.size() == 0) {

            } else if (listPesanan.size() == 1) {
                listIdToko.add(listPesanan.get(0).idToko);
                rv.setVisibility(View.VISIBLE);
                lySatu.setVisibility(View.VISIBLE);
                div.setVisibility(View.GONE);

                ambilDataSatu(listPesanan);

            } else if (listPesanan.size() > 1) {
                for (int i = 0; i < listPesanan.size(); i++) {
                    if (!listIdToko.contains(listPesanan.get(i).idToko)) {
                        listIdToko.add(listPesanan.get(i).idToko);
                    }
                }
//            Toast.makeText(this, "size "+listIdToko.size() + " " + listIdToko, Toast.LENGTH_SHORT).show();

                if (listIdToko.size() == 1) {
                    rv.setVisibility(View.VISIBLE);
                    lySatu.setVisibility(View.VISIBLE);
                    div.setVisibility(View.GONE);

                    ambilDataSatu(listPesanan);
                } else {
                    edtCatatan.setVisibility(View.GONE);
                    tvCatatan.setVisibility(View.GONE);
                    ambilDataPerId(listIdToko);
                }
//
            }
        }
    }

//    private void dataLama(){
//        for (int i = 0; i < list.size(); i++) {
//
//            listPesananByIdToko.removeAll(listPesananByIdToko);
////            Toast.makeText(this, "" + list.get(i), Toast.LENGTH_SHORT).show();
//
//            LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            View view = layoutInflater.inflate(R.layout.row_pembayaran, null);
//
//            LinearLayout div_pem = view.findViewById(R.id.div_pembayaran);
//
//            EditText edtCatatan = view.findViewById(R.id.edt_catatan);
//            final Spinner spn_kurir = view.findViewById(R.id.spn_kurir2);
//            final Spinner spn_Harga = view.findViewById(R.id.spn_harga2);
//            final TextView tvHargaOngkir = view.findViewById(R.id.tv_harga_ongkir);
//
//            listPesananByIdToko.addAll(Arrays.asList(db.pesananDAO().lihatPesanan("" + list.get(i))));
//
//            for (int j = 0; j < listPesananByIdToko.size(); j++) {
//
//                LayoutInflater layoutInflater2 = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                View view2 = layoutInflater2.inflate(R.layout.row_pesanan_dua, null);
//
//                TextView jumlahPesan = view2.findViewById(R.id.txt_jumlah_pesan);
//                TextView namaToko = view2.findViewById(R.id.tv_nama_toko);
//                TextView namaBarang = view2.findViewById(R.id.tv_nama_barang);
//                TextView Harga = view2.findViewById(R.id.tv_harga);
//                TextView Jumlah = view2.findViewById(R.id.tv_jumlah);
//                ImageView imgBarang = view2.findViewById(R.id.img_barang);
//
//                namaToko.setText(listPesananByIdToko.get(j).getNamaToko());
//                namaBarang.setText(listPesananByIdToko.get(j).getNamaBarang());
//                String jumlah = listPesananByIdToko.get(j).getJumlahBarang();
//
//                Jumlah.setText("Jumlah Pesanan " + jumlah);
//                Harga.setText("" + new HelperClass().convertRupiah(Integer.parseInt(listPesananByIdToko.get(j).getHargaBarang())));
//
//                String gambar = listPesananByIdToko.get(j).getImgBarang();
//                String gb = gambar.replace("|", " ");
//                String strArray[] = gb.split(" ");
//
//                Picasso.with(PembayaranActivity.this)
//                        .load(Config.IMAGE_BARANG + strArray[0])
//                        .into(imgBarang);
//
//                int nilai = Integer.parseInt(listPesananByIdToko.get(j).getSubHargaBarang());
//                int nBerat = Integer.parseInt(listPesananByIdToko.get(j).getBerat());
//                int nJumlah = Integer.parseInt(listPesananByIdToko.get(j).getJumlahBarang());
//                a = a + nilai;
//                b = b + nBerat;
//                jumlahBarang = jumlahBarang + nJumlah;
//
//                listHargaBarang.add(listPesanan.get(i).getHargaBarang());
//                listIdBarang.add(listPesanan.get(i).getIdBarang());
//                listJumlahBarang.add(listPesanan.get(i).getJumlahBarang());
//                listKodePromo.add("");
//
//
//                alamatToko = listPesanan.get(j).getIdKota();
//                totHargaBarang.setText("" + new HelperClass().convertRupiah(a));
//
////                getKurir(alamatToko, alamatKu, String.valueOf(b));
//                listIdPengiriman.add(listAlamat.get(posisi).getIDPENGIRIMAN());
//
//                div_pem.addView(view2);
//            }
//
//            listCatatan.add(edtCatatan.getText().toString());
//
//            final List<String> listKurir2 = new ArrayList<String>();
//            listKurir2.add("jne");
//            listKurir2.add("tiki");
//            listKurir2.add("pos");
//            listKurir2.add("pcp");
//            listKurir2.add("esl");
//            listKurir2.add("rpx");
//            listKurir2.add("pandu");
//            listKurir2.add("wahan");
//            listKurir2.add("jnt");
//            listKurir2.add("pahala");
//            listKurir2.add("cahaya");
//            listKurir2.add("sap");
//            listKurir2.add("jet");
//            listKurir2.add("indah");
//            listKurir2.add("dse");
//            listKurir2.add("slis");
//            listKurir2.add("first");
//            listKurir2.add("ncs");
//            listKurir2.add("sta");
//
//            ArrayAdapter<String> adp2 = new ArrayAdapter<String>(PembayaranActivity.this, R.layout.support_simple_spinner_dropdown_item, listKurir2);
//            spn_kurir.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
//            spn_kurir.setAdapter(adp2);
//
//            spn_kurir.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                @Override
//                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                    listKurir.removeAll(listKurir);
//                    String kurirku = spn_kurir.getSelectedItem().toString();
////                    Toast.makeText(PembayaranActivity.this, ""+kurirku, Toast.LENGTH_SHORT).show();
//                    listKurir.add(kurirku);
//
//                    listHarga.removeAll(listHarga);
//                    jasaPengiriman.removeAll(jasaPengiriman);
//
//                    String kurir = spn_kurir.getSelectedItem().toString();
//                    ApiService apiService = ApiConfig.getInstanceRetrofit();
//                    apiService.getOngkir("subdistrict", alamatToko,
//                            "subdistrict",
//                            alamatKu,
//                            String.valueOf(b),
//                            kurir).enqueue(new Callback<ResponseBody>() {
//                        @Override
//                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                            if (response.isSuccessful()) {
//                                try {
//                                    JSONObject jsonObject = new JSONObject(response.body().string());
//                                    final JSONObject ongkir = jsonObject.optJSONObject("rajaongkir");
//                                    JSONArray jsonArray = ongkir.optJSONArray("results");
//                                    JSONObject jsonObject1 = jsonArray.optJSONObject(0);
//                                    String kode_kurir = jsonObject1.optString("code");
//                                    JSONArray jsonArray1 = jsonObject1.optJSONArray("costs");
//                                    for (int j = 0; j < jsonArray1.length(); j++) {
//                                        JSONObject jsonObject2 = jsonArray1.optJSONObject(j);
//                                        String tipe = jsonObject2.optString("service");
//
//                                        JSONArray jsonArray2 = jsonObject2.optJSONArray("cost");
//                                        JSONObject jsonObject3 = jsonArray2.optJSONObject(0);
//                                        String harga = jsonObject3.optString("value");
//                                        String lama = jsonObject3.optString("etd");
//
//                                        listHarga.add(harga);
//                                        listTipe.add(tipe);
//                                        jasaPengiriman.add(kode_kurir + " " + tipe + " " + lama + " hari " + new HelperClass().convertRupiah(Integer.parseInt(harga)));
//                                    }
//
//                                    ArrayAdapter<String> adpHarga = new ArrayAdapter<String>(PembayaranActivity.this, R.layout.support_simple_spinner_dropdown_item, jasaPengiriman);
//                                    spn_Harga.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
//                                    spn_Harga.setAdapter(adpHarga);
//                                    spn_Harga.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                                        @Override
//                                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                                            listJenisKurir.removeAll(listJenisKurir);
//                                            listOngkir.removeAll(listOngkir);
//
//                                            int x = spn_Harga.getSelectedItemPosition();
//
//                                            ongkirku = Integer.parseInt(listHarga.get(x));
//
//                                            tvHargaOngkir.setText("" + ongkirku);
//
//                                            listOngkir.add("" + tvHargaOngkir.getText().toString());
//
////                                            Toast.makeText(PembayaranActivity.this, ""+listOngkir.size(), Toast.LENGTH_SHORT).show();
//
//                                            jenisKurir = listTipe.get(x);
//                                            listJenisKurir.add(jenisKurir);
//
//
////                                            for (int j = 0; j < listOngkir.size(); j++) {
////                                                int ab = Integer.parseInt(listOngkir.get(j));
////                                                semuaOngkir = semuaOngkir + ab;
////                                            }
//
//                                            biayaKirim.setText("" + new HelperClass().convertRupiah(ongkirku));
//
////                                            totHarga = a + Integer.parseInt(listHarga.get(x) + 534);
//
//                                            int tot = a + 345;
//                                            totHarga = (tot + semuaOngkir) - potongan;
//
//                                            totalSeluruhnya.setText("" + new HelperClass().convertRupiah(totHarga));
//
////                                            if (totHarga > Integer.parseInt(sharedPref.getSALDO())) {
////                                                edtPembayaranSaldo.setText(sharedPref.getSALDO());
////                                            } else {
////                                                edtPembayaranSaldo.setText("" + totHarga);
////                                            }
////                                            tvPotonganSaldo.setText("Saldo yang digunakan");
////                                            tvNilaiPotonganSaldo.setText(new HelperClass().convertRupiah(saldoDigunakan));
////                                            semuaOngkir = 0;
//                                        }
//
//                                        @Override
//                                        public void onNothingSelected(AdapterView<?> adapterView) {
//
//                                        }
//                                    });
//
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                } catch (IOException e) {
//                                    e.printStackTrace();
//                                }
//                            }
//                        }
//
//                        @Override
//                        public void onFailure(Call<ResponseBody> call, Throwable t) {
//
//                        }
//                    });
//                }
//
//                @Override
//                public void onNothingSelected(AdapterView<?> adapterView) {
//
//                }
//            });
//
//            div.addView(view);
//        }
//    }fset

    public void setHargaOngkir(ArrayList<Integer> list){
        listOngkir.removeAll(listOngkir);

        int harga=0;
        for (int i = 0; i < list.size(); i++) {
            harga = harga + list.get(i);
            listOngkir.add(""+list.get(i));
        }
//        Toast.makeText(PembayaranActivity.this, ""+listOngkir, Toast.LENGTH_SHORT).show();

        biayaKirim.setText(""+new HelperClass().convertRupiah(harga));

        if (metode.equals("saldo")) {
            if (sharedPref.getTipeVoucher().equals("DISCOUNT")) {
                int h = harga + a;
                int tot = h + 345;

                totHarga = (tot - potongan);

                totHarga = totHarga - saldoDigunakan;

                totalSeluruhnya.setText("" + new HelperClass().convertRupiah(totHarga));
                tvPotonganSaldo.setText("Saldo yang digunakan");
                tvNilaiPotonganSaldo.setText(new HelperClass().convertRupiah(saldoDigunakan));
            } else {
                potongan = 0;
                int h = harga + a;
                int tot = h + 345;

                totHarga = (tot - potongan);

                totHarga = totHarga - saldoDigunakan;

                totalSeluruhnya.setText("" + new HelperClass().convertRupiah(totHarga));
                tvPotonganSaldo.setText("Saldo yang digunakan");
                tvNilaiPotonganSaldo.setText(new HelperClass().convertRupiah(saldoDigunakan));
            }

        } else {
            if (sharedPref.getTipeVoucher().equals("DISCOUNT")) {
                int h = harga + a;
                int tot = h + 345;

                totHarga = (tot - potongan);

                totalSeluruhnya.setText("" + new HelperClass().convertRupiah(totHarga));
            } else {
                potongan = 0;
                int h = harga + a;
                int tot = h + 345;

                totHarga = (tot - potongan);

                totalSeluruhnya.setText("" + new HelperClass().convertRupiah(totHarga));
            }

        }

    }

    public void setKomen(ArrayList<String> list){
        for (int i = 0; i < list.size(); i++) {
            listCatatan.add(list.get(i));
        }
    }


    private void ambilDataPerId(List<String> list) {
        pemTokoAdapter = new PesananPemTokoAdapter(PembayaranActivity.this,
                list,
                listPesanan,
                listAlamat,
                posisi);
        rvPem.setAdapter(pemTokoAdapter);
//        rvPem.setNestedScrollingEnabled(false);


        for (int i = 0; i <list.size() ; i++) {
            listPesananByIdToko.removeAll(listPesananByIdToko);
            listPesananByIdToko.addAll(Arrays.asList(db.pesananDAO().lihatPesanan("" + list.get(i))));
            for (int j = 0; j < listPesananByIdToko.size(); j++) {

                int nilai = Integer.parseInt(listPesananByIdToko.get(j).getSubHargaBarang());
                int nBerat = Integer.parseInt(listPesananByIdToko.get(j).getBerat());
                int nJumlah = Integer.parseInt(listPesananByIdToko.get(j).getJumlahBarang());
                a = a + nilai;
                b = b + nBerat;
                jumlahBarang = jumlahBarang + nJumlah;

                listHargaBarang.add(listPesanan.get(i).getHargaBarang());
                listIdBarang.add(listPesanan.get(i).getIdBarang());
                listJumlahBarang.add(listPesanan.get(i).getJumlahBarang());
                listKodePromo.add("");


                totHargaBarang.setText("" + new HelperClass().convertRupiah(a));

                listIdPengiriman.add(listAlamat.get(posisi).getIDPENGIRIMAN());

            }
        }


//        ambilKomen();
    }

    private void ambilDataSatu(ArrayList<Pesanan> listPesanan) {
        mAdapter = new PesananAdapter(listPesanan, tipe,getApplicationContext());
        rv.setAdapter(mAdapter);

        for (int i = 0; i < listPesanan.size(); i++) {
            int nilai = Integer.parseInt(listPesanan.get(i).getSubHargaBarang());
            int nBerat = Integer.parseInt(listPesanan.get(i).getBerat());
            int nJumlah = Integer.parseInt(listPesanan.get(i).getJumlahBarang());
            a = a + nilai;
            b = b + nBerat;

            listIdPengiriman.add(listAlamat.get(posisi).getIDPENGIRIMAN());

            listHargaBarang.add(listPesanan.get(i).getHargaBarang());
            listIdBarang.add(listPesanan.get(i).getIdBarang());
            listJumlahBarang.add(listPesanan.get(i).getJumlahBarang());
            listCatatan.add("");
            listKodePromo.add("");

            jumlahBarang = jumlahBarang + nJumlah;
        }


        alamatToko = listPesanan.get(0).getIdKecamatan();

        totHargaBarang.setText("" + new HelperClass().convertRupiah(a));

        getKurir(alamatToko, alamatKu, String.valueOf(b));
    }

    private void ambilDataLangsung(ListProduct listPesanan) {
        mAdapter = new PesananAdapter(listPesanan, "langsung",getApplicationContext());
        rv.setAdapter(mAdapter);

            int nilai = Integer.parseInt(listPesanan.getBAPRICE());
            int nBerat = Integer.parseInt(listPesanan.getBAWEIGHT());
            int nJumlah = 1;
            a = a + nilai;
            b = b + nBerat;

            listHargaBarang.add(listPesanan.getBAPRICE());
            listIdBarang.add(listPesanan.getIDBARANG());
            listJumlahBarang.add("1");
            listCatatan.add("");
            listKodePromo.add("");

            jumlahBarang = jumlahBarang + nJumlah;

        listIdPengiriman.add(listAlamat.get(posisi).getIDPENGIRIMAN());
        alamatToko = listPesanan.getUSDIDKECAMATAN();

        totHargaBarang.setText("" + new HelperClass().convertRupiah(a));

        getKurir(alamatToko, alamatKu, String.valueOf(b));
    }

    private void ambilDataFav(FavoritModel listPesanan) {
        mAdapter = new PesananAdapter(listPesanan, "langsung",getApplicationContext());
        rv.setAdapter(mAdapter);

        int nilai = Integer.parseInt(listPesanan.getBAPRICE());
        int nBerat = Integer.parseInt(listPesanan.getBAWEIGHT());
        int nJumlah = 1;
        a = a + nilai;
        b = b + nBerat;

        listHargaBarang.add(listPesanan.getBAPRICE());
        listIdBarang.add(listPesanan.getIDBARANG());
        listJumlahBarang.add("1");
        listCatatan.add("");
        listKodePromo.add("");

        jumlahBarang = jumlahBarang + nJumlah;

        listIdPengiriman.add(listAlamat.get(posisi).getIDPENGIRIMAN());
        alamatToko = listPesanan.getUSDIDKECAMATAN();

        totHargaBarang.setText("" + new HelperClass().convertRupiah(a));

        getKurir(alamatToko, alamatKu, String.valueOf(b));
    }

    private void getKurir(final String awal, final String tujuan, final String berat) {
        final List<String> list = new ArrayList<String>();
        list.add("jne");
        list.add("tiki");
        list.add("pos");
        list.add("wahan");
        list.add("jnt");
//        list.add("pcp");
//        list.add("esl");
//        list.add("rpx");
//        list.add("pandu");
//        list.add("pahala");
//        list.add("cahaya");
//        list.add("sap");
//        list.add("jet");
//        list.add("indah");
//        list.add("dse");
//        list.add("slis");
//        list.add("first");
//        list.add("ncs");
//        list.add("sta");

        ArrayAdapter<String> adp2 = new ArrayAdapter<String>(PembayaranActivity.this, R.layout.support_simple_spinner_dropdown_item, list);
        spnKurir.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
        spnKurir.setAdapter(adp2);

        spnKurir.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                listKurir.removeAll(listKurir);

                listKurir.removeAll(listKurir);
                kurirku = spnKurir.getSelectedItem().toString();
                listKurir.add(kurirku);

                listHarga.removeAll(listHarga);
                jasaPengiriman.removeAll(jasaPengiriman);
                String kurir = spnKurir.getSelectedItem().toString();
                ApiService apiService = ApiConfig.getInstanceRetrofit();
                apiService.getOngkir("subdistrict", awal,
                        "subdistrict",
                        tujuan,
                        berat,
                        kurir).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()) {
                            try {
                                JSONObject jsonObject = new JSONObject(response.body().string());
                                final JSONObject ongkir = jsonObject.optJSONObject("rajaongkir");
                                JSONObject objectStatus = ongkir.optJSONObject("status");
                                String ket = objectStatus.optString("description");
                                if (ket.equals("This key has reached the daily limit.")){

                                }else {
                                    JSONArray jsonArray = ongkir.optJSONArray("results");
                                    if (jsonArray.length() == 0) {

                                    } else {
                                        JSONObject jsonObject1 = jsonArray.optJSONObject(0);
                                        String kode_kurir = jsonObject1.optString("code");
                                        JSONArray jsonArray1 = jsonObject1.optJSONArray("costs");
                                        for (int j = 0; j < jsonArray1.length(); j++) {
                                            JSONObject jsonObject2 = jsonArray1.optJSONObject(j);
                                            String tipe = jsonObject2.optString("service");

                                            JSONArray jsonArray2 = jsonObject2.optJSONArray("cost");
                                            JSONObject jsonObject3 = jsonArray2.optJSONObject(0);
                                            String harga = jsonObject3.optString("value");
                                            String lama = jsonObject3.optString("etd");

                                            listHarga.add(harga);
                                            listTipe.add(tipe);
                                            jasaPengiriman.add(kode_kurir + " " + tipe + " " + lama + " hari " + new HelperClass().convertRupiah(Integer.parseInt(harga)));
                                        }


                                        ArrayAdapter<String> adp2 = new ArrayAdapter<String>(PembayaranActivity.this, R.layout.support_simple_spinner_dropdown_item, jasaPengiriman);
                                        spnHarga.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
                                        spnHarga.setAdapter(adp2);
                                        spnHarga.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                            @Override
                                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                                listJenisKurir.removeAll(listJenisKurir);
                                                listOngkir.removeAll(listOngkir);
                                                int x = spnHarga.getSelectedItemPosition();

                                                ongkirku = Integer.parseInt(listHarga.get(x));
                                                listOngkir.add("" + ongkirku);

                                                jenisKurir = listTipe.get(x);
                                                listJenisKurir.add(jenisKurir);

                                                biayaKirim.setText("" + new HelperClass().convertRupiah(Integer.parseInt(listHarga.get(x))));

                                                int tot = a + 345;
                                                if (metode.equals("saldo")) {
                                                    if (sharedPref.getTipeVoucher().equals("DISCOUNT")) {
                                                        totHarga = (tot + Integer.parseInt(listHarga.get(x))) - potongan;

                                                        totHarga = totHarga - saldoDigunakan;

                                                        totalSeluruhnya.setText("" + new HelperClass().convertRupiah(totHarga));
                                                        tvPotonganSaldo.setText("Saldo yang digunakan");
                                                        tvNilaiPotonganSaldo.setText(new HelperClass().convertRupiah(saldoDigunakan));
                                                    } else {
                                                        potongan = 0;
                                                        totHarga = (tot + Integer.parseInt(listHarga.get(x))) - potongan;

                                                        totHarga = totHarga - saldoDigunakan;

                                                        totalSeluruhnya.setText("" + new HelperClass().convertRupiah(totHarga));
                                                        tvPotonganSaldo.setText("Saldo yang digunakan");
                                                        tvNilaiPotonganSaldo.setText(new HelperClass().convertRupiah(saldoDigunakan));
                                                    }

                                                } else {
                                                    if (sharedPref.getTipeVoucher().equals("DISCOUNT")) {
                                                        totHarga = (tot + Integer.parseInt(listHarga.get(x))) - potongan;

                                                        totalSeluruhnya.setText("" + new HelperClass().convertRupiah(totHarga));
                                                    } else {
                                                        potongan = 0;
                                                        totHarga = (tot + Integer.parseInt(listHarga.get(x))) - potongan;

                                                        totalSeluruhnya.setText("" + new HelperClass().convertRupiah(totHarga));
                                                    }

                                                }

//                                            if (totHarga > Integer.parseInt(sharedPref.getSALDO())) {
//                                                edtPembayaranSaldo.setText(sharedPref.getSALDO());
//                                            } else {
//                                                edtPembayaranSaldo.setText("" + totHarga);
//                                            }

//                                            tvPotonganSaldo.setText("Saldo yang digunakan");
//                                            tvNilaiPotonganSaldo.setText(new HelperClass().convertRupiah(saldoDigunakan));
                                            }

                                            @Override
                                            public void onNothingSelected(AdapterView<?> adapterView) {

                                            }
                                        });
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void bayarTf() {

        if (bankPin.equals("")) {
            Toast.makeText(this, "Silahkan Pilih Bank terlebih dahulu", Toast.LENGTH_SHORT).show();
        } else {
            if (edtCatatan.getText().toString().isEmpty()) {
                listCatatan.add("");
            } else {
                listCatatan.add(edtCatatan.getText().toString());
            }
//            Toast.makeText(this, "peng "+listIdPengiriman, Toast.LENGTH_SHORT).show();
//            Toast.makeText(this, "peng "+listJumlahBarang, Toast.LENGTH_SHORT).show();
            final ProgressDialog pd = new ProgressDialog(this);
            pd.setTitle("Proses ...");
            pd.setCancelable(false);
            pd.show();
            ApiService apiService = ApiConfig.getInstanceRetrofit();
            apiService.pembayaranBarang(sharedPref.getIdUser(),
                    sharedPref.getSESSION(),
                    "" + noRek,
                    "" + namaRek,
                    "" + namaBank,
                    "" + bankPin,
                    "" + rekPin,
//                ""+edtJmlTrans.getText().toString(),
                    "BAYAR_FULL",
                    "" + kodeVoucher,
                    "",
                    "345",
                    listKurir,
                    listJenisKurir,
                    listOngkir,
                    listIdToko,
                    listIdPengiriman,
                    listIdBarang,
                    listJumlahBarang,
                    listCatatan).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        pd.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response.body().string());
                            String sukses = jsonObject.optString("success");
                            String pesan = jsonObject.optString("message");
                            if (sukses.equals("1")) {
                                final String kode = jsonObject.optString("kode_payment");
                                new AlertDialog.Builder(PembayaranActivity.this)
                                        .setMessage("" + pesan)
                                        .setCancelable(false)
                                        .setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                for (int i = 0; i < listPesanan.size(); i++) {
                                                    db.pesananDAO().deletePesanan(listPesanan.get(i));
                                                }
                                                sharedPref.savePrefString(SharedPref.TIPE_VOUCHER,"");
                                                sharedPref.savePrefString(SharedPref.POTONGAN_VOUCHER,"");
//                                            Intent intent = new Intent(getApplicationContext(), BuktiTfSaldoActivity.class);
//                                            intent.putExtra("data", "bayar");
//                                            intent.putExtra("rekening", rekPin);
//                                            intent.putExtra("bank", bankPin);
//                                            intent.putExtra("jumlah", totalSeluruhnya.getText().toString());
//                                            intent.putExtra("nama", namaRek);
//                                            intent.putExtra("img",imgBank);
//                                            intent.putExtra("kode",kode);
//                                            startActivity(intent);
                                                startActivity(new Intent(getApplicationContext(), HomeLikeShopeeActivity.class)
                                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                                finish();

                                            }
                                        })
                                        .show();
                            } else {
                                Toast.makeText(PembayaranActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    pd.dismiss();
                    Toast.makeText(PembayaranActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }


    }

    private void bayarDua(String idTrans, String idBarang) {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Proses 2 ...");
        pd.setCancelable(false);
        pd.show();

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.bayarDua(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                idTrans,
                "" + listAlamat.get(posisi).getIDPENGIRIMAN(),
                idBarang,
                "" + jumlahBarang,
                "catatan",
                "",
                "" + kurirku,
                "" + jenisKurir,
                "" + ongkirku,
                "" + a,
                "" + totHarga).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String pesan = jsonObject.optString("message");
                        Toast.makeText(PembayaranActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                        if (pesan.equals("Parameter kurang")) {

                        } else {
                            for (int i = 0; i < listPesanan.size(); i++) {
                                db.pesananDAO().deletePesanan(listPesanan.get(i));
                            }
                            startActivity(new Intent(getApplicationContext(), MainActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                            finish();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(PembayaranActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void bayarSaldo() {
        if (edtCatatan.getText().toString().isEmpty()) {

            listCatatan.add("");

        } else {
            listCatatan.add(edtCatatan.getText().toString());
        }

        if (saldoDigunakan >= totHarga) {
            ketSaldo = "SALDO";

        } else {
            ketSaldo = "SALDO_BAYAR";
//            Toast.makeText(this, "Maaf Saldo Anda Tidak Cukup", Toast.LENGTH_SHORT).show();
        }

        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Proses ...");
        pd.setCancelable(false);
        pd.show();
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.pembayaranBarang(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                "",
                "",
                "",
                "",
                "",
//                "",
                "" + ketSaldo,
                "" + kodeVoucher,
                "" + edtPembayaranSaldo.getText().toString(),
                "345",
                listKurir,
                listJenisKurir,
                listOngkir,
                listIdToko,
                listIdPengiriman,
                listIdBarang,
                listJumlahBarang,
                listCatatan).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String sukses = jsonObject.optString("success");
                        String pesan = jsonObject.optString("message");
                        if (sukses.equals("1")) {
                            new AlertDialog.Builder(PembayaranActivity.this)
                                    .setMessage("" + pesan)
                                    .setCancelable(false)
                                    .setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            for (int i = 0; i < listPesanan.size(); i++) {
                                                db.pesananDAO().deletePesanan(listPesanan.get(i));
                                            }
                                            sharedPref.savePrefString(SharedPref.TIPE_VOUCHER,"");
                                            sharedPref.savePrefString(SharedPref.POTONGAN_VOUCHER,"");
                                            startActivity(new Intent(getApplicationContext(), MainActivity.class)
                                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                            finish();
                                        }
                                    })
                                    .show();
                        } else {
                            Toast.makeText(PembayaranActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(PembayaranActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void bayar() {
        if (metode.equals("")) {
            Toast.makeText(this, "Silahkan Pilih Metode Pembayaran", Toast.LENGTH_SHORT).show();
        } else {
            if (metode.equals("transfer")) {
                if (listIdToko.size() == 1){
                    bayarTf();
                }else {
                    String[] komen = ((PesananPemTokoAdapter) rvPem.getAdapter()).komen;
                    String[] kurir = ((PesananPemTokoAdapter) rvPem.getAdapter()).tipeKurir;
                    String[] jKurir = ((PesananPemTokoAdapter) rvPem.getAdapter()).jenisKurir;

                    for (int i = 0; i <komen.length ; i++) {
                        listCatatan.add(komen[i]);
                    }
                    for (int i = 0; i <kurir.length ; i++) {
                        listKurir.add(kurir[i]);


                    }
                    for (int i = 0; i <jKurir.length ; i++) {
                        listJenisKurir.add(jKurir[i]);
                    }
                    bayarTf();
                }

            } else {
                if (listIdToko.size() == 1){
                    bayarSaldo();
                }else {
                    String[] komen = ((PesananPemTokoAdapter) rvPem.getAdapter()).komen;
                    String[] kurir = ((PesananPemTokoAdapter) rvPem.getAdapter()).tipeKurir;
                    String[] jKurir = ((PesananPemTokoAdapter) rvPem.getAdapter()).jenisKurir;

                    for (int i = 0; i <komen.length ; i++) {
                        listCatatan.add(komen[i]);
                    }
                    for (int i = 0; i <kurir.length ; i++) {
                        listKurir.add(kurir[i]);


                    }
                    for (int i = 0; i <jKurir.length ; i++) {
                        listJenisKurir.add(jKurir[i]);
                    }
                    bayarSaldo();
                }

//            Toast.makeText(this, "Pembayaran dengan saldo masih proses", Toast.LENGTH_SHORT).show();
            }
        }

    }

    private void initView() {
//        lyTransfer = (LinearLayout) findViewById(R.id.ly_transfer);
        lySaldo = (LinearLayout) findViewById(R.id.ly_saldo);
        edtAlamat = (EditText) findViewById(R.id.edt_alamat);
        rv = (RecyclerView) findViewById(R.id.rv);
        spnKurir = (Spinner) findViewById(R.id.spn_kurir);
        cbVoucher = (CheckBox) findViewById(R.id.cb_voucher);
        lyVoucher = (LinearLayout) findViewById(R.id.ly_voucher);
        totHargaBarang = (TextView) findViewById(R.id.tot_harga_barang);
        biayaKirim = (TextView) findViewById(R.id.biaya_kirim);
        totalSeluruhnya = (TextView) findViewById(R.id.total_seluruhnya);
        btnBayar = (Button) findViewById(R.id.btn_bayar);
        spnHarga = (Spinner) findViewById(R.id.spn_harga);
        edtNoRek = (EditText) findViewById(R.id.edt_no_rek);
        edtNama = (EditText) findViewById(R.id.edt_nama);
        edtBank = (EditText) findViewById(R.id.edt_bank);

//        tvJumlahSaldo = (TextView) findViewById(R.id.tv_jumlah_saldo);
//        edtPembayaranSaldo = (EditText) findViewById(R.id.edt_pembayaran_saldo);

        lySatu = (LinearLayout) findViewById(R.id.ly_satu);
        edtCatatan = (EditText) findViewById(R.id.edt_catatan);
        tvCatatan = (TextView) findViewById(R.id.tv_catatan);
//        rbBankPin = (RadioButton) findViewById(R.id.rb_bank_pin);
//        divBank = (LinearLayout) findViewById(R.id.div_bank);
//        rbRekening = (RadioButton) findViewById(R.id.rb_rekening);
//        divRekening = (LinearLayout) findViewById(R.id.div_rekening);
//        lyBankPin = (LinearLayout) findViewById(R.id.ly_bank_pin);
//        lyRek = (LinearLayout) findViewById(R.id.ly_rek);
        lyPotongan = (LinearLayout) findViewById(R.id.ly_potongan);
        tvPotongan = (TextView) findViewById(R.id.tv_potongan);
        tvNilaiPotongan = (TextView) findViewById(R.id.tv_nilai_potongan);
        btnGunakan = (Button) findViewById(R.id.btn_gunakan);
        edtVoucher = (EditText) findViewById(R.id.edt_voucher);
        lyPotonganSaldo = (LinearLayout) findViewById(R.id.ly_potongan_saldo);
        tvPotonganSaldo = (TextView) findViewById(R.id.tv_potongan_saldo);
        tvNilaiPotonganSaldo = (TextView) findViewById(R.id.tv_nilai_potongan_saldo);
//        pilihBank = (TextView) findViewById(R.id.pilih_bank);
        image = (ImageView) findViewById(R.id.image);
        tvNamaBank = (TextView) findViewById(R.id.tv_nama_bank);
//        pilihRek = (TextView) findViewById(R.id.pilih_rek);
//        tvNamaBank2 = (TextView) findViewById(R.id.tv_nama_bank2);
        imgClose = (ImageView) findViewById(R.id.img_close);
//        lyRekening = (LinearLayout) findViewById(R.id.ly_rekening);
        imgDown = (ImageView) findViewById(R.id.img_down);
        imgUp = (ImageView) findViewById(R.id.img_up);
//        div2 = (LinearLayout) findViewById(R.id.div2);
        imgCkSaldo = (ImageView) findViewById(R.id.img_ck_saldo);
        lyTf = (LinearLayout) findViewById(R.id.ly_tf);
        imgCkTf = (ImageView) findViewById(R.id.img_ck_tf);
        lyKlikVoucher = (LinearLayout) findViewById(R.id.ly_klik_voucher);
        tvKlikVoucher = (TextView) findViewById(R.id.tv_klik_voucher);
        lyHasilVoucher = (LinearLayout) findViewById(R.id.ly_hasil_voucher);
        tvHasilVoucher = (TextView) findViewById(R.id.tv_hasil_voucher);
        tvHapusVoucher = (TextView) findViewById(R.id.tv_hapus_voucher);
        rvPem = (RecyclerView) findViewById(R.id.rv_pem);
        div = (LinearLayout) findViewById(R.id.div);
    }

    private void mainButton(){

        imgDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgDown.setVisibility(View.GONE);
                imgUp.setVisibility(View.VISIBLE);
                lySaldo.setVisibility(View.VISIBLE);
                lyTf.setVisibility(View.VISIBLE);
            }
        });

        imgUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgDown.setVisibility(View.VISIBLE);
                imgUp.setVisibility(View.GONE);
                lySaldo.setVisibility(View.GONE);
                lyTf.setVisibility(View.GONE);
            }
        });
        lySaldo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgCkSaldo.setVisibility(View.VISIBLE);
                imgCkTf.setVisibility(View.GONE);
                sheetSaldo();
                metode = "saldo";
            }
        });

        lyTf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgCkTf.setVisibility(View.VISIBLE);
                imgCkSaldo.setVisibility(View.GONE);
                sheetBank();
                metode = "transfer";
                tvPotonganSaldo.setText("");
                tvNilaiPotonganSaldo.setText("");
                totHarga = totHarga + saldoDigunakan;
                saldoDigunakan = 0;
                totalSeluruhnya.setText("" + new HelperClass().convertRupiah(totHarga));
            }
        });

        View bottomSheet = findViewById(R.id.bottom_sheet);
        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                // React to state change
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });

        cbVoucher.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    lyVoucher.setVisibility(View.VISIBLE);
                } else {
                    lyVoucher.setVisibility(View.GONE);
                }
            }
        });

        btnGunakan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cekVoucher();
            }
        });

        btnBayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bayar();
            }
        });

        tvKlikVoucher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), KodePromoActivity.class);
                intent.putStringArrayListExtra("id", listIdBarang);
                intent.putExtra("total", "" + totHarga);
                intent.putParcelableArrayListExtra("listAlamat", listAlamat);
                intent.putExtra("posisiAlamat", "" + posisi);
                startActivity(intent);
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), EditAlamatActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("data", "bayar");
                finish();
            }
        });

        edtAlamat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), EditAlamatActivity.class);
                intent.putExtra("data", "bayar");
                startActivity(intent);
            }
        });

        tvHapusVoucher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sharedPref.getTipeVoucher().equals("DISCOUNTS")) {
                    totHarga = totHarga - potongan;
                    totalSeluruhnya.setText("" + new HelperClass().convertRupiah(totHarga));

                    lyHasilVoucher.setVisibility(View.GONE);
                    lyKlikVoucher.setVisibility(View.VISIBLE);
                    tvPotongan.setText("");

                    tvNilaiPotongan.setText("");
                    tvHasilVoucher.setVisibility(View.GONE);
                    tvHasilVoucher.setText("");

                    sharedPref.savePrefString(SharedPref.TIPE_VOUCHER, "");
                    sharedPref.savePrefString(SharedPref.POTONGAN_VOUCHER, "");
                } else {
                    lyHasilVoucher.setVisibility(View.GONE);
                    lyKlikVoucher.setVisibility(View.VISIBLE);
                    tvPotongan.setText("");

                    tvNilaiPotongan.setText("");
                    tvHasilVoucher.setVisibility(View.GONE);
                    tvHasilVoucher.setText("");

                    sharedPref.savePrefString(SharedPref.TIPE_VOUCHER, "");
                    sharedPref.savePrefString(SharedPref.POTONGAN_VOUCHER, "");
                }

            }
        });
    }

    @Override
    public void onBackPressed() {
        sharedPref.savePrefString(SharedPref.TIPE_VOUCHER, "");
        sharedPref.savePrefString(SharedPref.POTONGAN_VOUCHER, "");
        super.onBackPressed();
    }
}
