package ai.agusibrhim.design.topedc.Fragment;


import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Activity.HomeLikeShopeeActivity;
import ai.agusibrhim.design.topedc.Activity.LoginActivity;
import ai.agusibrhim.design.topedc.Adapter.AdapterNotif;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.Model.NotifModel;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotifikasiFragment extends Fragment {

    private SwipeRefreshLayout swipe;
    private LinearLayout ly;
    private LinearLayout divKosong;
    private ImageView imgClose;
    private RecyclerView rv;
    private ImageView imgKosong;
    SharedPref sharedPref;
    RecyclerView.LayoutManager layoutManager;
    AdapterNotif mAdapter;

    ArrayList<NotifModel> list = new ArrayList<>();
    private ImageView imgInternet;

    public NotifikasiFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_notifikasi, container, false);
        initView(view);

        awal();

        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData();
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), HomeLikeShopeeActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));

            }
        });

        return view;
    }


    private void awal() {
        sharedPref = new SharedPref(getActivity());
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rv.setLayoutManager(layoutManager);

        if (sharedPref.getSudahLogin()) {
            isNetworkAvailable();
        } else {
            divKosong.setVisibility(View.VISIBLE);
            startActivity(new Intent(getActivity(), LoginActivity.class));
        }

    }

    public boolean isNetworkAvailable() {
        // Get Connectivity Manager class object from Systems Service
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        // Get Network Info from connectivity Manager
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();

        // if no network is available networkInfo will be null
        // otherwise check if we are connected
        if (networkInfo != null && networkInfo.isConnected()) {
            rv.setVisibility(View.VISIBLE);
            imgInternet.setVisibility(View.GONE);
            getData();
            return true;
        } else {
            rv.setVisibility(View.GONE);
            imgInternet.setVisibility(View.VISIBLE);
            imgKosong.setVisibility(View.GONE);

//            Toast.makeText(DireksiActivity.this, "internet elek", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    private void getData() {
        swipe.setRefreshing(true);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getNotif(sharedPref.getIdUser(),
                sharedPref.getSESSION()).enqueue(new Callback<ArrayList<NotifModel>>() {
            @Override
            public void onResponse(Call<ArrayList<NotifModel>> call, Response<ArrayList<NotifModel>> response) {
                if (response.isSuccessful()) {
                    swipe.setRefreshing(false);
                    list = response.body();
                    if (list.get(0).getMessage().equals("Berhasil")) {
                        mAdapter = new AdapterNotif(list, getActivity());
                        rv.setAdapter(mAdapter);
                    } else {
                        divKosong.setVisibility(View.VISIBLE);
                        rv.setVisibility(View.GONE);
                    }
                } else {
                    swipe.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<NotifModel>> call, Throwable t) {
                swipe.setRefreshing(false);
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initView(View view) {
        swipe = (SwipeRefreshLayout) view.findViewById(R.id.swipe);
        ly = (LinearLayout) view.findViewById(R.id.ly);
        divKosong = (LinearLayout) view.findViewById(R.id.div_kosong);
        imgClose = (ImageView) view.findViewById(R.id.img_close);
        rv = (RecyclerView) view.findViewById(R.id.rv);
        imgKosong = (ImageView) view.findViewById(R.id.img_kosong);
        imgInternet = (ImageView) view.findViewById(R.id.img_internet);
    }
}
