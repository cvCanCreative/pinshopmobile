package ai.agusibrhim.design.topedc.Model;

import java.io.Serializable;

public class TransaksiDetail implements Serializable {
    public String ID_TRANSAKSI_DETAIL;
    public String ID_TRANSAKSI;
    public String TSD_QTY;
    public String TSD_ONGKIR;
    public String TSD_HARGA_ASLI;
    public String CREATED_AT;
    public String UPDATED_AT;
    public String ID_SUB_KATEGORI;
    public String BA_IMAGE;
    public String BA_NAME;
    public String BA_SKU;

}
