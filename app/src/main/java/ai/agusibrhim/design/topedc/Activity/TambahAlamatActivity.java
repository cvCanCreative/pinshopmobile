package ai.agusibrhim.design.topedc.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.MainActivity;
import ai.agusibrhim.design.topedc.Model.AlamatModel;
import ai.agusibrhim.design.topedc.Model.KotaModel;
import ai.agusibrhim.design.topedc.Model.ProvinsiModel;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TambahAlamatActivity extends AppCompatActivity {

    private Spinner spnNamaAlamat;
    private EditText edtNama;
    private EditText edtNoTlp;
    private Spinner spnProvinsi;
    private Spinner spnKota;

    private EditText edtAlamat;
    private Button btnSimpan;
    List<String> listProvinsi = new ArrayList<>();
    List<String> listIdProvinsi = new ArrayList<>();
    List<String> listIdKota = new ArrayList<>();
    List<String> listIdKecamatan = new ArrayList<>();
    List<String> listTipe = new ArrayList<>();
    List<String> listKodePost = new ArrayList<>();

    List<String> setKota = new ArrayList<>();
    List<String> setKecamatan = new ArrayList<>();
    private EditText edtKodePos;
    SharedPref sharedPref;
    ArrayList<KotaModel> kota = new ArrayList<>();
    ArrayList<ProvinsiModel> provinsiMd = new ArrayList<>();
    ProvinsiModel provinsiModel = new ProvinsiModel();
    String idKota, idKecamatan;
    private Button btnEdit;
    String data;
    ArrayList<AlamatModel> listAlamat = new ArrayList<>();
    int posisi, a, b, c, d;
    private Spinner spnKecamatan;
    private ImageView imgClose;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_alamat);
        initView();

        awal();

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tambahAlamat();
            }
        });

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editAlamat();
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                startActivity(new Intent(getApplicationContext(), EditAlamatActivity.class)
//                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
//                finish();
                onBackPressed();
            }
        });
    }

    private void awal() {
        sharedPref = new SharedPref(this);
        data = getIntent().getStringExtra("data");
        if (data.equals("tambah")) {
            spinnerNama();
            setSpnProvinsi();
        } else if (data.equals("edit")) {
            btnEdit.setVisibility(View.VISIBLE);
            btnSimpan.setVisibility(View.GONE);
            listAlamat = getIntent().getParcelableArrayListExtra("list");
            posisi = Integer.parseInt(getIntent().getStringExtra("posisi"));

            edtNama.setText(listAlamat.get(posisi).getPENAMA());
            edtAlamat.setText(listAlamat.get(posisi).getPEALAMAT());
            edtKodePos.setText(listAlamat.get(posisi).getPEKODEPOS());
            edtNoTlp.setText(listAlamat.get(posisi).getPETELP());
            spinnerNamaEdit();
            setSpnProvinsiEdit();
        } else {
            spinnerNama();
            setSpnProvinsi();
        }

    }

    private void spinnerNamaEdit() {
        final List<String> list = new ArrayList<String>();
        list.add("Alamat Utama");
        list.add("Rumah");
        list.add("Apartemen");
        list.add("Kantor");

        ArrayAdapter<String> adp2 = new ArrayAdapter<String>(TambahAlamatActivity.this, R.layout.support_simple_spinner_dropdown_item, list);
        spnNamaAlamat.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
        spnNamaAlamat.setAdapter(adp2);

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).contains(listAlamat.get(posisi).getPEJUDUL())) {
                a = i;
            } else {

            }
        }
        spnNamaAlamat.setSelection(a);
    }

    private void setSpnProvinsiEdit() {
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getProvinsi().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        JSONObject jsonObject1 = jsonObject.optJSONObject("rajaongkir");
                        JSONArray jsonArray = jsonObject1.optJSONArray("results");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject2 = jsonArray.optJSONObject(i);
                            String provinsi = jsonObject2.optString("province");
                            String id = jsonObject2.optString("province_id");


                            provinsiModel.setProvince(provinsi);
                            provinsiModel.setProvinceId(id);

                            listProvinsi.add(provinsi);
                            listIdProvinsi.add(id);
                        }
                        ArrayAdapter<String> adp2 = new ArrayAdapter<String>(TambahAlamatActivity.this, R.layout.support_simple_spinner_dropdown_item, listProvinsi);
                        spnProvinsi.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
                        spnProvinsi.setAdapter(adp2);
                        for (int i = 0; i < listProvinsi.size(); i++) {
                            if (listProvinsi.get(i).contains(listAlamat.get(posisi).getPEPROVINSI())) {
                                b = i;

                            } else {

                            }
                        }
                        spnProvinsi.setSelection(b);

                        spnProvinsi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                int a = spnProvinsi.getSelectedItemPosition();
                                int idProv = a + 1;

//                                Toast.makeText(TambahAlamatActivity.this, ""+idProv, Toast.LENGTH_SHORT).show();
                                getKotaEdit(String.valueOf(idProv));
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(TambahAlamatActivity.this, "gagal ambil provinsi", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void tambahAlamat() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Proses ...");
        pd.setCancelable(false);
        pd.show();
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.addAlamatPengiriman(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                spnNamaAlamat.getSelectedItem().toString(),
                edtNama.getText().toString(),
                edtAlamat.getText().toString(),
                spnProvinsi.getSelectedItem().toString(),
                spnKota.getSelectedItem().toString(),
                idKota,
                spnKecamatan.getSelectedItem().toString(),
                idKecamatan,
                edtKodePos.getText().toString(),
                edtNoTlp.getText().toString()
        ).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String kode = jsonObject.optString("success");
                        String pesan = jsonObject.optString("message");
                        if (kode.equals("1")) {
                            Toast.makeText(TambahAlamatActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), EditAlamatActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                            intent.putExtra("data","edit");
                            intent.putExtra("data", "" + data);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(TambahAlamatActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(TambahAlamatActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void editAlamat() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Proses ...");
        pd.setCancelable(false);
        pd.show();
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.updateAlamatPengiriman(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                listAlamat.get(posisi).getIDPENGIRIMAN(),
                spnNamaAlamat.getSelectedItem().toString(),
                edtNama.getText().toString(),
                edtAlamat.getText().toString(),
                spnProvinsi.getSelectedItem().toString(),
                spnKota.getSelectedItem().toString(),
                idKota,
                spnKecamatan.getSelectedItem().toString(),
                idKecamatan,
                edtKodePos.getText().toString(),
                edtNoTlp.getText().toString()
        ).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String kode = jsonObject.optString("success");
                        String pesan = jsonObject.optString("message");
                        if (kode.equals("1")) {
                            Toast.makeText(TambahAlamatActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), EditAlamatActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("data", "edit");
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(TambahAlamatActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(TambahAlamatActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void spinnerNama() {
        final List<String> list = new ArrayList<String>();
        list.add("Alamat Utama");
        list.add("Rumah");
        list.add("Apartemen");
        list.add("Kantor");

        ArrayAdapter<String> adp2 = new ArrayAdapter<String>(TambahAlamatActivity.this, R.layout.support_simple_spinner_dropdown_item, list);
        spnNamaAlamat.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
        spnNamaAlamat.setAdapter(adp2);
    }

    private void setSpnProvinsi() {
        listProvinsi.removeAll(listProvinsi);
        listIdProvinsi.removeAll(listIdProvinsi);

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getProvinsi().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        JSONObject jsonObject1 = jsonObject.optJSONObject("rajaongkir");
                        JSONArray jsonArray = jsonObject1.optJSONArray("results");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject2 = jsonArray.optJSONObject(i);
                            String provinsi = jsonObject2.optString("province");
                            String id = jsonObject2.optString("province_id");


                            provinsiModel.setProvince(provinsi);
                            provinsiModel.setProvinceId(id);

                            listProvinsi.add(provinsi);
                            listIdProvinsi.add(id);
                        }
                        ArrayAdapter<String> adp2 = new ArrayAdapter<String>(TambahAlamatActivity.this, R.layout.support_simple_spinner_dropdown_item, listProvinsi);
                        spnProvinsi.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
                        spnProvinsi.setAdapter(adp2);

                        spnProvinsi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                int a = spnProvinsi.getSelectedItemPosition();
                                int idProv = a + 1;
//                                Toast.makeText(TambahAlamatActivity.this, "id prov "+idProv, Toast.LENGTH_SHORT).show();
                                getKota(String.valueOf(idProv));
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(TambahAlamatActivity.this, "gagal ambil provinsi", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void getKota(String id) {
        setKota.removeAll(setKota);
        listIdKota.removeAll(listIdKota);
        listKodePost.removeAll(listKodePost);

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getKota(id).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        JSONObject jsonObject1 = jsonObject.optJSONObject("rajaongkir");
                        JSONArray jsonArray = jsonObject1.optJSONArray("results");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject2 = jsonArray.optJSONObject(i);
                            String kota = jsonObject2.optString("city_name");
                            String kode = jsonObject2.optString("postal_code");
                            String tipe = jsonObject2.optString("type");
                            String idKota = jsonObject2.optString("city_id");
                            String idProvinsi = jsonObject2.optString("province_id");
                            String provinsi = jsonObject2.optString("province");

                            KotaModel kotaModel = new KotaModel();

                            kotaModel.setCityId(idKota);
                            kotaModel.setPostalCode(kode);
                            kotaModel.setCityName(tipe + " " + kota);
                            kotaModel.setProvinceId(idProvinsi);
                            kotaModel.setProvince(provinsi);

                            setKota.add(tipe + " " + kota);
                            listIdKota.add(idKota);
                            listKodePost.add(kode);
                        }
                        ArrayAdapter<String> adp2 = new ArrayAdapter<String>(TambahAlamatActivity.this, R.layout.support_simple_spinner_dropdown_item, setKota);
                        spnKota.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
                        spnKota.setAdapter(adp2);

                        spnKota.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                int a = spnKota.getSelectedItemPosition();
                                idKota = listIdKota.get(a);
                                edtKodePos.setText(listKodePost.get(a));
                                getKecamatan(idKota);
//                                Toast.makeText(TambahAlamatActivity.this, "id kota "+idKota + " pos "+edtKodePos.getText().toString(), Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void getKotaEdit(String id) {
        setKota.removeAll(setKota);
        listIdKota.removeAll(listIdKota);
        listKodePost.removeAll(listKodePost);

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getKota(id).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        JSONObject jsonObject1 = jsonObject.optJSONObject("rajaongkir");
                        JSONArray jsonArray = jsonObject1.optJSONArray("results");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject2 = jsonArray.optJSONObject(i);
                            String kota = jsonObject2.optString("city_name");
                            String kode = jsonObject2.optString("postal_code");
                            String tipe = jsonObject2.optString("type");
                            String idKota = jsonObject2.optString("city_id");
                            String idProvinsi = jsonObject2.optString("province_id");
                            String provinsi = jsonObject2.optString("province");

                            KotaModel kotaModel = new KotaModel();

                            kotaModel.setCityId(idKota);
                            kotaModel.setPostalCode(kode);
                            kotaModel.setCityName(tipe + " " + kota);
                            kotaModel.setProvinceId(idProvinsi);
                            kotaModel.setProvince(provinsi);

                            setKota.add(tipe + " " + kota);
                            listIdKota.add(idKota);
                            listKodePost.add(kode);
                        }
                        ArrayAdapter<String> adp2 = new ArrayAdapter<String>(TambahAlamatActivity.this, R.layout.support_simple_spinner_dropdown_item, setKota);
                        spnKota.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
                        spnKota.setAdapter(adp2);

                        for (int i = 0; i < setKota.size(); i++) {
                            if (setKota.get(i).contains(listAlamat.get(posisi).getPEKOTA())) {
                                c = i;
                            } else {

                            }
                        }
                        spnKota.setSelection(c);

                        spnKota.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                int a = spnKota.getSelectedItemPosition();
                                idKota = listIdKota.get(a);
                                edtKodePos.setText(listKodePost.get(a));
                                getKecamatanEdit(idKota);
//                                Toast.makeText(TambahAlamatActivity.this, "id "+idKota + " pos "+edtKodePos.getText().toString(), Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void getKecamatan(String id) {
        setKecamatan.removeAll(setKecamatan);
        listIdKecamatan.removeAll(listIdKecamatan);

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getKecamatan(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                id).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        JSONObject jsonObject1 = jsonObject.optJSONObject("rajaongkir");
                        JSONArray jsonArray = jsonObject1.optJSONArray("results");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject2 = jsonArray.optJSONObject(i);
                            String kecamatan = jsonObject2.optString("subdistrict_name");
                            String id_kecamatan = jsonObject2.optString("subdistrict_id");

                            setKecamatan.add(kecamatan);
                            listIdKecamatan.add(id_kecamatan);
                        }

                        ArrayAdapter<String> adp2 = new ArrayAdapter<String>(TambahAlamatActivity.this, R.layout.support_simple_spinner_dropdown_item, setKecamatan);
                        spnKecamatan.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
                        spnKecamatan.setAdapter(adp2);

                        spnKecamatan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                int a = spnKecamatan.getSelectedItemPosition();
                                idKecamatan = listIdKecamatan.get(a);
//                                Toast.makeText(TambahAlamatActivity.this, "id kec "+ idKecamatan, Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(TambahAlamatActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getKecamatanEdit(String id) {
        setKecamatan.removeAll(setKecamatan);
        listIdKecamatan.removeAll(listIdKecamatan);

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getKecamatan(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                id).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        JSONObject jsonObject1 = jsonObject.optJSONObject("rajaongkir");
                        JSONArray jsonArray = jsonObject1.optJSONArray("results");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject2 = jsonArray.optJSONObject(i);
                            String kecamatan = jsonObject2.optString("subdistrict_name");
                            String id_kecamatan = jsonObject2.optString("subdistrict_id");

                            setKecamatan.add(kecamatan);
                            listIdKecamatan.add(id_kecamatan);
                        }

                        ArrayAdapter<String> adp2 = new ArrayAdapter<String>(TambahAlamatActivity.this, R.layout.support_simple_spinner_dropdown_item, setKecamatan);
                        spnKecamatan.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
                        spnKecamatan.setAdapter(adp2);

                        for (int i = 0; i < setKecamatan.size(); i++) {
                            if (setKecamatan.get(i).contains(listAlamat.get(posisi).getPEKECAMATAN())) {
                                d = i;
                            } else {
                                d = 0;
                            }
                        }
                        spnKecamatan.setSelection(d);

                        spnKecamatan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                int a = spnKecamatan.getSelectedItemPosition();
                                idKecamatan = listIdKecamatan.get(a);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(TambahAlamatActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initView() {
        spnNamaAlamat = (Spinner) findViewById(R.id.spn_nama_alamat);
        edtNama = (EditText) findViewById(R.id.edt_nama);
        edtNoTlp = (EditText) findViewById(R.id.edt_no_tlp);
        spnProvinsi = (Spinner) findViewById(R.id.spn_provinsi);
        spnKota = (Spinner) findViewById(R.id.spn_kota);
        edtAlamat = (EditText) findViewById(R.id.edt_alamat);
        btnSimpan = (Button) findViewById(R.id.btn_simpan);
        edtKodePos = (EditText) findViewById(R.id.edt_kode_pos);
        btnEdit = (Button) findViewById(R.id.btn_edit);
        spnKecamatan = (Spinner) findViewById(R.id.spn_kecamatan);
        imgClose = (ImageView) findViewById(R.id.img_close);
    }
}
