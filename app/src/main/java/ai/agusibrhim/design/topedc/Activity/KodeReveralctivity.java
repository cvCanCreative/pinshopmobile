package ai.agusibrhim.design.topedc.Activity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.R;

public class KodeReveralctivity extends AppCompatActivity {

    private LinearLayout ly;
    private ImageView imgClose;
    private TextView tvReveral;
    private TextView tvSalin;
    private LinearLayout lyInvite;
    SharedPref pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kode_reveralctivity);
        initView();

        awal();
    }

    private void awal(){
        pref = new SharedPref(this);

        tvReveral.setText(pref.getKODEREVERAL());

        lyInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                share();
            }
        });

        tvSalin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClipboardManager clipboard = (ClipboardManager)getSystemService(CLIPBOARD_SERVICE);
                String getstring = tvReveral.getText().toString();
                Toast.makeText(getApplicationContext(), "Code Copied", Toast.LENGTH_SHORT).show();
                ClipData clip = ClipData.newPlainText("", getstring);
                clipboard.setPrimaryClip(clip);
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void share(){
        String message = ""+pref.getKODEREVERAL();
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, message);
        startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }

    private void initView() {
        ly = (LinearLayout) findViewById(R.id.ly);
        imgClose = (ImageView) findViewById(R.id.img_close);
        tvReveral = (TextView) findViewById(R.id.tv_reveral);
        tvSalin = (TextView) findViewById(R.id.tv_salin);
        lyInvite = (LinearLayout) findViewById(R.id.ly_invite);
    }
}
