package ai.agusibrhim.design.topedc.Adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Activity.DetailActivity;
import ai.agusibrhim.design.topedc.Activity.EditAlamatActivity;
import ai.agusibrhim.design.topedc.Activity.FavoritActivity;
import ai.agusibrhim.design.topedc.Activity.PembayaranActivity;
import ai.agusibrhim.design.topedc.DetailScrollingActivity;
import ai.agusibrhim.design.topedc.Helper.Config;
import ai.agusibrhim.design.topedc.Helper.HelperClass;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.MainActivity;
import ai.agusibrhim.design.topedc.Model.FavoritModel;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FavoritAdapter extends RecyclerView.Adapter<FavoritAdapter.Holder> {

    ArrayList<FavoritModel> list;
    Context context;
    SharedPref sharedPref;

    public FavoritAdapter(ArrayList<FavoritModel> list, Context context) {
        this.list = list;
        this.context = context;

        sharedPref = new SharedPref(context);
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_pesanan,parent,false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, final int position) {

        holder.btnTambah.setVisibility(View.GONE);
        holder.btnKurang.setVisibility(View.GONE);
        holder.hapus.setVisibility(View.GONE);
        holder.jumlahPesan.setVisibility(View.GONE);

        holder.namaToko.setText(list.get(position).getUSDTOKO());
        holder.namaBarang.setText(list.get(position).getBANAME());

        holder.Jumlah.setVisibility(View.GONE);
        holder.Harga.setText(""+new HelperClass().convertRupiah(Integer.parseInt(list.get(position).getBAPRICE())));

        String gambar = list.get(position).getBAIMAGE();
        String gb = gambar.replace("|"," ");
        String strArray[] = gb.split(" ");

        Picasso.with(context)
                .load(Config.IMAGE_BARANG + strArray[0])
                .into(holder.imgBarang);


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Holder extends RecyclerView.ViewHolder{

        TextView namaToko,namaBarang,Harga,Jumlah,hapus,jumlahPesan;
        ImageView imgBarang;
        Button btnKurang,btnTambah;
        public Holder(View itemView) {
            super(itemView);

            jumlahPesan = itemView.findViewById(R.id.txt_jumlah_pesan);
            btnKurang = itemView.findViewById(R.id.btn_kurang);
            btnTambah = itemView.findViewById(R.id.btn_tambah);
            hapus = itemView.findViewById(R.id.tv_hapus);
            namaToko = itemView.findViewById(R.id.tv_nama_toko);
            namaBarang = itemView.findViewById(R.id.tv_nama_barang);
            Harga = itemView.findViewById(R.id.tv_harga);
            Jumlah = itemView.findViewById(R.id.tv_jumlah);
            imgBarang = itemView.findViewById(R.id.img_barang);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    sharedPref.setFav(list.get(getAdapterPosition()));
                    sharedPref.savePrefString(SharedPref.TIPEKLIK,"fav");
                    Intent intent = new Intent(context, DetailScrollingActivity.class);
                    intent.putExtra("id_wis",list.get(getAdapterPosition()).getIDWISHLIST());
                    intent.putExtra("data", "fav");
                    intent.putExtra("list", list.get(getAdapterPosition()));
                    intent.putExtra("posisi",""+getAdapterPosition());
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });
        }
    }
}
