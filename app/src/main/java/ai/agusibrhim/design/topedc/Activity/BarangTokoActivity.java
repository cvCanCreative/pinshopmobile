package ai.agusibrhim.design.topedc.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ai.agusibrhim.design.topedc.Adapter.BarangTokoAdapter;
import ai.agusibrhim.design.topedc.Adapter.BarangTokoAdapterIndex;
import ai.agusibrhim.design.topedc.Adapter.ListProductAdapter;
import ai.agusibrhim.design.topedc.Adapter.ListProdukAdapterIndex;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.Model.BarangTokoModel;
import ai.agusibrhim.design.topedc.Model.ListProduct;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class BarangTokoActivity extends AppCompatActivity {

    private SwipeRefreshLayout swipe;
    private RecyclerView rv;
    RecyclerView.LayoutManager layoutManager;
    BarangTokoAdapter mAdapter;
    BarangTokoAdapterIndex mAdapter2;
    ArrayList<BarangTokoModel> list = new ArrayList<>();
    SharedPref sharedPref;
    private ImageView imgNoItem;
    private TextView tvNoItem;
    String data, id_toko;
    private EditText edtSearch;
    private Spinner spnFilter;
    int index =0;
    int indexTamu = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barang_toko);
        initView();

        awal();
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (data.equals("tamu")) {
                    id_toko = getIntent().getStringExtra("id");
                    if (id_toko.equals(sharedPref.getIdUser())) {
                        if (spnFilter.getSelectedItem().toString().equals("NEW")){
                            index = 1;
                            getFilterToko(index);
                        }else if (spnFilter.getSelectedItem().toString().equals("SECOND")){
                            index = 1;
                            getFilterToko(index);
                        }else {
                            getData();
                        }
                    } else {
                        if (spnFilter.getSelectedItem().toString().equals("NEW")){
                            indexTamu = 1;
                            getFilter(indexTamu);
                        }else if (spnFilter.getSelectedItem().toString().equals("SECOND")){
                            indexTamu = 1;
                            getFilter(indexTamu);
                        }else {
                            getDataTamu();
                        }
                    }
                } else {
                    if (spnFilter.getSelectedItem().toString().equals("NEW")){
                        index = 1;
                        getFilterToko(index);
                    }else if (spnFilter.getSelectedItem().toString().equals("SECOND")){
                        index = 1;
                        getFilterToko(index);
                    }else {
                        getData();
                    }
                }
            }
        });
        edtSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (data.equals("tamu")) {
                    id_toko = getIntent().getStringExtra("id");
                    if (id_toko.equals(sharedPref.getIdUser())) {
                        Intent intent = new Intent(getApplicationContext(), SearchActivity.class);
                        intent.putExtra("data", "toko");
                        intent.putExtra("id_toko", sharedPref.getIdUser());
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(getApplicationContext(), SearchActivity.class);
                        intent.putExtra("data", "tamu");
                        intent.putExtra("id_toko", id_toko);
                        startActivity(intent);
                    }
                } else {
                    Intent intent = new Intent(getApplicationContext(), SearchActivity.class);
                    intent.putExtra("data", "toko");
                    intent.putExtra("id_toko", sharedPref.getIdUser());
                    startActivity(intent);
                }

            }
        });
    }

    private void awal() {
        sharedPref = new SharedPref(this);
        data = getIntent().getStringExtra("data");

        mAdapter2 = new BarangTokoAdapterIndex(list, this,data);

        layoutManager = new GridLayoutManager(this, 2);
        rv.setLayoutManager(layoutManager);
        edtSearch.setFocusable(false);
        getSpinner();
//        getData();
    }

    private void getSpinner() {
        final List<String> list = new ArrayList<String>();
        list.add("ALL");
        list.add("NEW");
        list.add("SECOND");

        ArrayAdapter<String> adp2 = new ArrayAdapter<String>(BarangTokoActivity.this, R.layout.support_simple_spinner_dropdown_item, list);
        spnFilter.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
        spnFilter.setAdapter(adp2);

        spnFilter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (data.equals("tamu")) {
                    id_toko = getIntent().getStringExtra("id");
                    if (id_toko.equals(sharedPref.getIdUser())) {
                        if (spnFilter.getSelectedItem().toString().equals("NEW")){
                            index = 1;
                            getFilterToko(index);
                        }else if (spnFilter.getSelectedItem().toString().equals("SECOND")){
                            index = 1;
                            getFilterToko(index);
                        }else {
                            getData();
                        }
                    } else {
                        if (spnFilter.getSelectedItem().toString().equals("NEW")){
                            indexTamu = 1;
                            getFilter(indexTamu);
                        }else if (spnFilter.getSelectedItem().toString().equals("SECOND")){
                            indexTamu = 1;
                            getFilter(indexTamu);
                        }else {
                            getDataTamu();
                        }
                    }
                } else {
                    if (spnFilter.getSelectedItem().toString().equals("NEW")){
                        index = 1;
                        getFilterToko(index);
                    }else if (spnFilter.getSelectedItem().toString().equals("SECOND")){
                        index = 1;
                        getFilterToko(index);
                    }else {
                        getData();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void getFilter(int index) {
        swipe.setRefreshing(true);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getBarangTokoByKondisi(
                id_toko,
                spnFilter.getSelectedItem().toString(),
        index).enqueue(new Callback<ArrayList<BarangTokoModel>>() {
            @Override
            public void onResponse(Call<ArrayList<BarangTokoModel>> call, Response<ArrayList<BarangTokoModel>> response) {
                if (response.isSuccessful()) {
                    swipe.setRefreshing(false);
                    list = response.body();
                    if (list.size() > 0) {
                        mAdapter2 = new BarangTokoAdapterIndex(list, getApplicationContext(), data);
                        rv.setAdapter(mAdapter2);

                        mAdapter2.notifyDataSetChanged();

                        mAdapter2.setLoadMoreListener(new BarangTokoAdapterIndex.OnLoadMoreListener() {
                            @Override
                            public void onLoadMore() {
                                rv.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        indexTamu = indexTamu+1;
                                        loadMoreTamu(indexTamu);
                                    }
                                });
                            }
                        });
                    } else {
                        imgNoItem.setVisibility(View.VISIBLE);
                        tvNoItem.setVisibility(View.VISIBLE);
                        rv.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<BarangTokoModel>> call, Throwable t) {
                swipe.setRefreshing(false);
                Toast.makeText(BarangTokoActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }



    private void getFilterToko(int dex) {
        swipe.setRefreshing(true);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getBarangTokoByKondisi(
                sharedPref.getIdUser(),
                spnFilter.getSelectedItem().toString(),
                dex).enqueue(new Callback<ArrayList<BarangTokoModel>>() {
            @Override
            public void onResponse(Call<ArrayList<BarangTokoModel>> call, Response<ArrayList<BarangTokoModel>> response) {
                if (response.isSuccessful()) {
                    swipe.setRefreshing(false);
                    list = response.body();
                    if (list.size() > 0) {
                        mAdapter2 = new BarangTokoAdapterIndex(list, getApplicationContext(), data);
                        rv.setAdapter(mAdapter2);

                        mAdapter2.notifyDataSetChanged();

                        mAdapter2.setLoadMoreListener(new BarangTokoAdapterIndex.OnLoadMoreListener() {
                            @Override
                            public void onLoadMore() {
                                rv.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        index = index+1;
                                        loadMoreToko(index);
                                    }
                                });
                            }
                        });
                    } else {
                        imgNoItem.setVisibility(View.VISIBLE);
                        tvNoItem.setVisibility(View.VISIBLE);
                        rv.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<BarangTokoModel>> call, Throwable t) {
                swipe.setRefreshing(false);
                Toast.makeText(BarangTokoActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getDataTamu() {
        swipe.setRefreshing(true);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getBarangByIdToko(id_toko).enqueue(new Callback<ArrayList<BarangTokoModel>>() {
            @Override
            public void onResponse(Call<ArrayList<BarangTokoModel>> call, Response<ArrayList<BarangTokoModel>> response) {
                if (response.isSuccessful()) {
                    swipe.setRefreshing(false);
                    list = response.body();
                    if (list.get(0).getMessage().equals("Berhasil")) {
                        mAdapter = new BarangTokoAdapter(list, getApplicationContext(), data);
                        rv.setAdapter(mAdapter);
                    } else {
                        imgNoItem.setVisibility(View.VISIBLE);
                        tvNoItem.setVisibility(View.VISIBLE);
                        rv.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<BarangTokoModel>> call, Throwable t) {
                swipe.setRefreshing(false);
                Toast.makeText(BarangTokoActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getData() {
        swipe.setRefreshing(true);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getBarangToko(sharedPref.getIdUser(),
                sharedPref.getSESSION()).enqueue(new Callback<ArrayList<BarangTokoModel>>() {
            @Override
            public void onResponse(Call<ArrayList<BarangTokoModel>> call, Response<ArrayList<BarangTokoModel>> response) {
                if (response.isSuccessful()) {
                    swipe.setRefreshing(false);
                    list = response.body();
                    if (list.get(0).getMessage().equals("Berhasil")) {
                        mAdapter = new BarangTokoAdapter(list, getApplicationContext(), data);
                        rv.setAdapter(mAdapter);
                    } else {
                        imgNoItem.setVisibility(View.VISIBLE);
                        tvNoItem.setVisibility(View.VISIBLE);
                        rv.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<BarangTokoModel>> call, Throwable t) {
                swipe.setRefreshing(false);
                Toast.makeText(BarangTokoActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadMoreToko(int index) {
        list.add(new BarangTokoModel("mencari"));
        mAdapter2.notifyItemInserted(list.size() - 1);

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getBarangTokoByKondisi(
                sharedPref.getIdUser(),
                spnFilter.getSelectedItem().toString(),
                index).enqueue(new Callback<ArrayList<BarangTokoModel>>() {
            @Override
            public void onResponse(Call<ArrayList<BarangTokoModel>> call, Response<ArrayList<BarangTokoModel>> response) {
                if (response.isSuccessful()){
                    //remove loading view
                    list.remove(list.size() - 1);

                    ArrayList<BarangTokoModel> result = response.body();
                    ArrayList<String> listNama = new ArrayList<>();
                    if (result.size() > 0) {
                        //add loaded data
                        list.addAll(result);
                    } else {//result size 0 means there is no more data available at server
                        mAdapter2.setMoreDataAvailable(false);
                        //telling adapter to stop calling load more as no more server data available
//                        Toast.makeText(context, "No More Data Available", Toast.LENGTH_SHORT).show();
                    }
                    mAdapter2.notifyDataChanged();
                } else {
                    Log.e(TAG, " Load More Response Error " + String.valueOf(response.code()));
                }
            }

            @Override
            public void onFailure(Call<ArrayList<BarangTokoModel>> call, Throwable t) {
                Log.e(TAG, " Load More Response Error " + t.getMessage());
            }
        });
    }

    private void loadMoreTamu(int index) {
        list.add(new BarangTokoModel("mencari"));
        mAdapter2.notifyItemInserted(list.size() - 1);

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getBarangTokoByKondisi(
                id_toko,
                spnFilter.getSelectedItem().toString(),
                index).enqueue(new Callback<ArrayList<BarangTokoModel>>() {
            @Override
            public void onResponse(Call<ArrayList<BarangTokoModel>> call, Response<ArrayList<BarangTokoModel>> response) {
                if (response.isSuccessful()){
                    //remove loading view
                    list.remove(list.size() - 1);

                    ArrayList<BarangTokoModel> result = response.body();
                    ArrayList<String> listNama = new ArrayList<>();
                    if (result.size() > 0) {
                        //add loaded data
                        list.addAll(result);
                    } else {//result size 0 means there is no more data available at server
                        mAdapter2.setMoreDataAvailable(false);
                        //telling adapter to stop calling load more as no more server data available
//                        Toast.makeText(context, "No More Data Available", Toast.LENGTH_SHORT).show();
                    }
                    mAdapter2.notifyDataChanged();
                } else {
                    Log.e(TAG, " Load More Response Error " + String.valueOf(response.code()));
                }
            }

            @Override
            public void onFailure(Call<ArrayList<BarangTokoModel>> call, Throwable t) {
                Log.e(TAG, " Load More Response Error " + t.getMessage());
            }
        });
    }

    private void initView() {
        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe);
        rv = (RecyclerView) findViewById(R.id.rv);
        imgNoItem = (ImageView) findViewById(R.id.img_no_item);
        tvNoItem = (TextView) findViewById(R.id.tv_no_item);
        edtSearch = (EditText) findViewById(R.id.edt_search);
        spnFilter = (Spinner) findViewById(R.id.spn_filter);
    }
}
