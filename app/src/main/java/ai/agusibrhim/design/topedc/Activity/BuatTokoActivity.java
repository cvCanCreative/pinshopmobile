package ai.agusibrhim.design.topedc.Activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.MainActivity;
import ai.agusibrhim.design.topedc.Model.KotaModel;
import ai.agusibrhim.design.topedc.Model.ProvinsiModel;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BuatTokoActivity extends AppCompatActivity {

    private Toolbar tolbar;
    private EditText edtNama;
    private Spinner spnProvinsi;
    private Spinner spnKota;
    private EditText edtKodePos;
    private EditText edtAlamat;
    private Button btnSimpan;
    SharedPref sharedPref;
    ProvinsiModel provinsiModel = new ProvinsiModel();
    String idKota, idKecamatan;
    List<String> listProvinsi = new ArrayList<>();
    List<String> listIdProvinsi = new ArrayList<>();
    List<String> listIdKota = new ArrayList<>();
    List<String> listTipe = new ArrayList<>();
    List<String> listKodePost = new ArrayList<>();
    List<String> listIdKecamatan = new ArrayList<>();
    List<String> setKota = new ArrayList<>();
    List<String> setKecamatan = new ArrayList<>();
    String idProvinsi;
    private EditText edtDeskripsi;
    private Spinner spnKecamatan;
    private ImageView imgClose;
    private TextView tvHalo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buat_toko);
        initView();

        awal();

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tambahToko();
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void awal() {
        sharedPref = new SharedPref(this);
        tvHalo.setText("Halo, "+sharedPref.getUSERNAME());
        setSpnProvinsi();
    }

    private void tambahToko() {
        String alamat = spnProvinsi.getSelectedItem().toString() + " " + spnKota.getSelectedItem().toString() +
                " " + edtKodePos.getText().toString() + " " + edtAlamat.getText().toString();
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Proses ...");
        pd.setCancelable(false);
        pd.show();
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.tambahToko(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                alamat,
                "" + edtDeskripsi.getText().toString(),
                edtNama.getText().toString(),
                "" + idProvinsi,
                "" + idKota,
                "" + spnKota.getSelectedItem().toString(),
                "" + spnKecamatan.getSelectedItem().toString(),
                "" + idKecamatan
        ).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    if (response.isSuccessful()) {
                        pd.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response.body().string());
                            String pesan = jsonObject.optString("message");
                            if (pesan.equals("Berhasil ditambahkan")) {
                                new AlertDialog.Builder(BuatTokoActivity.this)
                                        .setMessage("" + pesan)
                                        .setCancelable(false)
                                        .setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                sharedPref.savePrefString(SharedPref.TOKO,"ada");
                                                startActivity(new Intent(getApplicationContext(), HomeLikeShopeeActivity.class)
                                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                                finish();
                                            }
                                        })
                                        .show();
                            } else {
                                Toast.makeText(BuatTokoActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(BuatTokoActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setSpnProvinsi() {
        listProvinsi.removeAll(listProvinsi);
        listIdProvinsi.removeAll(listIdProvinsi);

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getProvinsi().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        JSONObject jsonObject1 = jsonObject.optJSONObject("rajaongkir");
                        JSONArray jsonArray = jsonObject1.optJSONArray("results");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject2 = jsonArray.optJSONObject(i);
                            String provinsi = jsonObject2.optString("province");
                            String id = jsonObject2.optString("province_id");


                            provinsiModel.setProvince(provinsi);
                            provinsiModel.setProvinceId(id);

                            listProvinsi.add(provinsi);
                            listIdProvinsi.add(id);
                        }
                        ArrayAdapter<String> adp2 = new ArrayAdapter<String>(BuatTokoActivity.this, R.layout.support_simple_spinner_dropdown_item, listProvinsi);
                        spnProvinsi.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
                        spnProvinsi.setAdapter(adp2);

                        spnProvinsi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                int a = spnProvinsi.getSelectedItemPosition();
                                int idProv = a + 1;

                                idProvinsi = String.valueOf(idProv);
//                                Toast.makeText(TambahAlamatActivity.this, ""+idProv, Toast.LENGTH_SHORT).show();
                                getKota(String.valueOf(idProv));
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(BuatTokoActivity.this, "gagal ambil provinsi", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void getKota(String id) {
        setKota.removeAll(setKota);
        listIdKota.removeAll(listIdKota);
        listKodePost.removeAll(listKodePost);

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getKota(id).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        JSONObject jsonObject1 = jsonObject.optJSONObject("rajaongkir");
                        JSONArray jsonArray = jsonObject1.optJSONArray("results");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject2 = jsonArray.optJSONObject(i);
                            String kota = jsonObject2.optString("city_name");
                            String kode = jsonObject2.optString("postal_code");
                            String tipe = jsonObject2.optString("type");
                            String idKota = jsonObject2.optString("city_id");
                            String idProvinsi = jsonObject2.optString("province_id");
                            String provinsi = jsonObject2.optString("province");

                            KotaModel kotaModel = new KotaModel();

                            kotaModel.setCityId(idKota);
                            kotaModel.setPostalCode(kode);
                            kotaModel.setCityName(tipe + " " + kota);
                            kotaModel.setProvinceId(idProvinsi);
                            kotaModel.setProvince(provinsi);

                            setKota.add(tipe + " " + kota);
                            listIdKota.add(idKota);
                            listKodePost.add(kode);
                        }
                        ArrayAdapter<String> adp2 = new ArrayAdapter<String>(BuatTokoActivity.this, R.layout.support_simple_spinner_dropdown_item, setKota);
                        spnKota.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
                        spnKota.setAdapter(adp2);

                        spnKota.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                int a = spnKota.getSelectedItemPosition();
                                idKota = listIdKota.get(a);
                                edtKodePos.setText(listKodePost.get(a));
                                getKecamatan(idKota);
//                                Toast.makeText(TambahAlamatActivity.this, "id "+idKota + " pos "+edtKodePos.getText().toString(), Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void getKecamatan(String id) {
        setKecamatan.removeAll(setKecamatan);
        listIdKecamatan.removeAll(listIdKecamatan);

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getKecamatan(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                id).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        JSONObject jsonObject1 = jsonObject.optJSONObject("rajaongkir");
                        JSONArray jsonArray = jsonObject1.optJSONArray("results");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject2 = jsonArray.optJSONObject(i);
                            String kecamatan = jsonObject2.optString("subdistrict_name");
                            String id_kecamatan = jsonObject2.optString("subdistrict_id");

                            setKecamatan.add(kecamatan);
                            listIdKecamatan.add(id_kecamatan);
                        }

                        ArrayAdapter<String> adp2 = new ArrayAdapter<String>(BuatTokoActivity.this, R.layout.support_simple_spinner_dropdown_item, setKecamatan);
                        spnKecamatan.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
                        spnKecamatan.setAdapter(adp2);

                        spnKecamatan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                int a = spnKecamatan.getSelectedItemPosition();
                                idKecamatan = listIdKecamatan.get(a);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(BuatTokoActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void initView() {
        edtNama = (EditText) findViewById(R.id.edt_nama);
        spnProvinsi = (Spinner) findViewById(R.id.spn_provinsi);
        spnKota = (Spinner) findViewById(R.id.spn_kota);
        edtKodePos = (EditText) findViewById(R.id.edt_kode_pos);
        edtAlamat = (EditText) findViewById(R.id.edt_alamat);
        btnSimpan = (Button) findViewById(R.id.btn_simpan);
        edtDeskripsi = (EditText) findViewById(R.id.edt_deskripsi);
        spnKecamatan = (Spinner) findViewById(R.id.spn_kecamatan);
        imgClose = (ImageView) findViewById(R.id.img_close);
        tvHalo = (TextView) findViewById(R.id.tv_halo);
    }
}
