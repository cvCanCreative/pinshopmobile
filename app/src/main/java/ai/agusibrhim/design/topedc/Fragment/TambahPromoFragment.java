package ai.agusibrhim.design.topedc.Fragment;


import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import ai.agusibrhim.design.topedc.Activity.EditProfilActivity;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class TambahPromoFragment extends Fragment {


    private EditText edtNama;
    private EditText edtKode;
    private EditText edtPotongan;
    private EditText edtMin;
    private EditText edtKadaluarsa;
    private Button btnTambah;

    SharedPref sharedPref;
    DatePickerDialog datePickerDialog;
    int mYear;
    int mMonth;
    int mDay;
    private EditText edtPromoUntuk;
    String status;
    public TambahPromoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tambah_promo, container, false);
        initView(view);

        sharedPref = new SharedPref(getActivity());
        edtKadaluarsa.setFocusable(false);

        btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tambah();
            }
        });

        edtKadaluarsa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                settingTanggal();
                datePickerDialog.show();
            }
        });

        edtPromoUntuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilihan();
            }
        });


        return view;
    }

    private void clear() {
        edtNama.setText("");
        edtPotongan.setText("");
        edtKode.setText("");
        edtKadaluarsa.setText("");
        edtMin.setText("");
        edtPromoUntuk.setText("");
        edtNama.requestFocus();

    }

    private void settingTanggal() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        datePickerDialog = new
                DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                mYear = year;
                mMonth = month;
                mDay = dayOfMonth;

                GregorianCalendar c = new GregorianCalendar(mYear, mMonth, mDay);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                edtKadaluarsa.setText(sdf.format(c.getTime()));

                edtKadaluarsa.setFocusable(false);
                edtKadaluarsa.setCursorVisible(false);
                //edtTanggal.setText(tanggal);

            }
        }, year, month, day);
    }



    private void pilihan() {
        final CharSequence[] dialogItem = {"Member", "Non Member"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Tentukan Pilihan Anda");
        builder.setItems(dialogItem, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                switch (i) {
                    case 0:
                        status = "MEMBER";
                        edtPromoUntuk.setText("Member");
                        break;
                    case 1:
                        status = "NON_MEMBER";
                        edtPromoUntuk.setText("Non Member");
                        break;
                }
            }
        });
        builder.create().show();
    }

    private void tambah() {
        if (!validasi()) {
            return;
        }
        final ProgressDialog pd = new ProgressDialog(getActivity());
        pd.setMessage("Sending data...");
        pd.setCancelable(false);
        pd.show();
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.addPromo(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                edtNama.getText().toString(),
                edtKode.getText().toString(),
                edtPotongan.getText().toString(),
                edtKadaluarsa.getText().toString(),
                edtMin.getText().toString(),
                ""+status).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String pesan = jsonObject.optString("message");
                        if (pesan.equals("Berhasil ditambahkan")) {
                            Toast.makeText(getActivity(), "" + pesan, Toast.LENGTH_SHORT).show();
                            clear();
                        } else {
                            Toast.makeText(getActivity(), "" + pesan, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean validasi() {
        if (edtNama.getText().toString().isEmpty()) {
            edtNama.setError("Harus diisi");
            edtNama.requestFocus();

            return false;
        }
        if (edtKode.getText().toString().isEmpty()) {
            edtKode.setError("Harus diisi");
            edtKode.requestFocus();

            return false;
        }
        if (edtPotongan.getText().toString().isEmpty()) {
            edtPotongan.setError("Harus diisi");
            edtPotongan.requestFocus();

            return false;
        }
        if (edtKadaluarsa.getText().toString().isEmpty()) {
            edtKadaluarsa.setError("Harus diisi");
            edtKadaluarsa.requestFocus();

            return false;
        }
        if (edtPromoUntuk.getText().toString().isEmpty()) {
            edtPromoUntuk.setError("Harus diisi");
            edtPromoUntuk.requestFocus();

            return false;
        }

        return true;
    }

    private void initView(View view) {
        edtNama = (EditText) view.findViewById(R.id.edt_nama);
        edtKode = (EditText) view.findViewById(R.id.edt_kode);
        edtPotongan = (EditText) view.findViewById(R.id.edt_potongan);
        edtMin = (EditText) view.findViewById(R.id.edt_min);
        edtKadaluarsa = (EditText) view.findViewById(R.id.edt_kadaluarsa);
        btnTambah = (Button) view.findViewById(R.id.btn_tambah);
        edtPromoUntuk = (EditText) view.findViewById(R.id.edt_promo_untuk);
    }
}
