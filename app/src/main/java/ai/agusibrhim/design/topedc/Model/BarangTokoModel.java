package ai.agusibrhim.design.topedc.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BarangTokoModel implements Parcelable{
    @SerializedName("ID_BARANG")
    @Expose
    private String iDBARANG;
    @SerializedName("ID_USER")
    @Expose
    private String iDUSER;
    @SerializedName("ID_KATEGORI_BARANG")
    @Expose
    private String iDKATEGORIBARANG;
    @SerializedName("ID_PROMO")
    @Expose
    private String iDPROMO;
    @SerializedName("BA_IMAGE")
    @Expose
    private String bAIMAGE;
    @SerializedName("BA_NAME")
    @Expose
    private String bANAME;
    @SerializedName("BA_PRICE")
    @Expose
    private String bAPRICE;
    @SerializedName("BA_SKU")
    @Expose
    private String bASKU;
    @SerializedName("BA_DESCRIPTION")
    @Expose
    private String bADESCRIPTION;
    @SerializedName("BA_STOCK")
    @Expose
    private String bASTOCK;
    @SerializedName("BA_WEIGHT")
    @Expose
    private String bAWEIGHT;
    @SerializedName("BA_CONDITION")
    @Expose
    private String bACONDITION;
    @SerializedName("CREATED_AT")
    @Expose
    private String cREATEDAT;
    @SerializedName("UPDATED_AT")
    @Expose
    private String uPDATEDAT;
    @SerializedName("ID_USER_DETAIL")
    @Expose
    private String iDUSERDETAIL;
    @SerializedName("USD_FOTO")
    @Expose
    private String uSDFOTO;
    @SerializedName("USD_FULLNAME")
    @Expose
    private String uSDFULLNAME;
    @SerializedName("USD_BIRTH")
    @Expose
    private String uSDBIRTH;
    @SerializedName("USD_ADDRESS")
    @Expose
    private String uSDADDRESS;
    @SerializedName("USD_GENDER")
    @Expose
    private String uSDGENDER;
    @SerializedName("USD_TOKO")
    @Expose
    private String uSDTOKO;
    @SerializedName("USD_TOKO_DETAIL")
    @Expose
    private String uSDTOKODETAIL;
    @SerializedName("USD_ID_PROV")
    @Expose
    private String uSDIDPROV;
    @SerializedName("USD_ID_KOTA")
    @Expose
    private String uSDIDKOTA;
    @SerializedName("USD_KOTA")
    @Expose
    private String uSDKOTA;
    @SerializedName("USD_STATUS")
    @Expose
    private String uSDSTATUS;
    @SerializedName("USD_REFERRAL")
    @Expose
    private String uSDREFERRAL;
    @SerializedName("USD_REFERRAL_FROM")
    @Expose
    private String uSDREFERRALFROM;
    @SerializedName("PR_NAME")
    @Expose
    private String pRNAME;
    @SerializedName("PR_CODE")
    @Expose
    private Object pRCODE;
    @SerializedName("PR_POTONGAN")
    @Expose
    private String pRPOTONGAN;
    @SerializedName("PR_TYPE")
    @Expose
    private String pRTYPE;
    @SerializedName("PR_STATUS")
    @Expose
    private String pRSTATUS;
    @SerializedName("PR_EXPIRED")
    @Expose
    private String pREXPIRED;
    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("type")
    @Expose
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BarangTokoModel(String type) {
        this.type = type;
    }

    protected BarangTokoModel(Parcel in) {
        iDBARANG = in.readString();
        iDUSER = in.readString();
        iDKATEGORIBARANG = in.readString();
        iDPROMO = in.readString();
        bAIMAGE = in.readString();
        bANAME = in.readString();
        bAPRICE = in.readString();
        bASKU = in.readString();
        bADESCRIPTION = in.readString();
        bASTOCK = in.readString();
        bAWEIGHT = in.readString();
        bACONDITION = in.readString();
        cREATEDAT = in.readString();
        uPDATEDAT = in.readString();
        iDUSERDETAIL = in.readString();
        uSDFOTO = in.readString();
        uSDFULLNAME = in.readString();
        uSDBIRTH = in.readString();
        uSDADDRESS = in.readString();
        uSDGENDER = in.readString();
        uSDTOKO = in.readString();
        uSDTOKODETAIL = in.readString();
        uSDIDPROV = in.readString();
        uSDIDKOTA = in.readString();
        uSDKOTA = in.readString();
        uSDSTATUS = in.readString();
        uSDREFERRAL = in.readString();
        uSDREFERRALFROM = in.readString();
        pRNAME = in.readString();
        pRPOTONGAN = in.readString();
        pRTYPE = in.readString();
        pRSTATUS = in.readString();
        pREXPIRED = in.readString();
        success = in.readString();
        message = in.readString();
    }

    public static final Creator<BarangTokoModel> CREATOR = new Creator<BarangTokoModel>() {
        @Override
        public BarangTokoModel createFromParcel(Parcel in) {
            return new BarangTokoModel(in);
        }

        @Override
        public BarangTokoModel[] newArray(int size) {
            return new BarangTokoModel[size];
        }
    };

    public String getIDBARANG() {
        return iDBARANG;
    }

    public void setIDBARANG(String iDBARANG) {
        this.iDBARANG = iDBARANG;
    }

    public String getIDUSER() {
        return iDUSER;
    }

    public void setIDUSER(String iDUSER) {
        this.iDUSER = iDUSER;
    }

    public String getIDKATEGORIBARANG() {
        return iDKATEGORIBARANG;
    }

    public void setIDKATEGORIBARANG(String iDKATEGORIBARANG) {
        this.iDKATEGORIBARANG = iDKATEGORIBARANG;
    }

    public String getIDPROMO() {
        return iDPROMO;
    }

    public void setIDPROMO(String iDPROMO) {
        this.iDPROMO = iDPROMO;
    }

    public String getBAIMAGE() {
        return bAIMAGE;
    }

    public void setBAIMAGE(String bAIMAGE) {
        this.bAIMAGE = bAIMAGE;
    }

    public String getBANAME() {
        return bANAME;
    }

    public void setBANAME(String bANAME) {
        this.bANAME = bANAME;
    }

    public String getBAPRICE() {
        return bAPRICE;
    }

    public void setBAPRICE(String bAPRICE) {
        this.bAPRICE = bAPRICE;
    }

    public String getBASKU() {
        return bASKU;
    }

    public void setBASKU(String bASKU) {
        this.bASKU = bASKU;
    }

    public String getBADESCRIPTION() {
        return bADESCRIPTION;
    }

    public void setBADESCRIPTION(String bADESCRIPTION) {
        this.bADESCRIPTION = bADESCRIPTION;
    }

    public String getBASTOCK() {
        return bASTOCK;
    }

    public void setBASTOCK(String bASTOCK) {
        this.bASTOCK = bASTOCK;
    }

    public String getBAWEIGHT() {
        return bAWEIGHT;
    }

    public void setBAWEIGHT(String bAWEIGHT) {
        this.bAWEIGHT = bAWEIGHT;
    }

    public String getBACONDITION() {
        return bACONDITION;
    }

    public void setBACONDITION(String bACONDITION) {
        this.bACONDITION = bACONDITION;
    }

    public String getCREATEDAT() {
        return cREATEDAT;
    }

    public void setCREATEDAT(String cREATEDAT) {
        this.cREATEDAT = cREATEDAT;
    }

    public String getUPDATEDAT() {
        return uPDATEDAT;
    }

    public void setUPDATEDAT(String uPDATEDAT) {
        this.uPDATEDAT = uPDATEDAT;
    }

    public String getIDUSERDETAIL() {
        return iDUSERDETAIL;
    }

    public void setIDUSERDETAIL(String iDUSERDETAIL) {
        this.iDUSERDETAIL = iDUSERDETAIL;
    }

    public String getUSDFOTO() {
        return uSDFOTO;
    }

    public void setUSDFOTO(String uSDFOTO) {
        this.uSDFOTO = uSDFOTO;
    }

    public String getUSDFULLNAME() {
        return uSDFULLNAME;
    }

    public void setUSDFULLNAME(String uSDFULLNAME) {
        this.uSDFULLNAME = uSDFULLNAME;
    }

    public String getUSDBIRTH() {
        return uSDBIRTH;
    }

    public void setUSDBIRTH(String uSDBIRTH) {
        this.uSDBIRTH = uSDBIRTH;
    }

    public String getUSDADDRESS() {
        return uSDADDRESS;
    }

    public void setUSDADDRESS(String uSDADDRESS) {
        this.uSDADDRESS = uSDADDRESS;
    }

    public String getUSDGENDER() {
        return uSDGENDER;
    }

    public void setUSDGENDER(String uSDGENDER) {
        this.uSDGENDER = uSDGENDER;
    }

    public String getUSDTOKO() {
        return uSDTOKO;
    }

    public void setUSDTOKO(String uSDTOKO) {
        this.uSDTOKO = uSDTOKO;
    }

    public String getUSDTOKODETAIL() {
        return uSDTOKODETAIL;
    }

    public void setUSDTOKODETAIL(String uSDTOKODETAIL) {
        this.uSDTOKODETAIL = uSDTOKODETAIL;
    }

    public String getUSDIDPROV() {
        return uSDIDPROV;
    }

    public void setUSDIDPROV(String uSDIDPROV) {
        this.uSDIDPROV = uSDIDPROV;
    }

    public String getUSDIDKOTA() {
        return uSDIDKOTA;
    }

    public void setUSDIDKOTA(String uSDIDKOTA) {
        this.uSDIDKOTA = uSDIDKOTA;
    }

    public String getUSDKOTA() {
        return uSDKOTA;
    }

    public void setUSDKOTA(String uSDKOTA) {
        this.uSDKOTA = uSDKOTA;
    }

    public String getUSDSTATUS() {
        return uSDSTATUS;
    }

    public void setUSDSTATUS(String uSDSTATUS) {
        this.uSDSTATUS = uSDSTATUS;
    }

    public String getUSDREFERRAL() {
        return uSDREFERRAL;
    }

    public void setUSDREFERRAL(String uSDREFERRAL) {
        this.uSDREFERRAL = uSDREFERRAL;
    }

    public String getUSDREFERRALFROM() {
        return uSDREFERRALFROM;
    }

    public void setUSDREFERRALFROM(String uSDREFERRALFROM) {
        this.uSDREFERRALFROM = uSDREFERRALFROM;
    }

    public String getPRNAME() {
        return pRNAME;
    }

    public void setPRNAME(String pRNAME) {
        this.pRNAME = pRNAME;
    }

    public Object getPRCODE() {
        return pRCODE;
    }

    public void setPRCODE(Object pRCODE) {
        this.pRCODE = pRCODE;
    }

    public String getPRPOTONGAN() {
        return pRPOTONGAN;
    }

    public void setPRPOTONGAN(String pRPOTONGAN) {
        this.pRPOTONGAN = pRPOTONGAN;
    }

    public String getPRTYPE() {
        return pRTYPE;
    }

    public void setPRTYPE(String pRTYPE) {
        this.pRTYPE = pRTYPE;
    }

    public String getPRSTATUS() {
        return pRSTATUS;
    }

    public void setPRSTATUS(String pRSTATUS) {
        this.pRSTATUS = pRSTATUS;
    }

    public String getPREXPIRED() {
        return pREXPIRED;
    }

    public void setPREXPIRED(String pREXPIRED) {
        this.pREXPIRED = pREXPIRED;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(iDBARANG);
        parcel.writeString(iDUSER);
        parcel.writeString(iDKATEGORIBARANG);
        parcel.writeString(iDPROMO);
        parcel.writeString(bAIMAGE);
        parcel.writeString(bANAME);
        parcel.writeString(bAPRICE);
        parcel.writeString(bASKU);
        parcel.writeString(bADESCRIPTION);
        parcel.writeString(bASTOCK);
        parcel.writeString(bAWEIGHT);
        parcel.writeString(bACONDITION);
        parcel.writeString(cREATEDAT);
        parcel.writeString(uPDATEDAT);
        parcel.writeString(iDUSERDETAIL);
        parcel.writeString(uSDFOTO);
        parcel.writeString(uSDFULLNAME);
        parcel.writeString(uSDBIRTH);
        parcel.writeString(uSDADDRESS);
        parcel.writeString(uSDGENDER);
        parcel.writeString(uSDTOKO);
        parcel.writeString(uSDTOKODETAIL);
        parcel.writeString(uSDIDPROV);
        parcel.writeString(uSDIDKOTA);
        parcel.writeString(uSDKOTA);
        parcel.writeString(uSDSTATUS);
        parcel.writeString(uSDREFERRAL);
        parcel.writeString(uSDREFERRALFROM);
        parcel.writeString(pRNAME);
        parcel.writeString(pRPOTONGAN);
        parcel.writeString(pRTYPE);
        parcel.writeString(pRSTATUS);
        parcel.writeString(pREXPIRED);
        parcel.writeString(success);
        parcel.writeString(message);
    }
}
