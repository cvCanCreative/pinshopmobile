package ai.agusibrhim.design.topedc.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Model.HelpModel;
import ai.agusibrhim.design.topedc.R;

public class HelpAdapter extends RecyclerView.Adapter<HelpAdapter.Holder> {

    ArrayList<HelpModel> list;
    Context context;

    public HelpAdapter(ArrayList<HelpModel> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_help,parent,false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        holder.tvPesan.setText(""+list.get(position).getMessage());
        holder.tvJudul.setText(""+list.get(position).getHETITLE());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Holder extends RecyclerView.ViewHolder{

        TextView tvJudul,tvPesan;
        public Holder(View itemView) {
            super(itemView);

            tvJudul = itemView.findViewById(R.id.tv_tittle);
            tvPesan = itemView.findViewById(R.id.tv_pesan);
        }
    }
}
