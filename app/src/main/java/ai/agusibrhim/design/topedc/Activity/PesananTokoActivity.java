package ai.agusibrhim.design.topedc.Activity;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Adapter.PesananTokoAdater;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.Model.RequestTokoModel;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PesananTokoActivity extends AppCompatActivity {

    private SwipeRefreshLayout swipe;
    private TextView tvKet;
    private RecyclerView rv;
    SharedPref sharedPref;
    PesananTokoAdater mAdapter;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<RequestTokoModel> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pesanan_toko);
        initView();
        awal();

        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData();
            }
        });
    }

    private void awal(){
        sharedPref = new SharedPref(this);
        layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        rv.setLayoutManager(layoutManager);
        getData();
    }

    private void getData(){
        swipe.setRefreshing(true);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getRequestToko(sharedPref.getIdUser(),
                sharedPref.getSESSION()).enqueue(new Callback<ArrayList<RequestTokoModel>>() {
            @Override
            public void onResponse(Call<ArrayList<RequestTokoModel>> call, Response<ArrayList<RequestTokoModel>> response) {
                if (response.isSuccessful()){
                    swipe.setRefreshing(false);
                    list = response.body();
                    if (list.get(0).success == 1){
                        tvKet.setVisibility(View.GONE);
                        rv.setVisibility(View.VISIBLE);
                        mAdapter = new PesananTokoAdater(list,getApplicationContext());
                        rv.setAdapter(mAdapter);
                    }else {
                        tvKet.setVisibility(View.VISIBLE);
                        rv.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<RequestTokoModel>> call, Throwable t) {
                swipe.setRefreshing(false);
                Toast.makeText(PesananTokoActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initView() {
        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe);
        tvKet = (TextView) findViewById(R.id.tv_ket);
        rv = (RecyclerView) findViewById(R.id.rv);
    }
}
