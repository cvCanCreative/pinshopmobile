package ai.agusibrhim.design.topedc.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.request.RequestOptions;
import com.loopj.android.image.SmartImageView;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import ai.agusibrhim.design.topedc.Activity.DetailActivity;
import ai.agusibrhim.design.topedc.DetailScrollingActivity;
import ai.agusibrhim.design.topedc.Helper.Config;
import ai.agusibrhim.design.topedc.Helper.HelperClass;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.Model.ListProduct;
import ai.agusibrhim.design.topedc.Model.Product;
import ai.agusibrhim.design.topedc.R;

public class ListProductAdapter extends RecyclerView.Adapter<ListProductAdapter.Holdr>
{
	ArrayList<ListProduct> data;
	Context context;
	SharedPref sharedPref;

	private float mWidth,mHeight ;

	public ListProductAdapter(ArrayList<ListProduct> data, Context context) {
		this.data = data;
		this.context = context;
	}

	@Override
	public ListProductAdapter.Holdr onCreateViewHolder(ViewGroup p1, int p2) {
		return new Holdr(LayoutInflater.from(p1.getContext()).inflate(R.layout.row_produk_terbaru, null));
	}

	@Override
	public void onBindViewHolder(ListProductAdapter.Holdr holdr, int pos) {
		sharedPref = new SharedPref(context);

		ListProduct cat=data.get(pos);
		holdr.name.setText(cat.getBANAME());

		String gambar = cat.getBAIMAGE();
		String image = new HelperClass().splitText(gambar);

		Picasso.with(context)
				.load(Config.IMAGE_BARANG+image)
				.placeholder(R.drawable.image_loading)
				.error(R.drawable.no_image_found)
				.noFade()
				.into(holdr.img);

		holdr.store.setText(cat.getUSDTOKO());
		holdr.price.setText(""+new HelperClass().convertRupiah(Integer.valueOf(cat.getBAPRICE())));
		holdr.priceold.setText(_priceFormat("115000"));
		holdr.priceold.setPaintFlags(holdr.priceold.getPaintFlags() |android.graphics.Paint.STRIKE_THRU_TEXT_FLAG);
	}

	@Override
	public int getItemCount() {
		return 4;
	}

	public class Holdr extends RecyclerView.ViewHolder{
		TextView name, price, priceold, discount, store;
		SmartImageView img;
		RatingBar ratingbar;
		public Holdr(final View view){
			super(view);
			name=(TextView) view.findViewById(R.id.itemproductTextViewName);
			price=(TextView) view.findViewById(R.id.itemproductTextViewPrice);
			priceold=(TextView) view.findViewById(R.id.itemproductTextViewPold);
			discount=(TextView) view.findViewById(R.id.itemproductTextViewDisc);
			store=(TextView) view.findViewById(R.id.itemproductTextViewStore);
			img=(SmartImageView) view.findViewById(R.id.itemproductImageView1);
			ratingbar=(RatingBar) view.findViewById(R.id.itemproductRatingBar1);

			itemView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					sharedPref.setProduk(data.get(getAdapterPosition()));
					sharedPref.savePrefString(SharedPref.TIPEKLIK,"produk");

					Intent intent = new Intent(context, DetailScrollingActivity.class);
					intent.putExtra("data", "lihat");
					intent.putExtra("list", data.get(getAdapterPosition()));
					intent.putExtra("posisi",""+getAdapterPosition());
					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					context.startActivity(intent);
//					Toast.makeText(context, "bb"+getAdapterPosition(), Toast.LENGTH_SHORT).show();
				}
			});
		}
	}
	private static String _priceFormat(String s){
		double parsed = Double.parseDouble(s);
		String formatted = NumberFormat.getCurrencyInstance().format(parsed);
		return formatted;
	}
}
