package ai.agusibrhim.design.topedc.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatRatingBar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import ai.agusibrhim.design.topedc.Helper.Config;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.MainActivity;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RatingActivity extends AppCompatActivity {

    private AppCompatRatingBar rtBar;
    private TextView tvRate;
    private EditText edtKomentar;
    private Button btSubmit;
    private ImageView imgBarang;
    private TextView tvNamaBarang;
    SharedPref sharedPref;
    private ImageView imgClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating);
        initView();

        awal();

        rtBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                tvRate.setText("Rate : " + rating);
            }
        });

        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                post();
//                Toast.makeText(RatingActivity.this, " Rating " + "", Toast.LENGTH_SHORT).show();
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),DetailTransaksiActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("data", "user");
                finish();
            }
        });
    }

    private void awal() {
        sharedPref = new SharedPref(this);
        tvNamaBarang.setText(getIntent().getStringExtra("nama"));
        String gambar = getIntent().getStringExtra("image");
        String gb = gambar.replace("|", " ");
        String strArray[] = gb.split(" ");

        Picasso.with(getApplicationContext())
                .load(Config.IMAGE_BARANG + strArray[0])
                .into(imgBarang);

    }

    private void post() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Proses ...");
        pd.setCancelable(false);
        pd.show();
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.addRating(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                getIntent().getStringExtra("id"),
                "" + rtBar.getRating(),
                edtKomentar.getText().toString()
        ).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String kode = jsonObject.optString("success");
                        String pesan = jsonObject.optString("message");
                        if (kode.equals("1")) {
                            Toast.makeText(RatingActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(), MainActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                            finish();
                        } else {
                            Toast.makeText(RatingActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(RatingActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initView() {
        rtBar = (AppCompatRatingBar) findViewById(R.id.rt_bar);
        tvRate = (TextView) findViewById(R.id.tv_rate);
        edtKomentar = (EditText) findViewById(R.id.edt_komentar);
        btSubmit = (Button) findViewById(R.id.bt_submit);
        imgBarang = (ImageView) findViewById(R.id.img_barang);
        tvNamaBarang = (TextView) findViewById(R.id.tv_nama_barang);
        imgClose = (ImageView) findViewById(R.id.img_close);
    }
}
