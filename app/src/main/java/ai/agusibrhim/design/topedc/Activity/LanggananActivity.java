package ai.agusibrhim.design.topedc.Activity;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Adapter.AdapterMemberToko;
import ai.agusibrhim.design.topedc.Adapter.AdapterMemberUser;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.Model.MemberTokoModel;
import ai.agusibrhim.design.topedc.Model.MemberUserModel;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LanggananActivity extends AppCompatActivity {


    private LinearLayout ly;
    private TextView tvTittle;
    RecyclerView.LayoutManager layoutManager;
    AdapterMemberToko adapterToko;
    AdapterMemberUser adapterUser;
    ArrayList<MemberTokoModel> listToko = new ArrayList<>();
    ArrayList<MemberUserModel> listUser = new ArrayList<>();
    SharedPref sharedPref;
    private RecyclerView rv;
    private SwipeRefreshLayout swipe;
    String data, id;
    private TextView tvToko;
    private TextView tvUser;
    private ImageView imgClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_langganan);
        initView();

        awal();

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void awal() {
        sharedPref = new SharedPref(this);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rv.setLayoutManager(layoutManager);
        data = getIntent().getStringExtra("data");
        id = getIntent().getStringExtra("id");

        if (data.equals("toko")) {
            getToko();
            tvTittle.setText("Mengikuti");
        } else {
            tvTittle.setText("Pengikut");
            getUser();
        }
    }

    private void getToko() {
        swipe.setRefreshing(true);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.tokoYangDiikuti(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                id).enqueue(new Callback<ArrayList<MemberTokoModel>>() {
            @Override
            public void onResponse(Call<ArrayList<MemberTokoModel>> call, Response<ArrayList<MemberTokoModel>> response) {
                if (response.isSuccessful()) {
                    swipe.setRefreshing(false);
                    listToko = response.body();
                    if (listToko.get(0).getMessage().equals("Berhasil Diload")) {
                        adapterToko = new AdapterMemberToko(listToko, LanggananActivity.this);
                        rv.setAdapter(adapterToko);
                    } else {
                        tvToko.setVisibility(View.VISIBLE);
                        rv.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<MemberTokoModel>> call, Throwable t) {
                swipe.setRefreshing(false);
                Toast.makeText(getApplicationContext(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getUser() {
        swipe.setRefreshing(true);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.orangYangMengikutiKita(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                id).enqueue(new Callback<ArrayList<MemberUserModel>>() {
            @Override
            public void onResponse(Call<ArrayList<MemberUserModel>> call, Response<ArrayList<MemberUserModel>> response) {
                if (response.isSuccessful()) {
                    swipe.setRefreshing(false);
                    listUser = response.body();
                    if (listUser.get(0).getMessage().equals("Berhasil Diload")) {
                        adapterUser = new AdapterMemberUser(listUser, LanggananActivity.this, "member");
                        rv.setAdapter(adapterUser);
                    } else {
                        tvUser.setVisibility(View.VISIBLE);
                        rv.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<MemberUserModel>> call, Throwable t) {
                swipe.setRefreshing(false);
                Toast.makeText(getApplicationContext(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void initView() {

        ly = (LinearLayout) findViewById(R.id.ly);
        tvTittle = (TextView) findViewById(R.id.tv_tittle);
        rv = (RecyclerView) findViewById(R.id.rv);
        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe);
        tvToko = (TextView) findViewById(R.id.tv_toko);
        tvUser = (TextView) findViewById(R.id.tv_user);
        imgClose = (ImageView) findViewById(R.id.img_close);
    }
}
