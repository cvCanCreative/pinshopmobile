package ai.agusibrhim.design.topedc.Activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import ai.agusibrhim.design.topedc.Fragment.AkunFragment;
import ai.agusibrhim.design.topedc.Fragment.HomeFragment;
import ai.agusibrhim.design.topedc.Fragment.NotifikasiFragment;
import ai.agusibrhim.design.topedc.R;

public class HomeLikeShopeeActivity extends AppCompatActivity {

    private Menu menu;
    private MenuItem menuItem;
    private int chacked;
    private TextView mTextMessage;
//    android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_like_shopee);

        mTextMessage = (TextView) findViewById(R.id.message);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frame, new HomeFragment())
                .commit();

        buttomNav();
        getInten();


    }

    private void getInten(){
        if (getIntent() == null){
            menuItem = menu.getItem(0);
            menuItem.setChecked(true);
            callFragment(new HomeFragment());
        }else {
            String data = getIntent().getStringExtra("data");
            if (data == null){
//                Toast.makeText(this, ""+data, Toast.LENGTH_SHORT).show();
                menuItem = menu.getItem(0);
                menuItem.setChecked(true);
                callFragment(new HomeFragment());
            }else {
//                Toast.makeText(this, ""+data, Toast.LENGTH_SHORT).show();
                menuItem = menu.getItem(2);
                menuItem.setChecked(true);
                callFragment(new AkunFragment());
            }

        }
    }

    private void buttomNav(){
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.navigation);
        menu = bottomNavigationView.getMenu();
        menuItem = menu.getItem(0);
        menuItem.setChecked(true);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        menuItem = menu.getItem(0);
                        menuItem.setChecked(true);
                        callFragment(new HomeFragment());
                        break;
                    case R.id.navigation_dashboard:
                        menuItem = menu.getItem(1);
                        menuItem.setChecked(true);
                        callFragment(new NotifikasiFragment());
                        break;
                    case R.id.navigation_notifications:
                        menuItem = menu.getItem(2);
                        menuItem.setChecked(true);
                        callFragment(new AkunFragment());
                        break;
                }
                return false;
            }
        });
    }

    private void callFragment(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame, fragment)
                .commit();
    }

}
