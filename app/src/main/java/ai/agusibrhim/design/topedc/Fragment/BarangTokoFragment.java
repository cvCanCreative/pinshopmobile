package ai.agusibrhim.design.topedc.Fragment;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ai.agusibrhim.design.topedc.Activity.BarangTokoActivity;
import ai.agusibrhim.design.topedc.Activity.SearchActivity;
import ai.agusibrhim.design.topedc.Adapter.BarangTokoAdapter;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.Model.BarangTokoModel;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class BarangTokoFragment extends Fragment {


    private SwipeRefreshLayout swipe;
    private LinearLayout lySearch;
    private EditText edtSearch;
    private TextView filter;
    private Spinner spnFilter;
    private RecyclerView rv;
    private ImageView imgNoItem;
    private TextView tvNoItem;
    RecyclerView.LayoutManager layoutManager;
    BarangTokoAdapter mAdapter;
    ArrayList<BarangTokoModel> list = new ArrayList<>();
    String data, id_toko;
    SharedPref sharedPref;
    int index = 1;

    @SuppressLint("ValidFragment")
    public BarangTokoFragment(String data,String id_toko) {
        this.data = data;
        this.id_toko = id_toko;

        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_barang_toko, container, false);
        initView(view);

        awal();
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (data.equals("tamu")) {
                    if (id_toko.equals(sharedPref.getIdUser())) {
                        if (spnFilter.getSelectedItem().toString().equals("NEW")){
                            index = 1;
                            getFilterToko(index);
                        }else if (spnFilter.getSelectedItem().toString().equals("SECOND")){
                            index = 1;
                            getFilterToko(index);
                        }else {
                            getData();
                        }
                    } else {
                        if (spnFilter.getSelectedItem().toString().equals("NEW")){
                            index = 1;
                            getFilter(index);
                        }else if (spnFilter.getSelectedItem().toString().equals("SECOND")){
                            index = 1;
                            getFilter(index);
                        }else {
                            getDataTamu();
                        }
                    }
                } else {
                    if (spnFilter.getSelectedItem().toString().equals("NEW")){
                        index = 1;
                        getFilterToko(index);
                    }else if (spnFilter.getSelectedItem().toString().equals("SECOND")){
                        index = 1;
                        getFilterToko(index);
                    }else {
                        getData();
                    }
                }
            }
        });

        edtSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (data.equals("tamu")) {
                    if (id_toko.equals(sharedPref.getIdUser())) {
                        Intent intent = new Intent(getActivity(), SearchActivity.class);
                        intent.putExtra("data", "toko");
                        intent.putExtra("id_toko", sharedPref.getIdUser());
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(getActivity(), SearchActivity.class);
                        intent.putExtra("data", "tamu");
                        intent.putExtra("id_toko", id_toko);
                        startActivity(intent);
                    }
                } else {
                    Intent intent = new Intent(getActivity(), SearchActivity.class);
                    intent.putExtra("data", "toko");
                    intent.putExtra("id_toko", sharedPref.getIdUser());
                    startActivity(intent);
                }
            }
        });

        return view;
    }

    private void awal() {
        sharedPref = new SharedPref(getActivity());
        layoutManager = new GridLayoutManager(getActivity(), 2);
        rv.setLayoutManager(layoutManager);
        edtSearch.setFocusable(false);
        getSpinner();
//        getData();
    }

    private void getSpinner() {
        final List<String> list = new ArrayList<String>();
        list.add("ALL");
        list.add("NEW");
        list.add("SECOND");

        ArrayAdapter<String> adp2 = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, list);
        spnFilter.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
        spnFilter.setAdapter(adp2);

        spnFilter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (data.equals("tamu")) {
                    if (id_toko.equals(sharedPref.getIdUser())) {
                        if (spnFilter.getSelectedItem().toString().equals("NEW")){
                            index = 1;
                            getFilterToko(index);
                        }else if (spnFilter.getSelectedItem().toString().equals("SECOND")){
                            index = 1;
                            getFilterToko(index);
                        }else {
                            index = 1;
                            getData();
                        }
                    } else {
                        if (spnFilter.getSelectedItem().toString().equals("NEW")){
                            index = 1;
                            getFilter(index);
                        }else if (spnFilter.getSelectedItem().toString().equals("SECOND")){
                            index = 1;
                            getFilter(index);
                        }else {
                            index = 1;
                            getDataTamu();
                        }
                    }
                } else {
                    if (spnFilter.getSelectedItem().toString().equals("NEW")){
                        index = 1;
                        getFilterToko(index);
                    }else if (spnFilter.getSelectedItem().toString().equals("SECOND")){
                        index = 1;
                        getFilterToko(index);
                    }else {
                        index = 1;
                        getData();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void getFilter(int index) {
        swipe.setRefreshing(true);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getBarangTokoByKondisi(
                id_toko,
                spnFilter.getSelectedItem().toString(),
                index).enqueue(new Callback<ArrayList<BarangTokoModel>>() {
            @Override
            public void onResponse(Call<ArrayList<BarangTokoModel>> call, Response<ArrayList<BarangTokoModel>> response) {
                if (response.isSuccessful()) {
                    swipe.setRefreshing(false);
                    list = response.body();
                    if (list.size() > 0) {
                        mAdapter = new BarangTokoAdapter(list, getActivity(), data);
                        rv.setAdapter(mAdapter);
                    } else {
                        imgNoItem.setVisibility(View.VISIBLE);
                        tvNoItem.setVisibility(View.VISIBLE);
                        rv.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<BarangTokoModel>> call, Throwable t) {
                swipe.setRefreshing(false);
                Toast.makeText(getActivity(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getFilterToko(int index) {
        swipe.setRefreshing(true);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getBarangTokoByKondisi(
                sharedPref.getIdUser(),
                spnFilter.getSelectedItem().toString(),
                index).enqueue(new Callback<ArrayList<BarangTokoModel>>() {
            @Override
            public void onResponse(Call<ArrayList<BarangTokoModel>> call, Response<ArrayList<BarangTokoModel>> response) {
                if (response.isSuccessful()) {
                    swipe.setRefreshing(false);
                    list = response.body();
                    if (list.size() > 0) {
                        mAdapter = new BarangTokoAdapter(list, getActivity(), data);
                        rv.setAdapter(mAdapter);
                    } else {
                        imgNoItem.setVisibility(View.VISIBLE);
                        tvNoItem.setVisibility(View.VISIBLE);
                        rv.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<BarangTokoModel>> call, Throwable t) {
                swipe.setRefreshing(false);
                Toast.makeText(getActivity(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getDataTamu() {
        swipe.setRefreshing(true);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getBarangByIdToko(id_toko).enqueue(new Callback<ArrayList<BarangTokoModel>>() {
            @Override
            public void onResponse(Call<ArrayList<BarangTokoModel>> call, Response<ArrayList<BarangTokoModel>> response) {
                if (response.isSuccessful()) {
                    swipe.setRefreshing(false);
                    list = response.body();
                    if (list.get(0).getMessage().equals("Berhasil")) {
                        mAdapter = new BarangTokoAdapter(list,getActivity(), data);
                        rv.setAdapter(mAdapter);
                    } else {
                        imgNoItem.setVisibility(View.VISIBLE);
                        tvNoItem.setVisibility(View.VISIBLE);
                        rv.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<BarangTokoModel>> call, Throwable t) {
                swipe.setRefreshing(false);
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getData() {
        swipe.setRefreshing(true);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getBarangToko(sharedPref.getIdUser(),
                sharedPref.getSESSION()).enqueue(new Callback<ArrayList<BarangTokoModel>>() {
            @Override
            public void onResponse(Call<ArrayList<BarangTokoModel>> call, Response<ArrayList<BarangTokoModel>> response) {
                if (response.isSuccessful()) {
                    swipe.setRefreshing(false);
                    list = response.body();
                    if (list.get(0).getMessage().equals("Berhasil")) {
                        mAdapter = new BarangTokoAdapter(list, getActivity(), data);
                        rv.setAdapter(mAdapter);
                    } else {
                        imgNoItem.setVisibility(View.VISIBLE);
                        tvNoItem.setVisibility(View.VISIBLE);
                        rv.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<BarangTokoModel>> call, Throwable t) {
                swipe.setRefreshing(false);
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initView(View view) {
        swipe = (SwipeRefreshLayout) view.findViewById(R.id.swipe);
        lySearch = (LinearLayout) view.findViewById(R.id.ly_search);
        edtSearch = (EditText) view.findViewById(R.id.edt_search);
        filter = (TextView) view.findViewById(R.id.filter);
        spnFilter = (Spinner) view.findViewById(R.id.spn_filter);
        rv = (RecyclerView) view.findViewById(R.id.rv);
        imgNoItem = (ImageView) view.findViewById(R.id.img_no_item);
        tvNoItem = (TextView) view.findViewById(R.id.tv_no_item);
    }
}
