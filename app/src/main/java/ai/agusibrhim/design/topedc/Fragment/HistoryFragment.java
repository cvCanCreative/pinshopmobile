package ai.agusibrhim.design.topedc.Fragment;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Adapter.HistoryPembayaranAdapter;
import ai.agusibrhim.design.topedc.Adapter.HistorySaldoAdapter;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.Model.HistoryPembayaran;
import ai.agusibrhim.design.topedc.Model.HistorySaldo;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class HistoryFragment extends Fragment {


    String data;
    private SwipeRefreshLayout swipe;
    private RecyclerView rv;
    private LinearLayout lyKosong;
    RecyclerView.LayoutManager layoutManager;
    SharedPref sharedPref;
    ArrayList<HistorySaldo> list = new ArrayList<>();
    ArrayList<HistoryPembayaran> list2 = new ArrayList<>();
    HistorySaldoAdapter mAdapter;
    HistoryPembayaranAdapter mAdapter2;
    private ImageView imgNo;

    @SuppressLint("ValidFragment")
    public HistoryFragment(String data) {
        // Required empty public constructor
        this.data = data;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_history, container, false);
        initView(view);
        awal();

        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (data.equals("wd")) {
                    getDataWd();
                } else {
                    getDataTopup();
                }
            }
        });
        return view;
    }

    private void awal() {
        sharedPref = new SharedPref(getActivity());
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rv.setLayoutManager(layoutManager);

        if (data.equals("wd")) {
            getDataWd();
        } else {
            getDataTopup();
        }
    }

    private void getDataWd() {
        swipe.setRefreshing(true);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getHistoryWd(sharedPref.getIdUser(),
                sharedPref.getSESSION()).enqueue(new Callback<ArrayList<HistorySaldo>>() {
            @Override
            public void onResponse(Call<ArrayList<HistorySaldo>> call, Response<ArrayList<HistorySaldo>> response) {
                if (response.isSuccessful()) {
                    swipe.setRefreshing(false);
                    list = response.body();
                    if (list.get(0).getSuccess() == 1) {
                        mAdapter = new HistorySaldoAdapter(list, getActivity());
                        rv.setAdapter(mAdapter);
                    } else {
                        rv.setVisibility(View.GONE);
                        lyKosong.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<HistorySaldo>> call, Throwable t) {
                swipe.setRefreshing(false);
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getDataTopup() {
        swipe.setRefreshing(true);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getHistoryTopup(sharedPref.getIdUser(),
                sharedPref.getSESSION()).enqueue(new Callback<ArrayList<HistorySaldo>>() {
            @Override
            public void onResponse(Call<ArrayList<HistorySaldo>> call, Response<ArrayList<HistorySaldo>> response) {
                if (response.isSuccessful()) {
                    swipe.setRefreshing(false);
                    list = response.body();
                    if (list.get(0).getSuccess() == 1) {
                        mAdapter = new HistorySaldoAdapter(list, getActivity());
                        rv.setAdapter(mAdapter);
                    } else {
                        rv.setVisibility(View.GONE);
                        lyKosong.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<HistorySaldo>> call, Throwable t) {
                swipe.setRefreshing(false);
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initView(View view) {
        swipe = (SwipeRefreshLayout) view.findViewById(R.id.swipe);
        rv = (RecyclerView) view.findViewById(R.id.rv);
        lyKosong = (LinearLayout) view.findViewById(R.id.ly_kosong);
        imgNo = (ImageView) view.findViewById(R.id.img_no);
    }
}
