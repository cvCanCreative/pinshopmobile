package ai.agusibrhim.design.topedc.Activity;

import android.app.AlertDialog;
import android.arch.persistence.room.Room;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import java.util.ArrayList;
import java.util.Arrays;

import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.Helper.dataRoom.AppDatabase;
import ai.agusibrhim.design.topedc.Helper.dataRoom.Pesanan;
import ai.agusibrhim.design.topedc.R;

public class PengaturanAkunActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {


    SharedPref sharedPref;
    private RelativeLayout lyEditProfil;
    private RelativeLayout lyKeluar;
    private RelativeLayout lyEditAlamat;
    private RelativeLayout lyGantiPassword;
    private RelativeLayout lyEditRekening;
    GoogleApiClient googleApiClient;
    private ImageView imgClose;
    ArrayList<Pesanan> listPesanan = new ArrayList<>();
    private AppDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pengaturan_akun);
        initView();


        sharedPref = new SharedPref(this);

        db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "pesanandb").allowMainThreadQueries().build();

        listPesanan.removeAll(listPesanan);
        listPesanan.addAll(Arrays.asList(db.pesananDAO().lihatPesananById("" + sharedPref.getIdUser())));

        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        googleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this, this).addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions).build();

       mainButton();
    }

    private void mainButton(){
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),HomeLikeShopeeActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("data","profil");
                finish();
            }
        });
        lyKeluar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sharedPref.getTipeLogin().equals("manual")) {
                    new AlertDialog.Builder(PengaturanAkunActivity.this)
                            .setMessage("Apakah Anda ingin Keluar?")
                            .setCancelable(false)
                            .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    sharedPref.savePrefString(SharedPref.ID_USER, "");
                                    sharedPref.savePrefString(SharedPref.SESSION, "");
                                    sharedPref.savePrefString(SharedPref.SALDO, "");
                                    sharedPref.savePrefString(SharedPref.USERNAME, "");
                                    sharedPref.savePrefString(SharedPref.TOKO, "");
                                    sharedPref.savePrefBoolean(SharedPref.SUDAH_LOGIN, false);

                                    for (int i = 0; i < listPesanan.size(); i++) {
                                                    db.pesananDAO().deletePesanan(listPesanan.get(i));
                                                }
                                    startActivity(new Intent(getApplicationContext(), HomeLikeShopeeActivity.class)
                                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                    finish();
                                }
                            })
                            .setNegativeButton("Tidak", null)
                            .show();
                } else if (sharedPref.getTipeLogin().equals("gmail")) {
                    new AlertDialog.Builder(PengaturanAkunActivity.this)
                            .setMessage("Apakah Anda ingin Keluar?")
                            .setCancelable(false)
                            .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
                                        @Override
                                        public void onResult(@NonNull Status status) {
                                            sharedPref.savePrefString(SharedPref.ID_USER, "");
                                            sharedPref.savePrefString(SharedPref.SESSION, "");
                                            sharedPref.savePrefString(SharedPref.SALDO, "");
                                            sharedPref.savePrefString(SharedPref.USERNAME, "");
                                            sharedPref.savePrefString(SharedPref.TOKO, "");
                                            sharedPref.savePrefBoolean(SharedPref.SUDAH_LOGIN, false);
                                            for (int i = 0; i < listPesanan.size(); i++) {
                                                db.pesananDAO().deletePesanan(listPesanan.get(i));
                                            }
                                            startActivity(new Intent(getApplicationContext(), HomeLikeShopeeActivity.class)
                                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                            finish();
                                        }
                                    });
                                }
                            })
                            .setNegativeButton("Tidak", null)
                            .show();
                } else if (sharedPref.getTipeLogin().equals("facebook")) {
                    new AlertDialog.Builder(PengaturanAkunActivity.this)
                            .setMessage("Apakah Anda ingin Keluar?")
                            .setCancelable(false)
                            .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    LoginManager.getInstance().logOut();
                                    sharedPref.savePrefString(SharedPref.ID_USER, "");
                                    sharedPref.savePrefString(SharedPref.SESSION, "");
                                    sharedPref.savePrefString(SharedPref.SALDO, "");
                                    sharedPref.savePrefString(SharedPref.USERNAME, "");
                                    sharedPref.savePrefString(SharedPref.TOKO, "");
                                    sharedPref.savePrefBoolean(SharedPref.SUDAH_LOGIN, false);
                                    for (int i = 0; i < listPesanan.size(); i++) {
                                        db.pesananDAO().deletePesanan(listPesanan.get(i));
                                    }
                                    startActivity(new Intent(getApplicationContext(), HomeLikeShopeeActivity.class)
                                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                    finish();
                                }
                            })
                            .setNegativeButton("Tidak", null)
                            .show();
                }
            }
        });


        lyEditProfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), EditProfilActivity.class);
                startActivity(intent);
            }
        });

        lyEditAlamat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), EditAlamatActivity.class);
                intent.putExtra("data", "tambah");
                startActivity(intent);
            }
        });


        lyGantiPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), GantiPasswordActivity.class);
                startActivity(intent);
            }
        });


        lyEditRekening.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),LihatRekeningActivity.class);
                intent.putExtra("data", "tambah");
                startActivity(intent);
            }
        });

    }

    private void initView() {
        lyEditProfil = (RelativeLayout) findViewById(R.id.ly_edit_profil);
        lyKeluar = (RelativeLayout) findViewById(R.id.ly_keluar);
        lyEditAlamat = (RelativeLayout) findViewById(R.id.ly_edit_alamat);
        lyGantiPassword = (RelativeLayout) findViewById(R.id.ly_ganti_password);
        lyEditRekening = (RelativeLayout) findViewById(R.id.ly_edit_rekening);
        imgClose = (ImageView) findViewById(R.id.img_close);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
