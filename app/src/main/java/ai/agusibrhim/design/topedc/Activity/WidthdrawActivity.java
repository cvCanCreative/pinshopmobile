package ai.agusibrhim.design.topedc.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WidthdrawActivity extends AppCompatActivity {

    private EditText edtNominal;
    private ImageView imgSelfi;

    private ImageView imgKtp;

    private Button btnKirim;
    File imageFileSelfi, imageFileKtp;
    String pilih;
    SharedPref sharedPref;
    private EditText edtBank;
    private EditText edtNoRek;
    private ImageView imgClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_widthdraw);
        initView();
        sharedPref = new SharedPref(this);

        awal();

        imgSelfi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilih = "selfi";
                EasyImage.openChooserWithGallery(WidthdrawActivity.this, "chose", 3);
            }
        });
        imgKtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilih = "ktp";
                EasyImage.openChooserWithGallery(WidthdrawActivity.this, "chose", 3);
            }
        });

        btnKirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                kirim();
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), LihatRekeningActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("data",getIntent().getStringExtra("data"));
                finish();
            }
        });
    }

    private void awal() {
        edtBank.setFocusable(false);
        edtNoRek.setFocusable(false);

        edtBank.setText(getIntent().getStringExtra("nama"));
        edtNoRek.setText(getIntent().getStringExtra("no"));
    }

    private void kirim() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Loading ...");
        pd.setCancelable(true);
        pd.show();

        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), imageFileSelfi);
        MultipartBody.Part body = MultipartBody.Part.createFormData("image_selfie", imageFileSelfi.getName(), requestFile);

        RequestBody requestFile2 = RequestBody.create(MediaType.parse("multipart/form-data"), imageFileKtp);
        MultipartBody.Part body2 = MultipartBody.Part.createFormData("image_ktp", imageFileKtp.getName(), requestFile2);

        RequestBody idRek = RequestBody.create(MediaType.parse("text/plain"), getIntent().getStringExtra("id"));
        RequestBody nominal = RequestBody.create(MediaType.parse("text/plain"), edtNominal.getText().toString());
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.tarikSaldo(sharedPref.getIdUser(),
                sharedPref.getSESSION(), idRek,
                nominal,
                body, body2).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String pesan = jsonObject.optString("message");
                        String kode = jsonObject.optString("success");
                        if (kode.equals("1")) {
                            Toast.makeText(WidthdrawActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(WidthdrawActivity.this, HomeLikeShopeeActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                            finish();
                        } else {
                            Toast.makeText(WidthdrawActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(WidthdrawActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                //Some error handling
                Toast.makeText(WidthdrawActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                if (pilih.equals("selfi")) {

                    Glide.with(WidthdrawActivity.this)
                            .load(new File(imageFile.getPath()))
                            .into(imgSelfi);
                    imageFileSelfi = new File(imageFile.getPath());


                    try {
                        imageFileSelfi= new Compressor(WidthdrawActivity.this).compressToFile(imageFileSelfi);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {

                    Glide.with(WidthdrawActivity.this)
                            .load(new File(imageFile.getPath()))
                            .into(imgKtp);
                    imageFileKtp = new File(imageFile.getPath());

                    try {
                        imageFileKtp= new Compressor(WidthdrawActivity.this).compressToFile(imageFileKtp);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
                super.onCanceled(source, type);
            }
        });
    }

    private void initView() {
        edtNominal = (EditText) findViewById(R.id.edt_nominal);
        imgSelfi = (ImageView) findViewById(R.id.img_selfi);

        imgKtp = (ImageView) findViewById(R.id.img_ktp);

        btnKirim = (Button) findViewById(R.id.btn_kirim);
        edtBank = (EditText) findViewById(R.id.edt_bank);
        edtNoRek = (EditText) findViewById(R.id.edt_no_rek);
        imgClose = (ImageView) findViewById(R.id.img_close);
    }
}
