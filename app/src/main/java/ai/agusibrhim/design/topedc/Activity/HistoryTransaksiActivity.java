package ai.agusibrhim.design.topedc.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import ai.agusibrhim.design.topedc.Fragment.TransaksiFragment;
import ai.agusibrhim.design.topedc.MainActivity;
import ai.agusibrhim.design.topedc.R;

public class HistoryTransaksiActivity extends AppCompatActivity {

    private TabLayout viewpagertab2;
    private ViewPager viewPager;
    String data;
    private ImageView imgClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_transaksi);
        initView();

        awal();
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                startActivity(new Intent(getApplicationContext(), HomeLikeShopeeActivity.class)
//                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
//                finish();
                Intent intent = new Intent(getApplicationContext(),HomeLikeShopeeActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("data","profil");
                finish();

            }
        });
    }

    private void awal() {
        // data dari drawer dan adapter

        data = getIntent().getStringExtra("data");
        if (data.equals("drawer")) {
            setupViewPager(viewPager);
            viewpagertab2.setupWithViewPager(viewPager);
        }else if (data.equals("belumbayar")){
            setupViewPager(viewPager);
            viewpagertab2.setupWithViewPager(viewPager);
            int a = viewpagertab2.getSelectedTabPosition();
            if (a == 0) {

            } else {

            }
            TabLayout.Tab tab = viewpagertab2.getTabAt(0);
            tab.select();
        }else if (data.equals("dikemas")){
            setupViewPager(viewPager);
            viewpagertab2.setupWithViewPager(viewPager);
            int a = viewpagertab2.getSelectedTabPosition();
            if (a == 0) {

            } else {

            }
            TabLayout.Tab tab = viewpagertab2.getTabAt(1);
            tab.select();
        } else if (data.equals("dikirim")){
            setupViewPager(viewPager);
            viewpagertab2.setupWithViewPager(viewPager);
            int a = viewpagertab2.getSelectedTabPosition();
            if (a == 0) {

            } else {

            }
            TabLayout.Tab tab = viewpagertab2.getTabAt(2);
            tab.select();
        }else if (data.equals("sampai")){
            setupViewPager(viewPager);
            viewpagertab2.setupWithViewPager(viewPager);
            int a = viewpagertab2.getSelectedTabPosition();
            if (a == 0) {

            } else {

            }
            TabLayout.Tab tab = viewpagertab2.getTabAt(3);
            tab.select();
        }   else {
            setupViewPager(viewPager);
            viewpagertab2.setupWithViewPager(viewPager);
            int a = viewpagertab2.getSelectedTabPosition();
            if (a == 0) {

            } else {

            }
            TabLayout.Tab tab = viewpagertab2.getTabAt(3);
            tab.select();
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new TransaksiFragment("WAITING_PROSES"), "belum");
        adapter.addFragment(new TransaksiFragment("PROSES_VENDOR"), "dikemas");
        adapter.addFragment(new TransaksiFragment("PROSES_KIRIM"), "dikirim");
        adapter.addFragment(new TransaksiFragment("TERIMA"), "selesai");
        adapter.addFragment(new TransaksiFragment("BATAL"), "batal");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
//            return null;
        }
    }


    private void initView() {
        viewpagertab2 = (TabLayout) findViewById(R.id.viewpagertab2);
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        imgClose = (ImageView) findViewById(R.id.img_close);
    }
}
