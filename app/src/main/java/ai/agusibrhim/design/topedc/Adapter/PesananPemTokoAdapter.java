package ai.agusibrhim.design.topedc.Adapter;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ai.agusibrhim.design.topedc.Activity.PembayaranActivity;
import ai.agusibrhim.design.topedc.Helper.HelperClass;
import ai.agusibrhim.design.topedc.Helper.dataRoom.AppDatabase;
import ai.agusibrhim.design.topedc.Helper.dataRoom.Pesanan;
import ai.agusibrhim.design.topedc.Model.AlamatModel;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PesananPemTokoAdapter extends RecyclerView.Adapter<PesananPemTokoAdapter.Holder> {

    PembayaranActivity context;
    List<String> list;
    ArrayList<Pesanan> listPesananByIdToko = new ArrayList<>();

    ArrayList<Pesanan> listPesanan;
    ArrayList<AlamatModel> listAlamat;
    int posisiAlamat;
    String alamatKu, alamatToko;
    private AppDatabase db;
    int jumlahBarang;
    int a, b;
    int c =0;
    ListPesananAdapter pesananAdapter;
    RecyclerView.LayoutManager layoutManager;

    List<String> listKurir = new ArrayList<>();
    List<String> jasaPengiriman = new ArrayList<>();
    List<String> listHarga = new ArrayList<>();
    List<String> listTipe = new ArrayList<>();
    List<String> listOngkir = new ArrayList<>();
    List<String> listJenisKurir = new ArrayList<>();
    ArrayList<Integer> listHargaOngkir = new ArrayList<>();
    ArrayList<String> listCatatan = new ArrayList<>();
    int totHarga, ongkirku;
    String jkurir;
    public String[] komen;
    public String[] hargaOngkir;
    public String[] tipeKurir;
    public String[] jenisKurir;
    public String[] hargaBar;

    int berat = 0;

    public PesananPemTokoAdapter(PembayaranActivity context, List<String> list, ArrayList<Pesanan> listPesanan, ArrayList<AlamatModel> listAlamat, int posisiAlamat) {
        this.context = context;
        this.list = list;
        this.listPesanan = listPesanan;
        this.listAlamat = listAlamat;
        this.posisiAlamat = posisiAlamat;
        this.komen = new String[list.size()];
        this.hargaOngkir = new String[list.size()];
        this.tipeKurir = new String[list.size()];
        this.jenisKurir = new String[list.size()];
        this.hargaBar = new String[list.size()];

        for (int x = 0; x < list.size(); x++) {
            this.komen[x] = "Tidak ada catatan";
            this.hargaOngkir[x] = "0";
            this.tipeKurir[x] = "null";
            this.jenisKurir[x] = "null";
            this.hargaBar[x] = "0";
            this.listHargaOngkir.add(0);
        }

    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_pembayaran, parent, false);

        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final Holder holder, final int position) {
        String id = list.get(position);

        listPesananByIdToko.removeAll(listPesananByIdToko);

        db = Room.databaseBuilder(context,
                AppDatabase.class, "pesanandb").allowMainThreadQueries().build();

        listPesananByIdToko.addAll(Arrays.asList(db.pesananDAO().lihatPesanan("" + id)));

        for (int i = 0; i <listPesananByIdToko.size() ; i++) {
            int nBerat = Integer.parseInt(listPesananByIdToko.get(i).getBerat());
            b = b + nBerat;
        }


        layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        holder.divPembayaran.setLayoutManager(layoutManager);
        pesananAdapter = new ListPesananAdapter(context,
                listPesananByIdToko,
                listAlamat,
                posisiAlamat,
                listPesanan,
                list.get(position));

        holder.divPembayaran.setAdapter(pesananAdapter);

        holder.edtCatatan.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                PesananPemTokoAdapter.this.komen[position] = "" + charSequence.toString();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        alamatKu = listAlamat.get(posisiAlamat).getPEIDKECAMATAN();
        alamatToko = listPesanan.get(position).getIdKecamatan();

        final List<String> listKurir2 = new ArrayList<String>();
        listKurir2.add("jne");
        listKurir2.add("tiki");
        listKurir2.add("pos");
        listKurir2.add("wahan");
        listKurir2.add("jnt");

        ArrayAdapter<String> adp2 = new ArrayAdapter<String>(context, R.layout.support_simple_spinner_dropdown_item, listKurir2);
        holder.spnKurir2.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
        holder.spnKurir2.setAdapter(adp2);
        holder.spnKurir2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                listKurir.removeAll(listKurir);

                String kurirku = holder.spnKurir2.getSelectedItem().toString();

                PesananPemTokoAdapter.this.tipeKurir[holder.getAdapterPosition()] = "" + kurirku;

                listHarga.removeAll(listHarga);
                listTipe.removeAll(listTipe);

                jasaPengiriman.removeAll(jasaPengiriman);

                ApiService apiService = ApiConfig.getInstanceRetrofit();
                apiService.getOngkir("subdistrict", ""+alamatToko,
                        "subdistrict",
                        ""+alamatKu,
                        String.valueOf(b),
                        kurirku).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()) {
                            try {
                                JSONObject jsonObject = new JSONObject(response.body().string());
                                final JSONObject ongkir = jsonObject.optJSONObject("rajaongkir");
                                JSONObject objectStatus = ongkir.optJSONObject("status");
                                String ket = objectStatus.optString("description");
                                if (ket.equals("This key has reached the daily limit.")){

                                }else {
                                    JSONArray jsonArray = ongkir.optJSONArray("results");
                                    if (jsonArray.length() == 0) {

                                    } else {
                                        JSONObject jsonObject1 = jsonArray.optJSONObject(0);
                                        String kode_kurir = jsonObject1.optString("code");
                                        JSONArray jsonArray1 = jsonObject1.optJSONArray("costs");
                                        for (int j = 0; j < jsonArray1.length(); j++) {
                                            JSONObject jsonObject2 = jsonArray1.optJSONObject(j);
                                            String tipe = jsonObject2.optString("service");

                                            JSONArray jsonArray2 = jsonObject2.optJSONArray("cost");
                                            JSONObject jsonObject3 = jsonArray2.optJSONObject(0);
                                            String harga = jsonObject3.optString("value");
                                            String lama = jsonObject3.optString("etd");

                                            listHarga.add(harga);
                                            listTipe.add(tipe);

                                            jasaPengiriman.add(kode_kurir + " " + tipe + " " + lama + " hari " + new HelperClass().convertRupiah(Integer.parseInt(harga)));
                                        }
                                    }

//
                                    ArrayAdapter<String> adpHarga = new ArrayAdapter<String>(context, R.layout.support_simple_spinner_dropdown_item, jasaPengiriman);
                                    holder.spnHarga2.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
                                    holder.spnHarga2.setAdapter(adpHarga);
                                    holder.spnHarga2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                        @Override
                                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                            listJenisKurir.removeAll(listJenisKurir);
                                            listOngkir.removeAll(listOngkir);

                                            int x = holder.spnHarga2.getSelectedItemPosition();

                                            ongkirku = Integer.parseInt(listHarga.get(x));

//                                            PesananPemTokoAdapter.this.hargaOngkir[holder.getAdapterPosition()] = "" + ongkirku;

                                            jkurir = listTipe.get(x);


                                            PesananPemTokoAdapter.this.jenisKurir[holder.getAdapterPosition()] = "" + jkurir;


                                            listHargaOngkir.set(position, ongkirku);

                                            PembayaranActivity payment = (PembayaranActivity)context;
                                            payment.setHargaOngkir(listHargaOngkir);
                                        }

                                        @Override
                                        public void onNothingSelected(AdapterView<?> adapterView) {

                                        }
                                    });
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    public void setBerat(ArrayList<Integer> list){
        for (int i = 0; i < list.size(); i++) {
            berat = berat + list.get(i);
        }

    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        private RecyclerView divPembayaran;
        private Spinner spnKurir2;
        private Spinner spnHarga2;
        private TextView tvHargaOngkir;
        private EditText edtCatatan;

        public Holder(View itemView) {
            super(itemView);

            divPembayaran = (RecyclerView) itemView.findViewById(R.id.div_pembayaran);
            spnKurir2 = (Spinner) itemView.findViewById(R.id.spn_kurir2);
            spnHarga2 = (Spinner) itemView.findViewById(R.id.spn_harga2);
            tvHargaOngkir = (TextView) itemView.findViewById(R.id.tv_harga_ongkir);
            edtCatatan = (EditText) itemView.findViewById(R.id.edt_catatan);
        }
    }
}
