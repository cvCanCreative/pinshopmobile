package ai.agusibrhim.design.topedc.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Activity.LihatProductActivity;
import ai.agusibrhim.design.topedc.Activity.SubKategoriActivity;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Model.ListCategory;

public class CatagoryAdapter extends RecyclerView.Adapter<CatagoryAdapter.Holdr>
{

	ArrayList<ListCategory> data;
	Context context;
	String dataku;
	int size;

	public CatagoryAdapter(ArrayList<ListCategory> data, Context context,String dataku) {
		this.data = data;
		this.context = context;
		this.dataku = dataku;
	}

	@Override
	public CatagoryAdapter.Holdr onCreateViewHolder(ViewGroup p1, int p2) {
		if (dataku.equals("home")){
			return new Holdr(LayoutInflater.from(p1.getContext()).inflate(R.layout.item_card, null,false));
		}else {
			return new Holdr(LayoutInflater.from(p1.getContext()).inflate(R.layout.item_card2, p1,false));
		}

	}

	@Override
	public void onBindViewHolder(CatagoryAdapter.Holdr holdr, int pos) {
        final ListCategory cat =data.get(pos);
		holdr.title.setText(cat.getKBA_NAME());
//		Toast.makeText(context, ""+cat.getKBA_NAME(), Toast.LENGTH_SHORT).show();

		if(cat.getUrl()!=null && !cat.getUrl().isEmpty()){
			Picasso.with(context)
					.load( cat.getUrl() + cat.getKBA_IMAGE())
					.placeholder(R.drawable.ic_cat1)
					.error(R.drawable.ic_cat2)
					.fit()
					.noFade()
					.into(holdr.ic_cat);
		}else{
			holdr.ic_cat.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_cat8));
		}

//		holdr.ic_cat.setImageResource(R.drawable.ic_cat8);

		holdr.card.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(context,SubKategoriActivity.class);
				intent.putExtra("id",cat.getID_KATEGORI_BARANG());
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(intent);
			}
		});
	}

	@Override
	public int getItemCount() {
		if (dataku.equals("home")){
			size = 10;
		}else {
			size = data.size();
		}
		return size;
	}

	public class Holdr extends RecyclerView.ViewHolder{
		TextView title;
		ImageView ic_cat;
		View divider;
		LinearLayout card;

		public Holdr(final View itemView) {
			super(itemView);
			card = itemView.findViewById(R.id.card);
			title=(TextView) itemView.findViewById(R.id.itemcardTextView1);
			ic_cat=(ImageView) itemView.findViewById(R.id.itemcardImageView1);
			divider=itemView.findViewById(R.id.itemcardView1);

		}
	}
}
