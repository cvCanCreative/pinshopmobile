package ai.agusibrhim.design.topedc.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ai.agusibrhim.design.topedc.Adapter.CatagoryAdapter;
import ai.agusibrhim.design.topedc.Model.ListCategory;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class KategoriFragment extends Fragment {


    private RecyclerView rv;
    ArrayList<ListCategory> list = new ArrayList<>();
    CatagoryAdapter catagoryAdapter;
    RecyclerView.LayoutManager linearLayoutManager;

    public KategoriFragment() {
        // Required empty public constructor
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_kategori, container, false);
        initView(view);

        awal();

        return view;
    }

    private void awal() {
        linearLayoutManager = new GridLayoutManager(getActivity(), 2);
        rv.setLayoutManager(linearLayoutManager);

        getKategori();
    }

    private void getKategori() {
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getDataCatagory().enqueue(new Callback<ArrayList<ListCategory>>() {
            @Override
            public void onResponse(Call<ArrayList<ListCategory>> call, Response<ArrayList<ListCategory>> response) {
                if (response.isSuccessful()){
                    list = response.body();
                    catagoryAdapter = new CatagoryAdapter(list,getActivity(),"home");
                    rv.setAdapter(catagoryAdapter);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ListCategory>> call, Throwable t) {
                Toast.makeText(getActivity(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initView(View view) {
        rv = (RecyclerView) view.findViewById(R.id.rv);
    }
}
