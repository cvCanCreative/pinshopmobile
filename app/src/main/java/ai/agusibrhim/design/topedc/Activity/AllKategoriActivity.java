package ai.agusibrhim.design.topedc.Activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Adapter.CatagoryAdapter;
import ai.agusibrhim.design.topedc.Model.ListCategory;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllKategoriActivity extends AppCompatActivity {

    private LinearLayout ly;
    private RecyclerView rv;
    CatagoryAdapter adapter;
    ArrayList<ListCategory> list = new ArrayList<>();
    private ImageView imgClose;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_kategori);
        initView();
        awal();

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void awal() {
        rv.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        getData();
    }

    private void getData() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Loading ...");
        pd.setCancelable(false);
        pd.show();

        final ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getDataCatagory().enqueue(new Callback<ArrayList<ListCategory>>() {
            @Override
            public void onResponse(Call<ArrayList<ListCategory>> call, Response<ArrayList<ListCategory>> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    list = response.body();
                    adapter = new CatagoryAdapter(list, getApplicationContext(), "semua");
                    rv.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ListCategory>> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getApplicationContext(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initView() {
        ly = (LinearLayout) findViewById(R.id.ly);
        rv = (RecyclerView) findViewById(R.id.rv);
        imgClose = (ImageView) findViewById(R.id.img_close);
    }
}
