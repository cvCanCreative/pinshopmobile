package ai.agusibrhim.design.topedc.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Activity.LihatProductActivity;
import ai.agusibrhim.design.topedc.Helper.Config;
import ai.agusibrhim.design.topedc.Model.ListCategory;
import ai.agusibrhim.design.topedc.Model.SubKategoriModel;
import ai.agusibrhim.design.topedc.R;

public class SubKategoriAdapter extends RecyclerView.Adapter<SubKategoriAdapter.Holder> {

    ArrayList<SubKategoriModel> list;
    Context context;

    public SubKategoriAdapter(ArrayList<SubKategoriModel> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card,parent,false);
        return  new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        final SubKategoriModel cat =list.get(position);
        holder.title.setText(cat.getSKNAME());
//		Toast.makeText(context, ""+cat.getKBA_NAME(), Toast.LENGTH_SHORT).show();

        Picasso.with(context)
                .load( Config.IMAGE_SUB_KAT + cat.getSKIMAGE())
                .placeholder(R.drawable.ic_cat1)
                .error(R.drawable.ic_cat2)
                // To fit image into imageView
                .fit()
                // To prevent fade animation
                .noFade()
                .into(holder.ic_cat);

//		holdr.ic_cat.setImageResource(R.drawable.ic_cat8);

        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,LihatProductActivity.class);
                intent.putExtra("id",cat.getIDSUBKATEGORI());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Holder extends RecyclerView.ViewHolder{

        TextView title;
        ImageView ic_cat;
        View divider;
        LinearLayout card;

        public Holder(View itemView) {
            super(itemView);
            card = itemView.findViewById(R.id.card);
            title=(TextView) itemView.findViewById(R.id.itemcardTextView1);
            ic_cat=(ImageView) itemView.findViewById(R.id.itemcardImageView1);
            divider=itemView.findViewById(R.id.itemcardView1);
        }
    }
}
