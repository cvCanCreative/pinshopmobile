package ai.agusibrhim.design.topedc.Activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import ai.agusibrhim.design.topedc.Helper.Config;
import ai.agusibrhim.design.topedc.Helper.HelperClass;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfilActivity extends AppCompatActivity {

    private CircleImageView imgFoto;
    private TextView tvEditFoto;


    SharedPref sharedPref;
    private SwipeRefreshLayout swipe;
    File imageFile;
    private String imagePath, h;
    private Button btnEdit;
    String jk;

    DatePickerDialog datePickerDialog;
    int mYear;
    int mMonth;
    int mDay;
    private ImageView imgClose;
    private TextView edtJk;
    private TextView edtTtl;
    private TextView edtNama;
    private TextView edtTlp;
    private TextView edtEmail;
    AlertDialog.Builder dialog;
    LayoutInflater inflater;
    View dialogView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profil);
        initView();
        awal();

        tvEditFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFileChooserGalerry();
            }
        });

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editProfil();
            }
        });

        edtJk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gender();
            }
        });

        edtTtl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                settingTanggal();
                datePickerDialog.show();
            }
        });

        edtEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tampilDialokAlamat();
            }
        });

        edtNama.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tampilDialokNama();
            }
        });

        edtTlp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tampilDialokTlp();
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void tampilDialokAlamat() {
        dialog = new AlertDialog.Builder(EditProfilActivity.this);
        inflater = getLayoutInflater();
        dialogView = inflater.inflate(R.layout.row_masukkan_bank, null);
        dialog.setView(dialogView);

        final EditText edtBank2 = (EditText) dialogView.findViewById(R.id.edt_bank);
        final Button btn = (Button) dialogView.findViewById(R.id.btn_pilih);

        edtBank2.setText(""+edtEmail.getText().toString());
        final AlertDialog alertDialog = dialog.create();

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nama;
                nama = edtBank2.getText().toString();
                edtEmail.setText(nama);
                alertDialog.dismiss();
            }
        });
        alertDialog.show();

    }

    private void tampilDialokNama() {
        dialog = new AlertDialog.Builder(EditProfilActivity.this);
        inflater = getLayoutInflater();
        dialogView = inflater.inflate(R.layout.row_masukkan_bank, null);
        dialog.setView(dialogView);

        final EditText edtBank2 = (EditText) dialogView.findViewById(R.id.edt_bank);
        final Button btn = (Button) dialogView.findViewById(R.id.btn_pilih);

        edtBank2.setText(""+edtNama.getText().toString());
        final AlertDialog alertDialog = dialog.create();

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nama;
                nama = edtBank2.getText().toString();
                edtNama.setText(nama);
                alertDialog.dismiss();
            }
        });
        alertDialog.show();

    }

    private void tampilDialokTlp() {
        dialog = new AlertDialog.Builder(EditProfilActivity.this);
        inflater = getLayoutInflater();
        dialogView = inflater.inflate(R.layout.row_masukkan_bank, null);
        dialog.setView(dialogView);

        final EditText edtBank2 = (EditText) dialogView.findViewById(R.id.edt_bank);
        final Button btn = (Button) dialogView.findViewById(R.id.btn_pilih);

        edtBank2.setInputType(2);

        edtBank2.setText(""+edtTlp.getText().toString());
        final AlertDialog alertDialog = dialog.create();

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nama;
                nama = edtBank2.getText().toString();
                edtTlp.setText(nama);
                alertDialog.dismiss();
            }
        });
        alertDialog.show();

    }

    private void gender() {
        final CharSequence[] dialogItem = {"Laki-laki", "Perempuan"};
        AlertDialog.Builder builder = new AlertDialog.Builder(EditProfilActivity.this);
        builder.setTitle("Tentukan Pilihan Anda");
        builder.setItems(dialogItem, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                switch (i) {
                    case 0:
                        jk = "LK";
                        edtJk.setText("Laki-laki");
                        break;
                    case 1:
                        jk = "PR";
                        edtJk.setText("Perempuan");
                        break;
                }
            }
        });
        builder.create().show();
    }

    private void editProfil() {

        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Proses ...");
        pd.setCancelable(true);
        pd.show();
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.editProfil(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                edtNama.getText().toString(),
                jk,
                edtEmail.getText().toString(),
                edtTtl.getText().toString(),
                "+62" + edtTlp.getText().toString()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String pesan = jsonObject.optString("message");
                        if (pesan.equals("Parameter kurang")) {
                            new android.app.AlertDialog.Builder(EditProfilActivity.this)
                                    .setMessage("" + pesan)
                                    .setCancelable(false)
                                    .setNegativeButton("Oke", null)
                                    .show();
                        } else {
                            new android.app.AlertDialog.Builder(EditProfilActivity.this)
                                    .setMessage("" + pesan)
                                    .setCancelable(false)
                                    .setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            Intent intent = new Intent(getApplicationContext(), HomeLikeShopeeActivity.class)
                                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                            intent.putExtra("data", "data");
                                            startActivity(intent);
                                            finish();

                                        }
                                    })
                                    .show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(EditProfilActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void awal() {
        sharedPref = new SharedPref(this);
        edtJk.setFocusable(false);
        edtTtl.setFocusable(false);
        if (sharedPref.getSudahLogin()) {
            profil();
        } else {
            Toast.makeText(this, "Anda belum Login", Toast.LENGTH_SHORT).show();
        }

    }

    private void settingTanggal() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        datePickerDialog = new
                DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                mYear = year;
                mMonth = month;
                mDay = dayOfMonth;

                GregorianCalendar c = new GregorianCalendar(mYear, mMonth, mDay);
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

                edtTtl.setText(sdf.format(c.getTime()));

                edtTtl.setFocusable(false);
                edtTtl.setCursorVisible(false);
                //edtTanggal.setText(tanggal);

            }
        }, year, month, day);
    }

    private void profil() {
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getProfil(sharedPref.getIdUser(), sharedPref.getSESSION()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        JSONArray jsonArray = new JSONArray(response.body().string());
                        JSONObject jsonObject = jsonArray.optJSONObject(0);
                        String foto = jsonObject.optString("USD_FOTO");
                        String nama = jsonObject.optString("USD_FULLNAME");
                        String noHp = jsonObject.optString("US_TELP");
                        String email = jsonObject.optString("US_EMAIL");
                        String alamat = jsonObject.optString("USD_ADDRESS");
                        String kelamin = jsonObject.optString("USD_GENDER");
                        String ttl = jsonObject.optString("USD_BIRTH");

                        edtNama.setText(nama);
                        edtEmail.setText(alamat);
                        edtTtl.setText("" + new HelperClass().convertDateFormat(ttl));

                        if (noHp.contains("+62")) {
                            edtTlp.setText("" + noHp.replace("+62", ""));
                        } else {
                            edtTlp.setText(noHp);
                        }

                        if (kelamin.equals("LK")) {
                            jk = "LK";
                            edtJk.setText("Laki-laki");
                        } else {
                            jk = "Perempuan";
                        }

                        Picasso.with(getApplicationContext())
                                .load(Config.IMAGE_PROFIL + foto)
                                .into(imgFoto);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    swipe.setRefreshing(false);
                    Toast.makeText(EditProfilActivity.this, "gagal ambil data", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                Toast.makeText(EditProfilActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                new android.app.AlertDialog.Builder(EditProfilActivity.this)
                        .setMessage("Ulangi")
                        .setCancelable(false)
                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                profil();
                            }
                        })

                        .show();
            }
        });
    }

    private void editFoto(File imageFile) {
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), imageFile);
        MultipartBody.Part body = MultipartBody.Part.createFormData("picture", imageFile.getName(), requestFile);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.gantiFotoProfil(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                body).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String pesan = jsonObject.optString("message");
                        Toast.makeText(EditProfilActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(EditProfilActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    //gallery
    private void showFileChooserGalerry() {
        Intent qq = new Intent(Intent.ACTION_PICK);
        qq.setType("image/*");
        startActivityForResult(Intent.createChooser(qq, "Pilih Foto"), 100);
//        Intent intent = new Intent();
//        intent.setType("image/*");
//        intent.setAction(Intent.ACTION_GET_CONTENT);
//        startActivityForResult(Intent.createChooser(intent, "Pilih Foto"), Config.PICK_FILE_REQUEST);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                Toast.makeText(this, "Gambar Tidak Ada", Toast.LENGTH_SHORT).show();
                return;

            }
//            else {
//                Toast.makeText(this, "Gambar Ada", Toast.LENGTH_SHORT).show();
//            }
            Uri selectImageUri = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor c = getContentResolver().query(selectImageUri, filePathColumn, null, null, null);
            if (c != null) {
                c.moveToFirst();

                int columnIndex = c.getColumnIndex(filePathColumn[0]);
                imagePath = c.getString(columnIndex);

//                Glide.with(this).load(new File(imagePath)).into(cvEditProfil);
                Glide.with(this).load(new File(imagePath)).into(imgFoto);
                h = new File(imagePath).getName();
//                uploadImage();i
//                Toast.makeText(this, "Mbuh", Toast.LENGTH_SHORT).show();
                c.close();
                imageFile = new File(imagePath);

                editFoto(imageFile);

//                te.setVisibility(View.GONE);
//                imageVi.setVisibility(View.VISIBLE);
            } else {
//                te.setVisibility(View.VISIBLE);
//                imageVi.setVisibility(View.GONE);
                Toast.makeText(this, "Gambar Tidak Ada", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void initView() {
        imgFoto = (CircleImageView) findViewById(R.id.img_foto);
        tvEditFoto = (TextView) findViewById(R.id.tv_edit_foto);
        edtTtl = (TextView) findViewById(R.id.edt_ttl);
        edtJk = (TextView) findViewById(R.id.edt_jk);
        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe);
        btnEdit = (Button) findViewById(R.id.btn_edit);
        imgClose = (ImageView) findViewById(R.id.img_close);
        edtJk = (TextView) findViewById(R.id.edt_jk);
        edtTtl = (TextView) findViewById(R.id.edt_ttl);
        edtNama = (TextView) findViewById(R.id.edt_nama);
        edtTlp = (TextView) findViewById(R.id.edt_tlp);
        edtEmail = (TextView) findViewById(R.id.edt_email);
    }
}
