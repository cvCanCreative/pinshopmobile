package ai.agusibrhim.design.topedc.Fragment;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Activity.PromoActivity;
import ai.agusibrhim.design.topedc.Adapter.PromoAdapter;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.Model.PromoAktif;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class PromoTokoragment extends Fragment {


    private SwipeRefreshLayout swipe;
    private RecyclerView rv;
    private ImageView imgNo;
    private LinearLayout lyKosong;
    SharedPref sharedPref;
    RecyclerView.LayoutManager layoutManager;
    PromoAdapter mAdapter;
    ArrayList<PromoAktif> list = new ArrayList<>();
    String data,id;

    @SuppressLint("ValidFragment")
    public PromoTokoragment(String data,String id) {
        this.data = data;
        this.id = id;
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_promo_tokoragment, container, false);
        initView(view);

        awal();

        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe.setRefreshing(false);
            }
        });

        return view;
    }

    private void awal() {
        sharedPref = new SharedPref(getActivity());
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rv.setLayoutManager(layoutManager);
        if (data.equals("tamu")){
            getPromo(id);
        }else {
            getPromoToko();
        }
    }

    private void getPromo(String id){
        swipe.setRefreshing(true);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getPromoAktif(id).enqueue(new Callback<ArrayList<PromoAktif>>() {
            @Override
            public void onResponse(Call<ArrayList<PromoAktif>> call, Response<ArrayList<PromoAktif>> response) {
                if (response.isSuccessful()){
                    swipe.setRefreshing(false);
                    list = response.body();
                    if (list.get(0).getMessage().equals("Berhasil Diload")){
                        mAdapter = new PromoAdapter(list,getActivity(),data);
                        rv.setAdapter(mAdapter);
                    }else {
                        imgNo.setVisibility(View.VISIBLE);
                        rv.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<PromoAktif>> call, Throwable t) {
                swipe.setRefreshing(false);
                Toast.makeText(getActivity(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getPromoToko(){
        swipe.setRefreshing(true);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getPromoToko(sharedPref.getIdUser(),
                sharedPref.getSESSION()).enqueue(new Callback<ArrayList<PromoAktif>>() {
            @Override
            public void onResponse(Call<ArrayList<PromoAktif>> call, Response<ArrayList<PromoAktif>> response) {
                if (response.isSuccessful()){
                    swipe.setRefreshing(false);
                    list = response.body();
                    if (list.get(0).getMessage().equals("Berhasil Diload")){
                        mAdapter = new PromoAdapter(list,getActivity(),data);
                        rv.setAdapter(mAdapter);
                    }else {
                        lyKosong.setVisibility(View.VISIBLE);
                        rv.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<PromoAktif>> call, Throwable t) {
                swipe.setRefreshing(false);
                Toast.makeText(getActivity(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initView(View view) {
        swipe = (SwipeRefreshLayout) view.findViewById(R.id.swipe);
        rv = (RecyclerView) view.findViewById(R.id.rv);
        imgNo = (ImageView) view.findViewById(R.id.img_no);
        lyKosong = (LinearLayout) view.findViewById(R.id.ly_kosong);
    }
}
