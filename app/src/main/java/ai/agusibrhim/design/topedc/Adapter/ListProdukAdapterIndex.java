package ai.agusibrhim.design.topedc.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RatingBar;
import android.widget.TextView;

import com.loopj.android.image.SmartImageView;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Activity.DetailActivity;
import ai.agusibrhim.design.topedc.DetailScrollingActivity;
import ai.agusibrhim.design.topedc.Helper.Config;
import ai.agusibrhim.design.topedc.Helper.HelperClass;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.Model.ListProduct;
import ai.agusibrhim.design.topedc.R;

public class ListProdukAdapterIndex extends RecyclerView.Adapter<ListProdukAdapterIndex.Holde> implements Filterable {

    ArrayList<ListProduct> list;
    ArrayList<ListProduct> filteredList;
    Context context;
    public final int TYPE_AMBIL = 0;
    public final int TYPE_LOAD = 1;
    OnLoadMoreListener loadMoreListener;
    boolean isLoading = false, isMoreDataAvailable = true;
    SharedPref sharedPref;

    public ListProdukAdapterIndex(ArrayList<ListProduct> list, Context context) {
        this.list = list;
        this.context = context;
        this.filteredList = list;
        sharedPref = new SharedPref(context);

    }

    @NonNull
    @Override
    public Holde onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        if(viewType==TYPE_AMBIL){
            return new Holde(inflater.inflate(R.layout.item_product,parent,false));
        }else{
            return new Holde(inflater.inflate(R.layout.row_load,parent,false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull Holde holder, int position) {

        if(position>=getItemCount()-1 && isMoreDataAvailable && !isLoading && loadMoreListener!=null){
            isLoading = true;
            loadMoreListener.onLoadMore();
        }

        if(getItemViewType(position)==TYPE_AMBIL){
            ((Holde)holder).bindData(filteredList.get(position));
//            final BeritaModel bm = beritaModels.get(position);
//            holder.bm = bm;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if(filteredList.get(position).getType().equals("barang")){
            return TYPE_AMBIL;
        }else{
            return TYPE_LOAD;
        }
    }

    @Override
    public int getItemCount() {
        return filteredList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString().toLowerCase();

                if (charString.isEmpty()) {
                    filteredList = list;
                } else {
                    ArrayList<ListProduct> list2 = new ArrayList<>();

                    for (ListProduct model : list) {
                        if (model.getBANAME().toLowerCase().contains(charSequence) ||
                                model.getIDBARANG().toLowerCase().contains(charSequence)) {
                            Log.d("ID", "performFiltering: "+model.getIDBARANG().toLowerCase());
                            list2.add(model);
                        }
                    }
                    filteredList = list2;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredList = (ArrayList<ListProduct>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class Holde extends RecyclerView.ViewHolder{

        TextView name, price, priceold, discount, store;
        SmartImageView img;
        RatingBar ratingbar;

        public Holde(View itemView) {
            super(itemView);

            name=(TextView) itemView.findViewById(R.id.itemproductTextViewName);
            price=(TextView) itemView.findViewById(R.id.itemproductTextViewPrice);
            priceold=(TextView) itemView.findViewById(R.id.itemproductTextViewPold);
            discount=(TextView) itemView.findViewById(R.id.itemproductTextViewDisc);
            store=(TextView) itemView.findViewById(R.id.itemproductTextViewStore);
            img=(SmartImageView) itemView.findViewById(R.id.itemproductImageView1);
            ratingbar=(RatingBar) itemView.findViewById(R.id.itemproductRatingBar1);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    sharedPref.setProduk(list.get(getAdapterPosition()));
                    sharedPref.savePrefString(SharedPref.TIPEKLIK,"produk");
                    Intent intent = new Intent(context, DetailScrollingActivity.class);
                    intent.putExtra("data", "lihat");
                    intent.putExtra("list", filteredList.get(getAdapterPosition()));
                    intent.putExtra("posisi",""+getAdapterPosition());
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
//					Toast.makeText(context, "bb"+getAdapterPosition(), Toast.LENGTH_SHORT).show();
                }
            });
        }

        void bindData(ListProduct data){
            ListProduct cat=data;

            name.setText(cat.getBANAME());

            String gambar = cat.getBAIMAGE();
            String image = new HelperClass().splitText(gambar);

            Picasso.with(context)
                    .load(Config.IMAGE_BARANG + image)
                    .placeholder(R.drawable.image_loading)
                    .error(R.drawable.no_image_found)
                    .noFade()
                    .into(img);

            store.setText(cat.getUSDTOKO());
            if (cat.getBAPRICE().equals("")){

            }else {
                price.setText(""+new HelperClass().convertRupiah(Integer.valueOf(cat.getBAPRICE())));
            }
            priceold.setText(_priceFormat("115000"));
            priceold.setPaintFlags(priceold.getPaintFlags() |android.graphics.Paint.STRIKE_THRU_TEXT_FLAG);
        }
    }

    private static String _priceFormat(String s){
        double parsed = Double.parseDouble(s);
        String formatted = NumberFormat.getCurrencyInstance().format(parsed);
        return formatted;
    }

    static class LoadHolder extends RecyclerView.ViewHolder{
        public LoadHolder(View itemView) {
            super(itemView);
        }
    }

    public void setMoreDataAvailable(boolean moreDataAvailable) {
        isMoreDataAvailable = moreDataAvailable;
    }

    /* notifyDataSetChanged is final method so we can't override it
         call adapter.notifyDataChanged(); after update the list
         */
    public void notifyDataChanged(){
        notifyDataSetChanged();
        isLoading = false;
    }


    public interface OnLoadMoreListener{
        void onLoadMore();
    }

    public void setLoadMoreListener(OnLoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }
}
