package ai.agusibrhim.design.topedc.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HistoryTransaksiBaru implements Parcelable {
    @SerializedName("ID_TRANSAKSI")
    @Expose
    private String iDTRANSAKSI;
    @SerializedName("ID_USER")
    @Expose
    private String iDUSER;
    @SerializedName("ID_USER_TOKO")
    @Expose
    private String iDUSERTOKO;
    @SerializedName("ID_KOMPLAIN")
    @Expose
    private String iDKOMPLAIN;
    @SerializedName("ID_HISTORY_SALDO")
    @Expose
    private String iDHISTORYSALDO;
    @SerializedName("TS_KODE_PAYMENT")
    @Expose
    private String tSKODEPAYMENT;
    @SerializedName("TS_BUKTI_TF")
    @Expose
    private String tSBUKTITF;
    @SerializedName("TS_REKENING")
    @Expose
    private String tSREKENING;
    @SerializedName("TS_NAMA_REK")
    @Expose
    private String tSNAMAREK;
    @SerializedName("TS_BANK")
    @Expose
    private String tSBANK;
    @SerializedName("TS_BANK_PIN")
    @Expose
    private String tSBANKPIN;
    @SerializedName("TS_REK_PIN")
    @Expose
    private String tSREKPIN;
    @SerializedName("TS_TF_PIN")
    @Expose
    private String tSTFPIN;
    @SerializedName("TS_KODE_TRX")
    @Expose
    private String tSKODETRX;
    @SerializedName("TS_TOTAL_ITEM")
    @Expose
    private String tSTOTALITEM;
    @SerializedName("TS_METODE_BAYAR")
    @Expose
    private String tSMETODEBAYAR;
    @SerializedName("TS_SALDO_POINT")
    @Expose
    private String tSSALDOPOINT;
    @SerializedName("TS_KODE_UNIK")
    @Expose
    private String tSKODEUNIK;
    @SerializedName("TS_HARGA")
    @Expose
    private String tSHARGA;
    @SerializedName("TS_KODE_PROMO")
    @Expose
    private String tSKODEPROMO;
    @SerializedName("TS_JNS_PROMO")
    @Expose
    private String tSJNSPROMO;
    @SerializedName("TS_POT_PROMO")
    @Expose
    private String tSPOTPROMO;
    @SerializedName("TS_BAYAR_ONGKIR")
    @Expose
    private String tSBAYARONGKIR;
    @SerializedName("TS_TOTAL_BAYAR")
    @Expose
    private String tSTOTALBAYAR;
    @SerializedName("TS_STATUS")
    @Expose
    private String tSSTATUS;
    @SerializedName("TS_ID_CEKRESI")
    @Expose
    private String tSIDCEKRESI;
    @SerializedName("TS_RESI")
    @Expose
    private String tSRESI;
    @SerializedName("TS_SLUG_KURIR")
    @Expose
    private String tSSLUGKURIR;
    @SerializedName("TS_EXP")
    @Expose
    private String tSEXP;
    @SerializedName("CREATED_AT")
    @Expose
    private String cREATEDAT;
    @SerializedName("UPDATED_AT")
    @Expose
    private String uPDATEDAT;
    @SerializedName("ID_TRANSAKSI_DETAIL")
    @Expose
    private String iDTRANSAKSIDETAIL;
    @SerializedName("ID_BARANG")
    @Expose
    private String iDBARANG;
    @SerializedName("ID_PENGIRIMAN")
    @Expose
    private String iDPENGIRIMAN;
    @SerializedName("TSD_QTY")
    @Expose
    private String tSDQTY;
    @SerializedName("TSD_CATATAN")
    @Expose
    private String tSDCATATAN;
    @SerializedName("TSD_KODE_PROMO")
    @Expose
    private String tSDKODEPROMO;
    @SerializedName("TSD_SLUG_KURIR")
    @Expose
    private String tSDSLUGKURIR;
    @SerializedName("TSD_KURIR")
    @Expose
    private String tSDKURIR;
    @SerializedName("TSD_JENIS_KURIR")
    @Expose
    private String tSDJENISKURIR;
    @SerializedName("TSD_ONGKIR")
    @Expose
    private String tSDONGKIR;
    @SerializedName("TSD_HARGA_ASLI")
    @Expose
    private String tSDHARGAASLI;
    @SerializedName("TSD_HARGA_TOTAL")
    @Expose
    private String tSDHARGATOTAL;
    @SerializedName("BA_IMAGE")
    @Expose
    private String bAIMAGE;
    @SerializedName("USD_TOKO")
    @Expose
    private String uSDTOKO;
    @SerializedName("USD_TOKO_FOTO")
    @Expose
    private String uSDTOKOFOTO;
    @SerializedName("RK_IMAGE")
    @Expose
    private String rKIMAGE;

    protected HistoryTransaksiBaru(Parcel in) {
        iDTRANSAKSI = in.readString();
        iDUSER = in.readString();
        iDUSERTOKO = in.readString();
        iDKOMPLAIN = in.readString();
        iDHISTORYSALDO = in.readString();
        tSKODEPAYMENT = in.readString();
        tSBUKTITF = in.readString();
        tSREKENING = in.readString();
        tSNAMAREK = in.readString();
        tSBANK = in.readString();
        tSBANKPIN = in.readString();
        tSREKPIN = in.readString();
        tSTFPIN = in.readString();
        tSKODETRX = in.readString();
        tSTOTALITEM = in.readString();
        tSMETODEBAYAR = in.readString();
        tSSALDOPOINT = in.readString();
        tSKODEUNIK = in.readString();
        tSHARGA = in.readString();
        tSKODEPROMO = in.readString();
        tSJNSPROMO = in.readString();
        tSPOTPROMO = in.readString();
        tSBAYARONGKIR = in.readString();
        tSTOTALBAYAR = in.readString();
        tSSTATUS = in.readString();
        tSIDCEKRESI = in.readString();
        tSRESI = in.readString();
        tSSLUGKURIR = in.readString();
        tSEXP = in.readString();
        cREATEDAT = in.readString();
        uPDATEDAT = in.readString();
        iDTRANSAKSIDETAIL = in.readString();
        iDBARANG = in.readString();
        iDPENGIRIMAN = in.readString();
        tSDQTY = in.readString();
        tSDCATATAN = in.readString();
        tSDKODEPROMO = in.readString();
        tSDSLUGKURIR = in.readString();
        tSDKURIR = in.readString();
        tSDJENISKURIR = in.readString();
        tSDONGKIR = in.readString();
        tSDHARGAASLI = in.readString();
        tSDHARGATOTAL = in.readString();
        bAIMAGE = in.readString();
        uSDTOKO = in.readString();
        uSDTOKOFOTO = in.readString();
        rKIMAGE = in.readString();
    }

    public static final Creator<HistoryTransaksiBaru> CREATOR = new Creator<HistoryTransaksiBaru>() {
        @Override
        public HistoryTransaksiBaru createFromParcel(Parcel in) {
            return new HistoryTransaksiBaru(in);
        }

        @Override
        public HistoryTransaksiBaru[] newArray(int size) {
            return new HistoryTransaksiBaru[size];
        }
    };

    public String getIDTRANSAKSI() {
        return iDTRANSAKSI;
    }

    public void setIDTRANSAKSI(String iDTRANSAKSI) {
        this.iDTRANSAKSI = iDTRANSAKSI;
    }

    public String getIDUSER() {
        return iDUSER;
    }

    public void setIDUSER(String iDUSER) {
        this.iDUSER = iDUSER;
    }

    public String getIDUSERTOKO() {
        return iDUSERTOKO;
    }

    public void setIDUSERTOKO(String iDUSERTOKO) {
        this.iDUSERTOKO = iDUSERTOKO;
    }

    public String getIDKOMPLAIN() {
        return iDKOMPLAIN;
    }

    public void setIDKOMPLAIN(String iDKOMPLAIN) {
        this.iDKOMPLAIN = iDKOMPLAIN;
    }

    public String getIDHISTORYSALDO() {
        return iDHISTORYSALDO;
    }

    public void setIDHISTORYSALDO(String iDHISTORYSALDO) {
        this.iDHISTORYSALDO = iDHISTORYSALDO;
    }

    public String getTSKODEPAYMENT() {
        return tSKODEPAYMENT;
    }

    public void setTSKODEPAYMENT(String tSKODEPAYMENT) {
        this.tSKODEPAYMENT = tSKODEPAYMENT;
    }

    public String getTSBUKTITF() {
        return tSBUKTITF;
    }

    public void setTSBUKTITF(String tSBUKTITF) {
        this.tSBUKTITF = tSBUKTITF;
    }

    public String getTSREKENING() {
        return tSREKENING;
    }

    public void setTSREKENING(String tSREKENING) {
        this.tSREKENING = tSREKENING;
    }

    public String getTSNAMAREK() {
        return tSNAMAREK;
    }

    public void setTSNAMAREK(String tSNAMAREK) {
        this.tSNAMAREK = tSNAMAREK;
    }

    public String getTSBANK() {
        return tSBANK;
    }

    public void setTSBANK(String tSBANK) {
        this.tSBANK = tSBANK;
    }

    public String getTSBANKPIN() {
        return tSBANKPIN;
    }

    public void setTSBANKPIN(String tSBANKPIN) {
        this.tSBANKPIN = tSBANKPIN;
    }

    public String getTSREKPIN() {
        return tSREKPIN;
    }

    public void setTSREKPIN(String tSREKPIN) {
        this.tSREKPIN = tSREKPIN;
    }

    public String getTSTFPIN() {
        return tSTFPIN;
    }

    public void setTSTFPIN(String tSTFPIN) {
        this.tSTFPIN = tSTFPIN;
    }

    public String getTSKODETRX() {
        return tSKODETRX;
    }

    public void setTSKODETRX(String tSKODETRX) {
        this.tSKODETRX = tSKODETRX;
    }

    public String getTSTOTALITEM() {
        return tSTOTALITEM;
    }

    public void setTSTOTALITEM(String tSTOTALITEM) {
        this.tSTOTALITEM = tSTOTALITEM;
    }

    public String getTSMETODEBAYAR() {
        return tSMETODEBAYAR;
    }

    public void setTSMETODEBAYAR(String tSMETODEBAYAR) {
        this.tSMETODEBAYAR = tSMETODEBAYAR;
    }

    public String getTSSALDOPOINT() {
        return tSSALDOPOINT;
    }

    public void setTSSALDOPOINT(String tSSALDOPOINT) {
        this.tSSALDOPOINT = tSSALDOPOINT;
    }

    public String getTSKODEUNIK() {
        return tSKODEUNIK;
    }

    public void setTSKODEUNIK(String tSKODEUNIK) {
        this.tSKODEUNIK = tSKODEUNIK;
    }

    public String getTSHARGA() {
        return tSHARGA;
    }

    public void setTSHARGA(String tSHARGA) {
        this.tSHARGA = tSHARGA;
    }

    public String getTSKODEPROMO() {
        return tSKODEPROMO;
    }

    public void setTSKODEPROMO(String tSKODEPROMO) {
        this.tSKODEPROMO = tSKODEPROMO;
    }

    public String getTSJNSPROMO() {
        return tSJNSPROMO;
    }

    public void setTSJNSPROMO(String tSJNSPROMO) {
        this.tSJNSPROMO = tSJNSPROMO;
    }

    public String getTSPOTPROMO() {
        return tSPOTPROMO;
    }

    public void setTSPOTPROMO(String tSPOTPROMO) {
        this.tSPOTPROMO = tSPOTPROMO;
    }

    public String getTSBAYARONGKIR() {
        return tSBAYARONGKIR;
    }

    public void setTSBAYARONGKIR(String tSBAYARONGKIR) {
        this.tSBAYARONGKIR = tSBAYARONGKIR;
    }

    public String getTSTOTALBAYAR() {
        return tSTOTALBAYAR;
    }

    public void setTSTOTALBAYAR(String tSTOTALBAYAR) {
        this.tSTOTALBAYAR = tSTOTALBAYAR;
    }

    public String getTSSTATUS() {
        return tSSTATUS;
    }

    public void setTSSTATUS(String tSSTATUS) {
        this.tSSTATUS = tSSTATUS;
    }

    public String getTSIDCEKRESI() {
        return tSIDCEKRESI;
    }

    public void setTSIDCEKRESI(String tSIDCEKRESI) {
        this.tSIDCEKRESI = tSIDCEKRESI;
    }

    public String getTSRESI() {
        return tSRESI;
    }

    public void setTSRESI(String tSRESI) {
        this.tSRESI = tSRESI;
    }

    public String getTSSLUGKURIR() {
        return tSSLUGKURIR;
    }

    public void setTSSLUGKURIR(String tSSLUGKURIR) {
        this.tSSLUGKURIR = tSSLUGKURIR;
    }

    public String getTSEXP() {
        return tSEXP;
    }

    public void setTSEXP(String tSEXP) {
        this.tSEXP = tSEXP;
    }

    public String getCREATEDAT() {
        return cREATEDAT;
    }

    public void setCREATEDAT(String cREATEDAT) {
        this.cREATEDAT = cREATEDAT;
    }

    public String getUPDATEDAT() {
        return uPDATEDAT;
    }

    public void setUPDATEDAT(String uPDATEDAT) {
        this.uPDATEDAT = uPDATEDAT;
    }

    public String getIDTRANSAKSIDETAIL() {
        return iDTRANSAKSIDETAIL;
    }

    public void setIDTRANSAKSIDETAIL(String iDTRANSAKSIDETAIL) {
        this.iDTRANSAKSIDETAIL = iDTRANSAKSIDETAIL;
    }

    public String getIDBARANG() {
        return iDBARANG;
    }

    public void setIDBARANG(String iDBARANG) {
        this.iDBARANG = iDBARANG;
    }

    public String getIDPENGIRIMAN() {
        return iDPENGIRIMAN;
    }

    public void setIDPENGIRIMAN(String iDPENGIRIMAN) {
        this.iDPENGIRIMAN = iDPENGIRIMAN;
    }

    public String getTSDQTY() {
        return tSDQTY;
    }

    public void setTSDQTY(String tSDQTY) {
        this.tSDQTY = tSDQTY;
    }

    public String getTSDCATATAN() {
        return tSDCATATAN;
    }

    public void setTSDCATATAN(String tSDCATATAN) {
        this.tSDCATATAN = tSDCATATAN;
    }

    public String getTSDKODEPROMO() {
        return tSDKODEPROMO;
    }

    public void setTSDKODEPROMO(String tSDKODEPROMO) {
        this.tSDKODEPROMO = tSDKODEPROMO;
    }

    public String getTSDSLUGKURIR() {
        return tSDSLUGKURIR;
    }

    public void setTSDSLUGKURIR(String tSDSLUGKURIR) {
        this.tSDSLUGKURIR = tSDSLUGKURIR;
    }

    public String getTSDKURIR() {
        return tSDKURIR;
    }

    public void setTSDKURIR(String tSDKURIR) {
        this.tSDKURIR = tSDKURIR;
    }

    public String getTSDJENISKURIR() {
        return tSDJENISKURIR;
    }

    public void setTSDJENISKURIR(String tSDJENISKURIR) {
        this.tSDJENISKURIR = tSDJENISKURIR;
    }

    public String getTSDONGKIR() {
        return tSDONGKIR;
    }

    public void setTSDONGKIR(String tSDONGKIR) {
        this.tSDONGKIR = tSDONGKIR;
    }

    public String getTSDHARGAASLI() {
        return tSDHARGAASLI;
    }

    public void setTSDHARGAASLI(String tSDHARGAASLI) {
        this.tSDHARGAASLI = tSDHARGAASLI;
    }

    public String getTSDHARGATOTAL() {
        return tSDHARGATOTAL;
    }

    public void setTSDHARGATOTAL(String tSDHARGATOTAL) {
        this.tSDHARGATOTAL = tSDHARGATOTAL;
    }

    public String getBAIMAGE() {
        return bAIMAGE;
    }

    public void setBAIMAGE(String bAIMAGE) {
        this.bAIMAGE = bAIMAGE;
    }

    public String getUSDTOKO() {
        return uSDTOKO;
    }

    public void setUSDTOKO(String uSDTOKO) {
        this.uSDTOKO = uSDTOKO;
    }

    public String getUSDTOKOFOTO() {
        return uSDTOKOFOTO;
    }

    public void setUSDTOKOFOTO(String uSDTOKOFOTO) {
        this.uSDTOKOFOTO = uSDTOKOFOTO;
    }

    public String getRKIMAGE() {
        return rKIMAGE;
    }

    public void setRKIMAGE(String rKIMAGE) {
        this.rKIMAGE = rKIMAGE;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(iDTRANSAKSI);
        parcel.writeString(iDUSER);
        parcel.writeString(iDUSERTOKO);
        parcel.writeString(iDKOMPLAIN);
        parcel.writeString(iDHISTORYSALDO);
        parcel.writeString(tSKODEPAYMENT);
        parcel.writeString(tSBUKTITF);
        parcel.writeString(tSREKENING);
        parcel.writeString(tSNAMAREK);
        parcel.writeString(tSBANK);
        parcel.writeString(tSBANKPIN);
        parcel.writeString(tSREKPIN);
        parcel.writeString(tSTFPIN);
        parcel.writeString(tSKODETRX);
        parcel.writeString(tSTOTALITEM);
        parcel.writeString(tSMETODEBAYAR);
        parcel.writeString(tSSALDOPOINT);
        parcel.writeString(tSKODEUNIK);
        parcel.writeString(tSHARGA);
        parcel.writeString(tSKODEPROMO);
        parcel.writeString(tSJNSPROMO);
        parcel.writeString(tSPOTPROMO);
        parcel.writeString(tSBAYARONGKIR);
        parcel.writeString(tSTOTALBAYAR);
        parcel.writeString(tSSTATUS);
        parcel.writeString(tSIDCEKRESI);
        parcel.writeString(tSRESI);
        parcel.writeString(tSSLUGKURIR);
        parcel.writeString(tSEXP);
        parcel.writeString(cREATEDAT);
        parcel.writeString(uPDATEDAT);
        parcel.writeString(iDTRANSAKSIDETAIL);
        parcel.writeString(iDBARANG);
        parcel.writeString(iDPENGIRIMAN);
        parcel.writeString(tSDQTY);
        parcel.writeString(tSDCATATAN);
        parcel.writeString(tSDKODEPROMO);
        parcel.writeString(tSDSLUGKURIR);
        parcel.writeString(tSDKURIR);
        parcel.writeString(tSDJENISKURIR);
        parcel.writeString(tSDONGKIR);
        parcel.writeString(tSDHARGAASLI);
        parcel.writeString(tSDHARGATOTAL);
        parcel.writeString(bAIMAGE);
        parcel.writeString(uSDTOKO);
        parcel.writeString(uSDTOKOFOTO);
        parcel.writeString(rKIMAGE);
    }
}
