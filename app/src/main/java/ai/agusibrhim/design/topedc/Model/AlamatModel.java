package ai.agusibrhim.design.topedc.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AlamatModel implements Parcelable {

    @SerializedName("ID_PENGIRIMAN")
    @Expose
    private String iDPENGIRIMAN;
    @SerializedName("ID_USER")
    @Expose
    private String iDUSER;
    @SerializedName("PE_JUDUL")
    @Expose
    private String pEJUDUL;
    @SerializedName("PE_NAMA")
    @Expose
    private String pENAMA;
    @SerializedName("PE_ALAMAT")
    @Expose
    private String pEALAMAT;
    @SerializedName("PE_PROVINSI")
    @Expose
    private String pEPROVINSI;
    @SerializedName("PE_KOTA")
    @Expose
    private String pEKOTA;
    @SerializedName("PE_ID_KOTA")
    @Expose
    private String pEIDKOTA;
    @SerializedName("PE_KECAMATAN")
    @Expose
    private String pEKECAMATAN;
    @SerializedName("PE_ID_KECAMATAN")
    @Expose
    private String pEIDKECAMATAN;
    @SerializedName("PE_KODE_POS")
    @Expose
    private String pEKODEPOS;
    @SerializedName("PE_TELP")
    @Expose
    private String pETELP;
    @SerializedName("CREATED_AT")
    @Expose
    private String cREATEDAT;
    @SerializedName("UPDATED_AT")
    @Expose
    private String uPDATEDAT;
    @SerializedName("US_USERNAME")
    @Expose
    private String uSUSERNAME;
    @SerializedName("US_TELP")
    @Expose
    private String uSTELP;
    @SerializedName("US_PASSWORD")
    @Expose
    private String uSPASSWORD;
    @SerializedName("US_EMAIL")
    @Expose
    private String uSEMAIL;
    @SerializedName("US_RULE")
    @Expose
    private String uSRULE;
    @SerializedName("US_ROUTE")
    @Expose
    private String uSROUTE;
    @SerializedName("US_FCM")
    @Expose
    private String uSFCM;
    @SerializedName("US_TOKEN")
    @Expose
    private String uSTOKEN;
    @SerializedName("US_FORGOT_PASS")
    @Expose
    private String uSFORGOTPASS;
    @SerializedName("US_REQUEST_FORGOT_PASS")
    @Expose
    private String uSREQUESTFORGOTPASS;
    @SerializedName("CR_PENGIRIMAN")
    @Expose
    private String cRPENGIRIMAN;
    @SerializedName("UP_PENGIRIMAN")
    @Expose
    private String uPPENGIRIMAN;
    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("message")
    @Expose
    private String message;

    protected AlamatModel(Parcel in) {
        iDPENGIRIMAN = in.readString();
        iDUSER = in.readString();
        pEJUDUL = in.readString();
        pENAMA = in.readString();
        pEALAMAT = in.readString();
        pEPROVINSI = in.readString();
        pEKOTA = in.readString();
        pEIDKOTA = in.readString();
        pEKECAMATAN = in.readString();
        pEIDKECAMATAN = in.readString();
        pEKODEPOS = in.readString();
        pETELP = in.readString();
        cREATEDAT = in.readString();
        uPDATEDAT = in.readString();
        uSUSERNAME = in.readString();
        uSTELP = in.readString();
        uSPASSWORD = in.readString();
        uSEMAIL = in.readString();
        uSRULE = in.readString();
        uSROUTE = in.readString();
        uSFCM = in.readString();
        uSTOKEN = in.readString();
        uSFORGOTPASS = in.readString();
        uSREQUESTFORGOTPASS = in.readString();
        cRPENGIRIMAN = in.readString();
        uPPENGIRIMAN = in.readString();
        if (in.readByte() == 0) {
            success = null;
        } else {
            success = in.readInt();
        }
        message = in.readString();
    }

    public static final Creator<AlamatModel> CREATOR = new Creator<AlamatModel>() {
        @Override
        public AlamatModel createFromParcel(Parcel in) {
            return new AlamatModel(in);
        }

        @Override
        public AlamatModel[] newArray(int size) {
            return new AlamatModel[size];
        }
    };

    public String getIDPENGIRIMAN() {
        return iDPENGIRIMAN;
    }

    public void setIDPENGIRIMAN(String iDPENGIRIMAN) {
        this.iDPENGIRIMAN = iDPENGIRIMAN;
    }

    public String getIDUSER() {
        return iDUSER;
    }

    public void setIDUSER(String iDUSER) {
        this.iDUSER = iDUSER;
    }

    public String getPEJUDUL() {
        return pEJUDUL;
    }

    public void setPEJUDUL(String pEJUDUL) {
        this.pEJUDUL = pEJUDUL;
    }

    public String getPENAMA() {
        return pENAMA;
    }

    public void setPENAMA(String pENAMA) {
        this.pENAMA = pENAMA;
    }

    public String getPEALAMAT() {
        return pEALAMAT;
    }

    public void setPEALAMAT(String pEALAMAT) {
        this.pEALAMAT = pEALAMAT;
    }

    public String getPEPROVINSI() {
        return pEPROVINSI;
    }

    public void setPEPROVINSI(String pEPROVINSI) {
        this.pEPROVINSI = pEPROVINSI;
    }

    public String getPEKOTA() {
        return pEKOTA;
    }

    public void setPEKOTA(String pEKOTA) {
        this.pEKOTA = pEKOTA;
    }

    public String getPEIDKOTA() {
        return pEIDKOTA;
    }

    public void setPEIDKOTA(String pEIDKOTA) {
        this.pEIDKOTA = pEIDKOTA;
    }

    public String getPEKECAMATAN() {
        return pEKECAMATAN;
    }

    public void setPEKECAMATAN(String pEKECAMATAN) {
        this.pEKECAMATAN = pEKECAMATAN;
    }

    public String getPEIDKECAMATAN() {
        return pEIDKECAMATAN;
    }

    public void setPEIDKECAMATAN(String pEIDKECAMATAN) {
        this.pEIDKECAMATAN = pEIDKECAMATAN;
    }

    public String getPEKODEPOS() {
        return pEKODEPOS;
    }

    public void setPEKODEPOS(String pEKODEPOS) {
        this.pEKODEPOS = pEKODEPOS;
    }

    public String getPETELP() {
        return pETELP;
    }

    public void setPETELP(String pETELP) {
        this.pETELP = pETELP;
    }

    public String getCREATEDAT() {
        return cREATEDAT;
    }

    public void setCREATEDAT(String cREATEDAT) {
        this.cREATEDAT = cREATEDAT;
    }

    public String getUPDATEDAT() {
        return uPDATEDAT;
    }

    public void setUPDATEDAT(String uPDATEDAT) {
        this.uPDATEDAT = uPDATEDAT;
    }

    public String getUSUSERNAME() {
        return uSUSERNAME;
    }

    public void setUSUSERNAME(String uSUSERNAME) {
        this.uSUSERNAME = uSUSERNAME;
    }

    public String getUSTELP() {
        return uSTELP;
    }

    public void setUSTELP(String uSTELP) {
        this.uSTELP = uSTELP;
    }

    public String getUSPASSWORD() {
        return uSPASSWORD;
    }

    public void setUSPASSWORD(String uSPASSWORD) {
        this.uSPASSWORD = uSPASSWORD;
    }

    public String getUSEMAIL() {
        return uSEMAIL;
    }

    public void setUSEMAIL(String uSEMAIL) {
        this.uSEMAIL = uSEMAIL;
    }

    public String getUSRULE() {
        return uSRULE;
    }

    public void setUSRULE(String uSRULE) {
        this.uSRULE = uSRULE;
    }

    public String getUSROUTE() {
        return uSROUTE;
    }

    public void setUSROUTE(String uSROUTE) {
        this.uSROUTE = uSROUTE;
    }

    public String getUSFCM() {
        return uSFCM;
    }

    public void setUSFCM(String uSFCM) {
        this.uSFCM = uSFCM;
    }

    public String getUSTOKEN() {
        return uSTOKEN;
    }

    public void setUSTOKEN(String uSTOKEN) {
        this.uSTOKEN = uSTOKEN;
    }

    public String getUSFORGOTPASS() {
        return uSFORGOTPASS;
    }

    public void setUSFORGOTPASS(String uSFORGOTPASS) {
        this.uSFORGOTPASS = uSFORGOTPASS;
    }

    public String getUSREQUESTFORGOTPASS() {
        return uSREQUESTFORGOTPASS;
    }

    public void setUSREQUESTFORGOTPASS(String uSREQUESTFORGOTPASS) {
        this.uSREQUESTFORGOTPASS = uSREQUESTFORGOTPASS;
    }

    public String getCRPENGIRIMAN() {
        return cRPENGIRIMAN;
    }

    public void setCRPENGIRIMAN(String cRPENGIRIMAN) {
        this.cRPENGIRIMAN = cRPENGIRIMAN;
    }

    public String getUPPENGIRIMAN() {
        return uPPENGIRIMAN;
    }

    public void setUPPENGIRIMAN(String uPPENGIRIMAN) {
        this.uPPENGIRIMAN = uPPENGIRIMAN;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(iDPENGIRIMAN);
        parcel.writeString(iDUSER);
        parcel.writeString(pEJUDUL);
        parcel.writeString(pENAMA);
        parcel.writeString(pEALAMAT);
        parcel.writeString(pEPROVINSI);
        parcel.writeString(pEKOTA);
        parcel.writeString(pEIDKOTA);
        parcel.writeString(pEKECAMATAN);
        parcel.writeString(pEIDKECAMATAN);
        parcel.writeString(pEKODEPOS);
        parcel.writeString(pETELP);
        parcel.writeString(cREATEDAT);
        parcel.writeString(uPDATEDAT);
        parcel.writeString(uSUSERNAME);
        parcel.writeString(uSTELP);
        parcel.writeString(uSPASSWORD);
        parcel.writeString(uSEMAIL);
        parcel.writeString(uSRULE);
        parcel.writeString(uSROUTE);
        parcel.writeString(uSFCM);
        parcel.writeString(uSTOKEN);
        parcel.writeString(uSFORGOTPASS);
        parcel.writeString(uSREQUESTFORGOTPASS);
        parcel.writeString(cRPENGIRIMAN);
        parcel.writeString(uPPENGIRIMAN);
        if (success == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(success);
        }
        parcel.writeString(message);
    }
}
