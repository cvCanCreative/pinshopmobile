package ai.agusibrhim.design.topedc.Activity;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Adapter.FeedBackAdapter;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.Model.FeedbackModel;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LihatFeedbackActivity extends AppCompatActivity {

    private TextView tvKet;
    private RecyclerView rv;
    private SwipeRefreshLayout swipe;
    SharedPref sharedPref;
    FeedBackAdapter mAdapter;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<FeedbackModel> list = new ArrayList<>();
    private ImageView imgClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lihat_feedback);
        initView();

        awal();

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void awal() {
        sharedPref = new SharedPref(this);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rv.setLayoutManager(layoutManager);
        getData();
    }

    private void getData() {
        swipe.setRefreshing(true);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getFeedback(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                getIntent().getStringExtra("id")).enqueue(new Callback<ArrayList<FeedbackModel>>() {
            @Override
            public void onResponse(Call<ArrayList<FeedbackModel>> call, Response<ArrayList<FeedbackModel>> response) {
                if (response.isSuccessful()) {
                    swipe.setRefreshing(false);
                    list = response.body();
                    if (list.get(0).getSuccess() == 1) {
                        tvKet.setVisibility(View.GONE);
                        rv.setVisibility(View.VISIBLE);

                        mAdapter = new FeedBackAdapter(list, getApplicationContext());
                        rv.setAdapter(mAdapter);
                    } else {
                        rv.setVisibility(View.GONE);
                        tvKet.setVisibility(View.VISIBLE);
                    }

                }
            }

            @Override
            public void onFailure(Call<ArrayList<FeedbackModel>> call, Throwable t) {
                swipe.setRefreshing(false);
                Toast.makeText(LihatFeedbackActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initView() {
        tvKet = (TextView) findViewById(R.id.tv_ket);
        rv = (RecyclerView) findViewById(R.id.rv);
        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe);
        imgClose = (ImageView) findViewById(R.id.img_close);
    }
}
