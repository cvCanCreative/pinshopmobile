package ai.agusibrhim.design.topedc.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HelpModel {

    @SerializedName("ID_HELP")
    @Expose
    private String iDHELP;
    @SerializedName("HE_TITLE")
    @Expose
    private String hETITLE;
    @SerializedName("HE_MESSAGE")
    @Expose
    private String hEMESSAGE;
    @SerializedName("CREATED_AT")
    @Expose
    private String cREATEDAT;
    @SerializedName("UPDATE_AT")
    @Expose
    private String uPDATEAT;
    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("message")
    @Expose
    private String message;

    public String getIDHELP() {
        return iDHELP;
    }

    public void setIDHELP(String iDHELP) {
        this.iDHELP = iDHELP;
    }

    public String getHETITLE() {
        return hETITLE;
    }

    public void setHETITLE(String hETITLE) {
        this.hETITLE = hETITLE;
    }

    public String getHEMESSAGE() {
        return hEMESSAGE;
    }

    public void setHEMESSAGE(String hEMESSAGE) {
        this.hEMESSAGE = hEMESSAGE;
    }

    public String getCREATEDAT() {
        return cREATEDAT;
    }

    public void setCREATEDAT(String cREATEDAT) {
        this.cREATEDAT = cREATEDAT;
    }

    public String getUPDATEAT() {
        return uPDATEAT;
    }

    public void setUPDATEAT(String uPDATEAT) {
        this.uPDATEAT = uPDATEAT;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
