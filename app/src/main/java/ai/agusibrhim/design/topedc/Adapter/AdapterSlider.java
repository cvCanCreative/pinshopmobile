package ai.agusibrhim.design.topedc.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;

import java.util.List;

import ai.agusibrhim.design.topedc.Helper.Config;
import ai.agusibrhim.design.topedc.Model.PromoModel;
import ai.agusibrhim.design.topedc.R;

public class AdapterSlider extends PagerAdapter {

    List<PromoModel> models;
    LayoutInflater layoutInflater;
    Context context;

    public AdapterSlider(List<PromoModel> models, Context context) {
        this.models = models;
        this.context = context;
    }

    @Override
    public int getCount() {
        return models.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        final PromoModel o = models.get(position);
        layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.item_slider, container,false);

        ImageView imageView;
        CardView cv_main;

        imageView = view.findViewById(R.id.image);
        cv_main = view.findViewById(R.id.cv_slider);

        Picasso.with(context)
                .load(Config.IMAGE_Promo + o.getSLFILE())
                .into(imageView);

        container.addView(view, 0);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View)object);
    }

}
