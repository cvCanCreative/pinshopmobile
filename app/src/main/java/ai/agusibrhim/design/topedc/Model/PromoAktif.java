package ai.agusibrhim.design.topedc.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PromoAktif implements Parcelable {
    @SerializedName("ID_PROMO")
    @Expose
    private String iDPROMO;
    @SerializedName("ID_USER")
    @Expose
    private String iDUSER;
    @SerializedName("PR_JENIS")
    @Expose
    private String pRJENIS;
    @SerializedName("PR_NAME")
    @Expose

    private String pRMEMBER;
    @SerializedName("PR_MEMBER")
    @Expose

    private String pRNAME;
    @SerializedName("PR_CODE")
    @Expose
    private String pRCODE;
    @SerializedName("PR_POTONGAN")
    @Expose
    private String pRPOTONGAN;

    @SerializedName("PR_MIN")
    @Expose
    private String pRMIN;

    @SerializedName("PR_TYPE")
    @Expose
    private String pRTYPE;
    @SerializedName("PR_STATUS")
    @Expose
    private String pRSTATUS;
    @SerializedName("PR_EXPIRED")
    @Expose
    private String pREXPIRED;
    @SerializedName("CREATED_AT")
    @Expose
    private String cREATEDAT;
    @SerializedName("UPDATED_AT")
    @Expose
    private String uPDATEDAT;
    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("message")
    @Expose
    private String message;

    protected PromoAktif(Parcel in) {
        iDPROMO = in.readString();
        iDUSER = in.readString();
        pRJENIS = in.readString();
        pRMEMBER = in.readString();
        pRNAME = in.readString();
        pRCODE = in.readString();
        pRPOTONGAN = in.readString();
        pRMIN = in.readString();
        pRTYPE = in.readString();
        pRSTATUS = in.readString();
        pREXPIRED = in.readString();
        cREATEDAT = in.readString();
        uPDATEDAT = in.readString();
        if (in.readByte() == 0) {
            success = null;
        } else {
            success = in.readInt();
        }
        message = in.readString();
    }

    public static final Creator<PromoAktif> CREATOR = new Creator<PromoAktif>() {
        @Override
        public PromoAktif createFromParcel(Parcel in) {
            return new PromoAktif(in);
        }

        @Override
        public PromoAktif[] newArray(int size) {
            return new PromoAktif[size];
        }
    };

    public String getpRMIN() {
        return pRMIN;
    }

    public void setpRMIN(String pRMIN) {
        this.pRMIN = pRMIN;
    }

    public String getIDPROMO() {
        return iDPROMO;
    }

    public void setIDPROMO(String iDPROMO) {
        this.iDPROMO = iDPROMO;
    }

    public String getIDUSER() {
        return iDUSER;
    }

    public void setIDUSER(String iDUSER) {
        this.iDUSER = iDUSER;
    }

    public String getPRJENIS() {
        return pRJENIS;
    }

    public void setPRJENIS(String pRJENIS) {
        this.pRJENIS = pRJENIS;
    }

    public String getPRNAME() {
        return pRNAME;
    }

    public void setPRNAME(String pRNAME) {
        this.pRNAME = pRNAME;
    }

    public String getPRCODE() {
        return pRCODE;
    }

    public void setPRCODE(String pRCODE) {
        this.pRCODE = pRCODE;
    }

    public String getPRPOTONGAN() {
        return pRPOTONGAN;
    }

    public void setPRPOTONGAN(String pRPOTONGAN) {
        this.pRPOTONGAN = pRPOTONGAN;
    }

    public String getPRTYPE() {
        return pRTYPE;
    }

    public void setPRTYPE(String pRTYPE) {
        this.pRTYPE = pRTYPE;
    }

    public String getPRSTATUS() {
        return pRSTATUS;
    }

    public void setPRSTATUS(String pRSTATUS) {
        this.pRSTATUS = pRSTATUS;
    }

    public String getPREXPIRED() {
        return pREXPIRED;
    }

    public void setPREXPIRED(String pREXPIRED) {
        this.pREXPIRED = pREXPIRED;
    }

    public String getCREATEDAT() {
        return cREATEDAT;
    }

    public void setCREATEDAT(String cREATEDAT) {
        this.cREATEDAT = cREATEDAT;
    }

    public String getUPDATEDAT() {
        return uPDATEDAT;
    }

    public void setUPDATEDAT(String uPDATEDAT) {
        this.uPDATEDAT = uPDATEDAT;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getpRMEMBER() {
        return pRMEMBER;
    }

    public void setpRMEMBER(String pRMEMBER) {
        this.pRMEMBER = pRMEMBER;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(iDPROMO);
        parcel.writeString(iDUSER);
        parcel.writeString(pRJENIS);
        parcel.writeString(pRMEMBER);
        parcel.writeString(pRNAME);
        parcel.writeString(pRCODE);
        parcel.writeString(pRPOTONGAN);
        parcel.writeString(pRMIN);
        parcel.writeString(pRTYPE);
        parcel.writeString(pRSTATUS);
        parcel.writeString(pREXPIRED);
        parcel.writeString(cREATEDAT);
        parcel.writeString(uPDATEDAT);
        if (success == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(success);
        }
        parcel.writeString(message);
    }
}
