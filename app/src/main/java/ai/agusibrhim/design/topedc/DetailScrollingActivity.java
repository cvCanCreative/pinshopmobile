package ai.agusibrhim.design.topedc;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.arch.persistence.room.Room;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.SliderLayout;
import com.sackcentury.shinebuttonlib.ShineButton;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import ai.agusibrhim.design.topedc.Activity.ChatActivity;
import ai.agusibrhim.design.topedc.Activity.EditAlamatActivity;
import ai.agusibrhim.design.topedc.Activity.FullScreenActivity;
import ai.agusibrhim.design.topedc.Activity.LihatFeedbackActivity;
import ai.agusibrhim.design.topedc.Activity.LoginActivity;
import ai.agusibrhim.design.topedc.Activity.TokoKitaActivity;
import ai.agusibrhim.design.topedc.Adapter.AdapterProductImage;
import ai.agusibrhim.design.topedc.Helper.Config;
import ai.agusibrhim.design.topedc.Helper.HelperClass;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.Helper.dataRoom.AppDatabase;
import ai.agusibrhim.design.topedc.Helper.dataRoom.Pesanan;
import ai.agusibrhim.design.topedc.Model.FavoritModel;
import ai.agusibrhim.design.topedc.Model.FeedbackModel;
import ai.agusibrhim.design.topedc.Model.ListProduct;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailScrollingActivity extends AppCompatActivity {

    private SliderLayout sliderSlider;
    private TextView txtNamaBarang;
    private TextView txtHargaBarang;
    private TextView txtDeskripsiBarang;
    private LinearLayout ly;
    private Button btnDelete;
    private ImageView btnPesan;
    private ShineButton shineButton;
    //    ArrayList<ListProduct> list = new ArrayList<>();
//    ArrayList<FavoritModel> listFav = new ArrayList<>();
    FavoritModel listFav;
    int position;
    HashMap<String, String> url_maps;
    HashMap<String, Integer> file_maps;
    SharedPref sharedPref;
    ArrayList<String> listImage = new ArrayList<>();
    private ViewPager pager;
    private LinearLayout layoutDots;
    ListProduct list;
    private AppDatabase db;
    AlertDialog.Builder dialog;
    LayoutInflater inflater;
    View dialogView;
    ArrayList<String> listJumlah = new ArrayList<>();
    String jumlah;
    private TextView txtStok;
    private TextView txtKondisi;
    int stok;
    private Button btnFavorit;
    private TextView txtNamaToko;
    String data, idWish;
    private ImageView btnChat;
    private TextView tvLihatFeedback;
    private TextView txtJumlahFeedback;
    String id_barang;
    ArrayList<FeedbackModel> listFed = new ArrayList<>();
    private TextView txtBerat;
    List<String> idBarangChart = new ArrayList<>();
    ArrayList<Pesanan> listPesananByIdToko = new ArrayList<>();
    int jumlahPesananDisimpan;
    private CircleImageView imgToko;
    private TextView txtAlamatToko;
    private LinearLayout lyLihatToko;
    private AppBarLayout appBar;
    private CollapsingToolbarLayout toolbarLayout;
    private Toolbar toolbar;
    private TextView tvSelengkapnya;
    BottomSheetBehavior behavior;
    private BottomSheetDialog mBottomSheetDialog;
    private LinearLayout bottomSheet;
    String statusData;
    private Button btnBeli;
    private LinearLayout lyFeedbackNull;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_scrolling);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        initView();

        awal();
        mainButton();

        tvSelengkapnya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sheetDeskri();
            }
        });


        View bottomSheet = findViewById(R.id.bottom_sheet);
        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                // React to state change
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });

    }

    private void sheetDeskri() {
        if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        mBottomSheetDialog = new BottomSheetDialog(this);
        View view = getLayoutInflater().inflate(R.layout.sheet_detail_barang, null);
        final ImageView imgClose = view.findViewById(R.id.img_close);
        final TextView tvBerat = view.findViewById(R.id.txt_berat);
        final TextView tvStok = view.findViewById(R.id.txt_stok);
        final TextView tvKategori = view.findViewById(R.id.tv_kategori);
        final TextView tvKondisi = view.findViewById(R.id.tv_kondisi);
        final TextView tvNamaToko = view.findViewById(R.id.tv_nama_toko);
        final TextView tvDeskripsi = view.findViewById(R.id.txt_deskripsi_barang);


        if (data.equals("fav")) {

            tvDeskripsi.setText("" + Html.fromHtml(listFav.getBADESCRIPTION()));
            tvStok.setText(listFav.getBASTOCK());
            tvKondisi.setText(listFav.getBACONDITION());
            float berat = Float.parseFloat(listFav.getBAWEIGHT()) / 1000;
            tvBerat.setText("" + berat + " Kg");
            tvNamaToko.setText(listFav.getUSDTOKO());

        } else {

            tvDeskripsi.setText("" + Html.fromHtml(list.getBADESCRIPTION()));
            tvStok.setText(list.getBASTOCK());
            tvKondisi.setText(list.getBACONDITION());
            float berat = Float.parseFloat(list.getBAWEIGHT()) / 1000;
            tvBerat.setText("" + berat + " Kg");
            tvNamaToko.setText(list.getUSDTOKO());

        }

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });
    }

    private void awal() {
        sharedPref = new SharedPref(this);
        // initiate pemanggilan Room database
        db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "pesanandb").allowMainThreadQueries().build();

        listPesananByIdToko.addAll(Arrays.asList(db.pesananDAO().lihatPesananById("" + sharedPref.getIdUser())));

//        Toast.makeText(this, ""+listPesananByIdToko.size(), Toast.LENGTH_SHORT).show();

        data = getIntent().getStringExtra("data");

        if (sharedPref.getSudahLogin()) {

        } else {
            shineButton.setVisibility(View.GONE);
            shineButton.setEnabled(false);
        }

        if (data.equals("fav")) {
            idWish = getIntent().getStringExtra("id_wis");
            listFav = getIntent().getParcelableExtra("list");
            position = Integer.parseInt(getIntent().getStringExtra("posisi"));

            id_barang = listFav.getIDBARANG();

            getFeedBack();
            ambilStatusFav();
            ambilDataFav(listFav, position);
            displayImageSlideFavr(listFav, position);

        } else {
            list = getIntent().getParcelableExtra("list");
            position = Integer.parseInt(getIntent().getStringExtra("posisi"));

            id_barang = "" + list.getIDBARANG();

            getFeedBack();
            ambilStatus();
            ambilData(list, position);
            displayImageSlider(list, position);
        }
    }

    private void getFeedBack() {
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getFeedback(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                id_barang).enqueue(new Callback<ArrayList<FeedbackModel>>() {
            @Override
            public void onResponse(Call<ArrayList<FeedbackModel>> call, Response<ArrayList<FeedbackModel>> response) {
                if (response.isSuccessful()) {
                    listFed = response.body();
                    if (listFed.get(0).getSuccess() == 0) {
//                        txtJumlahFeedback.setText("Data feedback kosong");
                        txtJumlahFeedback.setVisibility(View.GONE);
                        lyFeedbackNull.setVisibility(View.VISIBLE);
                    } else {
                        txtJumlahFeedback.setText("Ada " + listFed.size() + " Feddback");
                    }

                }
            }

            @Override
            public void onFailure(Call<ArrayList<FeedbackModel>> call, Throwable t) {
                txtJumlahFeedback.setText("Klik lihat");
            }
        });
    }

    private void cekDatabase() {

        if (listPesananByIdToko.size() == 0) {
            masukKeranjang();
        } else {
            for (int i = 0; i < listPesananByIdToko.size(); i++) {
                idBarangChart.add(listPesananByIdToko.get(i).getIdBarang());
            }
            for (int i = 0; i < idBarangChart.size(); i++) {
                if (id_barang.contains(idBarangChart.get(i))) {

                    jumlahPesananDisimpan = Integer.parseInt(listPesananByIdToko.get(i).getJumlahBarang());
//                    updateDatabase(jumlahPesananDisimpan);
                    break;
                }
            }

            if (idBarangChart.contains(id_barang)) {

                updateDatabase(jumlahPesananDisimpan);
            } else {
                masukKeranjang();
            }
//            if (Arrays.asList(idBarangChart).contains(""+id_barang)) {
//                // true
//                updateDatabase(jumlahPesananDisimpan);
//            }else {
//                masukKeranjang();
//            }
        }

//        masukKeranjang();
    }

    private void updateDatabase(int jml) {
        jml = jml + Integer.parseInt(jumlah);
        if (data.equals("fav")) {
            int harga = Integer.parseInt(listFav.getBAPRICE());
            int sub = harga * jml;
            db.pesananDAO().updatePesanan("" + jml,
                    "" + sub,
                    "" + listFav.getIDBARANG());

            Toast.makeText(DetailScrollingActivity.this, "Barang Berhasil ditambahkan ", Toast.LENGTH_SHORT).show();
        } else {
            int harga = Integer.parseInt(list.getBAPRICE());
            int sub = harga * jml;
            db.pesananDAO().updatePesanan("" + jml,
                    "" + sub,
                    "" + list.getIDBARANG());

            Toast.makeText(DetailScrollingActivity.this, "Barang Berhasil ditambahkan ", Toast.LENGTH_SHORT).show();
        }

    }

    private void hapusFavorit() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Proses ...");
        pd.setCancelable(false);
        pd.show();
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.hapusWishList(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                id_barang).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String pesan = jsonObject.optString("message");
                        if (pesan.equals("Berhasil delete")) {
                            Toast.makeText(DetailScrollingActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                            btnFavorit.setText("Jadikan Favorit");
                            shineButton.setChecked(false);
//                            startActivity(new Intent(getApplicationContext(), FavoritActivity.class)
//                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
//                            finish();
                        } else {
                            Toast.makeText(DetailScrollingActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(DetailScrollingActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void hapusFavoritFav() {
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.hapusWishList(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                idWish).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {

                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String pesan = jsonObject.optString("message");
                        Toast.makeText(DetailScrollingActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Toast.makeText(DetailScrollingActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void ambilStatus() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Proses ...");
        pd.setCancelable(false);
        pd.show();
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getStatusFavorit(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                list.getIDBARANG()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONArray jsonArray = new JSONArray(response.body().string());
                        JSONObject jsonObject = jsonArray.optJSONObject(0);
                        String status = jsonObject.optString("WBR_STATUS");
//                        Toast.makeText(DetailActivity.this, ""+status, Toast.LENGTH_SHORT).show();
                        if (status.equals("Favorite")) {
                            btnFavorit.setText("Batalkan Favorit");
                            shineButton.setChecked(true);
                        } else {
                            btnFavorit.setText("Jadikan Favorit");
                            shineButton.setChecked(false);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(DetailScrollingActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void ambilStatusFav() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Proses ...");
        pd.setCancelable(false);
        pd.show();
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getStatusFavorit(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                listFav.getIDBARANG()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONArray jsonArray = new JSONArray(response.body().string());
                        JSONObject jsonObject = jsonArray.optJSONObject(0);
                        String status = jsonObject.optString("WBR_STATUS");
//                        Toast.makeText(DetailActivity.this, ""+status, Toast.LENGTH_SHORT).show();
                        if (status.equals("Favorite")) {
                            btnFavorit.setText("Batalkan Favorit");
                            shineButton.setChecked(true);
                        } else {
                            btnFavorit.setText("Jadikan Favorit");
                            shineButton.setChecked(false);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(DetailScrollingActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void addVaforit() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Proses ...");
        pd.setCancelable(false);
        pd.show();
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.addWishList(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                list.getIDBARANG()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String pesan = jsonObject.optString("message");
                        if (pesan.equals("Berhasil ditambahkan")) {
                            Toast.makeText(DetailScrollingActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                            btnFavorit.setText("Batalkan Favorit");
                            shineButton.setChecked(true);
                        } else {
                            Toast.makeText(DetailScrollingActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(DetailScrollingActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void tampilDialog() {
        for (int i = 0; i < 100; i++) {
            int nilai = i + 1;
            listJumlah.add("" + nilai);
        }

        dialog = new AlertDialog.Builder(DetailScrollingActivity.this);
        inflater = getLayoutInflater();
        dialogView = inflater.inflate(R.layout.row_masuk_keranjang, null);
        dialog.setView(dialogView);
        dialog.setIcon(R.drawable.ic_cat14);

        final Spinner spn = (Spinner) dialogView.findViewById(R.id.spn_jumlah);
        final TextView tvJumlahBeli = (TextView) dialogView.findViewById(R.id.tv_jumlah_beli);
        final TextView tvNama = (TextView) dialogView.findViewById(R.id.tv_nama_barang);
        final TextView tvHarga = (TextView) dialogView.findViewById(R.id.tv_harga);
        final TextView tvSubTotal = (TextView) dialogView.findViewById(R.id.tv_sub_total);
        final ImageView imgBarang = (ImageView) dialogView.findViewById(R.id.img_barang);
        final Button btnPesab = (Button) dialogView.findViewById(R.id.btn_pesan);

        final AlertDialog alertDialog = dialog.create();

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(DetailScrollingActivity.this, R.layout.support_simple_spinner_dropdown_item, listJumlah);
        spn.setAdapter(arrayAdapter);
        spn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
                jumlah = spn.getSelectedItem().toString();
                tvJumlahBeli.setText("jumlah beli " + jumlah);

                if (data.equals("fav")) {

                    int harga = Integer.parseInt(listFav.getBAPRICE());
                    int sub = harga * Integer.parseInt(jumlah);
                    tvSubTotal.setText("" + new HelperClass().convertRupiah(sub));
                } else {
                    int harga = Integer.parseInt(list.getBAPRICE());
                    int sub = harga * Integer.parseInt(jumlah);
                    tvSubTotal.setText("" + new HelperClass().convertRupiah(sub));
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                int harga = Integer.parseInt(list.getBAPRICE());
                int sub = harga * 1;
                tvSubTotal.setText("" + new HelperClass().convertRupiah(sub));
            }
        });

        if (data.equals("fav")) {

            tvNama.setText("" + listFav.getBANAME());
            tvHarga.setText("" + new HelperClass().convertRupiah(Integer.parseInt(listFav.getBAPRICE())));
            tvSubTotal.setText("" + new HelperClass().convertRupiah(Integer.parseInt(listFav.getBAPRICE())));

            String gambar = listFav.getBAIMAGE();
            String gb = gambar.replace("|", " ");
            String strArray[] = gb.split(" ");

            Picasso.with(getApplicationContext()).load(Config.IMAGE_BARANG + strArray[0]).into(imgBarang);
        } else {
            tvNama.setText("" + list.getBANAME());
            tvHarga.setText("" + new HelperClass().convertRupiah(Integer.parseInt(list.getBAPRICE())));
            tvSubTotal.setText("" + new HelperClass().convertRupiah(Integer.parseInt(list.getBAPRICE())));

            String gambar = list.getBAIMAGE();
            String gb = gambar.replace("|", " ");
            String strArray[] = gb.split(" ");

            Picasso.with(getApplicationContext()).load(Config.IMAGE_BARANG + strArray[0]).into(imgBarang);
        }


        btnPesab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                aturanPesan();
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }

    private void aturanPesan() {
        if (sharedPref.getSudahLogin()) {
            if (Integer.parseInt(jumlah) > stok) {
                Toast.makeText(this, "Stok Barang Tidak Cukup", Toast.LENGTH_SHORT).show();
            } else {
                cekDatabase();
            }

        } else {
            new android.app.AlertDialog.Builder(DetailScrollingActivity.this)
                    .setMessage("Anda Harus Login Terlebih Dahulu !")
                    .setCancelable(false)
                    .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            startActivity(new Intent(getApplicationContext(), LoginActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                            finish();
                        }
                    })
                    .setNegativeButton("Tidak", null)
                    .show();
        }
    }

    private void masukKeranjang() {

        if (data.equals("fav")) {
            Pesanan p = new Pesanan();

            p.setNamaToko("" + listFav.getUSDTOKO());
            p.setHargaBarang("" + listFav.getBAPRICE());
            p.setIdBarang("" + listFav.getIDBARANG());
            p.setIdToko("" + listFav.getIDUSER());
            p.setImgBarang("" + listFav.getBAIMAGE());
            p.setJumlahBarang("" + jumlah);
            p.setNamaBarang("" + listFav.getBANAME());
            p.setTipeBarang("" + listFav.getIDSUBKATEGORI());
            p.setBerat("" + listFav.getBAWEIGHT());
            p.setIdKota("" + listFav.getUSDIDKOTA());
            p.setIdUser("" + sharedPref.getIdUser());
            p.setIdKecamatan("" + listFav.getUSDIDKECAMATAN());
            p.setStok("" + listFav.getBASTOCK());
            int sub = Integer.parseInt(jumlah) * Integer.parseInt(listFav.getBAPRICE());
            p.setSubHargaBarang("" + sub);
            insertData(p);
        } else {
            Pesanan p = new Pesanan();

            p.setNamaToko("" + list.getUSDTOKO());
            p.setHargaBarang("" + list.getBAPRICE());
            p.setIdBarang("" + list.getIDBARANG());
            p.setIdToko("" + list.getIDUSER());
            p.setImgBarang("" + list.getBAIMAGE());
            p.setJumlahBarang("" + jumlah);
            p.setNamaBarang("" + list.getBANAME());
            p.setTipeBarang("" + list.getIDKATEGORIBARANG());
            p.setBerat("" + list.getBAWEIGHT());
            p.setIdKota("" + list.getUSDIDKOTA());
            p.setIdUser("" + sharedPref.getIdUser());
            p.setIdKecamatan("" + list.getUSDIDKECAMATAN());
            p.setStok("" + list.getBASTOCK());
            int sub = Integer.parseInt(jumlah) * Integer.parseInt(list.getBAPRICE());
            p.setSubHargaBarang("" + sub);
            insertData(p);
        }

    }

    private void mainButton() {
        btnPesan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tampilDialog();
            }
        });

        shineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (btnFavorit.getText().toString().equals("Jadikan Favorit")) {
                    addVaforit();
                } else {
                    hapusFavorit();
                }
            }
        });

        txtNamaToko.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (data.equals("fav")) {
                    Intent intent = new Intent(getApplicationContext(), TokoKitaActivity.class);
                    intent.putExtra("data", "tamu");
                    intent.putExtra("id", listFav.getIDUSER());
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(getApplicationContext(), TokoKitaActivity.class);
                    intent.putExtra("data", "tamu");
                    intent.putExtra("id", list.getIDUSER());
                    startActivity(intent);
                }
            }
        });

        lyLihatToko.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (data.equals("fav")) {
                    Intent intent = new Intent(getApplicationContext(), TokoKitaActivity.class);
                    intent.putExtra("data", "tamu");
                    intent.putExtra("id", listFav.getIDUSER());
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(getApplicationContext(), TokoKitaActivity.class);
                    intent.putExtra("data", "tamu");
                    intent.putExtra("id", list.getIDUSER());
                    startActivity(intent);
                }
            }
        });

        btnChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sharedPref.getSudahLogin()) {
                    if (data.equals("fav")) {
                        Intent intent = new Intent(getApplicationContext(), ChatActivity.class);
                        sharedPref.savePrefString(SharedPref.ID_CHAT, listFav.getIDUSER());
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(getApplicationContext(), ChatActivity.class);
                        sharedPref.savePrefString(SharedPref.ID_CHAT, list.getIDUSER());
                        startActivity(intent);
                    }
                } else {
                    Toast.makeText(DetailScrollingActivity.this, "Anda Harus Login Terlebih dahulu", Toast.LENGTH_SHORT).show();
                }
            }
        });

        tvLihatFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), LihatFeedbackActivity.class);
                intent.putExtra("id", id_barang);
                startActivity(intent);
            }
        });

        btnBeli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), EditAlamatActivity.class);
                intent.putExtra("data", "bayar");
                sharedPref.savePrefString(SharedPref.TIPEBELI, "langsung");
                startActivity(intent);
            }
        });
    }

    @SuppressLint("StaticFieldLeak")
    private void insertData(final Pesanan pesanan) {

        new AsyncTask<Void, Void, Long>() {
            @Override
            protected Long doInBackground(Void... voids) {
                // melakukan proses insert data
                long status = db.pesananDAO().insertPesanan(pesanan);
//                Toast.makeText(DetailActivity.this, ""+status, Toast.LENGTH_SHORT).show();
                return status;
            }

            @Override
            protected void onPostExecute(Long status) {
                Toast.makeText(DetailScrollingActivity.this, "Barang Berhasil ditambahkan ", Toast.LENGTH_SHORT).show();
            }
        }.execute();
//        db.pesananDAO().insertPesanan(pesanan);
//        Toast.makeText(this, "Barang Berhasil ditambah", Toast.LENGTH_SHORT).show();
    }

    private void ambilData(ListProduct list, int posisi) {
        txtNamaBarang.setText("" + list.getBANAME());
        txtHargaBarang.setText("" + new HelperClass().convertRupiah(Integer.parseInt(list.getBAPRICE())));
        txtDeskripsiBarang.setText("" + Html.fromHtml(list.getBADESCRIPTION()));
        txtStok.setText(list.getBASTOCK());
        txtKondisi.setText(list.getBACONDITION());
        float berat = Float.parseFloat(list.getBAWEIGHT()) / 1000;
        txtBerat.setText("" + berat + " Kg");

        txtNamaToko.setText(list.getUSDTOKO());
        txtAlamatToko.setText(list.getUSDKOTA());
        stok = Integer.parseInt(list.getBASTOCK());
        Picasso.with(getApplicationContext())
                .load(Config.IMAGE_TOKO + list.getUSDTOKOFOTO())
                .into(imgToko);
    }

    private void displayImageSlider(ListProduct list, int position) {
        final LinearLayout layout_dots = (LinearLayout) findViewById(R.id.layout_dots);
        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        final AdapterProductImage adapterSlider = new AdapterProductImage(this, new ArrayList<String>());

        String gambar = list.getBAIMAGE();
        String gb = gambar.replace("|", " ");
        String strArray[] = gb.split(" ");

        for (int i = 0; i < strArray.length; i++) {
            if (list != null) listImage.add(strArray[i]);
        }
        adapterSlider.setItems(listImage);
        viewPager.setAdapter(adapterSlider);

        // displaying selected image first
        viewPager.setCurrentItem(0);
        addBottomDots(layout_dots, adapterSlider.getCount(), 0);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int pos, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int pos) {
                addBottomDots(layout_dots, adapterSlider.getCount(), pos);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });


//        final ArrayList<String> images_list = new ArrayList<>();
//        for (int i = 0; i <listImage.size() ; i++) {
//            images_list.add(Config.IMAGE_BARANG+listImage.get(position));
//        }


        adapterSlider.setOnItemClickListener(new AdapterProductImage.OnItemClickListener() {
            @Override
            public void onItemClick(View view, String obj, int position) {
                Intent i = new Intent(DetailScrollingActivity.this, FullScreenActivity.class);
                i.putExtra("posisi", position);
                i.putStringArrayListExtra("list", listImage);
                startActivity(i);
            }
        });
    }

    private void ambilDataFav(FavoritModel list, int posisi) {
        txtNamaBarang.setText("" + list.getBANAME());
        txtHargaBarang.setText("" + new HelperClass().convertRupiah(Integer.parseInt(list.getBAPRICE())));
        txtDeskripsiBarang.setText("" + Html.fromHtml(list.getBADESCRIPTION()));
        txtStok.setText(list.getBASTOCK());
        txtKondisi.setText(list.getBACONDITION());
        float berat = Float.parseFloat(list.getBAWEIGHT()) / 1000;
        txtBerat.setText("" + berat + " Kg");
        txtNamaToko.setText(list.getUSDTOKO());
        txtAlamatToko.setText(list.getUSDKOTA());
        stok = Integer.parseInt(list.getBASTOCK());
        Picasso.with(getApplicationContext())
                .load(Config.IMAGE_TOKO + list.getUSDTOKOFOTO())
                .into(imgToko);

        stok = Integer.parseInt(list.getBASTOCK());
    }

    private void displayImageSlideFavr(FavoritModel list, int position) {
        final LinearLayout layout_dots = (LinearLayout) findViewById(R.id.layout_dots);
        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        final AdapterProductImage adapterSlider = new AdapterProductImage(this, new ArrayList<String>());

        String gambar = list.getBAIMAGE();
        String gb = gambar.replace("|", " ");
        String strArray[] = gb.split(" ");

        for (int i = 0; i < strArray.length; i++) {
            if (list != null) listImage.add(strArray[i]);
        }
        adapterSlider.setItems(listImage);
        viewPager.setAdapter(adapterSlider);

        // displaying selected image first
        viewPager.setCurrentItem(0);
        addBottomDots(layout_dots, adapterSlider.getCount(), 0);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int pos, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int pos) {
                addBottomDots(layout_dots, adapterSlider.getCount(), pos);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });


//        final ArrayList<String> images_list = new ArrayList<>();
//        for (int i = 0; i <listImage.size() ; i++) {
//            images_list.add(Config.IMAGE_BARANG+listImage.get(position));
//        }


        adapterSlider.setOnItemClickListener(new AdapterProductImage.OnItemClickListener() {
            @Override
            public void onItemClick(View view, String obj, int position) {
                Intent i = new Intent(DetailScrollingActivity.this, FullScreenActivity.class);
                i.putExtra("posisi", position);
                i.putStringArrayListExtra("list", listImage);
                startActivity(i);
            }
        });
    }

    private void addBottomDots(LinearLayout layout_dots, int size, int current) {
        ImageView[] dots = new ImageView[size];

        layout_dots.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new ImageView(this);
            int width_height = 15;
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(new ViewGroup.LayoutParams(width_height, width_height));
            params.setMargins(10, 10, 10, 10);
            dots[i].setLayoutParams(params);
            dots[i].setImageResource(R.drawable.shape_circle);
            dots[i].setColorFilter(ContextCompat.getColor(this, R.color.darkOverlaySoft));
            layout_dots.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[current].setColorFilter(ContextCompat.getColor(this, R.color.colorPrimaryLight));
    }

    private void initView() {
        appBar = (AppBarLayout) findViewById(R.id.app_bar);
        toolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        pager = (ViewPager) findViewById(R.id.pager);
        layoutDots = (LinearLayout) findViewById(R.id.layout_dots);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        shineButton = (ShineButton) findViewById(R.id.shine_button);
        txtNamaBarang = (TextView) findViewById(R.id.txt_nama_barang);
        txtHargaBarang = (TextView) findViewById(R.id.txt_harga_barang);
        txtKondisi = (TextView) findViewById(R.id.txt_kondisi);
        txtStok = (TextView) findViewById(R.id.txt_stok);
        txtBerat = (TextView) findViewById(R.id.txt_berat);
        imgToko = (CircleImageView) findViewById(R.id.img_toko);
        txtNamaToko = (TextView) findViewById(R.id.txt_nama_toko);
        txtAlamatToko = (TextView) findViewById(R.id.txt_alamat_toko);
        lyLihatToko = (LinearLayout) findViewById(R.id.ly_lihat_toko);
        txtDeskripsiBarang = (TextView) findViewById(R.id.txt_deskripsi_barang);
        tvLihatFeedback = (TextView) findViewById(R.id.tv_lihat_feedback);
        txtJumlahFeedback = (TextView) findViewById(R.id.txt_jumlah_feedback);
        btnChat = (ImageView) findViewById(R.id.btn_chat);
        btnPesan = (ImageView) findViewById(R.id.btn_pesan);
        btnFavorit = (Button) findViewById(R.id.btn_favorit);
        appBar = (AppBarLayout) findViewById(R.id.app_bar);
        toolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvSelengkapnya = (TextView) findViewById(R.id.tv_selengkapnya);
        btnBeli = (Button) findViewById(R.id.btn_beli);
        lyFeedbackNull = (LinearLayout) findViewById(R.id.div_feedbacknull);
    }

}
