package ai.agusibrhim.design.topedc.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ai.agusibrhim.design.topedc.Adapter.ListProductAdapter;
import ai.agusibrhim.design.topedc.Adapter.ListProdukAdapterIndex;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.Model.KotaModel;
import ai.agusibrhim.design.topedc.Model.ListProduct;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class LihatProductActivity extends AppCompatActivity {

    private RecyclerView rv;
    private SwipeRefreshLayout swipe;
    ListProductAdapter adapter;
    ListProdukAdapterIndex adapter2;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<ListProduct> list = new ArrayList<>();
    private ImageView imgNoItem;
    private TextView tvNoItem;
    private EditText edtSearch;
    private Spinner spnFilter;
    SharedPref sharedPref;
    int indexLoad = 1;
    int indexFilter = 1;
    int indexMinMax = 1;
    int indexKota =1;
    private CoordinatorLayout lyUtama;
    private LinearLayout lyDikirim;
    private LinearLayout lyFilter;
    private LinearLayout lyUrutkan;
    BottomSheetBehavior behavior;
    private BottomSheetDialog mBottomSheetDialog;
    private LinearLayout bottomSheet;
    String pilihan;
    private ImageView imgClose;
    String min,max;
    List<String> listProvinsi = new ArrayList<>();
    List<String> listIdProvinsi = new ArrayList<>();
    List<String> setKota = new ArrayList<>();
    List<String> listIdKota = new ArrayList<>();
    String idKota;
    String pilih = "1000";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lihat_product);
        initView();

        awal();

        mainButton();
    }

    private void mainButton(){
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (pilihan.equals("NEW")) {
                    indexFilter = 1;
                    getFilter(indexFilter);
                } else if (pilihan.equals("SECOND")) {
                    indexFilter = 1;
                    getFilter(indexFilter);
                } else {
                    indexLoad = 1;
                    getData(indexLoad);
                }
            }
        });
        edtSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (list.get(0).getMessage().equals("Berhasil")) {
                    Intent intent = new Intent(getApplicationContext(), SearchActivity.class);
                    intent.putExtra("data", "kategori");
                    intent.putExtra("id_kat", list.get(0).getIDSUBKATEGORI());
                    startActivity(intent);
                } else {
                    Toast.makeText(LihatProductActivity.this, "maaf tidak ada data yg dicari", Toast.LENGTH_SHORT).show();
                }
            }
        });

        lyUrutkan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showBottomSheetDialog();
            }
        });

        lyFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sheetFilter();
            }
        });

        lyDikirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sheetDikirim();
            }
        });

        View bottomSheet = findViewById(R.id.bottom_sheet);
        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                // React to state change
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });
    }

    private void filterMinMaxKat(String min,String max,int index){
        swipe.setRefreshing(true);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.filterBarangKat(getIntent().getStringExtra("id"),
                min,
                max,
                index).enqueue(new Callback<ArrayList<ListProduct>>() {
            @Override
            public void onResponse(Call<ArrayList<ListProduct>> call, Response<ArrayList<ListProduct>> response) {
                if (response.isSuccessful()){
                    swipe.setRefreshing(false);
                    list = response.body();
                    if (list.size() > 0) {
                        imgNoItem.setVisibility(View.GONE);
                        tvNoItem.setVisibility(View.GONE);
                        rv.setVisibility(View.VISIBLE);

                        adapter2 = new ListProdukAdapterIndex(list, getApplicationContext());
                        rv.setAdapter(adapter2);

                        adapter2.notifyDataSetChanged();

                        adapter2.setLoadMoreListener(new ListProdukAdapterIndex.OnLoadMoreListener() {
                            @Override
                            public void onLoadMore() {
                                rv.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        indexMinMax = indexMinMax + 1;
                                        loadMinMax(indexMinMax);
                                    }
                                });
                            }
                        });
                    } else {
                        imgNoItem.setVisibility(View.VISIBLE);
                        tvNoItem.setVisibility(View.VISIBLE);
                        rv.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ListProduct>> call, Throwable t) {
                swipe.setRefreshing(false);
                Toast.makeText(LihatProductActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void filterKota(String id,int index){
        swipe.setRefreshing(true);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.filterKotaBarangKat(getIntent().getStringExtra("id"),
                id,
                index).enqueue(new Callback<ArrayList<ListProduct>>() {
            @Override
            public void onResponse(Call<ArrayList<ListProduct>> call, Response<ArrayList<ListProduct>> response) {
                if (response.isSuccessful()){
                    swipe.setRefreshing(false);
                    list = response.body();
                    if (list.size() > 0) {
                        imgNoItem.setVisibility(View.GONE);
                        tvNoItem.setVisibility(View.GONE);
                        rv.setVisibility(View.VISIBLE);

                        adapter2 = new ListProdukAdapterIndex(list, getApplicationContext());
                        rv.setAdapter(adapter2);

                        adapter2.notifyDataSetChanged();

                        adapter2.setLoadMoreListener(new ListProdukAdapterIndex.OnLoadMoreListener() {
                            @Override
                            public void onLoadMore() {
                                rv.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        indexKota = indexKota + 1;
                                        loadKota(indexKota);
                                    }
                                });
                            }
                        });
                    } else {
                        imgNoItem.setVisibility(View.VISIBLE);
                        tvNoItem.setVisibility(View.VISIBLE);
                        rv.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ListProduct>> call, Throwable t) {
                swipe.setRefreshing(false);
                Toast.makeText(LihatProductActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void sheetDikirim() {
        if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        mBottomSheetDialog = new BottomSheetDialog(this);
        View view = getLayoutInflater().inflate(R.layout.sheet_dikirim, null);
        final ImageView imgClose = view.findViewById(R.id.img_close);
        final Spinner spnProvinsi = view.findViewById(R.id.spn_provinsi);
        final Spinner spnKota = view.findViewById(R.id.spn_kota);
        final Button btnSimpan = view.findViewById(R.id.btn_simpan);

//        setSpnProvinsi();

        ArrayAdapter<String> adp2 = new ArrayAdapter<String>(LihatProductActivity.this, R.layout.support_simple_spinner_dropdown_item, listProvinsi);
        spnProvinsi.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
        spnProvinsi.setAdapter(adp2);

        spnProvinsi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int a = spnProvinsi.getSelectedItemPosition();
                int idProv = a + 1;
//                                Toast.makeText(TambahAlamatActivity.this, "id prov "+idProv, Toast.LENGTH_SHORT).show();
                setKota.removeAll(setKota);
                listIdKota.removeAll(listIdKota);

                ApiService apiService = ApiConfig.getInstanceRetrofit();
                apiService.getKota(""+idProv).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()) {
                            try {
                                JSONObject jsonObject = new JSONObject(response.body().string());
                                JSONObject jsonObject1 = jsonObject.optJSONObject("rajaongkir");
                                JSONArray jsonArray = jsonObject1.optJSONArray("results");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject2 = jsonArray.optJSONObject(i);
                                    String kota = jsonObject2.optString("city_name");
                                    String kode = jsonObject2.optString("postal_code");
                                    String tipe = jsonObject2.optString("type");
                                    String idKota = jsonObject2.optString("city_id");
                                    String idProvinsi = jsonObject2.optString("province_id");
                                    String provinsi = jsonObject2.optString("province");

                                    setKota.add(tipe + " " + kota);
                                    listIdKota.add(idKota);

                                    ArrayAdapter<String> adp = new ArrayAdapter<String>(LihatProductActivity.this, R.layout.support_simple_spinner_dropdown_item, setKota);
                                    spnKota.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
                                    spnKota.setAdapter(adp);

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int a = spnKota.getSelectedItemPosition();
                idKota = listIdKota.get(a);
//                Toast.makeText(LihatProductActivity.this, "id kota "+idKota , Toast.LENGTH_SHORT).show();
                indexKota = 1;
                filterKota(idKota,indexKota);
                pilih = "1000";
                mBottomSheetDialog.dismiss();
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });
    }

    private void setSpnProvinsi() {
        listProvinsi.removeAll(listProvinsi);
        listIdProvinsi.removeAll(listIdProvinsi);

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getProvinsi().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        JSONObject jsonObject1 = jsonObject.optJSONObject("rajaongkir");
                        JSONObject objectStatus = jsonObject1.optJSONObject("status");
                        String ket = objectStatus.optString("description");
                        if (ket.equals("This key has reached the daily limit.")){

                        }else {
                            JSONArray jsonArray = jsonObject1.optJSONArray("results");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject2 = jsonArray.optJSONObject(i);
                                String provinsi = jsonObject2.optString("province");
                                String id = jsonObject2.optString("province_id");

                                listProvinsi.add(provinsi);
                                listIdProvinsi.add(id);

                                getKota("2");
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(LihatProductActivity.this, "gagal ambil provinsi", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void getKota(String id) {
        setKota.removeAll(setKota);
        listIdKota.removeAll(listIdKota);

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getKota(id).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        JSONObject jsonObject1 = jsonObject.optJSONObject("rajaongkir");
                        JSONArray jsonArray = jsonObject1.optJSONArray("results");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject2 = jsonArray.optJSONObject(i);
                            String kota = jsonObject2.optString("city_name");
                            String kode = jsonObject2.optString("postal_code");
                            String tipe = jsonObject2.optString("type");
                            String idKota = jsonObject2.optString("city_id");
                            String idProvinsi = jsonObject2.optString("province_id");
                            String provinsi = jsonObject2.optString("province");

                            setKota.add(tipe + " " + kota);
                            listIdKota.add(idKota);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void sheetFilter() {
        if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        mBottomSheetDialog = new BottomSheetDialog(this);
        View view = getLayoutInflater().inflate(R.layout.sheet_filter, null);
        final ImageView imgClose = view.findViewById(R.id.img_close);
        final EditText edtMin = view.findViewById(R.id.edt_min);
        final EditText edtMax = view.findViewById(R.id.edt_max);
        final Button btnAktifkan = view.findViewById(R.id.btn_aktifkan);

        btnAktifkan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilih = "1000";
                min = edtMin.getText().toString();
                max = edtMax.getText().toString();
                mBottomSheetDialog.dismiss();
                indexMinMax = 1;
                filterMinMaxKat(min,max,indexMinMax);
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });
    }

    private void showBottomSheetDialog() {
        //untuk hide
//        behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        mBottomSheetDialog = new BottomSheetDialog(this);
        View view = getLayoutInflater().inflate(R.layout.sheet_urutkan, null);
        final LinearLayout lyBaru = view.findViewById(R.id.ly_baru);
        final LinearLayout lyLama = view.findViewById(R.id.ly_bekas);
        final LinearLayout lyLaris = view.findViewById(R.id.ly_laris);
        final LinearLayout lyNormal = view.findViewById(R.id.ly_normal);
        final ImageView imgClose = view.findViewById(R.id.img_close);

        TextView tvNoBaru = view.findViewById(R.id.tv_no_baru);
        TextView tvNoBekas = view.findViewById(R.id.tv_no_bekas);
        TextView tvNoAll = view.findViewById(R.id.tv_no_semua);

        ImageView imgBaru = view.findViewById(R.id.img_baru);
        ImageView imgBekas = view.findViewById(R.id.img_bekas);
        ImageView imgAll = view.findViewById(R.id.img_semua);

        tvNoBaru.setText("1");
        tvNoBekas.setText("2");
        tvNoAll.setText("3");

        if (pilih.equals(tvNoBaru.getText().toString())){
            imgBaru.setVisibility(View.VISIBLE);
        }else if (pilih.equals(tvNoBekas.getText().toString())){
            imgBekas.setVisibility(View.VISIBLE);
        }else if (pilih.equals(tvNoAll.getText().toString())){
            imgAll.setVisibility(View.VISIBLE);
        }else {
            imgAll.setVisibility(View.GONE);
            imgBaru.setVisibility(View.GONE);
            imgBekas.setVisibility(View.GONE);
        }

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
            }
        });

        lyBaru.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilihan = "NEW";
                pilih = "1";
                indexFilter = 1;
                getFilter(indexFilter);
                mBottomSheetDialog.dismiss();
            }
        });

        lyLama.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilihan = "SECOND";
                pilih ="2";
                indexFilter = 1;
                getFilter(indexFilter);
                mBottomSheetDialog.dismiss();
            }
        });

        lyNormal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilihan = "ALL";
                pilih = "3";
                indexLoad = 1;
                getData(indexLoad);
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });
    }

    private void awal() {
        sharedPref = new SharedPref(this);

        adapter2 = new ListProdukAdapterIndex(list, this);

        layoutManager = new GridLayoutManager(this, 2);
        rv.setLayoutManager(layoutManager);

        rv.setAdapter(adapter2);

        edtSearch.setFocusable(false);

        pilihan = "ALL";
        indexLoad = 1;
        getData(indexLoad);
        setSpnProvinsi();
    }

    private void getSpinner() {
        final List<String> list = new ArrayList<String>();
        list.add("ALL");
        list.add("NEW");
        list.add("SECOND");

        ArrayAdapter<String> adp2 = new ArrayAdapter<String>(LihatProductActivity.this, R.layout.support_simple_spinner_dropdown_item, list);
        spnFilter.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
        spnFilter.setAdapter(adp2);

        spnFilter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (spnFilter.getSelectedItem().toString().equals("NEW")) {
                    indexFilter = 1;
                    getFilter(indexFilter);
                } else if (spnFilter.getSelectedItem().toString().equals("SECOND")) {
                    indexFilter = 1;
                    getFilter(indexFilter);
                } else {
                    indexLoad = 1;
                    getData(indexLoad);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void getFilter(int index) {
        swipe.setRefreshing(true);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getBarangByKondisi(
                getIntent().getStringExtra("id"),
                pilihan,
                index).enqueue(new Callback<ArrayList<ListProduct>>() {
            @Override
            public void onResponse(Call<ArrayList<ListProduct>> call, Response<ArrayList<ListProduct>> response) {
                if (response.isSuccessful()) {
                    swipe.setRefreshing(false);
                    list = response.body();
                    if (list.size() > 0) {
                        adapter2 = new ListProdukAdapterIndex(list, getApplicationContext());
                        rv.setAdapter(adapter2);

                        adapter2.notifyDataSetChanged();

                        adapter2.setLoadMoreListener(new ListProdukAdapterIndex.OnLoadMoreListener() {
                            @Override
                            public void onLoadMore() {
                                rv.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        indexLoad = indexLoad + 1;
                                        loadMoreFilter(indexLoad);
                                    }
                                });
                            }
                        });
                    } else {
                        imgNoItem.setVisibility(View.VISIBLE);
                        tvNoItem.setVisibility(View.VISIBLE);
                        rv.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ListProduct>> call, Throwable t) {
                swipe.setRefreshing(false);
                Toast.makeText(LihatProductActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void getData(int index) {
        swipe.setRefreshing(true);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getProductByCatagori(getIntent().getStringExtra("id"),
                index).enqueue(new Callback<ArrayList<ListProduct>>() {
            @Override
            public void onResponse(Call<ArrayList<ListProduct>> call, Response<ArrayList<ListProduct>> response) {
                if (response.isSuccessful()) {
                    swipe.setRefreshing(false);
                    list = response.body();
                    if (list.size() > 0) {
                        imgNoItem.setVisibility(View.GONE);
                        tvNoItem.setVisibility(View.GONE);
                        rv.setVisibility(View.VISIBLE);

                        adapter2 = new ListProdukAdapterIndex(list, getApplicationContext());
                        rv.setAdapter(adapter2);

                        adapter2.notifyDataSetChanged();

                        adapter2.setLoadMoreListener(new ListProdukAdapterIndex.OnLoadMoreListener() {
                            @Override
                            public void onLoadMore() {
                                rv.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        indexLoad = indexLoad + 1;
                                        loadMore(indexLoad);
                                    }
                                });
                            }
                        });
                    } else {
                        imgNoItem.setVisibility(View.VISIBLE);
                        tvNoItem.setVisibility(View.VISIBLE);
                        rv.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ListProduct>> call, Throwable t) {
                swipe.setRefreshing(false);
                Toast.makeText(LihatProductActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadMoreFilter(int index) {
        list.add(new ListProduct("mencari"));
        adapter2.notifyItemInserted(list.size() - 1);

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getBarangByKondisi(getIntent().getStringExtra("id"),
                pilihan,
                index).enqueue(new Callback<ArrayList<ListProduct>>() {
            @Override
            public void onResponse(Call<ArrayList<ListProduct>> call, Response<ArrayList<ListProduct>> response) {
                if (response.isSuccessful()) {
                    //remove loading view
                    list.remove(list.size() - 1);

                    ArrayList<ListProduct> result = response.body();
                    ArrayList<String> listNama = new ArrayList<>();
                    if (result.size() > 0) {
                        //add loaded data
                        list.addAll(result);
                    } else {//result size 0 means there is no more data available at server
                        adapter2.setMoreDataAvailable(false);
                        //telling adapter to stop calling load more as no more server data available
//                        Toast.makeText(context, "No More Data Available", Toast.LENGTH_SHORT).show();
                    }
                    adapter2.notifyDataChanged();
                } else {
                    Log.e(TAG, " Load More Response Error " + String.valueOf(response.code()));
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ListProduct>> call, Throwable t) {
                Log.e(TAG, " Load More Response Error " + t.getMessage());
            }
        });
    }

    private void loadMore(int index) {
        list.add(new ListProduct("mencari"));
        adapter2.notifyItemInserted(list.size() - 1);

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getProductByCatagori(getIntent().getStringExtra("id"),
                index).enqueue(new Callback<ArrayList<ListProduct>>() {
            @Override
            public void onResponse(Call<ArrayList<ListProduct>> call, Response<ArrayList<ListProduct>> response) {
                if (response.isSuccessful()) {
                    //remove loading view
                    list.remove(list.size() - 1);

                    ArrayList<ListProduct> result = response.body();
                    ArrayList<String> listNama = new ArrayList<>();
                    if (result.size() > 0) {
                        //add loaded data
                        list.addAll(result);
                    } else {//result size 0 means there is no more data available at server
                        adapter2.setMoreDataAvailable(false);
                        //telling adapter to stop calling load more as no more server data available
//                        Toast.makeText(context, "No More Data Available", Toast.LENGTH_SHORT).show();
                    }
                    adapter2.notifyDataChanged();
                } else {
                    Log.e(TAG, " Load More Response Error " + String.valueOf(response.code()));
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ListProduct>> call, Throwable t) {
                Log.e(TAG, " Load More Response Error " + t.getMessage());
            }
        });
    }

    private void loadMinMax(int index){
        list.add(new ListProduct("mencari"));
        adapter2.notifyItemInserted(list.size() - 1);

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.filterBarangKat(getIntent().getStringExtra("id"),
                min,max,
                index).enqueue(new Callback<ArrayList<ListProduct>>() {
            @Override
            public void onResponse(Call<ArrayList<ListProduct>> call, Response<ArrayList<ListProduct>> response) {
                if (response.isSuccessful()) {
                    //remove loading view
                    list.remove(list.size() - 1);

                    ArrayList<ListProduct> result = response.body();
                    ArrayList<String> listNama = new ArrayList<>();
                    if (result.size() > 0) {
                        //add loaded data
                        list.addAll(result);
                    } else {//result size 0 means there is no more data available at server
                        adapter2.setMoreDataAvailable(false);
                        //telling adapter to stop calling load more as no more server data available
//                        Toast.makeText(context, "No More Data Available", Toast.LENGTH_SHORT).show();
                    }
                    adapter2.notifyDataChanged();
                } else {
                    Log.e(TAG, " Load More Response Error " + String.valueOf(response.code()));
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ListProduct>> call, Throwable t) {
                Log.e(TAG, " Load More Response Error " + t.getMessage());
            }
        });
    }

    private void loadKota(int index){
        list.add(new ListProduct("mencari"));
        adapter2.notifyItemInserted(list.size() - 1);

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.filterKotaBarangKat(getIntent().getStringExtra("id"),
                idKota,
                index).enqueue(new Callback<ArrayList<ListProduct>>() {
            @Override
            public void onResponse(Call<ArrayList<ListProduct>> call, Response<ArrayList<ListProduct>> response) {
                if (response.isSuccessful()) {
                    //remove loading view
                    list.remove(list.size() - 1);

                    ArrayList<ListProduct> result = response.body();
                    ArrayList<String> listNama = new ArrayList<>();
                    if (result.size() > 0) {
                        //add loaded data
                        list.addAll(result);
                    } else {//result size 0 means there is no more data available at server
                        adapter2.setMoreDataAvailable(false);
                        //telling adapter to stop calling load more as no more server data available
//                        Toast.makeText(context, "No More Data Available", Toast.LENGTH_SHORT).show();
                    }
                    adapter2.notifyDataChanged();
                } else {
                    Log.e(TAG, " Load More Response Error " + String.valueOf(response.code()));
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ListProduct>> call, Throwable t) {
                Log.e(TAG, " Load More Response Error " + t.getMessage());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    private void initView() {
        rv = (RecyclerView) findViewById(R.id.rv);
        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe);
        imgNoItem = (ImageView) findViewById(R.id.img_no_item);
        tvNoItem = (TextView) findViewById(R.id.tv_no_item);
        edtSearch = (EditText) findViewById(R.id.edt_search);
        spnFilter = (Spinner) findViewById(R.id.spn_filter);
//        lyUtama = (CoordinatorLayout) findViewById(R.id.ly_utama);
        lyDikirim = (LinearLayout) findViewById(R.id.ly_dikirim);
        lyFilter = (LinearLayout) findViewById(R.id.ly_filter);
        lyUrutkan = (LinearLayout) findViewById(R.id.ly_urutkan);
        bottomSheet = (LinearLayout) findViewById(R.id.bottom_sheet);
        imgClose = (ImageView) findViewById(R.id.img_close);
    }
}
