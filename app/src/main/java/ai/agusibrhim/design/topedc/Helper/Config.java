package ai.agusibrhim.design.topedc.Helper;

/**
 * Created by cis on 09/05/2018.
 */

public final class Config {

    public static final String URL_IMAGE ="http://inewsplnkalselteng.info/gambar/";
    public static final String URL_POPUP = "http://inewsplnkalselteng.info/popup_image/";
    public static final String URL_COVER_VIDEO ="http://inewsplnkalselteng.info/video_cover/";
    public static final String TIPE_DATA = "video";
//    public static final int NOTIFICATION_ID = 100;

    public static final String TOPIC_GLOBAL = "inews";

    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";

    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

    public static final String SHARED_PREF = "ah_firebase";

    public static final String API = "vxnoew9cs98dasydhcnoql0cancoid";
    public static final String IMAGE_BARANG = "http://pinshop.can.co.id/images/img_barang/";
    public static final String IMAGE_TOKO = "http://pinshop.can.co.id/images/img_toko/";
    public static final String IMAGE_SUB_KAT = "http://pinshop.can.co.id/images/img_sub_kat/";
    public static final String IMAGE_Promo = "http://pinshop.can.co.id/images/img_slider/";
    public static final String IMAGE_PROFIL = "http://pinshop.can.co.id/images/img_user/";
    public static final String IMAGE_REKENING = "http://pinshop.can.co.id/images/img_rekening/";
    public static final String IMAGE_ISI_CHAT = "http://pinshop.can.co.id/images/img_video_chat/";
}
