package ai.agusibrhim.design.topedc.Activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TambahPromoActivity extends AppCompatActivity {

    private EditText edtNama;
    private EditText edtKode;
    private EditText edtPotongan;
    private EditText edtKadaluarsa;
    private Button btnTambah;
    SharedPref sharedPref;
    DatePickerDialog datePickerDialog;
    int mYear;
    int mMonth;
    int mDay;
    private EditText edtMin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_promo);
        initView();

        sharedPref = new SharedPref(this);
        edtKadaluarsa.setFocusable(false);

        btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tambah();
            }
        });

        edtKadaluarsa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                settingTanggal();
                datePickerDialog.show();
            }
        });
    }

    private void clear() {
        edtNama.setText("");
        edtPotongan.setText("");
        edtKode.setText("");
        edtKadaluarsa.setText("");
        edtNama.requestFocus();
    }

    private void settingTanggal() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        datePickerDialog = new
                DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                mYear = year;
                mMonth = month;
                mDay = dayOfMonth;

                GregorianCalendar c = new GregorianCalendar(mYear, mMonth, mDay);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                edtKadaluarsa.setText(sdf.format(c.getTime()));

                edtKadaluarsa.setFocusable(false);
                edtKadaluarsa.setCursorVisible(false);
                //edtTanggal.setText(tanggal);

            }
        }, year, month, day);
    }

    private void tambah() {
        if (!validasi()) {
            return;
        }
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Sending data...");
        pd.setCancelable(false);
        pd.show();
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.addPromo(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                edtNama.getText().toString(),
                edtKode.getText().toString(),
                edtPotongan.getText().toString(),
                edtKadaluarsa.getText().toString(),
                edtMin.getText().toString(),
                "").enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String pesan = jsonObject.optString("message");
                        if (pesan.equals("Berhasil ditambahkan")) {
                            Toast.makeText(TambahPromoActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                            clear();
                        } else {
                            Toast.makeText(TambahPromoActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(TambahPromoActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean validasi() {
        if (edtNama.getText().toString().isEmpty()) {
            edtNama.setError("Harus diisi");
            edtNama.requestFocus();

            return false;
        }
        if (edtKode.getText().toString().isEmpty()) {
            edtKode.setError("Harus diisi");
            edtKode.requestFocus();

            return false;
        }
        if (edtPotongan.getText().toString().isEmpty()) {
            edtPotongan.setError("Harus diisi");
            edtPotongan.requestFocus();

            return false;
        }
        if (edtKadaluarsa.getText().toString().isEmpty()) {
            edtKadaluarsa.setError("Harus diisi");
            edtKadaluarsa.requestFocus();

            return false;
        }

        return true;
    }

    private void initView() {
        edtNama = (EditText) findViewById(R.id.edt_nama);
        edtKode = (EditText) findViewById(R.id.edt_kode);
        edtPotongan = (EditText) findViewById(R.id.edt_potongan);
        edtKadaluarsa = (EditText) findViewById(R.id.edt_kadaluarsa);
        btnTambah = (Button) findViewById(R.id.btn_tambah);
        edtMin = (EditText) findViewById(R.id.edt_min);
    }
}
