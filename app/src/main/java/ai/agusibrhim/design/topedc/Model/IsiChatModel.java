package ai.agusibrhim.design.topedc.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IsiChatModel implements Parcelable {
    @SerializedName("ID_CHAT")
    @Expose
    private String iDCHAT;
    @SerializedName("ID_USER_FROM")
    @Expose
    private String iDUSERFROM;
    @SerializedName("ID_USER_TO")
    @Expose
    private String iDUSERTO;
    @SerializedName("CH_LASTVIEW_USR_FROM")
    @Expose
    private String cHLASTVIEWUSRFROM;
    @SerializedName("CH_LASTVIEW_USR_TO")
    @Expose
    private String cHLASTVIEWUSRTO;
    @SerializedName("CH_STATUS_VIEW")
    @Expose
    private String cHSTATUSVIEW;
    @SerializedName("CH_IMAGE")
    @Expose
    private String cHIMAGE;
    @SerializedName("CH_VIDEO")
    @Expose
    private String cHVIDEO;
    @SerializedName("CH_MESSAGE")
    @Expose
    private String cHMESSAGE;
    @SerializedName("CH_STATUS_REMOVE")
    @Expose
    private String cHSTATUSREMOVE;
    @SerializedName("CH_REMOVE_TIME")
    @Expose
    private String cHREMOVETIME;
    @SerializedName("CREATED_AT")
    @Expose
    private String cREATEDAT;
    @SerializedName("UPDATED_AT")
    @Expose
    private String uPDATEDAT;
    @SerializedName("USD_FOTO")
    @Expose
    private String uSDFOTO;
    @SerializedName("USD_FULLNAME")
    @Expose
    private String uSDFULLNAME;
    @SerializedName("USD_TOKO_FOTO")
    @Expose
    private String uSDTOKOFOTO;
    @SerializedName("USD_TOKO")
    @Expose
    private String uSDTOKO;
    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("message")
    @Expose
    private String message;

    protected IsiChatModel(Parcel in) {
        iDCHAT = in.readString();
        iDUSERFROM = in.readString();
        iDUSERTO = in.readString();
        cHLASTVIEWUSRFROM = in.readString();
        cHLASTVIEWUSRTO = in.readString();
        cHSTATUSVIEW = in.readString();
        cHIMAGE = in.readString();
        cHVIDEO = in.readString();
        cHMESSAGE = in.readString();
        cHSTATUSREMOVE = in.readString();
        cHREMOVETIME = in.readString();
        cREATEDAT = in.readString();
        uPDATEDAT = in.readString();
        uSDFOTO = in.readString();
        uSDFULLNAME = in.readString();
        uSDTOKOFOTO = in.readString();
        uSDTOKO = in.readString();
        success = in.readString();
        message = in.readString();
    }

    public static final Creator<IsiChatModel> CREATOR = new Creator<IsiChatModel>() {
        @Override
        public IsiChatModel createFromParcel(Parcel in) {
            return new IsiChatModel(in);
        }

        @Override
        public IsiChatModel[] newArray(int size) {
            return new IsiChatModel[size];
        }
    };

    public String getIDCHAT() {
        return iDCHAT;
    }

    public void setIDCHAT(String iDCHAT) {
        this.iDCHAT = iDCHAT;
    }

    public String getIDUSERFROM() {
        return iDUSERFROM;
    }

    public void setIDUSERFROM(String iDUSERFROM) {
        this.iDUSERFROM = iDUSERFROM;
    }

    public String getIDUSERTO() {
        return iDUSERTO;
    }

    public void setIDUSERTO(String iDUSERTO) {
        this.iDUSERTO = iDUSERTO;
    }

    public String getCHLASTVIEWUSRFROM() {
        return cHLASTVIEWUSRFROM;
    }

    public void setCHLASTVIEWUSRFROM(String cHLASTVIEWUSRFROM) {
        this.cHLASTVIEWUSRFROM = cHLASTVIEWUSRFROM;
    }

    public String getCHLASTVIEWUSRTO() {
        return cHLASTVIEWUSRTO;
    }

    public void setCHLASTVIEWUSRTO(String cHLASTVIEWUSRTO) {
        this.cHLASTVIEWUSRTO = cHLASTVIEWUSRTO;
    }

    public String getCHSTATUSVIEW() {
        return cHSTATUSVIEW;
    }

    public void setCHSTATUSVIEW(String cHSTATUSVIEW) {
        this.cHSTATUSVIEW = cHSTATUSVIEW;
    }

    public String getCHIMAGE() {
        return cHIMAGE;
    }

    public void setCHIMAGE(String cHIMAGE) {
        this.cHIMAGE = cHIMAGE;
    }

    public String getCHVIDEO() {
        return cHVIDEO;
    }

    public void setCHVIDEO(String cHVIDEO) {
        this.cHVIDEO = cHVIDEO;
    }

    public String getCHMESSAGE() {
        return cHMESSAGE;
    }

    public void setCHMESSAGE(String cHMESSAGE) {
        this.cHMESSAGE = cHMESSAGE;
    }

    public String getCHSTATUSREMOVE() {
        return cHSTATUSREMOVE;
    }

    public void setCHSTATUSREMOVE(String cHSTATUSREMOVE) {
        this.cHSTATUSREMOVE = cHSTATUSREMOVE;
    }

    public String getCHREMOVETIME() {
        return cHREMOVETIME;
    }

    public void setCHREMOVETIME(String cHREMOVETIME) {
        this.cHREMOVETIME = cHREMOVETIME;
    }

    public String getCREATEDAT() {
        return cREATEDAT;
    }

    public void setCREATEDAT(String cREATEDAT) {
        this.cREATEDAT = cREATEDAT;
    }

    public String getUPDATEDAT() {
        return uPDATEDAT;
    }

    public void setUPDATEDAT(String uPDATEDAT) {
        this.uPDATEDAT = uPDATEDAT;
    }

    public String getUSDFOTO() {
        return uSDFOTO;
    }

    public void setUSDFOTO(String uSDFOTO) {
        this.uSDFOTO = uSDFOTO;
    }

    public String getUSDFULLNAME() {
        return uSDFULLNAME;
    }

    public void setUSDFULLNAME(String uSDFULLNAME) {
        this.uSDFULLNAME = uSDFULLNAME;
    }

    public String getUSDTOKOFOTO() {
        return uSDTOKOFOTO;
    }

    public void setUSDTOKOFOTO(String uSDTOKOFOTO) {
        this.uSDTOKOFOTO = uSDTOKOFOTO;
    }

    public String getUSDTOKO() {
        return uSDTOKO;
    }

    public void setUSDTOKO(String uSDTOKO) {
        this.uSDTOKO = uSDTOKO;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(iDCHAT);
        parcel.writeString(iDUSERFROM);
        parcel.writeString(iDUSERTO);
        parcel.writeString(cHLASTVIEWUSRFROM);
        parcel.writeString(cHLASTVIEWUSRTO);
        parcel.writeString(cHSTATUSVIEW);
        parcel.writeString(cHIMAGE);
        parcel.writeString(cHVIDEO);
        parcel.writeString(cHMESSAGE);
        parcel.writeString(cHSTATUSREMOVE);
        parcel.writeString(cHREMOVETIME);
        parcel.writeString(cREATEDAT);
        parcel.writeString(uPDATEDAT);
        parcel.writeString(uSDFOTO);
        parcel.writeString(uSDFULLNAME);
        parcel.writeString(uSDTOKOFOTO);
        parcel.writeString(uSDTOKO);
        parcel.writeString(success);
        parcel.writeString(message);
    }
}
