package ai.agusibrhim.design.topedc.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.fxn.pix.Pix;
import com.fxn.utility.PermUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ai.agusibrhim.design.topedc.Adapter.CatagoryAdapter;
import ai.agusibrhim.design.topedc.Adapter.PromoAdapter;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.MainActivity;
import ai.agusibrhim.design.topedc.Model.PromoAktif;
import ai.agusibrhim.design.topedc.Model.SubKategoriModel;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TambahBarangActivity extends AppCompatActivity {

    private EditText edtNama;
    private Spinner spnKategori;
    private ImageView imgProduk;
    private EditText edtHarga;
    private EditText edtDeskripsi;
    private EditText edtSku;
    private EditText edtStok;
    private EditText edtBerat;
    private EditText edtKondisi;
    private Button btnTambah;
    int idKategori;
    SharedPref sharedPref;
    List<String> imagesEncodedList;
    ArrayList<String> listFoto = new ArrayList<>();
    private String h, imagePath;
    List<File> imageFile = new ArrayList<>();
    File fileImages;
    File imageFileSatu, imageFileDua, imageFileTiga;
    CatagoryAdapter catagoryAdapter;
    ArrayList<SubKategoriModel> list = new ArrayList<>();
    ArrayList<String> listKategori = new ArrayList<>();
    ArrayList<String> idListKategori = new ArrayList<>();
    private ImageView imgProduk2;
    private ImageView imgProduk3;

    private Spinner spnHargaPin;
    String pilih;
    ArrayList<String> listHarga = new ArrayList<>();
    ArrayList<String> listKodePromo = new ArrayList<>();
    PromoAdapter mAdapter;

    ArrayList<PromoAktif> listPromo = new ArrayList<>();
    private Spinner spnPromo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_barang);
        initView();

        awal();

        imgProduk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (imagesEncodedList != null) {
//                    imagesEncodedList.removeAll(imagesEncodedList);
//                }
//                Pix.start(TambahBarangActivity.this,                    //Activity or Fragment Instance
//                        100, 100);
                pilih = "satu";
                EasyImage.openChooserWithGallery(TambahBarangActivity.this, "chose", 3);

            }
        });

        imgProduk2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilih = "dua";
                EasyImage.openChooserWithGallery(TambahBarangActivity.this, "chose", 3);
            }
        });

        imgProduk3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilih = "tiga";
                EasyImage.openChooserWithGallery(TambahBarangActivity.this, "chose", 3);
            }
        });

        btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tambahBarang();
            }
        });

        edtKondisi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                kondisi();
            }
        });

    }

    private void awal() {
        edtKondisi.setFocusable(false);
        sharedPref = new SharedPref(this);
        spnHargaPin.setVisibility(View.GONE);
        getKategori();
        getHargaPin();
//        getPromo();
    }

    private void getHargaPin() {
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getHargaPin(sharedPref.getIdUser(),
                sharedPref.getSESSION()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        JSONArray jsonArray = new JSONArray(response.body().string());
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.optJSONObject(i);
                            String harga = jsonObject.optString("PP_PRICE");

                            listHarga.add(harga);
                            ArrayAdapter<String> adp2 = new ArrayAdapter<String>(TambahBarangActivity.this, R.layout.support_simple_spinner_dropdown_item, listHarga);
                            spnHargaPin.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
                            spnHargaPin.setAdapter(adp2);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(TambahBarangActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getKategori() {
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getAllSubKat().enqueue(new Callback<ArrayList<SubKategoriModel>>() {
            @Override
            public void onResponse(Call<ArrayList<SubKategoriModel>> call, Response<ArrayList<SubKategoriModel>> response) {
                if (response.isSuccessful()) {
                    list = response.body();

                    for (int i = 0; i < list.size(); i++) {
                        listKategori.add(list.get(i).getSKNAME());
                        idListKategori.add(list.get(i).getIDSUBKATEGORI());
                    }

                    ArrayAdapter<String> adp2 = new ArrayAdapter<String>(TambahBarangActivity.this, R.layout.support_simple_spinner_dropdown_item, listKategori);
                    spnKategori.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
                    spnKategori.setAdapter(adp2);

                    spnKategori.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            int idKat = spnKategori.getSelectedItemPosition();
                            idKategori = Integer.parseInt(idListKategori.get(idKat));
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<ArrayList<SubKategoriModel>> call, Throwable t) {
                Toast.makeText(TambahBarangActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void kondisi() {
        final CharSequence[] dialogItem = {"NEW", "SECOND"};
        AlertDialog.Builder builder = new AlertDialog.Builder(TambahBarangActivity.this);
        builder.setTitle("Tentukan Pilihan Anda");
        builder.setItems(dialogItem, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                switch (i) {
                    case 0:

                        edtKondisi.setText("NEW");
                        break;
                    case 1:

                        edtKondisi.setText("SECOND");
                        break;
                }
            }
        });
        builder.create().show();
    }

    private void tambahBarang() {
        if (imageFileSatu != null){
            imageFile.add(imageFileSatu);
        }
        if (imageFileDua != null){
            imageFile.add(imageFileDua);
        }
        if (imageFileTiga != null){
            imageFile.add(imageFileTiga);
        }

        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Proses ...");
        pd.setCancelable(false);
        pd.show();

        MultipartBody.Part[] surveyImagesParts = new MultipartBody.Part[imageFile.size()];
        for (int index = 0; index < imageFile.size(); index++) {

//            File file = new File(imageFile.get(index));
            RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), imageFile.get(index));
            surveyImagesParts[index] = MultipartBody.Part.createFormData("image[]", imageFile.get(index).getName(), surveyBody);
        }

        RequestBody id_kategori = RequestBody.create(MediaType.parse("text/plain"), "" + idKategori);
//        RequestBody idPromo = RequestBody.create(MediaType.parse("text/plain"), "" + spnPromo.getSelectedItem().toString());
//        RequestBody caption = RequestBody.create(MediaType.parse("text/plain"), listCaption);
        final RequestBody nama = RequestBody.create(MediaType.parse("text/plain"), edtNama.getText().toString());
        RequestBody harga = RequestBody.create(MediaType.parse("text/plain"), edtHarga.getText().toString());
//        RequestBody harga_pin = RequestBody.create(MediaType.parse("text/plain"), spnHargaPin.getSelectedItem().toString());
        RequestBody sku = RequestBody.create(MediaType.parse("text/plain"), edtSku.getText().toString());
        RequestBody deskripsi = RequestBody.create(MediaType.parse("text/plain"), edtDeskripsi.getText().toString());
        RequestBody stok = RequestBody.create(MediaType.parse("text/plain"), edtStok.getText().toString());
        RequestBody berat = RequestBody.create(MediaType.parse("text/plain"), edtBerat.getText().toString());
        RequestBody kondisi = RequestBody.create(MediaType.parse("text/plain"), edtKondisi.getText().toString());

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.tambahBarang(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                id_kategori,
//                idPromo,
                nama,
                harga,
//                harga_pin,
                sku,
                deskripsi,
                stok,
                berat,
                kondisi,
                surveyImagesParts).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String pesan = jsonObject.optString("message");
                        if (pesan.equals("Berhasil ditambahkan")) {
                            new AlertDialog.Builder(TambahBarangActivity.this)
                                    .setMessage("" + pesan)
                                    .setCancelable(false)
                                    .setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            startActivity(new Intent(getApplicationContext(), MainActivity.class)
                                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                            finish();
                                        }
                                    })
                                    .show();
                        } else {
                            Toast.makeText(TambahBarangActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(TambahBarangActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getPromo() {
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getPromoAktif(sharedPref.getIdUser()).enqueue(new Callback<ArrayList<PromoAktif>>() {
            @Override
            public void onResponse(Call<ArrayList<PromoAktif>> call, Response<ArrayList<PromoAktif>> response) {
                if (response.isSuccessful()) {

                    listPromo = response.body();
                    if (listPromo.get(0).getMessage().equals("Berhasil Diload")) {
                        for (int i = 0; i < listPromo.size(); i++) {
                            listKodePromo.add(listPromo.get(i).getPRCODE());
                        }

                        ArrayAdapter<String> adp2 = new ArrayAdapter<String>(TambahBarangActivity.this, R.layout.support_simple_spinner_dropdown_item, listKodePromo);
                        spnPromo.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
                        spnPromo.setAdapter(adp2);
                    } else {

                        final List<String> list2 = new ArrayList<String>();
                        list2.add("Tidak ada promo");

                        ArrayAdapter<String> adp2 = new ArrayAdapter<String>(TambahBarangActivity.this, R.layout.support_simple_spinner_dropdown_item, list2);
                        spnPromo.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
                        spnPromo.setAdapter(adp2);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<PromoAktif>> call, Throwable t) {

                Toast.makeText(TambahBarangActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    //gallery
    private void showFileChooserGalerry() {
        Intent qq = new Intent(Intent.ACTION_PICK);
        qq.setType("image/*");
        startActivityForResult(Intent.createChooser(qq, "Pilih Foto"), 100);
    }

    // TODO multi kaya wa
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (resultCode == Activity.RESULT_OK && requestCode == 100) {
//            ArrayList<String> returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);
////            Toast.makeText(this, ""+returnValue.size(), Toast.LENGTH_SHORT).show();
//            ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
//            imagesEncodedList = new ArrayList<>();
//            for (int i = 0; i < returnValue.size(); i++) {
//
//
////                imagePath = returnValue.get(i);
//                imagesEncodedList.add(returnValue.get(i));
//                listFoto.add(returnValue.get(i));
//                Glide.with(this).load(new File(imagesEncodedList.get(0))).into(imgProduk);
//
//                h = new File(imagesEncodedList.get(0)).getName();
//
//                Toast.makeText(this, "" + imagesEncodedList.size(), Toast.LENGTH_SHORT).show();
//
//                imageFile = imagesEncodedList;
//
//
//            }
//        } else {
//            Toast.makeText(this, "Gambar Tidak Ada", Toast.LENGTH_SHORT).show();
//        }
//    }

//
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
//            if (data == null) {
//                Toast.makeText(this, "Gambar Tidak Ada", Toast.LENGTH_SHORT).show();
//                return;
//
//            }
////            else {
////                Toast.makeText(this, "Gambar Ada", Toast.LENGTH_SHORT).show();
////            }
//            Uri selectImageUri = data.getData();
//            String[] filePathColumn = {MediaStore.Images.Media.DATA};
//
//            Cursor c = getContentResolver().query(selectImageUri, filePathColumn, null, null, null);
//            if (c != null) {
//                c.moveToFirst();
//
//                int columnIndex = c.getColumnIndex(filePathColumn[0]);
//                imagePath = c.getString(columnIndex);
//
////                Glide.with(this).load(new File(imagePath)).into(cvEditProfil);
//                Glide.with(this).load(new File(imagePath)).into(imgProduk);
//                h = new File(imagePath).getName();
////                uploadImage();i
////                Toast.makeText(this, "Mbuh", Toast.LENGTH_SHORT).show();
//                c.close();
//                 fileImages = new File(imagePath);
//
////                editFoto(imageFile);
//
////                te.setVisibility(View.GONE);
////                imageVi.setVisibility(View.VISIBLE);
//            } else {
////                te.setVisibility(View.VISIBLE);
////                imageVi.setVisibility(View.GONE);
//                Toast.makeText(this, "Gambar Tidak Ada", Toast.LENGTH_SHORT).show();
//            }
//        }
//    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                //Some error handling
                Toast.makeText(TambahBarangActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onImagePicked(File image, EasyImage.ImageSource source, int type) {
                if (pilih.equals("satu")){
                    Glide.with(TambahBarangActivity.this)
                            .load(new File(image.getPath()))
                            .into(imgProduk);
                    imageFileSatu = new File(image.getPath());
//                    imageFile.add(imageFileSatu);
                }else if (pilih.equals("dua")){
                    Glide.with(TambahBarangActivity.this)
                            .load(new File(image.getPath()))
                            .into(imgProduk2);
                    imageFileDua = new File(image.getPath());
//                    imageFile.add(imageFileDua);
                }else if (pilih.equals("tiga")){
                    Glide.with(TambahBarangActivity.this)
                            .load(new File(image.getPath()))
                            .into(imgProduk3);
                    imageFileTiga = new File(image.getPath());
//                    imageFile.add(imageFileTiga);
                }
            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
                super.onCanceled(source, type);
            }
        });
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PermUtil.REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Pix.start(TambahBarangActivity.this, 100);
                } else {
                    Toast.makeText(TambahBarangActivity.this, "Approve permissions to open Pix ImagePicker", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

    private void initView() {
        edtNama = (EditText) findViewById(R.id.edt_nama);
        spnKategori = (Spinner) findViewById(R.id.spn_kategori);
        imgProduk = (ImageView) findViewById(R.id.img_produk);
        edtHarga = (EditText) findViewById(R.id.edt_harga);
        edtDeskripsi = (EditText) findViewById(R.id.edt_deskripsi);
        edtSku = (EditText) findViewById(R.id.edt_sku);
        edtStok = (EditText) findViewById(R.id.edt_stok);
        edtBerat = (EditText) findViewById(R.id.edt_berat);
        edtKondisi = (EditText) findViewById(R.id.edt_kondisi);
        btnTambah = (Button) findViewById(R.id.btn_tambah);
        imgProduk2 = (ImageView) findViewById(R.id.img_produk2);
        imgProduk3 = (ImageView) findViewById(R.id.img_produk3);

        spnHargaPin = (Spinner) findViewById(R.id.spn_harga_pin);
        spnPromo = (Spinner) findViewById(R.id.spn_promo);
    }
}
