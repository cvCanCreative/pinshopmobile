package ai.agusibrhim.design.topedc.Fragment;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import ai.agusibrhim.design.topedc.Activity.TokoKitaActivity;
import ai.agusibrhim.design.topedc.Helper.Config;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class DataTokoFragment extends Fragment {


    private TextView tvAlamatToko;
    private TextView tvNoToko;
    private TextView tvEmailToko;
    String data;
    SharedPref sharedPref;
    String id;

    @SuppressLint("ValidFragment")
    public static  DataTokoFragment newInstance (String data,String id) {
        Bundle args = new Bundle();

        args.putString("id", id);
        args.putString("data",data);

        DataTokoFragment f = new DataTokoFragment();
        f.setArguments(args);
        return f;
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_data_toko, container, false);
        initView(view);

        awal();

        return view;
    }

    private void awal(){
        sharedPref = new SharedPref(getActivity());
        if (getArguments().getString("data").equals("tamu")){
            getDataTamu();
        }else {
            getData();
        }
    }

    private void getDataTamu() {
        final ProgressDialog pd = new ProgressDialog(getActivity());
        pd.setTitle("Mohon Tunggu ...");
        pd.setCancelable(false);
        pd.show();
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getProfilById(getArguments().getString("id")).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONArray jsonArray = new JSONArray(response.body().string());
                        JSONObject jsonObject = jsonArray.optJSONObject(0);
                        String alamat = jsonObject.optString("USD_ADDRESS");
                        String kota = jsonObject.optString("USD_KOTA");
                        String namaToko = jsonObject.optString("USD_TOKO");
                        String foto = jsonObject.optString("USD_TOKO_FOTO");
                        String email = jsonObject.optString("US_EMAIL");
                        String no = jsonObject.optString("US_TELP");

                        tvAlamatToko.setText(alamat + " " + kota);
                        tvNoToko.setText("" + no);
                        tvEmailToko.setText("" + email);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getData() {
        final ProgressDialog pd = new ProgressDialog(getActivity());
        pd.setTitle("Mohon Tunggu ...");
        pd.setCancelable(false);
        pd.show();
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getProfil(sharedPref.getIdUser(), sharedPref.getSESSION()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONArray jsonArray = new JSONArray(response.body().string());
                        JSONObject jsonObject = jsonArray.optJSONObject(0);
                        String alamat = jsonObject.optString("USD_ADDRESS");
                        String kota = jsonObject.optString("USD_KOTA");
                        String namaToko = jsonObject.optString("USD_TOKO");
                        String foto = jsonObject.optString("USD_TOKO_FOTO");
                        String email = jsonObject.optString("US_EMAIL");
                        String no = jsonObject.optString("US_TELP");

                        tvAlamatToko.setText(alamat + " " + kota);
                        tvNoToko.setText("" + no);
                        tvEmailToko.setText("" + email);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initView(View view) {
        tvAlamatToko = (TextView) view.findViewById(R.id.tv_alamat_toko);
        tvNoToko = (TextView) view.findViewById(R.id.tv_no_toko);
        tvEmailToko = (TextView) view.findViewById(R.id.tv_email_toko);
    }
}
