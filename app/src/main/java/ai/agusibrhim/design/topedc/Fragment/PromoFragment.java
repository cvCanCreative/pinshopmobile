package ai.agusibrhim.design.topedc.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;

import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Helper.Config;
import ai.agusibrhim.design.topedc.Model.PromoModel;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class PromoFragment extends Fragment implements ViewPagerEx.OnPageChangeListener, BaseSliderView.OnSliderClickListener {

    private SliderLayout sliderSlider;
    ArrayList<PromoModel> list = new ArrayList<>();

    public PromoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_promo, container, false);
        initView(view);

        getPromo();

        return view;
    }

    private void getPromo(){
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getPromo().enqueue(new Callback<ArrayList<PromoModel>>() {
            @Override
            public void onResponse(Call<ArrayList<PromoModel>> call, Response<ArrayList<PromoModel>> response) {
                if (response.isSuccessful()){
                    list = response.body();
                    for (PromoModel img : list) {
                        if (!img.getSLFILE().isEmpty()){
                            TextSliderView textSliderView = new TextSliderView(getContext());
                            textSliderView.image(Config.IMAGE_Promo + img.getSLFILE())
                                    .setScaleType(BaseSliderView.ScaleType.Fit).setOnSliderClickListener(PromoFragment.this);;
                            textSliderView.bundle(new Bundle());
                            textSliderView.getBundle().putString("img_path", img.getSLFILE());
                            textSliderView.getBundle().putString("nama_promo", img.getSLTITLE());
//                            textSliderView.getBundle().putString("detail_promo", img.getTpKeterangan());
                            sliderSlider.addSlider(textSliderView);
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<PromoModel>> call, Throwable t) {
                Toast.makeText(getActivity(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initView(View view) {
        sliderSlider = (SliderLayout) view.findViewById(R.id.sliderSlider);
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
