package ai.agusibrhim.design.topedc.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Model.FeedbackModel;
import ai.agusibrhim.design.topedc.R;

public class FeedBackAdapter extends RecyclerView.Adapter<FeedBackAdapter.Holder> {

    ArrayList<FeedbackModel> list;
    Context context;

    public FeedBackAdapter(ArrayList<FeedbackModel> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_fedd_back,parent,false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        holder.tvNamaAsli.setText(list.get(position).getUSDFULLNAME());
        holder.tvNama.setText(list.get(position).getCREATEDAT());
        holder.tvKomentar.setText(list.get(position).getFECOMMENT());
        holder.rtBar.setRating(Float.parseFloat(list.get(position).getFERATING()));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Holder extends RecyclerView.ViewHolder{

        TextView tvKomentar,tvNama,tvNamaAsli;
        AppCompatRatingBar rtBar;
        public Holder(View itemView) {
            super(itemView);

            tvNamaAsli = itemView.findViewById(R.id.tv_nama_asli);
            tvNama = itemView.findViewById(R.id.tv_nama);
            tvKomentar = itemView.findViewById(R.id.tv_komentar);
            rtBar = itemView.findViewById(R.id.rt_bar);
        }
    }
}
