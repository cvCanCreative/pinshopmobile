package ai.agusibrhim.design.topedc.Adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Activity.AddKomplainActivity;
import ai.agusibrhim.design.topedc.Activity.DetailTransaksiActivity;
import ai.agusibrhim.design.topedc.Activity.HistoryTransaksiActivity;
import ai.agusibrhim.design.topedc.Activity.RatingActivity;
import ai.agusibrhim.design.topedc.Helper.Config;
import ai.agusibrhim.design.topedc.Helper.HelperClass;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.Model.DetailTransaksiModel;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailTransaksiAdapter extends RecyclerView.Adapter<DetailTransaksiAdapter.Holder>{

    ArrayList<DetailTransaksiModel> list;
    Context context;
    String status;
    String status2;
    String data;
    SharedPref sharedPref;

    public DetailTransaksiAdapter(ArrayList<DetailTransaksiModel> list, Context context, String status,String data) {
        this.list = list;
        this.context = context;
        this.status = status;
        this.data = data;

    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_detail_transaksi,parent,false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, final int position) {
        holder.tvNama.setText(list.get(position).getBANAME());
        holder.tvJumlah.setText(list.get(position).getTSDQTY());
        int harga = Integer.valueOf(list.get(position).getBAPRICE());
        int total = harga * Integer.valueOf(list.get(position).getTSDQTY());
        holder.tvTotal.setText(new HelperClass().convertRupiah(total));

        String gambar = list.get(position).getBAIMAGE();
        String gb = gambar.replace("|", " ");
        String strArray[] = gb.split(" ");

        Picasso.with(context)
                .load(Config.IMAGE_BARANG + strArray[0])
                .into(holder.imgBarang);

        if (data.equals("toko")){
            holder.btnUlas.setVisibility(View.GONE);
            holder.btnKomplain.setVisibility(View.GONE);
            holder.btnTerima.setVisibility(View.GONE);
        }
        else if (list.get(position).getTSSTATUS().equals("PROSES_KIRIM")){

            holder.btnTerima.setText("TERIMA BARANG");
            holder.btnUlas.setVisibility(View.GONE);
            holder.btnKomplain.setVisibility(View.VISIBLE);
            holder.btnTerima.setVisibility(View.VISIBLE);
            status2 = "TERIMA";

        }else if (list.get(position).equals("TERIMA") ){
             status2 = "TERIMA_USR";
            holder.btnTerima.setText("YAKIN TERIMA");

            holder.btnUlas.setVisibility(View.GONE);
            holder.btnKomplain.setVisibility(View.VISIBLE);
            holder.btnTerima.setVisibility(View.VISIBLE);

        }else if (list.get(position).equals("TERIMA_USR")){

              holder.btnUlas.setVisibility(View.VISIBLE);
              holder.btnKomplain.setVisibility(View.GONE);
              holder.btnTerima.setVisibility(View.GONE);
        }else {
            holder.btnKomplain.setVisibility(View.GONE);
            holder.btnTerima.setVisibility(View.GONE);
            holder.btnUlas.setVisibility(View.GONE);
        }

        holder.btnUlas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, RatingActivity.class);
                intent.putExtra("id",list.get(position).getIDTRANSAKSIDETAIL());
                intent.putExtra("nama",list.get(position).getBANAME());
                intent.putExtra("image",list.get(position).getBAIMAGE());
                context.startActivity(intent);
            }
        });

        holder.btnTerima.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(context)
                        .setMessage("Dengan ini Anda Menyatakan Barang diterima, apakah anda yakin ?")
                        .setCancelable(false)
                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                terima(position,status2);
                            }
                        })
                        .setNegativeButton("Tidak", null)
                        .show();
            }
        });

        holder.btnKomplain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, AddKomplainActivity.class);
                intent.putExtra("id",list.get(position).getIDTRANSAKSI());
                context.startActivity(intent);
            }
        });
    }

    private void terima(Integer posisi,String statusku){
        sharedPref = new SharedPref(context);
        final ProgressDialog pd = new ProgressDialog(context);
        pd.setTitle("Proses ...");
        pd.setCancelable(false);
        pd.show();

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.terimaPesanan(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                list.get(posisi).getIDTRANSAKSI(),
                ""+statusku).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String kode = jsonObject.optString("success");
                        String pesan = jsonObject.optString("message");
                        if (kode.equals("1")){
                            Intent intent = new Intent(context, HistoryTransaksiActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("data", "adapter");
                            context.startActivity(intent);
                        }else {
                            Toast.makeText(context, ""+pesan, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(context, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Holder extends RecyclerView.ViewHolder{

        TextView tvNama,tvJumlah,tvTotal;
        ImageView imgBarang;
        LinearLayout btnUlas;
        Button btnKomplain,btnTerima;
        public Holder(View itemView) {
            super(itemView);

            btnUlas = itemView.findViewById(R.id.btn_ulas);
            tvNama = itemView.findViewById(R.id.tv_nama);
            tvJumlah = itemView.findViewById(R.id.tv_jumlah);
            tvTotal = itemView.findViewById(R.id.tv_total_harga);
            imgBarang = itemView.findViewById(R.id.img_barang);
            btnKomplain = itemView.findViewById(R.id.btn_komplain);
            btnTerima = itemView.findViewById(R.id.btn_terima);
        }
    }
}
