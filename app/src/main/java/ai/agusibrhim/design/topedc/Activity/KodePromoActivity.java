package ai.agusibrhim.design.topedc.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ai.agusibrhim.design.topedc.Adapter.PromoTokoAdapter;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.Model.AlamatModel;
import ai.agusibrhim.design.topedc.Model.PromoTokoModel;
import ai.agusibrhim.design.topedc.Model.VoucherModel;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KodePromoActivity extends AppCompatActivity {

    private LinearLayout ly;
    private ImageView imgClose;
    private LinearLayout lyVoucher;
    private EditText edtVoucher;
    private Button btnGunakan;
    private RecyclerView rv;
    private ImageView imgNo;
    SharedPref pref;
    RecyclerView.LayoutManager layoutManager;
    PromoTokoAdapter adapter;
    List<PromoTokoModel> list = new ArrayList<>();
    ArrayList<String> idToko = new ArrayList<>();
    String total;
    ArrayList<AlamatModel> listAlamat = new ArrayList<>();
    int posisi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kode_promo);
        initView();

        pref = new SharedPref(this);

        idToko = getIntent().getStringArrayListExtra("id");
        total = getIntent().getStringExtra("total");

        listAlamat = getIntent().getParcelableArrayListExtra("listAlamat");
        posisi = Integer.parseInt(getIntent().getStringExtra("posisiAlamat"));
//        Toast.makeText(this, "tot "+total, Toast.LENGTH_SHORT).show();


        layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        rv.setLayoutManager(layoutManager);

        getData();

        btnGunakan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edtVoucher.getText().toString().isEmpty()){
                    Toast.makeText(KodePromoActivity.this, "Masukkan Kode Voucher", Toast.LENGTH_SHORT).show();
                }else {
                    cekVoucher();
                }
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void cekVoucher(){
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Loading ...");
        pd.setCancelable(false);
        pd.show();

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.cekVoucher2(pref.getIdUser(),
                pref.getSESSION(),
                idToko,
                edtVoucher.getText().toString(),
                total).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    pd.dismiss();
                    try {
                        JSONArray jsonArray = new JSONArray(response.body().string());
                        JSONObject jsonObject = jsonArray.optJSONObject(0);
                        String pesan = jsonObject.optString("message");
                        if (pesan.equals("Kode Promo valid")){
                            Toast.makeText(KodePromoActivity.this, ""+pesan, Toast.LENGTH_SHORT).show();
                            String potongan = jsonObject.optString("PR_POTONGAN");
                            String tipe = jsonObject.optString("PR_JENIS");
                            String kodeVoucher = jsonObject.optString("PR_CODE");

                            pref.savePrefString(SharedPref.POTONGAN_VOUCHER,potongan);
                            pref.savePrefString(SharedPref.TIPE_VOUCHER,tipe);
                            pref.savePrefString(SharedPref.KODE_VOUCHER,kodeVoucher);

                            Intent intent = new Intent(KodePromoActivity.this,PembayaranActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putParcelableArrayListExtra("list",listAlamat);
                            intent.putExtra("posisi",""+posisi);
                            startActivity(intent);
                            finish();
                        }else {
                            Toast.makeText(KodePromoActivity.this, ""+pesan, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(KodePromoActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getData(){
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Loading ...");
        pd.setCancelable(false);
        pd.show();

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.daftarPromo(pref.getIdUser(),
                pref.getSESSION(),
                idToko).enqueue(new Callback<ArrayList<PromoTokoModel>>() {
            @Override
            public void onResponse(Call<ArrayList<PromoTokoModel>> call, Response<ArrayList<PromoTokoModel>> response) {
                if (response.isSuccessful()){
                    pd.dismiss();
                    list = response.body();
                    if (list.size()==0){
                        rv.setVisibility(View.GONE);
                        imgNo.setVisibility(View.VISIBLE);
                    }else {
                        rv.setVisibility(View.VISIBLE);
                        imgNo.setVisibility(View.GONE);
                        adapter = new PromoTokoAdapter(getApplicationContext(),list);
                        rv.setAdapter(adapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<PromoTokoModel>> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(KodePromoActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initView() {
        ly = (LinearLayout) findViewById(R.id.ly);
        imgClose = (ImageView) findViewById(R.id.img_close);
        lyVoucher = (LinearLayout) findViewById(R.id.ly_voucher);
        edtVoucher = (EditText) findViewById(R.id.edt_voucher);
        btnGunakan = (Button) findViewById(R.id.btn_gunakan);
        rv = (RecyclerView) findViewById(R.id.rv);
        imgNo = (ImageView) findViewById(R.id.img_no);
    }
}
