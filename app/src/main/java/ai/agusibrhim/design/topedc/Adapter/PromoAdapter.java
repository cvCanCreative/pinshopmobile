package ai.agusibrhim.design.topedc.Adapter;

import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Activity.EditPromoActivity;
import ai.agusibrhim.design.topedc.Activity.PromoActivity;
import ai.agusibrhim.design.topedc.Activity.TokoKitaActivity;
import ai.agusibrhim.design.topedc.Helper.HelperClass;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.MainActivity;
import ai.agusibrhim.design.topedc.Model.PromoAktif;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.CLIPBOARD_SERVICE;

public class PromoAdapter extends RecyclerView.Adapter<PromoAdapter.Holder> {

    ArrayList<PromoAktif> list;
    Context context;
    String data;
    SharedPref sharedPref;

    public PromoAdapter(ArrayList<PromoAktif> list, Context context, String data) {
        this.list = list;
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_promo,parent,false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final Holder holder, int position) {
        holder.tvKode.setText(""+list.get(position).getPRCODE());
        holder.tvJenis.setText("Jenis Promo " +list.get(position).getPRJENIS());
        holder.tvNama.setText(""+list.get(position).getPRNAME());
        holder.tvPotongan.setText(new HelperClass().convertRupiah(Integer.parseInt(list.get(position).getPRPOTONGAN())));
        holder.tvExpired.setText("Berlaku Sampai : " + new HelperClass().convertDateFormat(list.get(position).getPREXPIRED()));
        holder.btnMember.setText(list.get(position).getpRMEMBER());
        holder.tvMinTrans.setText(new HelperClass().convertRupiah(Integer.parseInt(list.get(position).getpRMIN())));
        holder.tvSalin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClipboardManager clipboard = (ClipboardManager) context.getSystemService(CLIPBOARD_SERVICE);
                String getstring = holder.tvKode.getText().toString();
                Toast.makeText(context, "Kode disalin", Toast.LENGTH_SHORT).show();
                ClipData clip = ClipData.newPlainText("", getstring);
                clipboard.setPrimaryClip(clip);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Holder extends RecyclerView.ViewHolder{

        TextView tvJenis,tvNama,tvKode,tvPotongan,tvExpired,tvSalin,tvMinTrans;
        Button btnMember;
//        ImageView imgCopy;
        public Holder(View itemView) {
            super(itemView);

            tvSalin = itemView.findViewById(R.id.tv_salin);
            tvExpired = itemView.findViewById(R.id.tv_expired);
            tvPotongan = itemView.findViewById(R.id.tv_potongan);
            tvJenis = itemView.findViewById(R.id.tv_jenis);
            tvNama = itemView.findViewById(R.id.tv_nama);
            tvKode = itemView.findViewById(R.id.tv_kode);
            btnMember = itemView.findViewById(R.id.btn_member);
            tvMinTrans = itemView.findViewById(R.id.tv_min_trans);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    if (data.equals("tamu")){

                    }else {
                        sharedPref = new SharedPref(view.getContext());
                        final CharSequence[] dialogItem = {"Edit", "Hapus"};
                        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                        builder.setTitle("Tentukan Pilihan Anda");
                        builder.setItems(dialogItem, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                switch (i) {
                                    case 0:
                                        Intent intent = new Intent(context, EditPromoActivity.class);
                                        intent.putExtra("list",list.get(getAdapterPosition()));
                                        context.startActivity(intent);
                                        break;
                                    case 1:
                                        final ProgressDialog pd = new ProgressDialog(view.getContext());
                                        pd.setTitle("Proses ...");
                                        pd.setCancelable(false);
                                        pd.show();
                                        ApiService apiService = ApiConfig.getInstanceRetrofit();
                                        apiService.deletePromo(sharedPref.getIdUser(),
                                                sharedPref.getSESSION(),
                                                list.get(getAdapterPosition()).getIDPROMO()).enqueue(new Callback<ResponseBody>() {
                                            @Override
                                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                                if (response.isSuccessful()){
                                                    pd.dismiss();
                                                    try {
                                                        JSONObject jsonObject =new JSONObject(response.body().string());
                                                        String pesan = jsonObject.optString("message");
                                                        if (pesan.equals("Berhasil delete")){
                                                            Toast.makeText(view.getContext(), ""+pesan, Toast.LENGTH_SHORT).show();
                                                            Intent intent = new Intent(view.getContext(), TokoKitaActivity.class)
                                                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                            intent.putExtra("data", "sendiri");
                                                            context.startActivity(intent);
                                                        }else {
                                                            Toast.makeText(view.getContext(), ""+pesan, Toast.LENGTH_SHORT).show();
                                                        }
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    } catch (IOException e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                                pd.dismiss();
                                                Toast.makeText(view.getContext(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                        break;
                                }
                            }
                        });
                        builder.create().show();
                    }
                }
            });
        }
    }
}
