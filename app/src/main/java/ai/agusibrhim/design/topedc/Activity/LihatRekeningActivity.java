package ai.agusibrhim.design.topedc.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Activity.AccountActivity;
import ai.agusibrhim.design.topedc.Activity.BuktiTfSaldoActivity;
import ai.agusibrhim.design.topedc.Activity.HomeLikeShopeeActivity;
import ai.agusibrhim.design.topedc.Activity.PengaturanAkunActivity;
import ai.agusibrhim.design.topedc.Activity.RekeningActivity;
import ai.agusibrhim.design.topedc.Activity.WidthdrawActivity;
import ai.agusibrhim.design.topedc.Helper.Config;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.MainActivity;
import ai.agusibrhim.design.topedc.Model.RekeningByIdModel;
import ai.agusibrhim.design.topedc.Model.ResponseModel;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LihatRekeningActivity extends AppCompatActivity {

    private SwipeRefreshLayout swipe;
    private LinearLayout div;
    SharedPref sharedPref;
    ArrayList<RekeningByIdModel> list = new ArrayList<>();
    private FloatingActionButton tvTambah;
    String data;
    String jumlah;
    private ImageView imgClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lihat_rekening);
        initView();

        awal();

        tvTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), RekeningActivity.class);
                intent.putExtra("data", "tambah");
                startActivity(intent);
            }
        });
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                ambil(data, true);
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (data.equals("tambah")){
                    Intent intent = new Intent(getApplicationContext(),PengaturanAkunActivity.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    finish();
                }else {
                    Intent intent = new Intent(getApplicationContext(),HomeLikeShopeeActivity.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("data","profil");
                    finish();
                }

            }
        });
    }

    private void awal() {
        sharedPref = new SharedPref(this);
        data = getIntent().getStringExtra("data");
        if (data.equals("tf")) {
            jumlah = getIntent().getStringExtra("jumlah");
        } else {

        }

        ambil(data, true);
    }

    private void ambil(final String dataku, boolean rm) {
        if (rm) {
            if (div.getChildCount() > 0) div.removeAllViews();
        }
        swipe.setRefreshing(true);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getRekeningById(sharedPref.getIdUser(),
                sharedPref.getSESSION()).enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                if (response.isSuccessful()){
                    swipe.setRefreshing(false);
                    list = response.body().rekening;
                    if (response.body().getSuccess() == 1) {
                        for (int i = 0; i < list.size(); i++) {
                            LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            View view = layoutInflater.inflate(R.layout.item_bank_user, null);
                            TextView tvNama = view.findViewById(R.id.bankuser_tv_bank);
                            TextView tvNomor = view.findViewById(R.id.bankuser_tv_norek);
                            ImageView imgBank = view.findViewById(R.id.img_bank);
                            LinearLayout layout = view.findViewById(R.id.bankuser_div_main);

                            final String idRek = list.get(i).ID_REKENING;
                            final String nama = list.get(i).BK_NAME;
                            final String no_rek = list.get(i).RK_NOMOR;
                            final String nama_rek = list.get(i).RK_NAME;

                            tvNama.setText(nama);
                            tvNomor.setText(nama_rek + " - " + no_rek);

                            Picasso.with(getApplicationContext())
                                    .load(Config.IMAGE_REKENING + list.get(i).RK_IMAGE)
                                    .into(imgBank);

                            final int posisi = i;
                            layout.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    if (dataku.equals("tarik")) {

                                        new AlertDialog.Builder(LihatRekeningActivity.this)
                                                .setMessage("Terima Uang direkening ini ?")
                                                .setCancelable(false)
                                                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {

                                                        terima(idRek,nama,no_rek,dataku);

                                                    }
                                                })
                                                .setNegativeButton("Tidak", null)
                                                .show();

                                    } else if (dataku.equals("tambah")) {

                                        final CharSequence[] dialogItem = {"Hapus", "Edit"};
                                        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                                        builder.setTitle("Tentukan Pilihan Anda");
                                        builder.setItems(dialogItem, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                switch (i) {
                                                    case 0:
                                                        new AlertDialog.Builder(LihatRekeningActivity.this)
                                                                .setMessage("Apakah anda akan menghapus rekening ini ?")
                                                                .setCancelable(false)
                                                                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                                                    public void onClick(DialogInterface dialog, int id) {
                                                                        hapusRekening(list.get(posisi).ID_REKENING);
                                                                    }
                                                                })
                                                                .setNegativeButton("Tidak", null)
                                                                .show();
                                                        break;
                                                    case 1:

                                                        edit(posisi);

                                                        break;
                                                }
                                            }
                                        });
                                        builder.create().show();

                                    } else {

                                        new AlertDialog.Builder(LihatRekeningActivity.this)
                                                .setMessage("Transfer dengan rekening ini ?")
                                                .setCancelable(false)
                                                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {

                                                        transfer(no_rek,nama,nama_rek);

                                                    }
                                                })
                                                .setNegativeButton("Tidak", null)
                                                .show();
                                    }
                                }
                            });

                            div.addView(view);
                        }
                    } else {
                        Toast.makeText(LihatRekeningActivity.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                swipe.setRefreshing(false);

                new android.app.AlertDialog.Builder(LihatRekeningActivity.this)
                        .setMessage("" + t.getMessage())
                        .setCancelable(false)
                        .setPositiveButton("Ulangi", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                ambil(data,true);
                            }
                        })
                        .show();
            }
        });
    }

    private void hapusRekening(final String idRek){
        final ProgressDialog pd = new ProgressDialog(LihatRekeningActivity.this);
        pd.setCancelable(false);
        pd.setTitle("Proses ...");
        pd.show();
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.hapusRekening(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                idRek).enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                if (response.isSuccessful()){
                    pd.dismiss();
                    if (response.body().getSuccess() == 1){

                    }else {
                        Toast.makeText(LihatRekeningActivity.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                pd.dismiss();
                new android.app.AlertDialog.Builder(LihatRekeningActivity.this)
                        .setMessage("" + t.getMessage())
                        .setCancelable(false)
                        .setPositiveButton("Ulangi", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                hapusRekening(idRek);
                            }
                        })
                        .show();
            }
        });
    }

    private void edit(int posisi){
        Intent intent = new Intent(getApplicationContext(), RekeningActivity.class);
        intent.putExtra("data", "edit");
        intent.putExtra("list", list.get(posisi));
        startActivity(intent);
    }

    private void terima(String idRek,String nama,String no_rek,String dataku){
        Intent intent = new Intent(getApplicationContext(), WidthdrawActivity.class);
        intent.putExtra("id", "" + idRek);
        intent.putExtra("nama", "" + nama);
        intent.putExtra("no", "" + no_rek);
        intent.putExtra("data",""+dataku);
        startActivity(intent);
    }

    private void transfer(String no_rek,String nama,String nama_rek){
        Intent intent = new Intent(getApplicationContext(), BuktiTfSaldoActivity.class);
        intent.putExtra("rekening", "" + no_rek);
        intent.putExtra("bank", "" + nama);
        intent.putExtra("nama", "" + nama_rek);
        intent.putExtra("jumlah", "" + jumlah);
        intent.putExtra("kode",getIntent().getStringExtra("kode"));
        intent.putExtra("bankpin",""+getIntent().getStringExtra("bankpin"));
        intent.putExtra("rekpin",""+getIntent().getStringExtra("rekpin"));
        intent.putExtra("imgpin",""+getIntent().getStringExtra("imgpin"));
        intent.putExtra("data", "tf");
        startActivity(intent);
    }

    private void initView() {
        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe);
        div = (LinearLayout) findViewById(R.id.div);
        tvTambah = (FloatingActionButton) findViewById(R.id.tv_tambah);
        imgClose = (ImageView) findViewById(R.id.img_close);
    }
}
