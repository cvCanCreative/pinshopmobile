package ai.agusibrhim.design.topedc.Fragment;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Activity.CekPengirimanActivity;
import ai.agusibrhim.design.topedc.Activity.DetailTransaksiActivity;
import ai.agusibrhim.design.topedc.Adapter.HistoryTransaksiAdapter;
import ai.agusibrhim.design.topedc.Helper.Config;
import ai.agusibrhim.design.topedc.Helper.HelperClass;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.Model.HistoryTransaksi;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class TransaksiTokoFragment extends Fragment {


    private SwipeRefreshLayout swipe;
    private TextView tvKet;

    SharedPref sharedPref;
    String data;
    RecyclerView.LayoutManager layoutManager;
    HistoryTransaksiAdapter mAdapter;
    ArrayList<HistoryTransaksi> list = new ArrayList<>();
    private ImageView imgNo;
    private LinearLayout div;
    private LinearLayout lyKosong;
    String statusku, st;

    @SuppressLint("ValidFragment")
    public TransaksiTokoFragment(String data) {
        this.data = data;
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_transaksi_toko, container, false);
        initView(view);

        awal();

        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData(true);
            }
        });

        return view;
    }

    private void awal() {
//        Toast.makeText(getActivity(), ""+data, Toast.LENGTH_SHORT).show();
        sharedPref = new SharedPref(getActivity());

        getData(true);
    }

    private void getData(boolean rm) {

        if (rm){
            if (div.getChildCount() > 0)div.removeAllViews();
        }

        swipe.setRefreshing(true);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.HistoryTransToko(sharedPref.getIdUser(),
                sharedPref.getSESSION(), data).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    swipe.setRefreshing(false);

                }
                try {
                    JSONArray jsonArray = new JSONArray(response.body().string());
                    JSONObject object = jsonArray.optJSONObject(0);
                    String pesan = object.optString("message");

                    if (pesan.equals("Berhasil")) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject dataObject = jsonArray.optJSONObject(i);
                            if (getActivity()!= null){
                                LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                View view = layoutInflater.inflate(R.layout.row_transaksi, null);

                                LinearLayout rv = (LinearLayout) view.findViewById(R.id.rv);
                                LinearLayout lyBayar = (LinearLayout) view.findViewById(R.id.ly_bayar);
                                Button btnBayar = (Button) view.findViewById(R.id.btn_bayar);
                                Button btnKomplain = (Button) view.findViewById(R.id.btn_komplain);
                                Button btnTerima = (Button) view.findViewById(R.id.btn_terima);
                                TextView tvTotal = (TextView) view.findViewById(R.id.tv_total);

                                final String kodePayment = dataObject.optString("kode_payment");
                                final String jumlahTf = dataObject.optString("jumlah_tf");
                                final String BankPin = dataObject.optString("bank_pin");
                                final String RekPin = dataObject.optString("rek_pin");
                                final String imgBank = dataObject.optString("img_bank");
                                String totalBayar = dataObject.optString("total_pay");


                                JSONArray dataArray = dataObject.optJSONArray("data");

                                for (int j = 0; j < dataArray.length(); j++) {
                                    LayoutInflater layoutInflater1 = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                                    JSONObject jsonObject = dataArray.optJSONObject(j);

                                    View view1 = layoutInflater1.inflate(R.layout.row_his_trans, null);

                                    CircleImageView imgToko = (CircleImageView) view1.findViewById(R.id.img_toko);
                                    TextView tvNamaToko = (TextView) view1.findViewById(R.id.tv_nama_toko);
                                    TextView tvStatus = (TextView) view1.findViewById(R.id.tv_status);
                                    ImageView imgBarang = (ImageView) view1.findViewById(R.id.img_barang);
                                    TextView tvNamaBarang = (TextView) view1.findViewById(R.id.tv_nama_barang);
                                    TextView tvJumlahBeli = (TextView) view1.findViewById(R.id.tv_jumlah_beli);
                                    TextView tvHargaBarang = (TextView) view1.findViewById(R.id.tv_harga_barang);

                                    TextView tvLihatPengiriman = (TextView) view1.findViewById(R.id.tv_lihat_pengiriman);
                                    LinearLayout ly = (LinearLayout)view1.findViewById(R.id.ly_trans);

                                    final String status = jsonObject.optString("TS_STATUS");
                                    String jumlah = jsonObject.optString("TSD_QTY");
                                    String hargaAsli = jsonObject.optString("TSD_HARGA_ASLI");
//                                String total = jsonObject.optString("TS_TOTAL_BAYAR");
                                    String fotoToko = jsonObject.optString("USD_TOKO_FOTO");
                                    String namaToko = jsonObject.optString("USD_TOKO");
                                    String fotoBarang = jsonObject.optString("BA_IMAGE");
                                    final String resi = jsonObject.optString("TS_RESI");
                                    final String kurir = jsonObject.optString("TS_SLUG_KURIR");
                                    final String idTrans = jsonObject.optString("ID_TRANSAKSI");

                                    final String hargaBarang = jsonObject.optString("TSD_HARGA_ASLI");
                                    final String jenisKurir = jsonObject.optString("TSD_JENIS_KURIR");
                                    final String kodeUnik = jsonObject.optString("TS_KODE_UNIK");
                                    final String potongan = jsonObject.optString("ID_TRANSAKSI");
                                    final String jenisPromo = jsonObject.optString("TS_JNS_PROMO");
                                    final String kodePromo = jsonObject.optString("TS_KODE_PROMO");

//                                final String kodePayment = jsonObject.optString("TS_KODE_PAYMENT");
                                    final String metodeBayar = jsonObject.optString("TS_METODE_BAYAR");
                                    final String waktuPesan = jsonObject.optString("CREATED_AT");

                                    lyBayar.setVisibility(View.GONE);
                                    btnTerima.setVisibility(View.GONE);
                                    btnKomplain.setVisibility(View.GONE);
                                    btnBayar.setVisibility(View.GONE);

                                    if (status.equals("PROSES_KIRIM")) {
                                        statusku = "TERIMA";

                                    } else if (status.equals("WAITING_TF")) {

                                    } else if (status.equals("TERIMA")) {
                                        statusku = "TERIMA_USR";

                                    } else {

                                    }

                                    st = status;
                                    if (st.equals("WAITING_TF")) {
                                        st = "Menunggu Transfer";
                                    } else if (st.equals("PROSES_ADMIN")) {
                                        st = "Menunggu Admin";
                                    } else if (st.equals("PROSES_VENDOR")) {
                                        st = "Diproses Toko";
                                    } else if (st.equals("PROSES_KIRIM")) {
                                        st = "Dikirim";
                                    } else if (st.equals("TERIMA_USR")) {
                                        st = "Diterima Oleh Pelanggan";
                                    } else if (st.equals("TERIMA")) {
                                        st = "Diterima";
                                    } else {
                                        st = status;
                                    }

                                    tvStatus.setText(st);
                                    tvJumlahBeli.setText("x " + jumlah);
                                    tvHargaBarang.setText(""+new HelperClass().convertRupiah(Integer.parseInt(hargaAsli)));

                                    if (fotoToko.equals("")) {
                                        imgToko.setImageResource(R.drawable.no_image_found);
                                    } else {
                                        Picasso.with(getActivity())
                                                .load(Config.IMAGE_TOKO + fotoToko)
                                                .error(R.drawable.no_image_found)
                                                .into(imgToko);
                                    }
                                    tvNamaToko.setText("" + namaToko);

                                    String gambar = fotoBarang;
                                    String gb = new HelperClass().splitText(gambar);

                                    Picasso.with(getActivity())
                                            .load(Config.IMAGE_BARANG + gb)
                                            .error(R.drawable.no_image_found)
                                            .into(imgBarang);

                                    tvLihatPengiriman.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Intent intent = new Intent(getActivity(), CekPengirimanActivity.class);
                                            intent.putExtra("kurir", kurir);
                                            intent.putExtra("resi", resi);
                                            intent.putExtra("id", idTrans);
                                            startActivity(intent);
                                        }
                                    });

                                    ly.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Intent intent = new Intent(getActivity(), DetailTransaksiActivity.class);
                                            intent.putExtra("id",idTrans);
                                            intent.putExtra("status",st);
                                            intent.putExtra("kurir",kurir);
                                            intent.putExtra("resi",resi);
                                            intent.putExtra("metodebayar",metodeBayar);
                                            intent.putExtra("waktupesan",waktuPesan);
                                            intent.putExtra("status2",status);
                                            intent.putExtra("harga",""+hargaBarang);
                                            intent.putExtra("jeniskurir",""+jenisKurir);
                                            intent.putExtra("pot",""+potongan);
                                            intent.putExtra("jenispromo",""+jenisPromo);
                                            intent.putExtra("kodepromo",""+kodePromo);
                                            intent.putExtra("kodeunik",""+kodeUnik);
                                            intent.putExtra("data", data);
                                            startActivity(intent);
                                        }
                                    });

                                    rv.addView(view1);
                                }

                                tvTotal.setText(""+new HelperClass().convertRupiah(Integer.parseInt(totalBayar)));

                                div.addView(view);
                            }
                        }

                    } else {
                        tvKet.setVisibility(View.VISIBLE);
                        div.setVisibility(View.GONE);
                        lyKosong.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                swipe.setRefreshing(false);
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initView(View view) {
        swipe = (SwipeRefreshLayout) view.findViewById(R.id.swipe);
        tvKet = (TextView) view.findViewById(R.id.tv_ket);

        imgNo = (ImageView) view.findViewById(R.id.img_no);
        div = (LinearLayout) view.findViewById(R.id.div);
        lyKosong = (LinearLayout) view.findViewById(R.id.ly_kosong);
    }
}
