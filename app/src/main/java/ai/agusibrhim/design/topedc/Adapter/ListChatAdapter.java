package ai.agusibrhim.design.topedc.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Activity.ChatActivity;
import ai.agusibrhim.design.topedc.Activity.ListChatActivity;
import ai.agusibrhim.design.topedc.Helper.Config;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.Model.AddFirebase;
import ai.agusibrhim.design.topedc.Model.ChatModel;
import ai.agusibrhim.design.topedc.Model.ListChatModel;
import ai.agusibrhim.design.topedc.R;

public class ListChatAdapter extends RecyclerView.Adapter<ListChatAdapter.Holder> {

    Context context;
    ArrayList<ListChatModel> list;
    SharedPref sharedPref;

    public ListChatAdapter(Context context, ArrayList<ListChatModel> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_list_chat,parent,false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        holder.tvNama.setText(list.get(position).getUSDFULLNAME());
        holder.tvEmail.setText(list.get(position).getUSDTOKO());

        if (list.get(position).getUSDFOTO().equals("")){
            holder.imgAvatar.setImageResource(R.drawable.avatar);
        }else {
            Picasso.with(context)
                    .load(Config.IMAGE_PROFIL + list.get(position).getUSDFOTO())
                    .into(holder.imgAvatar);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Holder extends RecyclerView.ViewHolder{

        TextView tvNama,tvEmail;
        ImageView imgAvatar;
        public Holder(View itemView) {
            super(itemView);

            imgAvatar = itemView.findViewById(R.id.img_avatar);
            tvNama = itemView.findViewById(R.id.tv_nama);
            tvEmail = itemView.findViewById(R.id.tv_email);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    sharedPref = new SharedPref(context);
                    sharedPref.savePrefString(SharedPref.ID_CHAT,""+list.get(getAdapterPosition()).getIDUSERTO());
                    Intent intent = new Intent(context, ChatActivity.class);
                    intent.putExtra("id",list.get(getAdapterPosition()).getIDUSERTO());
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });
        }
    }
}
