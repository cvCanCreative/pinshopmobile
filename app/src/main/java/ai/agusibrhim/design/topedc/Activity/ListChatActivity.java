package ai.agusibrhim.design.topedc.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Adapter.ListChatAdapter;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.MainActivity;
import ai.agusibrhim.design.topedc.Model.ListChatModel;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListChatActivity extends AppCompatActivity {

    private RecyclerView rv;
    ArrayList<ListChatModel> list = new ArrayList<>();
    ListChatAdapter mAdapter;
    RecyclerView.LayoutManager layoutManager;
    SharedPref sharedPref;
    private TextView tvKet;
    private ImageView imgClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_chat);
        initView();

        awal();
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), HomeLikeShopeeActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();
            }
        });
    }

    private void awal() {
        sharedPref = new SharedPref(this);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rv.setLayoutManager(layoutManager);
        if (sharedPref.getSudahLogin()) {
            getData2();
        } else {
            Toast.makeText(this, "Anda Harus Login Terlebih dahulu", Toast.LENGTH_SHORT).show();
        }
    }

    private void getData2() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Loading ...");
        pd.setCancelable(false);
        pd.show();

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getListChat(sharedPref.getIdUser(),
                sharedPref.getSESSION()).enqueue(new Callback<ArrayList<ListChatModel>>() {
            @Override
            public void onResponse(Call<ArrayList<ListChatModel>> call, Response<ArrayList<ListChatModel>> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    list = response.body();
                    if (list.get(0).getSuccess().equals("1")) {
                        mAdapter = new ListChatAdapter(getApplicationContext(), list);
                        rv.setAdapter(mAdapter);
                    } else {
                        tvKet.setVisibility(View.VISIBLE);
                        rv.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ListChatModel>> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(ListChatActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initView() {
        rv = (RecyclerView) findViewById(R.id.rv);
        tvKet = (TextView) findViewById(R.id.tv_ket);
        imgClose = (ImageView) findViewById(R.id.img_close);
    }
}
