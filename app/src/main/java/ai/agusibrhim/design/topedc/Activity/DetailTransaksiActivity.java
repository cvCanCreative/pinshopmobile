package ai.agusibrhim.design.topedc.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Adapter.DetailTransaksiAdapter;
import ai.agusibrhim.design.topedc.Helper.HelperClass;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.Model.DetailTransaksiModel;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailTransaksiActivity extends AppCompatActivity {

    private RecyclerView div;
    RecyclerView.LayoutManager layoutManager;
    DetailTransaksiAdapter mAdapter;
    SharedPref sharedPref;
    ArrayList<DetailTransaksiModel> list = new ArrayList<>();
    private TextView tvKet;
    String status;
    String data;
    private ImageView imgClose;
    private LinearLayout lyUtama;
    private LinearLayout ly;
    private TextView tvStatus;
    private TextView tvKetPesanan;
    private TextView tvWaktuPesan;
    private TextView tvAlamat;
    private TextView tvLihatPengiriman;
    private TextView tvKurir;
    private TextView tvMetodeBayar;
    String id, kurir, waktu, resi, metodeBayar,jenisKurir;
    String status2;
    private TextView tvTotalProduk;
    private TextView tvOngkir;
    private TextView tvTotalSemua;
    private TextView tvHargaBarang;
    private TextView tvJumlahBeli;
    private TextView tvKodeUnik;
    private TextView tvJenisPromo;
    private TextView tvKodePromo;
    private TextView tvNilaiPromo;
    int jumlahBeli =0;
    int Beli =0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_transaksi);
        initView();

        awal();

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), HistoryTransaksiActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("data", "drawer");
                finish();
            }
        });

        tvLihatPengiriman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), CekPengirimanActivity.class);
                intent.putExtra("kurir", kurir);
                intent.putExtra("resi", resi);
                intent.putExtra("id", id);
                startActivity(intent);
            }
        });
    }

    private void awal() {
        sharedPref = new SharedPref(this);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        div.setLayoutManager(layoutManager);
        status = getIntent().getStringExtra("status");
        data = getIntent().getStringExtra("data");

        if (data.equals("toko")) {
            getData2(status);

        } else {
            getData(status);

        }

        id = getIntent().getStringExtra("id");
        kurir = getIntent().getStringExtra("kurir");
        waktu = getIntent().getStringExtra("waktupesan");
        resi = getIntent().getStringExtra("resi");
        jenisKurir = getIntent().getStringExtra("jeniskurir");
        metodeBayar = getIntent().getStringExtra("metodebayar");

        String promo = getIntent().getStringExtra("pot");
        String harga = getIntent().getStringExtra("harga");
        String jenisPromo = getIntent().getStringExtra("jenispromo");
        String kodeUnik = getIntent().getStringExtra("kodeunik");
        String kodePromo = getIntent().getStringExtra("kodepromo");

        tvHargaBarang.setText(new HelperClass().convertRupiah(Integer.parseInt(harga)));
        tvKodeUnik.setText(new HelperClass().convertRupiah(Integer.parseInt(kodeUnik)));
        tvNilaiPromo.setText(new HelperClass().convertRupiah(Integer.parseInt(promo)));
        tvJenisPromo.setText(jenisPromo);
        tvKodePromo.setText(kodePromo);

        status2 = getIntent().getStringExtra("status2");

        if (status2.equals("WAITING_TF")) {
            status2 = "Menunggu Transfer";
        } else if (status2.equals("PROSES_ADMIN")) {
            status2 = "Menunggu Admin";
        } else if (status2.equals("PROSES_VENDOR")) {
            status2 = "Diproses Toko";
        } else if (status2.equals("PROSES_KIRIM")) {
            status2 = "Dikirim";
        } else if (status2.equals("TERIMA_USR")) {
            status2 = "Diterima Oleh Pelanggan";
        } else if (status2.equals("TERIMA")) {
            status2 = "Diterima";
        } else {
            status2 = getIntent().getStringExtra("status2");
        }

        tvStatus.setText("" + status2);
        tvKetPesanan.setText("Pesanan " + status2);
        tvWaktuPesan.setText("Waktu Pesanan " + waktu);
        tvAlamat.setText("Alamat");
        tvKurir.setText("" + kurir.toUpperCase() + " "+ jenisKurir.toUpperCase() + " : " + resi);

        if (metodeBayar.equals("BAYAR_FULL")) {
            tvMetodeBayar.setText("Transfer");
        } else {
            tvMetodeBayar.setText("Saldo");
        }

    }

    private void getData(final String status) {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Loading ...");
        pd.setCancelable(false);
        pd.show();
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getDetailTransaksi(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                getIntent().getStringExtra("id")).enqueue(new Callback<ArrayList<DetailTransaksiModel>>() {
            @Override
            public void onResponse(Call<ArrayList<DetailTransaksiModel>> call, Response<ArrayList<DetailTransaksiModel>> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    list = response.body();
                    if (list.get(0).getSuccess().equals(1)) {
                        lyUtama.setVisibility(View.VISIBLE);
                        tvKet.setVisibility(View.GONE);

                        mAdapter = new DetailTransaksiAdapter(list, getApplicationContext(), status, "" + data);
                        div.setAdapter(mAdapter);

                        tvAlamat.setText("" + list.get(0).getPENAMA() + "\n"
                                + list.get(0).getPETELP() + "\n"
                                + list.get(0).getPEKOTA() + "," + list.get(0).getPEKECAMATAN() + "," + list.get(0).getPEALAMAT() +
                                "," + list.get(0).getPEPROVINSI() + "," + list.get(0).getPEKODEPOS());

                        tvTotalSemua.setText("" + new HelperClass().convertRupiah(Integer.parseInt(list.get(0).getTSTOTALBAYAR())));
                        tvOngkir.setText("" + new HelperClass().convertRupiah(Integer.parseInt(list.get(0).getTSBAYARONGKIR())));
                        tvTotalProduk.setText("" + new HelperClass().convertRupiah(Integer.parseInt(list.get(0).getTSHARGA())));

                        for (int i = 0; i <list.size() ; i++) {
                            Beli = Integer.parseInt(list.get(i).getTSDQTY());

                            jumlahBeli = jumlahBeli + Beli;
                        }

                        tvJumlahBeli.setText(""+jumlahBeli);
                    } else {
                        tvKet.setVisibility(View.VISIBLE);
                        lyUtama.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<DetailTransaksiModel>> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(DetailTransaksiActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getData2(final String status) {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Loading ...");
        pd.setCancelable(false);
        pd.show();
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getDetailTransaksiToko(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                getIntent().getStringExtra("id")).enqueue(new Callback<ArrayList<DetailTransaksiModel>>() {
            @Override
            public void onResponse(Call<ArrayList<DetailTransaksiModel>> call, Response<ArrayList<DetailTransaksiModel>> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    list = response.body();
                    if (list.get(0).getSuccess().equals(1)) {
                        div.setVisibility(View.VISIBLE);
                        tvKet.setVisibility(View.GONE);

                        mAdapter = new DetailTransaksiAdapter(list, getApplicationContext(), status, "" + data);
                        div.setAdapter(mAdapter);

                        tvAlamat.setText("" + list.get(0).getPENAMA() + "\n"
                                + list.get(0).getPETELP() + "\n"
                                + list.get(0).getPEKOTA() + "," + list.get(0).getPEKECAMATAN() + "," + list.get(0).getPEALAMAT() +
                                "," + list.get(0).getPEPROVINSI() + "," + list.get(0).getPEKODEPOS());
                    } else {
                        tvKet.setVisibility(View.VISIBLE);
                        div.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<DetailTransaksiModel>> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(DetailTransaksiActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initView() {
        div = (RecyclerView) findViewById(R.id.div);
        tvKet = (TextView) findViewById(R.id.tv_ket);
        imgClose = (ImageView) findViewById(R.id.img_close);
        lyUtama = (LinearLayout) findViewById(R.id.ly_utama);
        ly = (LinearLayout) findViewById(R.id.ly);
        tvStatus = (TextView) findViewById(R.id.tv_status);
        tvKetPesanan = (TextView) findViewById(R.id.tv_ket_pesanan);
        tvWaktuPesan = (TextView) findViewById(R.id.tv_waktu_pesan);
        tvAlamat = (TextView) findViewById(R.id.tv_alamat);
        tvLihatPengiriman = (TextView) findViewById(R.id.tv_lihat_pengiriman);
        tvKurir = (TextView) findViewById(R.id.tv_kurir);
        tvMetodeBayar = (TextView) findViewById(R.id.tv_metode_bayar);
        tvTotalProduk = (TextView) findViewById(R.id.tv_total_produk);
        tvOngkir = (TextView) findViewById(R.id.tv_ongkir);
        tvTotalSemua = (TextView) findViewById(R.id.tv_total_semua);
        tvHargaBarang = (TextView) findViewById(R.id.tv_harga_barang);
        tvJumlahBeli = (TextView) findViewById(R.id.tv_jumlah_beli);
        tvKodeUnik = (TextView) findViewById(R.id.tv_kode_unik);
        tvJenisPromo = (TextView) findViewById(R.id.tv_jenis_promo);
        tvKodePromo = (TextView) findViewById(R.id.tv_kode_promo);
        tvNilaiPromo = (TextView) findViewById(R.id.tv_nilai_promo);
    }
}
