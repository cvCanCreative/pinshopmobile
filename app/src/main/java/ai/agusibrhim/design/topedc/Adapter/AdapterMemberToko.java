package ai.agusibrhim.design.topedc.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Activity.TokoKitaActivity;
import ai.agusibrhim.design.topedc.Helper.Config;
import ai.agusibrhim.design.topedc.Model.MemberTokoModel;
import ai.agusibrhim.design.topedc.R;

public class AdapterMemberToko extends RecyclerView.Adapter<AdapterMemberToko.Holder>{
    ArrayList<MemberTokoModel> list;
    Context context;

    public AdapterMemberToko(ArrayList<MemberTokoModel> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_member,parent,false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        holder.nama.setText(""+list.get(position).getUSDTOKO());

        holder.username.setVisibility(View.VISIBLE);
        holder.img.setVisibility(View.VISIBLE);

        holder.username.setText(""+list.get(position).getUSDFULLNAME());
        Glide.with(context)
                .load(Config.IMAGE_PROFIL + list.get(position).getUSDFOTO())
                .into(holder.img);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Holder extends RecyclerView.ViewHolder{

        TextView nama,username;
        ImageView img;

        public Holder(View itemView) {
            super(itemView);

            username = itemView.findViewById(R.id.tv_username);
            img = itemView.findViewById(R.id.img_avatar);
            nama = itemView.findViewById(R.id.tv_nama);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, TokoKitaActivity.class);
                    intent.putExtra("data","tamu");
                    intent.putExtra("id",list.get(getAdapterPosition()).getIDUSER());
                    context.startActivity(intent);
                }
            });
        }
    }
}
