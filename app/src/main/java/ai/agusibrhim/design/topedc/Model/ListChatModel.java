package ai.agusibrhim.design.topedc.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListChatModel implements Parcelable {
    @SerializedName("ID_CHAT_LIST")
    @Expose
    private String iDCHATLIST;
    @SerializedName("ID_USER_FROM")
    @Expose
    private String iDUSERFROM;
    @SerializedName("ID_USER_TO")
    @Expose
    private String iDUSERTO;
    @SerializedName("CREATED_AT")
    @Expose
    private String cREATEDAT;
    @SerializedName("UPDATED_AT")
    @Expose
    private String uPDATEDAT;
    @SerializedName("USD_FOTO")
    @Expose
    private String uSDFOTO;
    @SerializedName("USD_FULLNAME")
    @Expose
    private String uSDFULLNAME;
    @SerializedName("USD_TOKO_FOTO")
    @Expose
    private String uSDTOKOFOTO;
    @SerializedName("USD_TOKO")
    @Expose
    private String uSDTOKO;
    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("message")
    @Expose
    private String message;

    protected ListChatModel(Parcel in) {
        iDCHATLIST = in.readString();
        iDUSERFROM = in.readString();
        iDUSERTO = in.readString();
        cREATEDAT = in.readString();
        uPDATEDAT = in.readString();
        uSDFOTO = in.readString();
        uSDFULLNAME = in.readString();
        uSDTOKOFOTO = in.readString();
        uSDTOKO = in.readString();
        success = in.readString();
        message = in.readString();
    }

    public static final Creator<ListChatModel> CREATOR = new Creator<ListChatModel>() {
        @Override
        public ListChatModel createFromParcel(Parcel in) {
            return new ListChatModel(in);
        }

        @Override
        public ListChatModel[] newArray(int size) {
            return new ListChatModel[size];
        }
    };

    public String getIDCHATLIST() {
        return iDCHATLIST;
    }

    public void setIDCHATLIST(String iDCHATLIST) {
        this.iDCHATLIST = iDCHATLIST;
    }

    public String getIDUSERFROM() {
        return iDUSERFROM;
    }

    public void setIDUSERFROM(String iDUSERFROM) {
        this.iDUSERFROM = iDUSERFROM;
    }

    public String getIDUSERTO() {
        return iDUSERTO;
    }

    public void setIDUSERTO(String iDUSERTO) {
        this.iDUSERTO = iDUSERTO;
    }

    public String getCREATEDAT() {
        return cREATEDAT;
    }

    public void setCREATEDAT(String cREATEDAT) {
        this.cREATEDAT = cREATEDAT;
    }

    public String getUPDATEDAT() {
        return uPDATEDAT;
    }

    public void setUPDATEDAT(String uPDATEDAT) {
        this.uPDATEDAT = uPDATEDAT;
    }

    public String getUSDFOTO() {
        return uSDFOTO;
    }

    public void setUSDFOTO(String uSDFOTO) {
        this.uSDFOTO = uSDFOTO;
    }

    public String getUSDFULLNAME() {
        return uSDFULLNAME;
    }

    public void setUSDFULLNAME(String uSDFULLNAME) {
        this.uSDFULLNAME = uSDFULLNAME;
    }

    public String getUSDTOKOFOTO() {
        return uSDTOKOFOTO;
    }

    public void setUSDTOKOFOTO(String uSDTOKOFOTO) {
        this.uSDTOKOFOTO = uSDTOKOFOTO;
    }

    public String getUSDTOKO() {
        return uSDTOKO;
    }

    public void setUSDTOKO(String uSDTOKO) {
        this.uSDTOKO = uSDTOKO;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(iDCHATLIST);
        parcel.writeString(iDUSERFROM);
        parcel.writeString(iDUSERTO);
        parcel.writeString(cREATEDAT);
        parcel.writeString(uPDATEDAT);
        parcel.writeString(uSDFOTO);
        parcel.writeString(uSDFULLNAME);
        parcel.writeString(uSDTOKOFOTO);
        parcel.writeString(uSDTOKO);
        parcel.writeString(success);
        parcel.writeString(message);
    }
}
