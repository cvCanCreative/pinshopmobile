package ai.agusibrhim.design.topedc.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import ai.agusibrhim.design.topedc.Adapter.IsiChatAdapter;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.Model.ChatModel;
import ai.agusibrhim.design.topedc.Model.IsiChatModel;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatActivity extends AppCompatActivity {

    private RecyclerView messageRecyclerView;
    private EditText editText;
    private ImageView btnSend;
    DatabaseReference databaseReference;
    String mNama;
    long mTime;
    String MESSAGE_CHILD;
    String h;
    SharedPref sharedPref;
    private ImageView btnAdd;
    IsiChatAdapter mAdapter;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<IsiChatModel> list = new ArrayList<>();
    File imageFileSatu;
    Handler handler = new Handler();
    private TextView tvKet;
    private ImageView imgClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        initView();

        awal();
//        this.handler = new Handler();
//        this.handler.postDelayed(this.runnable, 1000);

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                kirimData();
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EasyImage.openChooserWithGallery(ChatActivity.this, "chose", 3);
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    private void awal() {
        sharedPref = new SharedPref(this);
        databaseReference = FirebaseDatabase.getInstance().getReference();

        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        messageRecyclerView.setLayoutManager(layoutManager);

        getChat();
    }

    private void kirimData() {
//        if (h == null){
//            kirimDua();
//        }else {
//            kirimAdaImage();
//        }
        kirimDua();
    }

    private void getChat() {
//       final ProgressDialog pd = new ProgressDialog(this);
//       pd.setTitle("Loading ....");
//       pd.setCancelable(false);
//       pd.show();
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.isiChat(sharedPref.getIdUser(),
                sharedPref.getSESSION(), sharedPref.getIdChat()).enqueue(new Callback<ArrayList<IsiChatModel>>() {
            @Override
            public void onResponse(Call<ArrayList<IsiChatModel>> call, Response<ArrayList<IsiChatModel>> response) {
                if (response.isSuccessful()) {
//                  pd.dismiss();
                    list = response.body();
                    if (list.get(0).getSuccess().equals("0")) {
                        tvKet.setVisibility(View.VISIBLE);
                        messageRecyclerView.setVisibility(View.GONE);
                    } else {
                        tvKet.setVisibility(View.GONE);
                        messageRecyclerView.setVisibility(View.VISIBLE);

                        mAdapter = new IsiChatAdapter(list, getApplicationContext());
                        messageRecyclerView.setAdapter(mAdapter);
                        messageRecyclerView.smoothScrollToPosition(list.size() - 1);
                        mAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<IsiChatModel>> call, Throwable t) {
//                pd.dismiss();
                Toast.makeText(ChatActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void kirim() {
        String message = editText.getText().toString();
        mTime = new Date().getTime();
        //cek apakah teks ada isinya
        if (TextUtils.isEmpty(message)) {
            Toast.makeText(getApplicationContext(), "Tidak bisa mengirim teks kosong", Toast.LENGTH_SHORT).show();
        } else {
            ChatModel chatMessage = new ChatModel(message, "", "" + sharedPref.getUSERNAME(), "" + getIntent().getStringExtra("nama_toko"), mTime);
            databaseReference.child("users").child(sharedPref.getUSERNAME()).child("chat").child("nama1_nama2").push().setValue(chatMessage, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                    if (databaseError != null) {
                        Toast.makeText(ChatActivity.this, "Gagal Terkirim.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ChatActivity.this, "Terkirim.", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            editText.setText("");
        }
    }

    private void kirimDua() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Proses ...");
        pd.setCancelable(false);
        pd.show();

        RequestBody userTo = RequestBody.create(MediaType.parse("text/plain"), sharedPref.getIdChat());
        RequestBody lastView = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody pesan = RequestBody.create(MediaType.parse("text/plain"), editText.getText().toString());

//        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), imageFileSatu);
//        MultipartBody.Part body = MultipartBody.Part.createFormData("image", imageFileSatu.getName(), requestFile);
////
//        RequestBody requestFile2 = RequestBody.create(MediaType.parse("multipart/form-data"), imageFileSatu);
//        MultipartBody.Part body2 = MultipartBody.Part.createFormData("video",imageFileSatu.getName(), requestFile2);

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.kirimPesan(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                userTo,
                lastView,
//                body,
//                body2,
                pesan).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String pesan = jsonObject.optString("message");
                        if (pesan.equals("Berhasil ditambahkan")) {
                            Toast.makeText(ChatActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                            editText.setText("");
                            getChat();
                        } else {
                            Toast.makeText(ChatActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                        }
                        Toast.makeText(ChatActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(ChatActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void kirimAdaImage() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Proses ...");
        pd.setCancelable(false);
        pd.show();

        RequestBody userTo = RequestBody.create(MediaType.parse("text/plain"), sharedPref.getIdChat());
        RequestBody lastView = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody pesan = RequestBody.create(MediaType.parse("text/plain"), editText.getText().toString());

        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), imageFileSatu);
        MultipartBody.Part body = MultipartBody.Part.createFormData("image", imageFileSatu.getName(), requestFile);

//        RequestBody requestFile2 = RequestBody.create(MediaType.parse("multipart/form-data"), imageFileSatu);
//        MultipartBody.Part body2 = MultipartBody.Part.createFormData("video",imageFileSatu.getName(), requestFile2);

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.kirimPesanAdaImage(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                userTo,
                lastView,
                body,
//                body2,
                pesan).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String pesan = jsonObject.optString("message");
                        if (pesan.equals("Berhasil ditambahkan")) {
                            Toast.makeText(ChatActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                            editText.setText("");
                            getChat();
                        } else {
                            Toast.makeText(ChatActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                        }
                        Toast.makeText(ChatActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(ChatActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                //Some error handling
                Toast.makeText(ChatActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onImagePicked(File image, EasyImage.ImageSource source, int type) {
//                Glide.with(ChatActivity.this)
//                        .load(new File(image.getPath()))
//                        .into(imgHasil3);
                h = new File(image.getPath()).getName();
                imageFileSatu = new File(image.getPath());

                kirimAdaImage();
            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
                super.onCanceled(source, type);
            }
        });
    }

    private void initView() {
        messageRecyclerView = (RecyclerView) findViewById(R.id.messageRecyclerView);
        editText = (EditText) findViewById(R.id.edit_text);
        btnSend = (ImageView) findViewById(R.id.btnSend);
        btnAdd = (ImageView) findViewById(R.id.btnAdd);
        tvKet = (TextView) findViewById(R.id.tv_ket);
        imgClose = (ImageView) findViewById(R.id.img_close);
    }

//    public final Runnable runnable = new Runnable() {
//        @Override
//        public void run() {
////            getChat();
//            handler.postDelayed(runnable, 1000);
//        }
//    };
}
