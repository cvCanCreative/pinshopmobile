package ai.agusibrhim.design.topedc.Helper;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import ai.agusibrhim.design.topedc.Helper.dataRoom.Pesanan;
import ai.agusibrhim.design.topedc.Model.FavoritModel;
import ai.agusibrhim.design.topedc.Model.ListProduct;

public class SharedPref {
    public static final String SP = "pinshope";
    public static final String PERTAMA_MASUK = "pertama";
    public static final String SUDAH_LOGIN = "sudah_login";
    public static final String ID_USER = "idUser";
    public static final String SESSION = "session";
    public static final String USERNAME = "usernamem";
    public static final String SALDO = "sado";
    public static final String TOKO = "toko";
    public static final String TIPE_LOGIN ="tTipelogin";
    public static final String ID_CHAT ="idChat";
    public static final String VOUCHER = "voucher";
    public static final String TIPE_VOUCHER = "tipe_voucher";
    public static final String POTONGAN_VOUCHER = "potongan_voucher";
    public static final String KODE_VOUCHER = "kode_voucher";
    public static final String TIPEBELI = "tipe_beli";
    public static final String PRODUK = "PRODUK";
    public static final String FAVORIT = "FAVORIT";
    public static final String TIPEKLIK = "tipe_klik";
    public static final String KODEREVERAL = "kodeReveral";

    SharedPreferences sp;
    SharedPreferences.Editor editor;

    public SharedPref(Context context) {
        sp = context.getSharedPreferences(SP,Context.MODE_PRIVATE);
        editor = sp.edit();
    }

    public void savePrefString(String key, String value){
        editor.putString(key,value);
        editor.commit();
    }

    public void savePrefBoolean(String key, Boolean value){
        editor.putBoolean(key,value);
        editor.commit();
    }

    public ListProduct setProduk(ListProduct produk) {
        if (produk == null) return null;
        String json = new Gson().toJson(produk, ListProduct.class);
        sp.edit().putString(PRODUK, json).apply();
        return getProduk();
    }
    public ListProduct getProduk() {
        String data = sp.getString(PRODUK, null);
        if (data == null) return null;
        return new Gson().fromJson(data, ListProduct.class);
    }

    public FavoritModel getFav() {
        String data = sp.getString(FAVORIT, null);
        if (data == null) return null;
        return new Gson().fromJson(data, FavoritModel.class);
    }

    public FavoritModel setFav(FavoritModel pesanan) {
        if (pesanan == null) return null;
        String json = new Gson().toJson(pesanan, FavoritModel.class);
        sp.edit().putString(FAVORIT, json).apply();
        return getFav();
    }


    public void clearAll(){
        editor.clear();
        editor.commit();
    }

    public String getKODEREVERAL() {
        return sp.getString(KODEREVERAL,"");
    }

    public String getTIPEKLIK() {
        return sp.getString(TIPEKLIK,"");
    }

    public String getTipeVoucher() {
        return sp.getString(TIPE_VOUCHER,"");
    }

    public String getPotonganVoucher() {
        return sp.getString(POTONGAN_VOUCHER,"");
    }

    public String getVOUCHER() {
        return sp.getString(VOUCHER,"");
    }

    public String getKodeVoucher() {
        return sp.getString(KODE_VOUCHER,"");
    }

    public String getIdChat() {
        return sp.getString(ID_CHAT,"");
    }

    public String getTipeLogin() {
        return sp.getString(TIPE_LOGIN,"");
    }

    public String getTOKO() {
        return sp.getString(TOKO,"");
    }

    public String getSALDO() {
        return sp.getString(SALDO,"");
    }

    public String getUSERNAME() {
        return sp.getString(USERNAME,"");
    }

    public String getIdUser() {
        return sp.getString(ID_USER,"");
    }

    public String getSESSION() {
        return sp.getString(SESSION,"");
    }

    public  String getTIPEBELI() {
        return sp.getString(TIPEBELI,"");
    }

    public Boolean getSudahLogin(){
        return sp.getBoolean(SUDAH_LOGIN,false);
    }

    public Boolean  getPertamaMasuk() {
        return sp.getBoolean(PERTAMA_MASUK, true);
    }
}
