package ai.agusibrhim.design.topedc.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PromoModel implements Parcelable {

    @SerializedName("ID_SLIDER")
    @Expose
    private String iDSLIDER;
    @SerializedName("SL_TITLE")
    @Expose
    private String sLTITLE;
    @SerializedName("SL_DESCRIPTION")
    @Expose
    private String sLDESCRIPTION;
    @SerializedName("SL_FILE")
    @Expose
    private String sLFILE;
    @SerializedName("CREATED_AT")
    @Expose
    private String cREATEDAT;
    @SerializedName("UPDATED_AT")
    @Expose
    private String uPDATEDAT;
    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("message")
    @Expose
    private String message;

    protected PromoModel(Parcel in) {
        iDSLIDER = in.readString();
        sLTITLE = in.readString();
        sLDESCRIPTION = in.readString();
        sLFILE = in.readString();
        cREATEDAT = in.readString();
        uPDATEDAT = in.readString();
        success = in.readString();
        message = in.readString();
    }

    public static final Creator<PromoModel> CREATOR = new Creator<PromoModel>() {
        @Override
        public PromoModel createFromParcel(Parcel in) {
            return new PromoModel(in);
        }

        @Override
        public PromoModel[] newArray(int size) {
            return new PromoModel[size];
        }
    };

    public String getIDSLIDER() {
        return iDSLIDER;
    }

    public void setIDSLIDER(String iDSLIDER) {
        this.iDSLIDER = iDSLIDER;
    }

    public String getSLTITLE() {
        return sLTITLE;
    }

    public void setSLTITLE(String sLTITLE) {
        this.sLTITLE = sLTITLE;
    }

    public String getSLDESCRIPTION() {
        return sLDESCRIPTION;
    }

    public void setSLDESCRIPTION(String sLDESCRIPTION) {
        this.sLDESCRIPTION = sLDESCRIPTION;
    }

    public String getSLFILE() {
        return sLFILE;
    }

    public void setSLFILE(String sLFILE) {
        this.sLFILE = sLFILE;
    }

    public String getCREATEDAT() {
        return cREATEDAT;
    }

    public void setCREATEDAT(String cREATEDAT) {
        this.cREATEDAT = cREATEDAT;
    }

    public String getUPDATEDAT() {
        return uPDATEDAT;
    }

    public void setUPDATEDAT(String uPDATEDAT) {
        this.uPDATEDAT = uPDATEDAT;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(iDSLIDER);
        parcel.writeString(sLTITLE);
        parcel.writeString(sLDESCRIPTION);
        parcel.writeString(sLFILE);
        parcel.writeString(cREATEDAT);
        parcel.writeString(uPDATEDAT);
        parcel.writeString(success);
        parcel.writeString(message);
    }
}
