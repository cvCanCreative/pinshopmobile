package ai.agusibrhim.design.topedc.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class RequestTokoModel implements Parcelable {
    @SerializedName("ID_TRANSAKSI")
    @Expose
    public String iDTRANSAKSI;
    public String TS_KODE_TRX;
    public String TS_KODE_UNIK;
    @SerializedName("ID_USER")
    @Expose
    public String iDUSER;
    @SerializedName("success")
    @Expose
    public Integer success;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("transaksi_detail")
    @Expose
   public ArrayList<TransaksiDetail> transaksiDetail = new ArrayList<>();

    protected RequestTokoModel(Parcel in) {
        iDTRANSAKSI = in.readString();
        iDUSER = in.readString();
        if (in.readByte() == 0) {
            success = null;
        } else {
            success = in.readInt();
        }
        message = in.readString();
    }

    public static final Creator<RequestTokoModel> CREATOR = new Creator<RequestTokoModel>() {
        @Override
        public RequestTokoModel createFromParcel(Parcel in) {
            return new RequestTokoModel(in);
        }

        @Override
        public RequestTokoModel[] newArray(int size) {
            return new RequestTokoModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(iDTRANSAKSI);
        parcel.writeString(iDUSER);
        if (success == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(success);
        }
        parcel.writeString(message);
    }
}
