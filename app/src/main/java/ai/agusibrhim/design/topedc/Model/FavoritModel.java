package ai.agusibrhim.design.topedc.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FavoritModel implements Parcelable {
    @SerializedName("ID_WISHLIST")
    @Expose
    private String iDWISHLIST;
    @SerializedName("ID_USER")
    @Expose
    private String iDUSER;
    @SerializedName("ID_BARANG")
    @Expose
    private String iDBARANG;
    @SerializedName("CREATED_AT")
    @Expose
    private String cREATEDAT;
    @SerializedName("UPDATED_AT")
    @Expose
    private String uPDATEDAT;
    @SerializedName("ID_SUB_KATEGORI")
    @Expose
    private String iDSUBKATEGORI;
    @SerializedName("BA_IMAGE")
    @Expose
    private String bAIMAGE;
    @SerializedName("BA_NAME")
    @Expose
    private String bANAME;
    @SerializedName("BA_PSHOP")
    @Expose
    private String bAPSHOP;
    @SerializedName("BA_PPIN")
    @Expose
    private String bAPPIN;
    @SerializedName("BA_PRICE")
    @Expose
    private String bAPRICE;
    @SerializedName("BA_SKU")
    @Expose
    private String bASKU;
    @SerializedName("BA_DESCRIPTION")
    @Expose
    private String bADESCRIPTION;
    @SerializedName("BA_STOCK")
    @Expose
    private String bASTOCK;
    @SerializedName("BA_WEIGHT")
    @Expose
    private String bAWEIGHT;
    @SerializedName("BA_CONDITION")
    @Expose
    private String bACONDITION;
    @SerializedName("US_USERNAME")
    @Expose
    private String uSUSERNAME;
    @SerializedName("US_TELP")
    @Expose
    private String uSTELP;
    @SerializedName("US_PASSWORD")
    @Expose
    private String uSPASSWORD;
    @SerializedName("US_EMAIL")
    @Expose
    private String uSEMAIL;
    @SerializedName("US_RULE")
    @Expose
    private String uSRULE;
    @SerializedName("US_ROUTE")
    @Expose
    private String uSROUTE;
    @SerializedName("US_FCM")
    @Expose
    private String uSFCM;
    @SerializedName("US_TOKEN")
    @Expose
    private String uSTOKEN;
    @SerializedName("US_FORGOT_PASS")
    @Expose
    private String uSFORGOTPASS;
    @SerializedName("US_REQUEST_FORGOT_PASS")
    @Expose
    private String uSREQUESTFORGOTPASS;
    @SerializedName("ID_USER_DETAIL")
    @Expose
    private String iDUSERDETAIL;
    @SerializedName("USD_FOTO")
    @Expose
    private String uSDFOTO;
    @SerializedName("USD_FULLNAME")
    @Expose
    private String uSDFULLNAME;
    @SerializedName("USD_BIRTH")
    @Expose
    private String uSDBIRTH;
    @SerializedName("USD_ADDRESS")
    @Expose
    private String uSDADDRESS;
    @SerializedName("USD_GENDER")
    @Expose
    private String uSDGENDER;
    @SerializedName("USD_TOKO")
    @Expose
    private String uSDTOKO;
    @SerializedName("USD_TOKO_FOTO")
    @Expose
    private String uSDTOKOFOTO;
    @SerializedName("USD_TOKO_DETAIL")
    @Expose
    private String uSDTOKODETAIL;
    @SerializedName("ID_TOKO_SKAT")
    @Expose
    private String iDTOKOSKAT;
    @SerializedName("USD_ID_PROV")
    @Expose
    private String uSDIDPROV;
    @SerializedName("USD_ID_KOTA")
    @Expose
    private String uSDIDKOTA;
    @SerializedName("USD_KOTA")
    @Expose
    private String uSDKOTA;
    @SerializedName("USD_ID_KECAMATAN")
    @Expose
    private String uSDIDKECAMATAN;
    @SerializedName("USD_KECAMATAN")
    @Expose
    private String uSDKECAMATAN;
    @SerializedName("USD_STATUS")
    @Expose
    private String uSDSTATUS;
    @SerializedName("USD_REFERRAL")
    @Expose
    private String uSDREFERRAL;
    @SerializedName("USD_REFERRAL_FROM")
    @Expose
    private String uSDREFERRALFROM;
    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("message")
    @Expose
    private String message;

    protected FavoritModel(Parcel in) {
        iDWISHLIST = in.readString();
        iDUSER = in.readString();
        iDBARANG = in.readString();
        cREATEDAT = in.readString();
        uPDATEDAT = in.readString();
        iDSUBKATEGORI = in.readString();
        bAIMAGE = in.readString();
        bANAME = in.readString();
        bAPSHOP = in.readString();
        bAPPIN = in.readString();
        bAPRICE = in.readString();
        bASKU = in.readString();
        bADESCRIPTION = in.readString();
        bASTOCK = in.readString();
        bAWEIGHT = in.readString();
        bACONDITION = in.readString();
        uSUSERNAME = in.readString();
        uSTELP = in.readString();
        uSPASSWORD = in.readString();
        uSEMAIL = in.readString();
        uSRULE = in.readString();
        uSROUTE = in.readString();
        uSFCM = in.readString();
        uSTOKEN = in.readString();
        uSFORGOTPASS = in.readString();
        uSREQUESTFORGOTPASS = in.readString();
        iDUSERDETAIL = in.readString();
        uSDFOTO = in.readString();
        uSDFULLNAME = in.readString();
        uSDBIRTH = in.readString();
        uSDADDRESS = in.readString();
        uSDGENDER = in.readString();
        uSDTOKO = in.readString();
        uSDTOKOFOTO = in.readString();
        uSDTOKODETAIL = in.readString();
        iDTOKOSKAT = in.readString();
        uSDIDPROV = in.readString();
        uSDIDKOTA = in.readString();
        uSDKOTA = in.readString();
        uSDIDKECAMATAN = in.readString();
        uSDKECAMATAN = in.readString();
        uSDSTATUS = in.readString();
        uSDREFERRAL = in.readString();
        uSDREFERRALFROM = in.readString();
        if (in.readByte() == 0) {
            success = null;
        } else {
            success = in.readInt();
        }
        message = in.readString();
    }

    public static final Creator<FavoritModel> CREATOR = new Creator<FavoritModel>() {
        @Override
        public FavoritModel createFromParcel(Parcel in) {
            return new FavoritModel(in);
        }

        @Override
        public FavoritModel[] newArray(int size) {
            return new FavoritModel[size];
        }
    };

    public String getIDWISHLIST() {
        return iDWISHLIST;
    }

    public void setIDWISHLIST(String iDWISHLIST) {
        this.iDWISHLIST = iDWISHLIST;
    }

    public String getIDUSER() {
        return iDUSER;
    }

    public void setIDUSER(String iDUSER) {
        this.iDUSER = iDUSER;
    }

    public String getIDBARANG() {
        return iDBARANG;
    }

    public void setIDBARANG(String iDBARANG) {
        this.iDBARANG = iDBARANG;
    }

    public String getCREATEDAT() {
        return cREATEDAT;
    }

    public void setCREATEDAT(String cREATEDAT) {
        this.cREATEDAT = cREATEDAT;
    }

    public String getUPDATEDAT() {
        return uPDATEDAT;
    }

    public void setUPDATEDAT(String uPDATEDAT) {
        this.uPDATEDAT = uPDATEDAT;
    }

    public String getIDSUBKATEGORI() {
        return iDSUBKATEGORI;
    }

    public void setIDSUBKATEGORI(String iDSUBKATEGORI) {
        this.iDSUBKATEGORI = iDSUBKATEGORI;
    }

    public String getBAIMAGE() {
        return bAIMAGE;
    }

    public void setBAIMAGE(String bAIMAGE) {
        this.bAIMAGE = bAIMAGE;
    }

    public String getBANAME() {
        return bANAME;
    }

    public void setBANAME(String bANAME) {
        this.bANAME = bANAME;
    }

    public String getBAPSHOP() {
        return bAPSHOP;
    }

    public void setBAPSHOP(String bAPSHOP) {
        this.bAPSHOP = bAPSHOP;
    }

    public String getBAPPIN() {
        return bAPPIN;
    }

    public void setBAPPIN(String bAPPIN) {
        this.bAPPIN = bAPPIN;
    }

    public String getBAPRICE() {
        return bAPRICE;
    }

    public void setBAPRICE(String bAPRICE) {
        this.bAPRICE = bAPRICE;
    }

    public String getBASKU() {
        return bASKU;
    }

    public void setBASKU(String bASKU) {
        this.bASKU = bASKU;
    }

    public String getBADESCRIPTION() {
        return bADESCRIPTION;
    }

    public void setBADESCRIPTION(String bADESCRIPTION) {
        this.bADESCRIPTION = bADESCRIPTION;
    }

    public String getBASTOCK() {
        return bASTOCK;
    }

    public void setBASTOCK(String bASTOCK) {
        this.bASTOCK = bASTOCK;
    }

    public String getBAWEIGHT() {
        return bAWEIGHT;
    }

    public void setBAWEIGHT(String bAWEIGHT) {
        this.bAWEIGHT = bAWEIGHT;
    }

    public String getBACONDITION() {
        return bACONDITION;
    }

    public void setBACONDITION(String bACONDITION) {
        this.bACONDITION = bACONDITION;
    }

    public String getUSUSERNAME() {
        return uSUSERNAME;
    }

    public void setUSUSERNAME(String uSUSERNAME) {
        this.uSUSERNAME = uSUSERNAME;
    }

    public String getUSTELP() {
        return uSTELP;
    }

    public void setUSTELP(String uSTELP) {
        this.uSTELP = uSTELP;
    }

    public String getUSPASSWORD() {
        return uSPASSWORD;
    }

    public void setUSPASSWORD(String uSPASSWORD) {
        this.uSPASSWORD = uSPASSWORD;
    }

    public String getUSEMAIL() {
        return uSEMAIL;
    }

    public void setUSEMAIL(String uSEMAIL) {
        this.uSEMAIL = uSEMAIL;
    }

    public String getUSRULE() {
        return uSRULE;
    }

    public void setUSRULE(String uSRULE) {
        this.uSRULE = uSRULE;
    }

    public String getUSROUTE() {
        return uSROUTE;
    }

    public void setUSROUTE(String uSROUTE) {
        this.uSROUTE = uSROUTE;
    }

    public String getUSFCM() {
        return uSFCM;
    }

    public void setUSFCM(String uSFCM) {
        this.uSFCM = uSFCM;
    }

    public String getUSTOKEN() {
        return uSTOKEN;
    }

    public void setUSTOKEN(String uSTOKEN) {
        this.uSTOKEN = uSTOKEN;
    }

    public String getUSFORGOTPASS() {
        return uSFORGOTPASS;
    }

    public void setUSFORGOTPASS(String uSFORGOTPASS) {
        this.uSFORGOTPASS = uSFORGOTPASS;
    }

    public String getUSREQUESTFORGOTPASS() {
        return uSREQUESTFORGOTPASS;
    }

    public void setUSREQUESTFORGOTPASS(String uSREQUESTFORGOTPASS) {
        this.uSREQUESTFORGOTPASS = uSREQUESTFORGOTPASS;
    }

    public String getIDUSERDETAIL() {
        return iDUSERDETAIL;
    }

    public void setIDUSERDETAIL(String iDUSERDETAIL) {
        this.iDUSERDETAIL = iDUSERDETAIL;
    }

    public String getUSDFOTO() {
        return uSDFOTO;
    }

    public void setUSDFOTO(String uSDFOTO) {
        this.uSDFOTO = uSDFOTO;
    }

    public String getUSDFULLNAME() {
        return uSDFULLNAME;
    }

    public void setUSDFULLNAME(String uSDFULLNAME) {
        this.uSDFULLNAME = uSDFULLNAME;
    }

    public String getUSDBIRTH() {
        return uSDBIRTH;
    }

    public void setUSDBIRTH(String uSDBIRTH) {
        this.uSDBIRTH = uSDBIRTH;
    }

    public String getUSDADDRESS() {
        return uSDADDRESS;
    }

    public void setUSDADDRESS(String uSDADDRESS) {
        this.uSDADDRESS = uSDADDRESS;
    }

    public String getUSDGENDER() {
        return uSDGENDER;
    }

    public void setUSDGENDER(String uSDGENDER) {
        this.uSDGENDER = uSDGENDER;
    }

    public String getUSDTOKO() {
        return uSDTOKO;
    }

    public void setUSDTOKO(String uSDTOKO) {
        this.uSDTOKO = uSDTOKO;
    }

    public String getUSDTOKOFOTO() {
        return uSDTOKOFOTO;
    }

    public void setUSDTOKOFOTO(String uSDTOKOFOTO) {
        this.uSDTOKOFOTO = uSDTOKOFOTO;
    }

    public String getUSDTOKODETAIL() {
        return uSDTOKODETAIL;
    }

    public void setUSDTOKODETAIL(String uSDTOKODETAIL) {
        this.uSDTOKODETAIL = uSDTOKODETAIL;
    }

    public String getIDTOKOSKAT() {
        return iDTOKOSKAT;
    }

    public void setIDTOKOSKAT(String iDTOKOSKAT) {
        this.iDTOKOSKAT = iDTOKOSKAT;
    }

    public String getUSDIDPROV() {
        return uSDIDPROV;
    }

    public void setUSDIDPROV(String uSDIDPROV) {
        this.uSDIDPROV = uSDIDPROV;
    }

    public String getUSDIDKOTA() {
        return uSDIDKOTA;
    }

    public void setUSDIDKOTA(String uSDIDKOTA) {
        this.uSDIDKOTA = uSDIDKOTA;
    }

    public String getUSDKOTA() {
        return uSDKOTA;
    }

    public void setUSDKOTA(String uSDKOTA) {
        this.uSDKOTA = uSDKOTA;
    }

    public String getUSDIDKECAMATAN() {
        return uSDIDKECAMATAN;
    }

    public void setUSDIDKECAMATAN(String uSDIDKECAMATAN) {
        this.uSDIDKECAMATAN = uSDIDKECAMATAN;
    }

    public String getUSDKECAMATAN() {
        return uSDKECAMATAN;
    }

    public void setUSDKECAMATAN(String uSDKECAMATAN) {
        this.uSDKECAMATAN = uSDKECAMATAN;
    }

    public String getUSDSTATUS() {
        return uSDSTATUS;
    }

    public void setUSDSTATUS(String uSDSTATUS) {
        this.uSDSTATUS = uSDSTATUS;
    }

    public String getUSDREFERRAL() {
        return uSDREFERRAL;
    }

    public void setUSDREFERRAL(String uSDREFERRAL) {
        this.uSDREFERRAL = uSDREFERRAL;
    }

    public String getUSDREFERRALFROM() {
        return uSDREFERRALFROM;
    }

    public void setUSDREFERRALFROM(String uSDREFERRALFROM) {
        this.uSDREFERRALFROM = uSDREFERRALFROM;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(iDWISHLIST);
        parcel.writeString(iDUSER);
        parcel.writeString(iDBARANG);
        parcel.writeString(cREATEDAT);
        parcel.writeString(uPDATEDAT);
        parcel.writeString(iDSUBKATEGORI);
        parcel.writeString(bAIMAGE);
        parcel.writeString(bANAME);
        parcel.writeString(bAPSHOP);
        parcel.writeString(bAPPIN);
        parcel.writeString(bAPRICE);
        parcel.writeString(bASKU);
        parcel.writeString(bADESCRIPTION);
        parcel.writeString(bASTOCK);
        parcel.writeString(bAWEIGHT);
        parcel.writeString(bACONDITION);
        parcel.writeString(uSUSERNAME);
        parcel.writeString(uSTELP);
        parcel.writeString(uSPASSWORD);
        parcel.writeString(uSEMAIL);
        parcel.writeString(uSRULE);
        parcel.writeString(uSROUTE);
        parcel.writeString(uSFCM);
        parcel.writeString(uSTOKEN);
        parcel.writeString(uSFORGOTPASS);
        parcel.writeString(uSREQUESTFORGOTPASS);
        parcel.writeString(iDUSERDETAIL);
        parcel.writeString(uSDFOTO);
        parcel.writeString(uSDFULLNAME);
        parcel.writeString(uSDBIRTH);
        parcel.writeString(uSDADDRESS);
        parcel.writeString(uSDGENDER);
        parcel.writeString(uSDTOKO);
        parcel.writeString(uSDTOKOFOTO);
        parcel.writeString(uSDTOKODETAIL);
        parcel.writeString(iDTOKOSKAT);
        parcel.writeString(uSDIDPROV);
        parcel.writeString(uSDIDKOTA);
        parcel.writeString(uSDKOTA);
        parcel.writeString(uSDIDKECAMATAN);
        parcel.writeString(uSDKECAMATAN);
        parcel.writeString(uSDSTATUS);
        parcel.writeString(uSDREFERRAL);
        parcel.writeString(uSDREFERRALFROM);
        if (success == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(success);
        }
        parcel.writeString(message);
    }
}
