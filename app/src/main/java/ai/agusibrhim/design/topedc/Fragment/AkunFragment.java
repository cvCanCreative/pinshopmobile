package ai.agusibrhim.design.topedc.Fragment;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import ai.agusibrhim.design.topedc.Activity.AboutActivity;
import ai.agusibrhim.design.topedc.Activity.AddKeluhanActivity;
import ai.agusibrhim.design.topedc.Activity.BuatTokoActivity;
import ai.agusibrhim.design.topedc.Activity.FavoritActivity;
import ai.agusibrhim.design.topedc.Activity.HelpActivity;
import ai.agusibrhim.design.topedc.Activity.HistoryActivity;
import ai.agusibrhim.design.topedc.Activity.HistoryTransaksiActivity;
import ai.agusibrhim.design.topedc.Activity.HomeLikeShopeeActivity;
import ai.agusibrhim.design.topedc.Activity.KodeReveralctivity;
import ai.agusibrhim.design.topedc.Activity.LoginActivity;
import ai.agusibrhim.design.topedc.Activity.PengaturanAkunActivity;
import ai.agusibrhim.design.topedc.Activity.TokoKitaActivity;
import ai.agusibrhim.design.topedc.Helper.Config;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class AkunFragment extends Fragment {


    private SwipeRefreshLayout swipe;
    private LinearLayout ly;
    private ImageView imgClose;
    private LinearLayout mainLinearLayout1;
    private CardView cvNews;
    private CircleImageView imgProfil;
    private TextView tvNama;
    private LinearLayout lyAturAkun;
    private View toolbarShadow;
    private TextView tvNoTlpProfil;
    private TextView tvEmailProfil;
    private TextView tvJenisKelaminProfil;
    private TextView tvTglLahirProfil;
    private TextView tvBelumLogin;
    SharedPref sharedPref;
    private LinearLayout lyBelanjaan;
    private LinearLayout lyFavorit;
    private LinearLayout lyHisSaldo;
    private LinearLayout lyLapor;
    private LinearLayout lyBantuan;
    private LinearLayout lySetting;
    private LinearLayout lyRatting;
    private LinearLayout lyAkun;
    private LinearLayout lyToko;
    private TextView tvToko;
    private LinearLayout lyAbout;
    private TextView tvToko1;
    private ImageView imgBelumBayar;
    private ImageView imgDikemas;
    private ImageView imgDikirim;
    private ImageView imgSampai;
    private LinearLayout lyReveral;


    public AkunFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_akun, container, false);
        initView(view);

        awal();

        mainButton();

        return view;
    }

    private void mainButton() {
        lyAturAkun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), PengaturanAkunActivity.class);
                startActivity(intent);
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), HomeLikeShopeeActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));

            }
        });

        lyBelanjaan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), HistoryTransaksiActivity.class);
                intent.putExtra("data", "drawer");
                startActivity(intent);
            }
        });

        lyFavorit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), FavoritActivity.class));
            }
        });

        lyHisSaldo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), HistoryActivity.class));
            }
        });

        lyLapor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), AddKeluhanActivity.class));
            }
        });

        lyBantuan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), HelpActivity.class));
            }
        });

        lyRatting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        lySetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), PengaturanAkunActivity.class);
                startActivity(intent);
            }
        });

        lyAkun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        lyToko.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sharedPref.getTOKO().equals("")) {
                    new AlertDialog.Builder(getActivity())
                            .setMessage("Anda Belum Membuat Toko, Apakah Anda ingin membuatnya ?")
                            .setCancelable(false)
                            .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent intent = new Intent(getActivity(), BuatTokoActivity.class);
                                    startActivity(intent);
                                }
                            })
                            .setNegativeButton("Tidak", null)
                            .show();
                } else {

                    Intent intent = new Intent(getActivity(), TokoKitaActivity.class);
                    intent.putExtra("data", "sendiri");
                    startActivity(intent);
                }
            }
        });

        lyAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), AboutActivity.class));
            }
        });

        imgBelumBayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), HistoryTransaksiActivity.class);
                intent.putExtra("data", "belumbayar");
                startActivity(intent);
            }
        });

        imgDikemas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), HistoryTransaksiActivity.class);
                intent.putExtra("data", "dikemas");
                startActivity(intent);
            }
        });

        imgDikirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), HistoryTransaksiActivity.class);
                intent.putExtra("data", "dikirim");
                startActivity(intent);
            }
        });

        imgSampai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), HistoryTransaksiActivity.class);
                intent.putExtra("data", "sampai");
                startActivity(intent);
            }
        });

        lyReveral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), KodeReveralctivity.class);
                startActivity(intent);
            }
        });
    }

    private void awal() {
        sharedPref = new SharedPref(getActivity());

        if (sharedPref.getSudahLogin()) {
            profil();

            if (sharedPref.getTOKO().equals("")) {
                tvToko.setText("Buat Toko");
                tvToko1.setText("Buat Toko");
            }
        } else {
            startActivity(new Intent(getActivity(), LoginActivity.class));
            mainLinearLayout1.setVisibility(View.GONE);
//            tvBelumLogin.setVisibility(View.VISIBLE);
//            Toast.makeText(getActivity(), "Anda belum Login", Toast.LENGTH_SHORT).show();
        }
    }

    private void profil() {

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getProfil(sharedPref.getIdUser(), sharedPref.getSESSION()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {

                    try {
                        JSONArray jsonArray = new JSONArray(response.body().string());
                        JSONObject jsonObject = jsonArray.optJSONObject(0);
                        String foto = jsonObject.optString("USD_FOTO");
                        String nama = jsonObject.optString("USD_FULLNAME");
                        String noHp = jsonObject.optString("US_TELP");
                        String email = jsonObject.optString("US_EMAIL");
                        String kelamin = jsonObject.optString("USD_GENDER");
                        String ttl = jsonObject.optString("USD_BIRTH");
                        String namaToko = jsonObject.optString("USD_TOKO");

//                        sharedPref.savePrefString(SharedPref.TOKO,namaToko);

                        tvNama.setText(nama);
                        tvEmailProfil.setText(email);
                        tvTglLahirProfil.setText(ttl);

                        if (kelamin.equals("LK")) {
                            tvJenisKelaminProfil.setText("Laki-laki");
                        } else {
                            tvJenisKelaminProfil.setText("Perempuan");
                        }

                        tvNoTlpProfil.setText(noHp);

                        Picasso.with(getActivity())
                                .load(Config.IMAGE_PROFIL + foto)
                                .placeholder(R.drawable.image_loading)
                                .error(R.drawable.no_image_found)
                                .noFade()
                                .into(imgProfil);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(getActivity(), "gagal", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initView(View view) {

        ly = (LinearLayout) view.findViewById(R.id.ly);
        imgClose = (ImageView) view.findViewById(R.id.img_close);
        mainLinearLayout1 = (LinearLayout) view.findViewById(R.id.mainLinearLayout1);
        cvNews = (CardView) view.findViewById(R.id.cvNews);
        imgProfil = (CircleImageView) view.findViewById(R.id.img_profil);
        tvNama = (TextView) view.findViewById(R.id.tv_nama);
        lyAturAkun = (LinearLayout) view.findViewById(R.id.ly_atur_akun);
        toolbarShadow = (View) view.findViewById(R.id.toolbar_shadow);
        tvNoTlpProfil = (TextView) view.findViewById(R.id.tv_noTlp_profil);
        tvEmailProfil = (TextView) view.findViewById(R.id.tv_email_profil);
        tvJenisKelaminProfil = (TextView) view.findViewById(R.id.tv_jenis_kelamin_profil);
        tvTglLahirProfil = (TextView) view.findViewById(R.id.tv_tgl_lahir_profil);
        tvBelumLogin = (TextView) view.findViewById(R.id.tv_belum_login);
        lyBelanjaan = (LinearLayout) view.findViewById(R.id.ly_belanjaan);
        lyFavorit = (LinearLayout) view.findViewById(R.id.ly_favorit);
        lyHisSaldo = (LinearLayout) view.findViewById(R.id.ly_his_saldo);
        lyLapor = (LinearLayout) view.findViewById(R.id.ly_lapor);
        lyBantuan = (LinearLayout) view.findViewById(R.id.ly_bantuan);
        lySetting = (LinearLayout) view.findViewById(R.id.ly_setting);
        lyRatting = (LinearLayout) view.findViewById(R.id.ly_ratting);
        lyAkun = (LinearLayout) view.findViewById(R.id.ly_akun);
        lyToko = (LinearLayout) view.findViewById(R.id.ly_toko);
        tvToko = (TextView) view.findViewById(R.id.tv_toko);
        lyAbout = (LinearLayout) view.findViewById(R.id.ly_about);
        tvToko1 = (TextView) view.findViewById(R.id.tv_toko1);
        imgBelumBayar = (ImageView) view.findViewById(R.id.img_belum_bayar);
        imgDikemas = (ImageView) view.findViewById(R.id.img_dikemas);
        imgDikirim = (ImageView) view.findViewById(R.id.img_dikirim);
        imgSampai = (ImageView) view.findViewById(R.id.img_sampai);
        lyReveral = (LinearLayout) view.findViewById(R.id.ly_reveral);
    }
}
