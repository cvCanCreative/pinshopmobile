package ai.agusibrhim.design.topedc.Activity;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Spinner;

import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Adapter.SubKategoriAdapter;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.Model.SubKategoriModel;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubKategoriActivity extends AppCompatActivity {

    private SwipeRefreshLayout swipe;
    private RecyclerView rv;
    SharedPref sharedPref;
    RecyclerView.LayoutManager layoutManager;
    SubKategoriAdapter mAdapter;
    ArrayList<SubKategoriModel> list = new ArrayList<>();
    private ImageView imgNo;
    private Spinner spnFilter;
    private ImageView imgClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_kategori);
        initView();
        awal();
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData();
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void awal() {
        sharedPref = new SharedPref(this);
        layoutManager = new GridLayoutManager(this, 2);
        rv.setLayoutManager(layoutManager);

        getData();
    }

    private void getData() {
        swipe.setRefreshing(true);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getSubKategori(getIntent().getStringExtra("id")).enqueue(new Callback<ArrayList<SubKategoriModel>>() {
            @Override
            public void onResponse(Call<ArrayList<SubKategoriModel>> call, Response<ArrayList<SubKategoriModel>> response) {
                if (response.isSuccessful()) {
                    swipe.setRefreshing(false);
                    list = response.body();
                    if (list.get(0).getMessage().equals("Berhasil")) {
                        mAdapter = new SubKategoriAdapter(list, getApplicationContext());
                        rv.setAdapter(mAdapter);
                    } else {
                        rv.setVisibility(View.GONE);
                        swipe.setVisibility(View.GONE);
//                        Toast.makeText(SubKategoriActivity.this, "no data", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<SubKategoriModel>> call, Throwable t) {
                swipe.setRefreshing(false);
            }
        });
    }

    private void initView() {
        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe);
        rv = (RecyclerView) findViewById(R.id.rv);
        imgNo = (ImageView) findViewById(R.id.img_no);
        spnFilter = (Spinner) findViewById(R.id.spn_filter);
        imgClose = (ImageView) findViewById(R.id.img_close);
    }
}
