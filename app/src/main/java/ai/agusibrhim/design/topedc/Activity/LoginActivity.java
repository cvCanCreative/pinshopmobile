package ai.agusibrhim.design.topedc.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.MainActivity;
import ai.agusibrhim.design.topedc.Model.AddFirebase;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Url;

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    TextView btnTidakPunyaAkun;
    EditText edtEmail, edtPassword;
    Button btnLogin;
    ProgressDialog pd;
    SharedPref sharedPref;
    private TextView txtLupaKataSandi;
    private SignInButton btnGmail;
    GoogleApiClient googleApiClient;
    private static final int REQ_CODE = 9001;
    String email;
    String telp = "8";
    CallbackManager callbackManager;
    private LoginButton loginButton;
    String cek = "null";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();

        sharedPref = new SharedPref(this);
        lihatHash();

        callbackManager = CallbackManager.Factory.create();

        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        googleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this, this).addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions).build();

        loginButton.setReadPermissions(Arrays.asList("public_profile", "email", "user_birthday", "user_friends"));
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                cek = "facebook";
                final ProgressDialog pd = new ProgressDialog(LoginActivity.this);
                pd.setTitle("Ambil data ....");
                pd.setCancelable(false);
                pd.show();

                String token = loginResult.getAccessToken().getToken();
                GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        pd.dismiss();
//                        Log.d("response",response.toString());
//                        edtEmail.setText(""+response.toString());
                        getData(object);
                    }
                });
                Bundle parameter = new Bundle();
                parameter.putString("fields", "id,email,birthday,friends");
                graphRequest.setParameters(parameter);
                graphRequest.executeAsync();
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });
        if (AccessToken.getCurrentAccessToken() != null) {
//            edtEmail.setText(""+AccessToken.getCurrentAccessToken().);
        } else {

        }
        mainButton();
    }

    private void getData(JSONObject graphObject) {
        try {
//            URL pictur = new URL(""+object.getString("id")+"/picture?width=250&height=250");
//            String email = object.getString("email");
//            edtEmail.setText(graphObject.getString("email"));
            cekEmailFacebook(graphObject.getString("email"));
//            Toast.makeText(this, "ini " + edtEmail.getText().toString(), Toast.LENGTH_SHORT).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void lihatHash() {
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo("ai.agusibrhim.design.topedc", PackageManager.GET_SIGNATURES);
            for (Signature signature : packageInfo.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
//                edtEmail.setText("" + Base64.encodeToString(md.digest(), Base64.DEFAULT));
                Log.d("KeyHash", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }


    }

    void initView() {
        pd = new ProgressDialog(this);
        btnTidakPunyaAkun = (TextView) findViewById(R.id.btn_tidak_punya_akun);
        btnLogin = (Button) findViewById(R.id.btn_login);
        edtEmail = (EditText) findViewById(R.id.edt_email);
        edtPassword = (EditText) findViewById(R.id.edt_password);
        txtLupaKataSandi = (TextView) findViewById(R.id.txt_lupa_kata_sandi);
        btnGmail = (SignInButton) findViewById(R.id.btn_gmail);
        loginButton = (LoginButton) findViewById(R.id.login_button);
    }

    void login() {
        if (!validasi()) {
            return;
        }
        pd.setMessage("Sending data...");
        pd.setCancelable(false);
        pd.show();

        String sApi = "vxnoew9cs98dasydhcnoql0cancoid";
        String sEmail = edtEmail.getText().toString();
        String sPassword = edtPassword.getText().toString();

        ApiService api = ApiConfig.getInstanceRetrofit();
        api.login(sApi, sEmail, sPassword).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                pd.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String message = jsonObject.getString("message");
                    String success = jsonObject.getString("success");
                    String id = jsonObject.optString("id_user");
                    String session = jsonObject.optString("session");
                    String username = jsonObject.optString("username");
                    String namaToko = jsonObject.optString("toko");
                    String statusToko = jsonObject.optString("status_toko");
                    String reveral = jsonObject.optString("referral");

                    if (success.equals("1")) {
                        sharedPref.savePrefString(SharedPref.ID_USER, id);
                        sharedPref.savePrefString(SharedPref.TIPE_LOGIN, "manual");
                        sharedPref.savePrefString(SharedPref.SESSION, session);
                        sharedPref.savePrefString(SharedPref.USERNAME, username);
                        sharedPref.savePrefString(SharedPref.TOKO,namaToko);
                        sharedPref.savePrefString(SharedPref.KODEREVERAL, reveral);
                        sharedPref.savePrefBoolean(SharedPref.SUDAH_LOGIN, true);

                        startActivity(new Intent(LoginActivity.this, HomeLikeShopeeActivity.class)
                         .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                        finish();
//                        Toast.makeText(LoginActivity.this, message  + "id "+id + "s "+ session, Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();

            }
        });
    }
    private void loginGmail() {
        Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(intent, REQ_CODE);
    }

    private Boolean validasi() {

        if (edtEmail.getText().toString().isEmpty()) {
            edtEmail.setError("username harus diisi");
            edtEmail.requestFocus();

            return false;
        }
        if (edtPassword.getText().toString().isEmpty()) {
            edtPassword.setError("Kata Sandi harus diisi");
            edtPassword.requestFocus();

            return false;
        }
        return true;
    }

    void mainButton() {
        btnTidakPunyaAkun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pd.dismiss();
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                intent.putExtra("data", "manual");
                startActivity(intent);
            }
        });

        txtLupaKataSandi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, LupaKataSandiActivity.class);
                startActivity(intent);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });

        btnGmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cek = "gmail";
                loginGmail();
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        callbackManager.onActivityResult(requestCode,resultCode,data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (cek.equals("gmail")) {
            if (requestCode == REQ_CODE) {
                GoogleSignInResult googleSignInResult = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                handleRequest(googleSignInResult);
            }
        } else {

        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    private void handleRequest(GoogleSignInResult result) {
        if (result.isSuccess()) {
//            Toast.makeText(this, ""+result.getStatus(), Toast.LENGTH_SHORT).show();
            GoogleSignInAccount account = result.getSignInAccount();
            String nama = account.getDisplayName();
            email = account.getEmail();

//            String img_url = account.getPhotoUrl().toString();
            cekEmail(email);
//            Toast.makeText(this, ""+ " "+email, Toast.LENGTH_SHORT).show();
        } else {
            updateUI(false);
        }
    }

    private void cekEmail(String email) {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Proses ...");
        pd.setCancelable(false);
        pd.show();
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.cekEmail(email, telp).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String pesan = jsonObject.optString("message");
                        String id_user = jsonObject.optString("id_user");
                        String session = jsonObject.optString("session");
                        String reveral = jsonObject.optString("referral");
                        if (pesan.equals("Email Sudah Ada")) {
//                            Toast.makeText(LoginActivity.this, "1", Toast.LENGTH_SHORT).show();
                            String namaToko = jsonObject.optString("toko");
                            sharedPref.savePrefString(SharedPref.ID_USER, id_user);
                            sharedPref.savePrefString(SharedPref.SESSION, session);
                            sharedPref.savePrefString(SharedPref.TIPE_LOGIN, "gmail");
                            sharedPref.savePrefString(SharedPref.TOKO,""+namaToko);
                            sharedPref.savePrefString(SharedPref.KODEREVERAL, reveral);
                            updateUI(true);
                        } else {
//                            Toast.makeText(LoginActivity.this, "2", Toast.LENGTH_SHORT).show();
                            sharedPref.savePrefString(SharedPref.ID_USER, id_user);
                            sharedPref.savePrefString(SharedPref.SESSION, session);
                            sharedPref.savePrefString(SharedPref.TIPE_LOGIN, "gmail");
                            updateUI2(true);
//                            Toast.makeText(LoginActivity.this, "maaf", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(LoginActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void cekEmailFacebook(String email) {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Proses ...");
        pd.setCancelable(false);
        pd.show();
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.cekEmail(email, telp).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String pesan = jsonObject.optString("message");
                        String id_user = jsonObject.optString("id_user");
                        String session = jsonObject.optString("session");
                        String reveral = jsonObject.optString("referral");
                        if (pesan.equals("Email Sudah Ada")) {
                            sharedPref.savePrefString(SharedPref.ID_USER, id_user);
                            sharedPref.savePrefString(SharedPref.SESSION, session);
                            sharedPref.savePrefString(SharedPref.TIPE_LOGIN, "facebook");
                            sharedPref.savePrefString(SharedPref.KODEREVERAL, reveral);
                            updateUI(true);
                        } else {
                            sharedPref.savePrefString(SharedPref.ID_USER, id_user);
                            sharedPref.savePrefString(SharedPref.SESSION, session);
                            sharedPref.savePrefString(SharedPref.TIPE_LOGIN, "facebook");
                            updateUI2(true);
//                            Toast.makeText(LoginActivity.this, "maaf", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(LoginActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateUI(boolean isLogin) {
        if (isLogin) {
            if (cek.equals("gmail")){
                Toast.makeText(this, "Login gmail berhasil", Toast.LENGTH_SHORT).show();
                sharedPref.savePrefBoolean(SharedPref.SUDAH_LOGIN, true);
                startActivity(new Intent(getApplicationContext(), HomeLikeShopeeActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();
//                startActivity(new Intent(LoginActivity.this, MainActivity.class));
//                finish();
            }else if (cek.equals("facebook")){
                Toast.makeText(this, "Login facebook berhasil", Toast.LENGTH_SHORT).show();
                sharedPref.savePrefBoolean(SharedPref.SUDAH_LOGIN, true);
                startActivity(new Intent(getApplicationContext(), HomeLikeShopeeActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();
//                startActivity(new Intent(LoginActivity.this, MainActivity.class));
//                finish();
            }

        }
    }

    private void updateUI2(boolean isRegister) {
        if (isRegister) {
            if (cek.equals("gmail")){
                Toast.makeText(this, "Register gmail berhasil", Toast.LENGTH_SHORT).show();
                sharedPref.savePrefBoolean(SharedPref.SUDAH_LOGIN, true);
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("data", "gmail");
                startActivity(intent);
                finish();
            }else if (cek.equals("facebook")){
                Toast.makeText(this, "Register facebook berhasil", Toast.LENGTH_SHORT).show();
                sharedPref.savePrefBoolean(SharedPref.SUDAH_LOGIN, true);
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("data", "facebook");
                startActivity(intent);
                finish();
            }

//            startActivity(new Intent(LoginActivity.this, MainActivity.class));
//            finish();
        }
    }
}
