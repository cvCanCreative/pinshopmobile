package ai.agusibrhim.design.topedc.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Helper.Config;
import ai.agusibrhim.design.topedc.Model.PromoModel;
import ai.agusibrhim.design.topedc.R;

public class PromoHariIniAdapter extends RecyclerView.Adapter<PromoHariIniAdapter.Holder> {
    ArrayList<PromoModel> list;
    Context context;


    public PromoHariIniAdapter(ArrayList<PromoModel> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_promo_hari_ini, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        Glide.with(context)
                .load(Config.IMAGE_Promo + list.get(position).getSLFILE())
                .into(holder.imgPromo);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        private ImageView imgPromo;

        public Holder(View itemView) {
            super(itemView);
            imgPromo = (ImageView) itemView.findViewById(R.id.img_promo);
        }
    }
}
