package ai.agusibrhim.design.topedc.Model;

import java.util.ArrayList;
import java.util.List;

public class ResponModel {

    String kode;

    private String success;
    private String message;

    private ArrayList<BankModel> bank = null;

    ArrayList<ListProduct> result;

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public ArrayList<ListProduct> getResult() {
        return result;
    }

    public void setResult(ArrayList<ListProduct> result) {
        this.result = result;
    }
}
