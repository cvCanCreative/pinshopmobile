package ai.agusibrhim.design.topedc.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Adapter.AdapterFullScreenImage;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Utils.Tools;

public class FullScreenActivity extends AppCompatActivity {

    private ViewPager pager;
    private ImageButton btnClose;
    private TextView textPage;
    ArrayList<String> items = new ArrayList<>();
    private AdapterFullScreenImage adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen);
        initView();

        awal();
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void awal(){
        Intent i = getIntent();
        final int position = i.getIntExtra("posisi", 0);
        items = i.getStringArrayListExtra("list");
        adapter = new AdapterFullScreenImage(FullScreenActivity.this, items);
        final int total = adapter.getCount();
        pager.setAdapter(adapter);

        textPage.setText(String.format(getString(R.string.image_of), (position + 1), total));

        // displaying selected image first
        pager.setCurrentItem(position);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int pos, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int pos) {
                textPage.setText(String.format(getString(R.string.image_of), (pos + 1), total));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        Tools.systemBarLolipop(this);
    }

    private void initView() {
        pager = (ViewPager) findViewById(R.id.pager);
        btnClose = (ImageButton) findViewById(R.id.btnClose);
        textPage = (TextView) findViewById(R.id.text_page);
    }
}
