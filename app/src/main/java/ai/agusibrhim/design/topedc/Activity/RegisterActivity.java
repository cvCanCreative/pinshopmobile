package ai.agusibrhim.design.topedc.Activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.MainActivity;
import ai.agusibrhim.design.topedc.Model.AddFirebase;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    Button btnRegister;
    EditText username, email, password, tlp, birthday, referral;
    ProgressDialog pd;
    String pesan;
    DatePickerDialog datePickerDialog;
    int mYear;
    int mMonth;
    int mDay;
    String data;
    SharedPref sharedPref;
    private ImageView appIvClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initView();

        appIvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        awal();
        birthday.setFocusable(false);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (data.equals("manual")) {
                    prosesRegister();
                } else if (data.equals("gmail")) {
                    registerGmail();
                } else if (data.equals("facebook")) {
                    registerGmail();
                }
            }
        });

        birthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                settingTanggal();
                datePickerDialog.show();
            }
        });
    }

    private void awal() {
        sharedPref = new SharedPref(this);
        data = getIntent().getStringExtra("data");
        if (data.equals("manual")) {

        } else if (data.equals("gmail")) {
            email.setVisibility(View.GONE);
            birthday.setVisibility(View.GONE);
            btnRegister.setText("Lengkapi Data");
        } else if (data.equals("facebook")) {
            email.setVisibility(View.GONE);
            birthday.setVisibility(View.GONE);
            btnRegister.setText("Lengkapi Data");
        }
    }

    private void settingTanggal() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        datePickerDialog = new
                DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                mYear = year;
                mMonth = month;
                mDay = dayOfMonth;

                GregorianCalendar c = new GregorianCalendar(mYear, mMonth, mDay);
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

                birthday.setText(sdf.format(c.getTime()));

                birthday.setFocusable(false);
                birthday.setCursorVisible(false);
                //edtTanggal.setText(tanggal);

            }
        }, year, month, day);
    }

    void initView() {
        pd = new ProgressDialog(this);
        btnRegister = (Button) findViewById(R.id.btn_daftar);
        username = (EditText) findViewById(R.id.edt_username);
        password = (EditText) findViewById(R.id.edt_password);
        email = (EditText) findViewById(R.id.edt_email);
        tlp = (EditText) findViewById(R.id.edt_tlp);
        birthday = (EditText) findViewById(R.id.edt_birth_day);
        referral = (EditText) findViewById(R.id.edt_referral);
        appIvClose = findViewById(R.id.app_iv_close);
    }

    void register() {
        pd.setMessage("Sending data...");
        pd.setCancelable(false);
        pd.show();

        final String sUsername = username.getText().toString();
        String sPassword = password.getText().toString();
        String sEmail = email.getText().toString();
        String sTelp = "+62" + tlp.getText().toString();
        String sBirth = birthday.getText().toString();
        String sReferral = referral.getText().toString();

        ApiService api = ApiConfig.getInstanceRetrofit();
        api.register(sUsername, sPassword, sEmail, sTelp, sBirth, sReferral).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String kode = jsonObject.getString("success");
                        pesan = jsonObject.getString("message");

                        if (kode.equals("1")) {
                            Toast.makeText(RegisterActivity.this, pesan, Toast.LENGTH_SHORT).show();
                            sendDataFirebase(sUsername);
                            startLoginActivity();
                        } else {
                            Toast.makeText(RegisterActivity.this, pesan, Toast.LENGTH_SHORT).show();
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    pd.dismiss();
                    Toast.makeText(RegisterActivity.this, "Register Gagal", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(RegisterActivity.this, "Gagal Mengirim Data", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void sendDataFirebase(String username) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference databaseRef = database.getReference("users");

        databaseRef.push();
        AddFirebase addFirebase = new AddFirebase("" + username);
        databaseRef.child("" + username).setValue(addFirebase);
        Toast.makeText(this, "data terkirim", Toast.LENGTH_SHORT).show();
    }

    private void registerGmail() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Proses ...");
        pd.setCancelable(false);
        pd.show();
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.updateGmail(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                username.getText().toString(),
                password.getText().toString(),
                referral.getText().toString(),
                tlp.getText().toString()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String sukses = jsonObject.optString("success");
                        String pesan = jsonObject.optString("message");
                        if (sukses.equals("1")) {
                            sendDataFirebase(username.getText().toString());
                            Toast.makeText(RegisterActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(), MainActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                            finish();
                        } else {
                            Toast.makeText(RegisterActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void prosesRegister() {
        if (!validasi()) {
            return;
        }
        register();
    }

    private Boolean validasi() {
        if (username.getText().toString().isEmpty()) {
            username.setError("Username harus diisi");
            username.requestFocus();

            return false;
        }
        if (username.getText().toString().contains(" ")) {
            username.setError("Username Tidak boleh ada spasi");
            username.requestFocus();

            return false;
        }

        if (email.getText().toString().isEmpty()) {
            email.setError("Email Harus diisi");
            email.requestFocus();

            return false;
        }
        if (!email.getText().toString().contains("@")) {
            email.setError("Email tidak valid");
            email.requestFocus();

            return false;
        }
        if (email.getText().toString().contains(" ")) {
            email.setError("Tidak boleh ada spasi");
            email.requestFocus();

            return false;
        }
        if (tlp.getText().toString().isEmpty()) {
            tlp.setError("Telepon Harus diisi");
            tlp.requestFocus();

            return false;
        }
        if (birthday.getText().toString().isEmpty()) {
            birthday.setError("Tanggal Lahir Harus diisi");
            birthday.requestFocus();

            return false;
        }
        if (password.getText().toString().isEmpty()) {
            password.setError("Password harus diisi");
            password.requestFocus();

            return false;
        }
        return true;
    }

    void startLoginActivity() {
        Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
        startActivity(intent);
    }
}
