package ai.agusibrhim.design.topedc.Adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Activity.AddKomplainActivity;
import ai.agusibrhim.design.topedc.Activity.CekPengirimanActivity;
import ai.agusibrhim.design.topedc.Activity.DetailTransaksiActivity;
import ai.agusibrhim.design.topedc.Activity.HistoryTransaksiActivity;
import ai.agusibrhim.design.topedc.Activity.LihatRekeningActivity;
import ai.agusibrhim.design.topedc.Helper.Config;
import ai.agusibrhim.design.topedc.Helper.HelperClass;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.Model.HistoryTransaksi;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HistoryTransaksiAdapter extends RecyclerView.Adapter<HistoryTransaksiAdapter.Holder> {

    ArrayList<HistoryTransaksi> list;
    Context context;
    SharedPref sharedPref;
    String status;
    String st;
    String data;

    public HistoryTransaksiAdapter(ArrayList<HistoryTransaksi> list, Context context,String data) {
        this.list = list;
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_transaksi,parent,false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, final int position) {
        if (data.equals("toko")){
            holder.lyBayar.setVisibility(View.GONE);
            holder.btnTerima.setVisibility(View.GONE);
            holder.btnKomplian.setVisibility(View.GONE);

//            Toast.makeText(context, ""+list.get(position).getTSSTATUS(), Toast.LENGTH_SHORT).show();
            st =""+list.get(position).getTSSTATUS();

            if (st.equals("WAITING_TF")){
                st = "Menunggu Transfer";
            }else if (st.equals("PROSES_ADMIN")){
                st = "Menunggu Admin";
            }else if (st.equals("PROSES_VENDOR")){
                st = "Diproses Toko";
            }else if (st.equals("PROSES_KIRIM")){
                st = "Dikirim";
            }else if (st.equals("TERIMA_USR")){
                st = "Diterima Oleh Pelanggan";
            }else if (st.equals("TERIMA")){
                st = "Diterima";
            }else {
                st =""+list.get(position).getTSSTATUS();
            }

            holder.tvStatus.setText(""+list.get(position).getTSSTATUS());
//            holder.tvJumlah.setText("x " +list.get(position).getTSDQTY());
//            holder.tvHarga.setText(new HelperClass().convertRupiah(Integer.parseInt(list.get(position).getTSDHARGAASLI())));
//            holder.tvTotal.setText(new HelperClass().convertRupiah(Integer.parseInt(list.get(position).getTSTOTALBAYAR())));
//
////        Toast.makeText(context, "f t" + list.get(position).getUSDTOKOFOTO(), Toast.LENGTH_SHORT).show();
//            if (list.get(position).getUSDTOKOFOTO().equals("")){
//                holder.imgToko.setImageResource(R.drawable.no_image_found);
//            }else {
//                Picasso.with(context)
//                        .load(Config.IMAGE_TOKO + list.get(position).getUSDTOKOFOTO())
//                        .error(R.drawable.no_image_found)
//                        .into(holder.imgToko);
//            }
//            holder.tvNamaToko.setText(""+list.get(position).getUSDTOKO());
//
//            String gambar = list.get(position).getBAIMAGE();
//            String gb = new HelperClass().splitText(gambar);
//
//            Picasso.with(context)
//                    .load(Config.IMAGE_BARANG + gb)
//                    .error(R.drawable.no_image_found)
//                    .into(holder.imgBarang);
        }else {
            if (list.get(position).getTSSTATUS().equals("PROSES_KIRIM")){
                status = "TERIMA";
                holder.btnTerima.setText("TERIMA BARANG");

            }else if (list.get(position).getTSSTATUS().equals("WAITING_TF")){
                holder.lyBayar.setVisibility(View.VISIBLE);
                holder.btnTerima.setVisibility(View.GONE);
                holder.btnKomplian.setVisibility(View.GONE);
            }else if (list.get(position).getTSSTATUS().equals("TERIMA")){
                status = "TERIMA_USR";
                holder.btnTerima.setText("YAKIN TERIMA");
            } else{
                holder.lyBayar.setVisibility(View.GONE);
                holder.btnTerima.setVisibility(View.GONE);
                holder.btnKomplian.setVisibility(View.GONE);
            }

            holder.tvLihat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, CekPengirimanActivity.class);
                    intent.putExtra("kurir",list.get(position).getTSSLUGKURIR());
                    intent.putExtra("resi",list.get(position).getTSRESI());
                    intent.putExtra("id",list.get(position).getIDTRANSAKSI());
                    context.startActivity(intent);
                }
            });


            holder.btnBayar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, LihatRekeningActivity.class);
                    intent.putExtra("data", "tf");
                    intent.putExtra("jumlah",""+list.get(position).getTSTFPIN());
                    intent.putExtra("kode",list.get(position).getTSKODEPAYMENT());
                    intent.putExtra("bankpin",""+list.get(position).getTSBANKPIN());
                    intent.putExtra("rekpin",""+list.get(position).getTSREKPIN());
                    intent.putExtra("imgpin",""+list.get(position).getRKIMAGE());
                    context.startActivity(intent);
                }
            });


            holder.btnKomplian.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, AddKomplainActivity.class);
                    intent.putExtra("id",list.get(position).getIDTRANSAKSI());
                    context.startActivity(intent);
                }
            });


            holder.btnTerima.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    new AlertDialog.Builder(context)
                            .setMessage("Dengan ini Anda Menyatakan Barang diterima, apakah anda yakin ?")
                            .setCancelable(false)
                            .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    terima(position,status);
                                }
                            })
                            .setNegativeButton("Tidak", null)
                            .show();
                }
            });

            st =list.get(position).getTSSTATUS();

            if (st.equals("WAITING_TF")){
                st = "Menunggu Transfer";
            }else if (st.equals("PROSES_ADMIN")){
                st = "Menunggu Admin";
            }else if (st.equals("PROSES_VENDOR")){
                st = "Diproses Toko";
            }else if (st.equals("PROSES_KIRIM")){
                st = "Dikirim";
            }else if (st.equals("TERIMA_USR")){
                st = "Diterima Oleh Pelanggan";
            }else if (st.equals("TERIMA")){
                st = "Diterima";
            }else {
                st =list.get(position).getTSSTATUS();
            }

            holder.tvStatus.setText(""+st);
            holder.tvJumlah.setText("x " +list.get(position).getTSDQTY());
            holder.tvHarga.setText(new HelperClass().convertRupiah(Integer.parseInt(list.get(position).getTSDHARGAASLI())));
            holder.tvTotal.setText(new HelperClass().convertRupiah(Integer.parseInt(list.get(position).getTSTOTALBAYAR())));

//        Toast.makeText(context, "f t" + list.get(position).getUSDTOKOFOTO(), Toast.LENGTH_SHORT).show();
            if (list.get(position).getUSDTOKOFOTO().equals("")){
                holder.imgToko.setImageResource(R.drawable.no_image_found);
            }else {
                Picasso.with(context)
                        .load(Config.IMAGE_TOKO + list.get(position).getUSDTOKOFOTO())
                        .error(R.drawable.no_image_found)
                        .into(holder.imgToko);
            }
            holder.tvNamaToko.setText(""+list.get(position).getUSDTOKO());

            String gambar = list.get(position).getBAIMAGE();
            String gb = new HelperClass().splitText(gambar);

            Picasso.with(context)
                    .load(Config.IMAGE_BARANG + gb)
                    .error(R.drawable.no_image_found)
                    .into(holder.imgBarang);
        }


    }

    private void terima(Integer posisi,String statusku){
        sharedPref = new SharedPref(context);
        final ProgressDialog pd = new ProgressDialog(context);
        pd.setTitle("Proses ...");
        pd.setCancelable(false);
        pd.show();

        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.terimaPesanan(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                list.get(posisi).getIDTRANSAKSI(),
                ""+statusku).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String kode = jsonObject.optString("success");
                        String pesan = jsonObject.optString("message");
                        if (kode.equals("1")){
                            Intent intent = new Intent(context, HistoryTransaksiActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("data", "adapter");
                            context.startActivity(intent);
                        }else {
                            Toast.makeText(context, ""+pesan, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(context, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Holder extends RecyclerView.ViewHolder{

        TextView tvStatus,tvNama,tvJumlah,tvHarga,tvTotal,tvLihat,tvNamaToko;
        Button btnKomplian,btnTerima,btnBayar;
        LinearLayout lyBayar;
        ImageView imgToko,imgBarang;
        LinearLayout rv;

        public Holder(View itemView) {
            super(itemView);

            lyBayar = itemView.findViewById(R.id.ly_bayar);
            btnBayar = itemView.findViewById(R.id.btn_bayar);
            btnKomplian = itemView.findViewById(R.id.btn_komplain);
            btnTerima = itemView.findViewById(R.id.btn_terima);
            tvStatus = itemView.findViewById(R.id.tv_status);
            tvNama = itemView.findViewById(R.id.tv_nama_barang);
            tvJumlah = itemView.findViewById(R.id.tv_jumlah_beli);
            tvHarga = itemView.findViewById(R.id.tv_harga_barang);
            tvTotal = itemView.findViewById(R.id.tv_total);
            tvStatus = itemView.findViewById(R.id.tv_status);
            tvLihat = itemView.findViewById(R.id.tv_lihat_pengiriman);
            tvNamaToko = itemView.findViewById(R.id.tv_nama_toko);
            imgToko = itemView.findViewById(R.id.img_toko);
            imgBarang = itemView.findViewById(R.id.img_barang);
//            rv = itemView.findViewById(R.id.rv);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, DetailTransaksiActivity.class);
                    intent.putExtra("id",list.get(getAdapterPosition()).getTSKODEPAYMENT());
                    intent.putExtra("status",list.get(getAdapterPosition()).getTSSTATUS());
                    intent.putExtra("kurir",list.get(getAdapterPosition()).getTSSLUGKURIR());
                    intent.putExtra("resi",list.get(getAdapterPosition()).getTSRESI());
                    intent.putExtra("metodebayar",list.get(getAdapterPosition()).getTSMETODEBAYAR());
                    intent.putExtra("waktupesan",list.get(getAdapterPosition()).getCREATEDAT());
                    intent.putExtra("status2",list.get(getAdapterPosition()).getTSSTATUS());
                    intent.putExtra("data",data);
                    context.startActivity(intent);
                }
            });
        }
    }
}
