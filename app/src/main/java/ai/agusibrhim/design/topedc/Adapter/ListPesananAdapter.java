package ai.agusibrhim.design.topedc.Adapter;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ai.agusibrhim.design.topedc.Activity.PembayaranActivity;
import ai.agusibrhim.design.topedc.Helper.Config;
import ai.agusibrhim.design.topedc.Helper.HelperClass;
import ai.agusibrhim.design.topedc.Helper.dataRoom.AppDatabase;
import ai.agusibrhim.design.topedc.Helper.dataRoom.Pesanan;
import ai.agusibrhim.design.topedc.Model.AlamatModel;
import ai.agusibrhim.design.topedc.R;

public class ListPesananAdapter extends RecyclerView.Adapter<ListPesananAdapter.Holder> {

    Context context;
    ArrayList<Pesanan> listPesananByIdToko;
    ArrayList<Pesanan> listPesananByIdToko2 = new ArrayList<>();

    ArrayList<AlamatModel> listAlamat;
    int posisiAlamat;
    ArrayList<Pesanan> listPesanan;

    private AppDatabase db;

    String kodeToko;
    public String[] beratBarang;
    public String[] alamatKirim;
    public String[] hargaBarang;
    ArrayList<Integer> listBerat = new ArrayList<>();

    public ListPesananAdapter(Context context, ArrayList<Pesanan> listPesananByIdToko, ArrayList<AlamatModel> listAlamat, int posisiAlamat, ArrayList<Pesanan> listPesanan,String kodeToko) {
        this.context = context;
        this.listPesananByIdToko = listPesananByIdToko;
        this.listAlamat = listAlamat;
        this.posisiAlamat = posisiAlamat;
        this.listPesanan = listPesanan;
        this.kodeToko = kodeToko;
        this.beratBarang = new String[listPesananByIdToko.size()];
        this.alamatKirim = new String[listPesananByIdToko.size()];
        this.hargaBarang = new String[listPesananByIdToko.size()];

        for (int x = 0; x < listPesananByIdToko.size(); x++) {
            this.beratBarang[x] = "0";
            this.alamatKirim[x] = "null";
            this.hargaBarang[x] = "0";
            this.listBerat.add(0);
        }
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_pesanan_dua, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
       listPesananByIdToko2.removeAll(listPesananByIdToko2);

        db = Room.databaseBuilder(context,
                AppDatabase.class, "pesanandb").allowMainThreadQueries().build();
        listPesananByIdToko2.addAll(Arrays.asList(db.pesananDAO().lihatPesanan("" + kodeToko)));

        holder.tvNamaToko.setText(listPesananByIdToko2.get(position).getNamaToko());
        holder.tvNamaBarang.setText(listPesananByIdToko2.get(position).getNamaBarang());
        String jumlah = listPesananByIdToko2.get(position).getJumlahBarang();

        holder.tvJumlah.setText("Jumlah Pesanan " + jumlah);
        holder.tvHarga.setText("" + new HelperClass().convertRupiah(Integer.parseInt(listPesananByIdToko.get(position).getHargaBarang())));

        String gambar = listPesananByIdToko2.get(position).getImgBarang();
        String gb = new HelperClass().splitText(gambar);

        Picasso.with(context)
                .load(Config.IMAGE_BARANG + gb)
                .into(holder.imgBarang);

    }

    @Override
    public int getItemCount() {
        return listPesananByIdToko.size();
    }


    public class Holder extends RecyclerView.ViewHolder {

        private TextView tvNamaToko;
        private TextView tvNamaBarang;
        private ImageView imgBarang;
        private TextView tvHarga;
        private TextView tvJumlah;

        public Holder(View itemView) {
            super(itemView);

            tvNamaToko = (TextView) itemView.findViewById(R.id.tv_nama_toko);
            tvNamaBarang = (TextView) itemView.findViewById(R.id.tv_nama_barang);
            imgBarang = (ImageView) itemView.findViewById(R.id.img_barang);
            tvHarga = (TextView) itemView.findViewById(R.id.tv_harga);
            tvJumlah = (TextView) itemView.findViewById(R.id.tv_jumlah);
        }
    }
}
