package ai.agusibrhim.design.topedc.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.MainActivity;
import ai.agusibrhim.design.topedc.Model.AlamatModel;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditAlamatActivity extends AppCompatActivity {


    private FloatingActionButton tvTambah;
    private RecyclerView rv;
    private LinearLayout div;
    private SwipeRefreshLayout swipe;
    SharedPref sharedPref;
    ArrayList<AlamatModel> list = new ArrayList<>();

    String dataInten = "";
    private ImageView imgClose;
    String data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_alamat);
        initView();
        awal();

        tvTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), TambahAlamatActivity.class);
                intent.putExtra("data", ""+data);
                startActivity(intent);
            }
        });

        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getAlamat(true);
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (data.equals("bayar")){
//                    startActivity(new Intent(getApplicationContext(), KeranjangActivity.class)
//                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
//                    finish();
                    onBackPressed();
                }else {
                    Intent intent = new Intent(getApplicationContext(),PengaturanAkunActivity.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("data","profil");
                    finish();
                }

            }
        });
    }

    private void awal() {
        sharedPref = new SharedPref(this);
        getAlamat(true);
        data = getIntent().getStringExtra("data");
//        if (data.equals("bayar")) {
//            tvTambah.setVisibility(View.GONE);
//        } else {
//            tvTambah.setVisibility(View.VISIBLE);
//        }
    }

    private void getAlamat(boolean rm) {
        if (rm) {
            if (div.getChildCount() > 0) div.removeAllViews();
        }
        swipe.setRefreshing(true);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getAlamatPengiriman(sharedPref.getIdUser(),
                sharedPref.getSESSION()).enqueue(new Callback<ArrayList<AlamatModel>>() {
            @Override
            public void onResponse(Call<ArrayList<AlamatModel>> call, Response<ArrayList<AlamatModel>> response) {
                if (response.isSuccessful()) {
                    swipe.setRefreshing(false);
                    list = response.body();

                    if (list.get(0).getMessage().equals("Berhasil Diload")) {
                        for (int i = 0; i < list.size(); i++) {
                            LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            View view = layoutInflater.inflate(R.layout.row_alamat, null);

                            TextView tvNamaAlamat = view.findViewById(R.id.tv_nama_alamat);
                            TextView tvKota = view.findViewById(R.id.tv_kota);
                            TextView tvNama = view.findViewById(R.id.tv_nama);
                            TextView tvAlamat = view.findViewById(R.id.tv_alamat);
                            TextView tvTlp = view.findViewById(R.id.tv_tlp);
                            LinearLayout ly = view.findViewById(R.id.ly);

                            tvNamaAlamat.setText("" + list.get(i).getPEJUDUL());
                            tvKota.setText("" + list.get(i).getPEKECAMATAN() + " " + list.get(i).getPEKOTA() + " " + list.get(i).getPEPROVINSI());
                            tvNama.setText("" + list.get(i).getPENAMA());
                            tvAlamat.setText("" + list.get(i).getPEALAMAT() + "\n" + list.get(i).getPEKODEPOS());
                            tvTlp.setText("" + list.get(i).getPETELP());


                            final String posisi = "" + i;
                            ly.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dataInten = getIntent().getStringExtra("data");
                                    if (dataInten.equals("bayar")) {
                                        new AlertDialog.Builder(EditAlamatActivity.this)
                                                .setMessage("Jadikan Alamat Pengiriman ?")
                                                .setCancelable(false)
                                                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        Intent intent = new Intent(getApplicationContext(), PembayaranActivity.class);
                                                        intent.putParcelableArrayListExtra("list", list);
                                                        intent.putExtra("posisi", "" + posisi);
                                                        startActivity(intent);
                                                    }
                                                })
                                                .setNegativeButton("Tidak", null)
                                                .show();
                                    } else {
                                        final CharSequence[] dialogItem = {"Hapus", "Edit"};
                                        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                                        builder.setTitle("Tentukan Pilihan Anda");
                                        builder.setItems(dialogItem, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                switch (i) {
                                                    case 0:
                                                        new AlertDialog.Builder(EditAlamatActivity.this)
                                                                .setMessage("Apakah anda akan menghapus alamat ini ?")
                                                                .setCancelable(false)
                                                                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                                                    public void onClick(DialogInterface dialog, int id) {
                                                                        hapusAlamat(list, posisi);
                                                                    }
                                                                })
                                                                .setNegativeButton("Tidak", null)
                                                                .show();
                                                        break;
                                                    case 1:
                                                        Intent intent = new Intent(getApplicationContext(), TambahAlamatActivity.class);
                                                        intent.putParcelableArrayListExtra("list", list);
                                                        intent.putExtra("posisi", "" + posisi);
                                                        intent.putExtra("data", "edit");
                                                        startActivity(intent);
                                                        break;
                                                }
                                            }
                                        });
                                        builder.create().show();
                                    }
                                }
                            });

                            div.addView(view);
                        }
                    } else {
                        Toast.makeText(EditAlamatActivity.this, "belum ada alamat yg disimpan", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<AlamatModel>> call, Throwable t) {
                swipe.setRefreshing(false);
                Toast.makeText(EditAlamatActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void hapusAlamat(ArrayList<AlamatModel> list, String posisi) {
        int a = Integer.parseInt(posisi);
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setCancelable(false);
        pd.setTitle("Proses ...");
        pd.show();
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.hapusAlamat(sharedPref.getIdUser(),
                sharedPref.getSESSION(),
                list.get(a).getIDPENGIRIMAN()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String pesan = jsonObject.optString("message");
                        String sukses = jsonObject.optString("success");
                        Toast.makeText(EditAlamatActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();

                        getAlamat(true);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(EditAlamatActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initView() {

        tvTambah = (FloatingActionButton)findViewById(R.id.tv_tambah);
        rv = (RecyclerView) findViewById(R.id.rv);
        div = (LinearLayout) findViewById(R.id.div);
        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe);
        imgClose = (ImageView) findViewById(R.id.img_close);
    }
}
