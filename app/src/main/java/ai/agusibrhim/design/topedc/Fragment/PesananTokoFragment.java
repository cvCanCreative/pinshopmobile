package ai.agusibrhim.design.topedc.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import ai.agusibrhim.design.topedc.Adapter.PesananTokoAdater;
import ai.agusibrhim.design.topedc.Helper.SharedPref;
import ai.agusibrhim.design.topedc.Model.RequestTokoModel;
import ai.agusibrhim.design.topedc.Model.TransaksiDetail;
import ai.agusibrhim.design.topedc.R;
import ai.agusibrhim.design.topedc.Retrofit.ApiConfig;
import ai.agusibrhim.design.topedc.Retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class PesananTokoFragment extends Fragment {


    private SwipeRefreshLayout swipe;
    private TextView tvKet;
    private RecyclerView rv;
    private LinearLayout lyKosong;
    SharedPref sharedPref;
    PesananTokoAdater mAdapter;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<RequestTokoModel> list = new ArrayList<>();
    private ImageView imgNo;

    public PesananTokoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pesanan_toko, container, false);
        initView(view);

        awal();

        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData();
            }
        });

        return view;
    }

    private void awal() {
        sharedPref = new SharedPref(getActivity());
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rv.setLayoutManager(layoutManager);
        getData();
    }

    private void getData() {
        swipe.setRefreshing(true);
        ApiService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getRequestToko(sharedPref.getIdUser(),
                sharedPref.getSESSION()).enqueue(new Callback<ArrayList<RequestTokoModel>>() {
            @Override
            public void onResponse(Call<ArrayList<RequestTokoModel>> call, Response<ArrayList<RequestTokoModel>> response) {
                if (response.isSuccessful()) {
                    swipe.setRefreshing(false);
                    list = response.body();
                    if (list.get(0).success == 1) {
                        tvKet.setVisibility(View.GONE);
                        lyKosong.setVisibility(View.GONE);
                        rv.setVisibility(View.VISIBLE);
                        mAdapter = new PesananTokoAdater(list, getActivity());
                        rv.setAdapter(mAdapter);
                    } else {
                        lyKosong.setVisibility(View.VISIBLE);
                        tvKet.setVisibility(View.VISIBLE);
                        rv.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<RequestTokoModel>> call, Throwable t) {
                swipe.setRefreshing(false);
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initView(View view) {
        swipe = (SwipeRefreshLayout) view.findViewById(R.id.swipe);
        tvKet = (TextView) view.findViewById(R.id.tv_ket);
        rv = (RecyclerView) view.findViewById(R.id.rv);
        lyKosong = (LinearLayout) view.findViewById(R.id.ly_kosong);
        imgNo = (ImageView) view.findViewById(R.id.img_no);
    }
}
