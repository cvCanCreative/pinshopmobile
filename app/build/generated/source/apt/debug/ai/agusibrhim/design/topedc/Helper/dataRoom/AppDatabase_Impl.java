package ai.agusibrhim.design.topedc.Helper.dataRoom;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.db.SupportSQLiteOpenHelper.Callback;
import android.arch.persistence.db.SupportSQLiteOpenHelper.Configuration;
import android.arch.persistence.room.DatabaseConfiguration;
import android.arch.persistence.room.InvalidationTracker;
import android.arch.persistence.room.RoomOpenHelper;
import android.arch.persistence.room.RoomOpenHelper.Delegate;
import android.arch.persistence.room.util.TableInfo;
import android.arch.persistence.room.util.TableInfo.Column;
import android.arch.persistence.room.util.TableInfo.ForeignKey;
import android.arch.persistence.room.util.TableInfo.Index;
import java.lang.IllegalStateException;
import java.lang.Override;
import java.lang.String;
import java.util.HashMap;
import java.util.HashSet;

public class AppDatabase_Impl extends AppDatabase {
  private volatile PesananDAO _pesananDAO;

  protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration configuration) {
    final SupportSQLiteOpenHelper.Callback _openCallback = new RoomOpenHelper(configuration, new RoomOpenHelper.Delegate(2) {
      public void createAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("CREATE TABLE IF NOT EXISTS `tpesanan` (`idPesanan` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `id_barang` TEXT, `id_user` TEXT, `id_toko` TEXT, `nama_toko` TEXT, `nama_barang` TEXT, `jumlah_barang` TEXT, `tipe_barang` TEXT, `harga_barang` TEXT, `img_barang` TEXT, `sub_harga_barang` TEXT, `berat` TEXT, `id_kota` TEXT, `id_kecamatan` TEXT, `stok` TEXT)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        _db.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, \"8424df333e565d25e50d81a016ac7485\")");
      }

      public void dropAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("DROP TABLE IF EXISTS `tpesanan`");
      }

      protected void onCreate(SupportSQLiteDatabase _db) {
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onCreate(_db);
          }
        }
      }

      public void onOpen(SupportSQLiteDatabase _db) {
        mDatabase = _db;
        internalInitInvalidationTracker(_db);
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onOpen(_db);
          }
        }
      }

      protected void validateMigration(SupportSQLiteDatabase _db) {
        final HashMap<String, TableInfo.Column> _columnsTpesanan = new HashMap<String, TableInfo.Column>(15);
        _columnsTpesanan.put("idPesanan", new TableInfo.Column("idPesanan", "INTEGER", true, 1));
        _columnsTpesanan.put("id_barang", new TableInfo.Column("id_barang", "TEXT", false, 0));
        _columnsTpesanan.put("id_user", new TableInfo.Column("id_user", "TEXT", false, 0));
        _columnsTpesanan.put("id_toko", new TableInfo.Column("id_toko", "TEXT", false, 0));
        _columnsTpesanan.put("nama_toko", new TableInfo.Column("nama_toko", "TEXT", false, 0));
        _columnsTpesanan.put("nama_barang", new TableInfo.Column("nama_barang", "TEXT", false, 0));
        _columnsTpesanan.put("jumlah_barang", new TableInfo.Column("jumlah_barang", "TEXT", false, 0));
        _columnsTpesanan.put("tipe_barang", new TableInfo.Column("tipe_barang", "TEXT", false, 0));
        _columnsTpesanan.put("harga_barang", new TableInfo.Column("harga_barang", "TEXT", false, 0));
        _columnsTpesanan.put("img_barang", new TableInfo.Column("img_barang", "TEXT", false, 0));
        _columnsTpesanan.put("sub_harga_barang", new TableInfo.Column("sub_harga_barang", "TEXT", false, 0));
        _columnsTpesanan.put("berat", new TableInfo.Column("berat", "TEXT", false, 0));
        _columnsTpesanan.put("id_kota", new TableInfo.Column("id_kota", "TEXT", false, 0));
        _columnsTpesanan.put("id_kecamatan", new TableInfo.Column("id_kecamatan", "TEXT", false, 0));
        _columnsTpesanan.put("stok", new TableInfo.Column("stok", "TEXT", false, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysTpesanan = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesTpesanan = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoTpesanan = new TableInfo("tpesanan", _columnsTpesanan, _foreignKeysTpesanan, _indicesTpesanan);
        final TableInfo _existingTpesanan = TableInfo.read(_db, "tpesanan");
        if (! _infoTpesanan.equals(_existingTpesanan)) {
          throw new IllegalStateException("Migration didn't properly handle tpesanan(ai.agusibrhim.design.topedc.Helper.dataRoom.Pesanan).\n"
                  + " Expected:\n" + _infoTpesanan + "\n"
                  + " Found:\n" + _existingTpesanan);
        }
      }
    }, "8424df333e565d25e50d81a016ac7485");
    final SupportSQLiteOpenHelper.Configuration _sqliteConfig = SupportSQLiteOpenHelper.Configuration.builder(configuration.context)
        .name(configuration.name)
        .callback(_openCallback)
        .build();
    final SupportSQLiteOpenHelper _helper = configuration.sqliteOpenHelperFactory.create(_sqliteConfig);
    return _helper;
  }

  @Override
  protected InvalidationTracker createInvalidationTracker() {
    return new InvalidationTracker(this, "tpesanan");
  }

  @Override
  public PesananDAO pesananDAO() {
    if (_pesananDAO != null) {
      return _pesananDAO;
    } else {
      synchronized(this) {
        if(_pesananDAO == null) {
          _pesananDAO = new PesananDAO_Impl(this);
        }
        return _pesananDAO;
      }
    }
  }
}
