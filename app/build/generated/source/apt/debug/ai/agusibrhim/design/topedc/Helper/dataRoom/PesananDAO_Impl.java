package ai.agusibrhim.design.topedc.Helper.dataRoom;

import android.arch.persistence.db.SupportSQLiteStatement;
import android.arch.persistence.room.EntityDeletionOrUpdateAdapter;
import android.arch.persistence.room.EntityInsertionAdapter;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.RoomSQLiteQuery;
import android.arch.persistence.room.SharedSQLiteStatement;
import android.database.Cursor;
import java.lang.Override;
import java.lang.String;

public class PesananDAO_Impl implements PesananDAO {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter __insertionAdapterOfPesanan;

  private final EntityDeletionOrUpdateAdapter __deletionAdapterOfPesanan;

  private final SharedSQLiteStatement __preparedStmtOfUpdatePesanan;

  public PesananDAO_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfPesanan = new EntityInsertionAdapter<Pesanan>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `tpesanan`(`idPesanan`,`id_barang`,`id_user`,`id_toko`,`nama_toko`,`nama_barang`,`jumlah_barang`,`tipe_barang`,`harga_barang`,`img_barang`,`sub_harga_barang`,`berat`,`id_kota`,`id_kecamatan`,`stok`) VALUES (nullif(?, 0),?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Pesanan value) {
        stmt.bindLong(1, value.idPesanan);
        if (value.idBarang == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.idBarang);
        }
        if (value.idUser == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.idUser);
        }
        if (value.idToko == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.idToko);
        }
        if (value.namaToko == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.namaToko);
        }
        if (value.namaBarang == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.namaBarang);
        }
        if (value.jumlahBarang == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindString(7, value.jumlahBarang);
        }
        if (value.tipeBarang == null) {
          stmt.bindNull(8);
        } else {
          stmt.bindString(8, value.tipeBarang);
        }
        if (value.hargaBarang == null) {
          stmt.bindNull(9);
        } else {
          stmt.bindString(9, value.hargaBarang);
        }
        if (value.imgBarang == null) {
          stmt.bindNull(10);
        } else {
          stmt.bindString(10, value.imgBarang);
        }
        if (value.subHargaBarang == null) {
          stmt.bindNull(11);
        } else {
          stmt.bindString(11, value.subHargaBarang);
        }
        if (value.berat == null) {
          stmt.bindNull(12);
        } else {
          stmt.bindString(12, value.berat);
        }
        if (value.idKota == null) {
          stmt.bindNull(13);
        } else {
          stmt.bindString(13, value.idKota);
        }
        if (value.idKecamatan == null) {
          stmt.bindNull(14);
        } else {
          stmt.bindString(14, value.idKecamatan);
        }
        if (value.stok == null) {
          stmt.bindNull(15);
        } else {
          stmt.bindString(15, value.stok);
        }
      }
    };
    this.__deletionAdapterOfPesanan = new EntityDeletionOrUpdateAdapter<Pesanan>(__db) {
      @Override
      public String createQuery() {
        return "DELETE FROM `tpesanan` WHERE `idPesanan` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Pesanan value) {
        stmt.bindLong(1, value.idPesanan);
      }
    };
    this.__preparedStmtOfUpdatePesanan = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "UPDATE tpesanan SET jumlah_barang =?, sub_harga_barang=? WHERE id_barang=?";
        return _query;
      }
    };
  }

  @Override
  public long insertPesanan(Pesanan pesanan) {
    __db.beginTransaction();
    try {
      long _result = __insertionAdapterOfPesanan.insertAndReturnId(pesanan);
      __db.setTransactionSuccessful();
      return _result;
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public int deletePesanan(Pesanan pesanan) {
    int _total = 0;
    __db.beginTransaction();
    try {
      _total +=__deletionAdapterOfPesanan.handle(pesanan);
      __db.setTransactionSuccessful();
      return _total;
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void updatePesanan(String jumlah, String sub, String id) {
    final SupportSQLiteStatement _stmt = __preparedStmtOfUpdatePesanan.acquire();
    __db.beginTransaction();
    try {
      int _argIndex = 1;
      if (jumlah == null) {
        _stmt.bindNull(_argIndex);
      } else {
        _stmt.bindString(_argIndex, jumlah);
      }
      _argIndex = 2;
      if (sub == null) {
        _stmt.bindNull(_argIndex);
      } else {
        _stmt.bindString(_argIndex, sub);
      }
      _argIndex = 3;
      if (id == null) {
        _stmt.bindNull(_argIndex);
      } else {
        _stmt.bindString(_argIndex, id);
      }
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfUpdatePesanan.release(_stmt);
    }
  }

  @Override
  public Pesanan[] selectAllPesanan() {
    final String _sql = "SELECT * FROM tpesanan";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfIdPesanan = _cursor.getColumnIndexOrThrow("idPesanan");
      final int _cursorIndexOfIdBarang = _cursor.getColumnIndexOrThrow("id_barang");
      final int _cursorIndexOfIdUser = _cursor.getColumnIndexOrThrow("id_user");
      final int _cursorIndexOfIdToko = _cursor.getColumnIndexOrThrow("id_toko");
      final int _cursorIndexOfNamaToko = _cursor.getColumnIndexOrThrow("nama_toko");
      final int _cursorIndexOfNamaBarang = _cursor.getColumnIndexOrThrow("nama_barang");
      final int _cursorIndexOfJumlahBarang = _cursor.getColumnIndexOrThrow("jumlah_barang");
      final int _cursorIndexOfTipeBarang = _cursor.getColumnIndexOrThrow("tipe_barang");
      final int _cursorIndexOfHargaBarang = _cursor.getColumnIndexOrThrow("harga_barang");
      final int _cursorIndexOfImgBarang = _cursor.getColumnIndexOrThrow("img_barang");
      final int _cursorIndexOfSubHargaBarang = _cursor.getColumnIndexOrThrow("sub_harga_barang");
      final int _cursorIndexOfBerat = _cursor.getColumnIndexOrThrow("berat");
      final int _cursorIndexOfIdKota = _cursor.getColumnIndexOrThrow("id_kota");
      final int _cursorIndexOfIdKecamatan = _cursor.getColumnIndexOrThrow("id_kecamatan");
      final int _cursorIndexOfStok = _cursor.getColumnIndexOrThrow("stok");
      final Pesanan[] _result = new Pesanan[_cursor.getCount()];
      int _index = 0;
      while(_cursor.moveToNext()) {
        final Pesanan _item;
        _item = new Pesanan();
        _item.idPesanan = _cursor.getInt(_cursorIndexOfIdPesanan);
        _item.idBarang = _cursor.getString(_cursorIndexOfIdBarang);
        _item.idUser = _cursor.getString(_cursorIndexOfIdUser);
        _item.idToko = _cursor.getString(_cursorIndexOfIdToko);
        _item.namaToko = _cursor.getString(_cursorIndexOfNamaToko);
        _item.namaBarang = _cursor.getString(_cursorIndexOfNamaBarang);
        _item.jumlahBarang = _cursor.getString(_cursorIndexOfJumlahBarang);
        _item.tipeBarang = _cursor.getString(_cursorIndexOfTipeBarang);
        _item.hargaBarang = _cursor.getString(_cursorIndexOfHargaBarang);
        _item.imgBarang = _cursor.getString(_cursorIndexOfImgBarang);
        _item.subHargaBarang = _cursor.getString(_cursorIndexOfSubHargaBarang);
        _item.berat = _cursor.getString(_cursorIndexOfBerat);
        _item.idKota = _cursor.getString(_cursorIndexOfIdKota);
        _item.idKecamatan = _cursor.getString(_cursorIndexOfIdKecamatan);
        _item.stok = _cursor.getString(_cursorIndexOfStok);
        _result[_index] = _item;
        _index ++;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public Pesanan[] lihatPesanan(String idToko) {
    final String _sql = "SELECT * FROM tpesanan WHERE id_toko =?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (idToko == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, idToko);
    }
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfIdPesanan = _cursor.getColumnIndexOrThrow("idPesanan");
      final int _cursorIndexOfIdBarang = _cursor.getColumnIndexOrThrow("id_barang");
      final int _cursorIndexOfIdUser = _cursor.getColumnIndexOrThrow("id_user");
      final int _cursorIndexOfIdToko = _cursor.getColumnIndexOrThrow("id_toko");
      final int _cursorIndexOfNamaToko = _cursor.getColumnIndexOrThrow("nama_toko");
      final int _cursorIndexOfNamaBarang = _cursor.getColumnIndexOrThrow("nama_barang");
      final int _cursorIndexOfJumlahBarang = _cursor.getColumnIndexOrThrow("jumlah_barang");
      final int _cursorIndexOfTipeBarang = _cursor.getColumnIndexOrThrow("tipe_barang");
      final int _cursorIndexOfHargaBarang = _cursor.getColumnIndexOrThrow("harga_barang");
      final int _cursorIndexOfImgBarang = _cursor.getColumnIndexOrThrow("img_barang");
      final int _cursorIndexOfSubHargaBarang = _cursor.getColumnIndexOrThrow("sub_harga_barang");
      final int _cursorIndexOfBerat = _cursor.getColumnIndexOrThrow("berat");
      final int _cursorIndexOfIdKota = _cursor.getColumnIndexOrThrow("id_kota");
      final int _cursorIndexOfIdKecamatan = _cursor.getColumnIndexOrThrow("id_kecamatan");
      final int _cursorIndexOfStok = _cursor.getColumnIndexOrThrow("stok");
      final Pesanan[] _result = new Pesanan[_cursor.getCount()];
      int _index = 0;
      while(_cursor.moveToNext()) {
        final Pesanan _item;
        _item = new Pesanan();
        _item.idPesanan = _cursor.getInt(_cursorIndexOfIdPesanan);
        _item.idBarang = _cursor.getString(_cursorIndexOfIdBarang);
        _item.idUser = _cursor.getString(_cursorIndexOfIdUser);
        _item.idToko = _cursor.getString(_cursorIndexOfIdToko);
        _item.namaToko = _cursor.getString(_cursorIndexOfNamaToko);
        _item.namaBarang = _cursor.getString(_cursorIndexOfNamaBarang);
        _item.jumlahBarang = _cursor.getString(_cursorIndexOfJumlahBarang);
        _item.tipeBarang = _cursor.getString(_cursorIndexOfTipeBarang);
        _item.hargaBarang = _cursor.getString(_cursorIndexOfHargaBarang);
        _item.imgBarang = _cursor.getString(_cursorIndexOfImgBarang);
        _item.subHargaBarang = _cursor.getString(_cursorIndexOfSubHargaBarang);
        _item.berat = _cursor.getString(_cursorIndexOfBerat);
        _item.idKota = _cursor.getString(_cursorIndexOfIdKota);
        _item.idKecamatan = _cursor.getString(_cursorIndexOfIdKecamatan);
        _item.stok = _cursor.getString(_cursorIndexOfStok);
        _result[_index] = _item;
        _index ++;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public Pesanan[] lihatPesananById(String idUser) {
    final String _sql = "SELECT * FROM tpesanan WHERE id_user =?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (idUser == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, idUser);
    }
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfIdPesanan = _cursor.getColumnIndexOrThrow("idPesanan");
      final int _cursorIndexOfIdBarang = _cursor.getColumnIndexOrThrow("id_barang");
      final int _cursorIndexOfIdUser = _cursor.getColumnIndexOrThrow("id_user");
      final int _cursorIndexOfIdToko = _cursor.getColumnIndexOrThrow("id_toko");
      final int _cursorIndexOfNamaToko = _cursor.getColumnIndexOrThrow("nama_toko");
      final int _cursorIndexOfNamaBarang = _cursor.getColumnIndexOrThrow("nama_barang");
      final int _cursorIndexOfJumlahBarang = _cursor.getColumnIndexOrThrow("jumlah_barang");
      final int _cursorIndexOfTipeBarang = _cursor.getColumnIndexOrThrow("tipe_barang");
      final int _cursorIndexOfHargaBarang = _cursor.getColumnIndexOrThrow("harga_barang");
      final int _cursorIndexOfImgBarang = _cursor.getColumnIndexOrThrow("img_barang");
      final int _cursorIndexOfSubHargaBarang = _cursor.getColumnIndexOrThrow("sub_harga_barang");
      final int _cursorIndexOfBerat = _cursor.getColumnIndexOrThrow("berat");
      final int _cursorIndexOfIdKota = _cursor.getColumnIndexOrThrow("id_kota");
      final int _cursorIndexOfIdKecamatan = _cursor.getColumnIndexOrThrow("id_kecamatan");
      final int _cursorIndexOfStok = _cursor.getColumnIndexOrThrow("stok");
      final Pesanan[] _result = new Pesanan[_cursor.getCount()];
      int _index = 0;
      while(_cursor.moveToNext()) {
        final Pesanan _item;
        _item = new Pesanan();
        _item.idPesanan = _cursor.getInt(_cursorIndexOfIdPesanan);
        _item.idBarang = _cursor.getString(_cursorIndexOfIdBarang);
        _item.idUser = _cursor.getString(_cursorIndexOfIdUser);
        _item.idToko = _cursor.getString(_cursorIndexOfIdToko);
        _item.namaToko = _cursor.getString(_cursorIndexOfNamaToko);
        _item.namaBarang = _cursor.getString(_cursorIndexOfNamaBarang);
        _item.jumlahBarang = _cursor.getString(_cursorIndexOfJumlahBarang);
        _item.tipeBarang = _cursor.getString(_cursorIndexOfTipeBarang);
        _item.hargaBarang = _cursor.getString(_cursorIndexOfHargaBarang);
        _item.imgBarang = _cursor.getString(_cursorIndexOfImgBarang);
        _item.subHargaBarang = _cursor.getString(_cursorIndexOfSubHargaBarang);
        _item.berat = _cursor.getString(_cursorIndexOfBerat);
        _item.idKota = _cursor.getString(_cursorIndexOfIdKota);
        _item.idKecamatan = _cursor.getString(_cursorIndexOfIdKecamatan);
        _item.stok = _cursor.getString(_cursorIndexOfStok);
        _result[_index] = _item;
        _index ++;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }
}
